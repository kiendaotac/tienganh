<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
	protected $table = "categories";

    protected $fillable=[ 'name', 'slug', 'desc', 'icon', 'img', 'parent_id', 'ordering' ];

    public function childs() {
        return $this->hasMany('App\Category','parent_id','id') ;
    }
}
