<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TestShift extends Model
{
    protected $table    = 'test_shifts';
    protected $fillable =   ['id', 'name', 'test_form_id', 'test_numbers', 'test_type', 'time_active', 'time_late', 'state', 'online'];

    public function Testform(){
        return $this->belongsTo('App\TestForm','test_form_id','id');
    }
}
