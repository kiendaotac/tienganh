<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;

class Curriculum extends Model {

	protected $table = "curriculums";
	protected $fillable=[ 'id', 'cate_id', 'name', 'slug', 'short_desc', 'desc', 'img', 'featured', 'logintolearn', 'maximum', 'user_id', 'states', 'ordering', 'percent_pass', 'result_assesment' ];
	/* get category */
	public function categories() {
		return $this->belongsTo('App\Category', 'cate_id', 'id');
	}
	public function lessons(){
		return $this->hasMany('App\Lesson');
	}
	public function lesson(){
		return $this->hasOne('App\Lesson');
	}

	/* get data role */
	public function scopeBelong($query)
	{
		if(Auth::user()->roles->first()->name === 'Admin') return $query->where('user_id', Auth::user()->id);
		return $query;
	}

	/* get user */
	public function user()
	{
		return $this->belongsTo('App\User', 'user_id', 'id');
	}

	/* get student */
	public function students()
	{
		return $this->hasMany('App\StudentCurriculum','curriculum_id','id');
	}
}
