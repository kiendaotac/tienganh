<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TestGroup extends Model
{
    protected $table = 'test_groups';
    protected $fillable = [
        'test_subject_id', 'name', 'content', 'media_type', 'media_source', 'question_type', 'question_shuff', 'level', 'state'
    ];

    public function Questions(){
        return $this->hasMany('App\TestGroupQuestion','group_id','id');
    }
}
