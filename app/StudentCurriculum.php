<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StudentCurriculum extends Model
{
	protected $table = "student_curriculums";
	protected $fillable = ['stdcode','curriculum_id','curriculums_name','payment','active_date','expire_date','states'];
	public function curriculum(){
		return $this->belongsTo('App\Curriculum');
	}

	/* get student */
	public function student()
	{
		return $this->belongsTo('App\Student','stdcode','stdcode');
	}

	/* student info */
    public function info()
    {
        return $this->hasOne('App\StudentInfo','stdcode','stdcode');
    }
}
