<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;

class Lesson extends Model
{
	protected $table = "lessons";
	protected $fillable=[ 'curriculum_id', 'parent_id', 'name', 'slug', 'desc', 'type', 'review', 'states', 'user_id', 'ordering' ];

	public function childs() {
        return $this->hasMany('App\Lesson','parent_id','id')->where('states',1);
    }

    /* get curriculums */
    public function curriculums()
    {
    	return $this->belongsTo('App\Curriculum','curriculum_id','id') ;
    }
    /* get quicktest */
    public function quicktest()
    {
        return $this->hasMany('App\LessonQuicktest','lesson_id','id');
    }
    public function childsContent(){
        return $this->hasOne('App\LessonContent','lesson_id','id');
    }
    /* get lesson content */
    public function content()
    {
        return $this->hasMany('App\LessonContent','lesson_id','id');
    }

    /* get document */
    public function documents()
    {
        return $this->hasMany('App\Document','object_id','id')->where('object_key','lesson');
    }

    /* get data role */
    public function scopeBelong($query)
    {
        if(Auth::user()->roles->first()->name === 'Admin') return $query->where('user_id', Auth::user()->id);
        return $query;
    }
}
