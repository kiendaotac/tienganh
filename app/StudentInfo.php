<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StudentInfo extends Model
{
	protected $table = "student_info";

	protected $fillable = [
        'stdcode', 'start_date', 'uni', 'dept', 'majo', 'course', 'class'
    ];
}
