<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LessonQuicktestQuestion extends Model
{
	protected $table = "lesson_quicktest_questions";
	protected $fillable=[ 'quicktest_id', 'question', 'options', 'correct', 'explain', 'hint', 'states'];
}
