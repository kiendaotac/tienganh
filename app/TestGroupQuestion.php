<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TestGroupQuestion extends Model
{
    protected $table = 'test_group_questions';
    protected $fillable = [
        'group_id', 'question', 'answer_options', 'correct_answer', 'answer_shuff', 'explain', 'hint', 'state'
    ];
}
