<?php

namespace App;

use Illuminate\Filesystem\Cache;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable;
//    use EntrustUserTrait; // add this trait to your user model
//    use SoftDeletes;
    use SoftDeletes, EntrustUserTrait {
        SoftDeletes::restore as sfRestore;
        EntrustUserTrait::restore as euRestore;

    }

    /* where get data user is owner */
    public function scopeListowner($query)
    {
        return $query->join('role_user', 'users.id','role_user.user_id')->where('role_user.user_id',1);
    }

    /* where get data user is admin */
    public function scopeListadmin($query)
    {
        return $query->join('role_user', 'users.id','role_user.user_id')->where('role_user.role_id',2);
    }

    /* get data profile */
    public function profile()
    {
        return $this->hasMany('App\UserProfile','user_id','id');
    }

    /* check user is owner */
    public function isOwner()
    {
        foreach (Auth::user()->roles as $role) {
            if($role->name === 'Owner') return true;
        }
        return false;
    }

    /* check user is admin */
    public function isAdmin()
    {
        foreach (Auth::user()->roles as $role) {
            if($role->name === 'Admin') return true;
        }
        return false;
    }

    public function restore() {
        $this->sfRestore();
        Cache::tags(Config::get('entrust.role_user_table'))->flush();
    }
    public static function funcIds(){
        $roles = Auth::user()->roles;
        $permissions    =   array();
        $functionsId    =   array();
        if (!empty($roles)){
            foreach ($roles as $role){
                $newPermissions =   $role->permissions;
                if (!empty($newPermissions)){
                    foreach ($newPermissions as $newPermission){
                        array_push($permissions, $newPermission);
                    }
                }
            }
        }

        if (!empty($permissions)){
            foreach ($permissions as $permission){
                array_push($functionsId,explode('-',$permission->name)[1]);
            }
        }

        /* Function need login */
        $funcLogin    =   Functions::where('state',1)->where('access',2)->select('id')->get()->toArray();
        $funcLogin    =   array_column($funcLogin,'id');
        foreach ($funcLogin as $item){
            array_push($functionsId,$item);
        }

        /* Function don't need login */
        $funcGuest    =   Functions::where('state',1)->where('access',3)->select('id')->get()->toArray();
        $funcGuest    =   array_column($funcGuest,'id');
        foreach ($funcGuest as $item){
            array_push($functionsId,$item);
        }
        return array_unique($functionsId);
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $dates = ['deleted_at'];
    protected $fillable = [
        'name', 'email', 'display_name', 'state', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
