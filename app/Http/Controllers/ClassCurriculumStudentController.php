<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student;
use App\StudentCurriculum;
use App\ClassCurriculumStudent; 
use Auth;

class ClassCurriculumStudentController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->ajax()) {
            if($request->type == null){
                $classCurriculumStudent = ClassCurriculumStudent::create($request->all());
                if($classCurriculumStudent) {
                    return response()->json([
                        'type'    => 'success',
                        'title'   => 'Thành công',
                        'content' => 'Chọn sinh viên thành công',
                        'id'      => $classCurriculumStudent->id
                    ]);
                }
                else return $this->returnWarning();
            }
            else {
                if($request->type == 'chosed') {
                    $stdcode             = json_decode($request->stdcode);
                    $class_curriculum_id = json_decode($request->class_curriculum_id);
                    $id                  = [];
                    if(count($stdcode) <= 0) {
                        return response()->json([
                            'type'    => 'error',
                            'title'   => 'Lỗi',
                            'content' => 'Không có sinh viên nào',
                        ]);
                    }
                    for ($i=0; $i < count($stdcode); $i++) { 
                        $data = [
                            'class_curriculum_id' => $class_curriculum_id[$i],
                            'stdcode'             => $stdcode[$i],
                        ];
                        $classCurriculumStudent = ClassCurriculumStudent::create($data);
                        if($classCurriculumStudent) array_push($id,$classCurriculumStudent->id);
                    }
                    return response()->json([
                        'type'    => 'success',
                        'title'   => 'Thành công',
                        'content' => 'Chọn sinh viên thành công',
                        'id'      => $id
                    ]);
                }
                else {
                    $id = json_decode($request->id);
                    if(count($id) <= 0) {
                        return response()->json([
                            'type'    => 'error',
                            'title'   => 'Lỗi',
                            'content' => 'Không có sinh viên nào',
                        ]);
                    }
                    for ($i=0; $i < count(json_decode($request->id)); $i++) { 
                        ClassCurriculumStudent::find($id[$i])->delete();
                    }
                    return response()->json([
                        'type' => 'success',
                        'title' => 'Thành công',
                        'content' => 'Bỏ chọn sinh viên thành công',
                    ]);
                }
            }
        } else {
            return $this->returnNotAjax();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        if ($request->ajax()) {
            if ($id === 'getStudent') {
                return response()->json($this->getStudentCurriculums($request));
            }
            else {
                return response()->json($this->getStudentChosed($request));
            }
        } else {
            return $this->returnNotAjax();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        if ($request->ajax()) {
            if(ClassCurriculumStudent::find($id)->delete()) {
                return response()->json([
                    'type' => 'success',
                    'title' => 'Thành công',
                    'content' => 'Bỏ chọn sinh viên thành công',
                ]);
            }
            else return $this->returnWarning();
            
        } else {
            return $this->returnNotAjax();
        }
    }

    /* get data student join curriculum */
    public function getStudentCurriculums($request)
    {
        $class = ClassCurriculumStudent::select('stdcode')->where('class_curriculum_id',$request->class_curriculum_id)->get();
        $students = StudentCurriculum::where('curriculum_id',$request->curriculum_id)->whereNotIn('stdcode',$class)->get();
        return $this->customStudent($students)->toArray();
    }

    /* get data student join class */
    public function getStudentChosed($request)
    {
        $students = ClassCurriculumStudent::where('class_curriculum_id',$request->class_curriculum_id)->get();
        return $this->customStudent($students)->toArray();
    }

    /* custom list student  */
    public function customStudent($students)
    {
        foreach ($students as $item) {
            $item->student->info;
            switch ($item->student['sex']) {
                case 1:
                $item->student['sex'] = 'Nam';
                break;
                case 2:
                $item->student['sex'] = 'Nữ';
                break;
                default:
                $item->student['sex'] = 'Khác';
                break;
            }
        }
        return $students;
    }
}
