<?php

namespace App\Http\Controllers;

use App\Functions;
use App\QuestionType;
use App\TestForm;
use App\TestFormDetail;
use App\TestShift;
use App\TestSubject;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;
use Validator;

class TestFormController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $url                =   getURL();
        $currentFunction    =   Functions::where('route',$url)->where('state',1)->orderBy('id','desc')->first();
        $testforms          =   TestForm::where('state',1)->get();
        $subjects           =   TestSubject::where('state',1)->get();
        $question_type      =   QuestionType::all();
        return view('backend.questionbank.testform.main', compact('currentFunction', 'testforms', 'subjects', 'question_type'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $formData  =   $this->getFormArray($request->form);
        if ($request->ajax()) {
            $validator  =   Validator::make($formData, [
                'name'          =>  'required|string',
                'subject_id'    =>  'required|numeric',
                'time'          =>  'required|numeric',
                'state'         =>  'required|numeric',
            ]);
            if($validator->fails()){
                return response()->json([
                    'type'      =>  'error',
                    'title'     =>  'Lỗi!',
                    'content'   =>  $validator->errors()->all()]);
            }
            $id =   $this->addItem($formData);
            $this->addFormDetails($request->detail,$id);
            return response()->json([
                'type'      => 'success',
                'title'     => 'Thành công!',
                'content'   => 'Thêm test form thành công !!',
//                'id'        =>  $id
            ]);
        } else {
            return response()->json([
                'type'      => 'error',
                'title'     => 'Lỗi!',
                'content'   => 'Không phải ajax request']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        if ($request->ajax()) {
            if ($id === 'getDatatable'){
                return DataTables::eloquent(TestForm::query())
                    ->addColumn('subject', function ($form){
                        return TestSubject::find($form->subject_id)->name;
                    })
                    ->addColumn('creator', function ($form){
                        return User::find($form->user_id)->display_name;
                    })
                    ->make(true);
            }

            if ($id === 'getDetails'){
                $subject_id             =   $request->subject_id;
                $form_id                =   $request->form_id;
                $subject_id             =   TestSubject::find($subject_id)->childs->pluck('id');//subject_id bi gi de.
                $test_details           =   TestFormDetail::where('form_id',$form_id)->whereIn('subject_id',$subject_id)->get();
                $data                   =   array();
                $data['subject']        =   TestSubject::find($subject_id);
                $data['question_type']  =   QuestionType::where('state',1)->get();
                $test_details->map(function ($element){
                    $element->question_types =   TestSubject::find($element->subject_id)->TestGroup()->where('level',$element->level)->groupBy('question_type')->pluck('question_type');
                    return $element;
                });
                $data['details']        =   $test_details;
                return $data;
            }

            if ($id === 'getQuestionType'){
                $subject_id         =   $request->subject_id;
                $level              =   $request->level;
//                $question_type_id   =   TestSubject::find($subject_id)->TestGroup()->orderBy('question_type')->pluck('id');
                $question_type_id   =   TestSubject::find($subject_id)->childs()->first()->TestGroup()->where('level',$level)->pluck('question_type');
                return QuestionType::whereIn('id',$question_type_id)->where('state',1)->orderBy('id')->get();
            }

            if ($id === 'getQuestionTypeBySubjectDetails'){
                $subject_id         =   $request->subject_id;
                $level              =   $request->level;
                $question_type_id   =   TestSubject::find($subject_id)->TestGroup()->where('level',$level)->pluck('question_type');
                return QuestionType::whereIn('id',$question_type_id)->where('state',1)->orderBy('id')->get();
            }

        } else {
            return response()->json([
                'type'      => 'error',
                'title'     => 'Lỗi!',
                'content'   => 'Không phải ajax request']);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $formData   =   $this->getFormArray($request->form);
        if ($request->ajax()) {
            $validator  =   Validator::make($formData, [
                'name'          =>  'required|string',
                'subject_id'    =>  'required|numeric',
                'time'          =>  'required|numeric',
                'state'         =>  'required|numeric',
            ]);
            if($validator->fails()){
                return response()->json([
                    'type'      =>  'error',
                    'title'     =>  'Lỗi!',
                    'content'   =>  $validator->errors()->all()]);
            }
            $this->editItem($formData, $id);
            $this->addFormDetails($request->detail,$id);
            return response()->json([
                'type'      => 'success',
                'title'     => 'Thành công!',
                'content'   => 'Sửa test form thành công !!',
                'id'        =>  $id
            ]);
        } else {
            return response()->json([
                'type'      => 'error',
                'title'     => 'Lỗi!',
                'content'   => 'Không phải ajax request']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->ajax()) {
            /* Kiểm tra các điều kiện xóa */
            /* Kiểm tra xem có ca thi nào sử dụng chưa, nếu có sẽ ko cho xóa */
            if (count(TestShift::where('test_form_id',$id)->get())){
                return response()->json([
                    'type'      => 'error',
                    'title'     => 'Lỗi!',
                    'content'   => 'Không thể xóa form do đã sử dụng trong ca thi !!',
                ]);
            }

            /* Xóa form details */
            TestFormDetail::where('form_id',$id)->delete();
            TestForm::where('id',$id)->delete();
            return response()->json([
                'type'      => 'success',
                'title'     => 'Thành công!',
                'content'   => 'Xóa test form thành công !!',
            ]);
        } else {
            return response()->json([
                'type'      => 'error',
                'title'     => 'Lỗi!',
                'content'   => 'Không phải ajax request']);
        }
    }

    private function addItem($request){
        $item               =   new TestForm();
        $item->subject_id   =   $request['subject_id'];
        $item->name         =   $request['name'];
        $item->time         =   $request['time'];
        $item->show_result  =   isset($request['show_result'])   ?   1   :   0;
        $item->show_explain =   isset($request['show_explain'])  ?   1   :   0;
        $item->allow_review =   isset($request['allow_review'])  ?   1   :   0;
        $item->show_hint    =   isset($request['show_hint'])     ?   1   :   0;
        $item->user_id      =   Auth::user()->id;
        $item->state        =   $request['state'];
        $item->save();
        return $item->id;
    }

    private function editItem($request, $id){
        $item               =   TestForm::find($id);
        $item->subject_id   =   $request['subject_id'];
        $item->name         =   $request['name'];
        $item->time         =   $request['time'];
        $item->show_result  =   isset($request['show_result'])  ?   1   :   0;
        $item->show_explain =   isset($request['show_explain']) ?   1   :   0;
        $item->allow_review =   isset($request['allow_review']) ?   1   :   0;
        $item->show_hint    =   isset($request['show_hint'])    ?   1   :   0;
//        $item->user_id      =   Auth::user()->id;
        $item->state        =   $request['state'];
        $item->save();
    }

    private function getFormArray($form){
        $key    =   array_column($form,'name');
        $value  =   array_column($form,'value');
        $form   =   array_combine($key,$value);
        return $form;
    }

    /* Thêm form details */
    private function addFormDetails($dataDetails, $formId){
        /* Xóa hết details cũ */
        TestFormDetail::where('form_id',$formId)->delete();
        foreach ($dataDetails as $detail){
            $this->addDetail($detail, $formId);
        }
    }

    private function addDetail($detail, $formId){
        $detail =   (array)$detail;
        $validator  =   Validator::make($detail, [
            'form_id'       =>  'required|numeric|min:0',
            'subject_id'    =>  'required|numeric|min:0',
            'level'         =>  'required|numeric|min:0',
            'number_group'  =>  'required|numeric|min:0',
            'total_point'   =>  'required|numeric|min:0',
            'question_type' =>  'required|numeric|min:0|max:13'
        ]);
        if(!$validator->fails()){
            $item               =   new TestFormDetail();
            $item->form_id      =   $formId;
            $item->subject_id   =   $detail['subject_id'];
            $item->skill        =   TestSubject::find($detail['subject_id'])->name;
            $item->level        =   $detail['level'];
            $item->number_group =   $detail['number_group'];
            $item->total_point  =   $detail['total_point'];
            $item->question_type=   $detail['question_type'];
            $item->save();
        }
    }
}
