<?php

namespace App\Http\Controllers;

use App\TestGenerateGroupOffline;
use App\TestGroup;
use App\TestShift;
use Illuminate\Http\Request;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $shif   =   TestShift::with('Testform')->find(2);
        $form   =   $shif->Testform;
        $detail =   $form->Details()->first();
        $exam_id    =   1;
        $testnumber =   $shif->test_numbers;

        $subject_id =   $form->subject_id;
        $level      =   $detail->level;
        $question_type  =   $detail->question_type;
        $number_groups  =   $detail->number_groups;
        $group  =   TestGroup::query()->where('test_subject_id',2)->where('question_type',2)->where('level',3)->inRandomOrder()->limit(1)->with('Questions')->get();
//        $group->where(['test_subject_id' => 2, 'question_type' => 2])->get();
//        dd($group);
        return view('backend.system.authentication.home');
    }
}
