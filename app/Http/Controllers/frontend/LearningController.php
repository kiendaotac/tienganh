<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use App\Curriculum;
use App\Lesson;
use App\LessonContent;
use App\LessonQuicktest;
use App\LessonQuicktestQuestion;
use App\StudentCurriculum;
use App\StudentCurriculumLessonStatus;
use App\Document;
use Auth;
use Carbon\Carbon;

class LearningController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->ajax()) {
            if($request->type == 'submitTest') {
                $correct = json_decode($request->correct);
                $questionID = json_decode($request->question);
                $quicktest = LessonQuicktest::find($request->quicktest_id);
                $lesson = $quicktest->lesson;
                $curriculum = $lesson->curriculum;
                $questions = LessonQuicktestQuestion::where('quicktest_id', $request->quicktest_id)->get();
                $numberCorrect = 0;
                $pass = 0;
                foreach ($questions as $item) {
                    for ($i=0; $i < count($questionID); $i++) { 
                        if($item->id == $questionID[$i]) {
                            if($item->correct == $correct[$i]) $numberCorrect++;
                        }
                    }
                }
                $percentPass = ($numberCorrect/count($questions)) * 100;
                if($percentPass >= $quicktest->percent_pass) $pass = 1;
                $lesson = Lesson::find($request->lesson_id);
                $studentCurriculum =  StudentCurriculum::where([
                    ['stdcode',Auth::user()->stdcode],
                    ['curriculum_id',$lesson->curriculum_id]
                ])->first();
                $status = StudentCurriculumLessonStatus::where([
                    ['student_curriculum_id', $studentCurriculum->id],
                    ['lesson_id',$lesson->id]
                ])->get();
                if(count($status) > 0) {
                    $status[0]->pass    = $pass;
                    $status[0]->correct = $numberCorrect;
                    $status[0]->options = $request->options;
                    if($status[0]->save()) {
                        return response()->json([
                            'correct'      => $numberCorrect,
                            'pass'         => $pass,
                            'total'        => count($questions)
                        ]);
                    }
                } 
            }
        }
        else return $this->returnNotAjax();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $curriculum = Curriculum::find($id);
        $studentCurriculum =  StudentCurriculum::where([
            ['stdcode',Auth::user()->stdcode],
            ['curriculum_id',$id]
        ])->first();
        $studentCurriculumStatus = StudentCurriculumLessonStatus::where('student_curriculum_id',$studentCurriculum->id)->get();
        $lessons = Lesson::where([
            ['curriculum_id',$id],
            ['states',1]
        ])->oldest()->get();
        $listLesson = [];
        foreach ($lessons as &$item) {
            $item->status = [];
            $item->content;
            if($item->parent_id == 0) {
                array_push($listLesson, $item);
                foreach ($item->childs as &$i) {
                    foreach ($studentCurriculumStatus as $value) {
                        if($i->id == $value->lesson_id) {
                            $i->status = $value;
                        }
                    }
                    array_push($listLesson, $i);

                }
            }
        } 
        return view('frontend.learning.o_learning.main', compact('curriculum','listLesson'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->ajax()) {
            switch ($request->type) {
                case 'init':
                $respon = [];
                $lesson = Lesson::find($id);
                $studentCurriculum =  StudentCurriculum::where([
                    ['stdcode',Auth::user()->stdcode],
                    ['curriculum_id',$lesson->curriculum_id]
                ])->first();
                $status = StudentCurriculumLessonStatus::where([
                    ['student_curriculum_id', $studentCurriculum->id],
                    ['lesson_id',$id]
                ])->get();
                if(count($status) > 0 && $status[0]->pass == 0) {
                    // kiểm tra bài học trước đó đã hoàn thành hay chưa
                    if($request->idLessonPrev != 0) {
                        $statusLessonPrev = StudentCurriculumLessonStatus::where([
                            ['student_curriculum_id', $studentCurriculum->id],
                            ['lesson_id',$request->idLessonPrev]
                        ])->first();
                        if($statusLessonPrev->pass == 0) {
                            return response()->json([
                                'type' => 'error',
                                'title' => 'Lỗi',
                                'content' => 'Bạn chưa hoàn thành bài học trước đó'
                            ]);
                        }
                    }
                }
                $value = [
                    'student_curriculum_id' => $studentCurriculum->id,
                    'lesson_id'             => $id,
                    'pass'                  => 0,
                    'correct'               => 0,
                    'total'                 => 0,
                    'note'                  => '',
                ];
                $data = null;
                if($lesson->type === 'lesson') {
                    $data = LessonContent::where('lesson_id',$id)->first();
                    $respon = [
                        'lesson' => $lesson,
                        'data'   => $data,
                    ];
                    if(count($status) <= 0) $respon['status'] = StudentCurriculumLessonStatus::create($value);
                    else $respon['status'] = $status[0];
                }
                if($lesson->type === 'test') {
                    $quicktest = LessonQuicktest::where('lesson_id',$id)->first();
                    $questions = LessonQuicktestQuestion::where('quicktest_id',$quicktest->id)->get();
                    $correctForce = ($quicktest->percent_pass/100)*count($questions);
                    $value['total'] = count($questions);
                    if(count($status) <= 0) $status = StudentCurriculumLessonStatus::create($value);
                    $respon = [
                        'lesson'       => $lesson,
                        'quicktest'    => $quicktest,
                        'questions'    => $questions,
                        'correctForce' => $correctForce,
                    ];
                    if($quicktest->review_question == 1) $respon['status'] =  $status[0];
                    if($status[0]->pass == 1 && $quicktest->showcorrect_answer == 1){
                        $correct = [];
                        foreach ($questions as $item) {
                            $correct[$item->id] = $item->correct;
                        }
                        $respon['correct'] = response()->json($correct);
                    }
                }
                /* get document lesson */
                $respon['documents'] = Document::where([
                    ['object_key','lesson'],
                    ['object_id',$id] 
                ])->latest()->get();
                /* get note lesson */
                if(count($status) > 0) {
                    $notes = json_decode($status[0]->note,true);
                    if($notes != '') {
                        foreach ($notes as $key => $value) {
                            $temp = date('d-m-Y H:i:s',$key) .'-'.$key;
                            $notes[$temp] = $notes[$key];
                            unset($notes[$key]);
                        }
                        $respon["notes"] = $notes;
                    }
                    else $respon["notes"] = [];
                }
                return response()->json($respon);
                break;
                case 'pass':
                $status = StudentCurriculumLessonStatus::find($id);
                $status->pass = 1;
                if($status->save()) {
                    return response()->json([   
                        'type'    => 'success',
                        'title'   => 'Thành công',
                        'content' => 'Hoàn thành bài học'
                    ]);
                }
                else return $this->returnWarning();
                break;
                case 'writeNote':
                $status = StudentCurriculumLessonStatus::find($id);
                $time = time();
                $note = '';
                if($status->note != '') {
                    $note = $status->note;
                    $note = substr($note, strpos($note,'{')+1);
                    $noteNew = '{"'.$time.'":"'.$request->note.'",';
                    $note = $noteNew.$note;
                }
                else {
                    $note = '{"'.$time.'":"'.$request->note.'"}';
                }
                $status->note = $note;
                if($status->save()){
                    return response()->json([
                        'type'    => 'success',
                        'title'   => 'Thành công',
                        'content' => 'Thêm ghi chú thành công.',
                        'date'    => date("d-m-Y H:i:s",$time),
                        'time'    => $time,
                        'status'  => $status
                    ]);
                }
                else return $this->returnWarning();
                break;
                case 'deleteNote':
                $status = StudentCurriculumLessonStatus::find($id);
                $note = json_decode($status->note,true);
                if(count($note) > 1) unset($note[$request->date]);
                else $note = '';
                $status->note = json_encode($note);
                if($status->save()){
                    return response()->json([
                        'type'    => 'success',
                        'title'   => 'Thành công',
                        'content' => 'Xóa ghi chú thành công.',
                    ]);
                }
                else return $this->returnWarning();
                break;
            }
        }
        else return returnNotAjax();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
