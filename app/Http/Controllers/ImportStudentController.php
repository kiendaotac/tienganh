<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Functions;
use App\Student;
use App\StudentInfo;
use App\File;
use App\Unis;
use Auth;
use Carbon\Carbon;
use Validator;
use Response;
use Hash;
use Excel;

class ImportStudentController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $url = getURL();
        $currentFunction = Functions::where('route', $url)->where('state', 1)->orderBy('id', 'desc')->first();
        $user = Auth::user();
        $profile = $user->profile[0];
        $uniId = explode(",",$profile->value)[0];
        return view('backend.student.import.main', compact('currentFunction','uniId'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->ajax()) {
            $students = Excel::load($request->chose_excel,function($reader){
            })->get();
            $count = 0;
            foreach ($students as &$student) {
                $error = $this->validatorStudent($student);
                if(count($error) <= 0) {
                    $studentCreate = '';
                    $data = $this->processStudent($student,$request->uni_id);
                    $studentCreate = Student::create($data);
                    if($studentCreate != '') {
                        $count++;
                        $data = $this->processInfoStudent($data);
                        $studentInfo = StudentInfo::create($data);
                    }
                }
                $student["col"] = $error;
            }
            return response()->json([
                'type'     => 'success',
                'count'    => $count,
                'total'    => count($students),
                'students' => $students
            ]);
        }   
        else return $this->returnNotAjax();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        if($request->ajax()) {
            return $this->getMajors($id);
        }   
        else return $this->returnNotAjax();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {

    }

    /* get data major */
    public function getMajors($id)
    {
        return response()->json(Unis::where('parent_id',$id)->latest()->get());
    }
    /* validator Student */
    public function validatorStudent($student)
    {
        $std = Student::select('stdcode')->get();
        $data = [];
        $error = [];
        foreach ($std as $item) {
            array_push($data,$item->stdcode);
        }
        if($student->ma_sv == '') array_push($error,2);
        else {
            if(in_array($student->ma_sv, $data)) array_push($error,2);
        }
        if($student->ho_va_ten == '') array_push($error,3);
        if($student->gioi_tinh == '') array_push($error,4);
        if($student->nam_vao_truong == '') array_push($error,5);
        $d = substr($student->nam_vao_truong,strpos($student->nam_vao_truong,'/'));
        $m = substr($student->nam_vao_truong,strpos($student->nam_vao_truong,'/'));
        if($student->ngay_sinh == '') array_push($error,6);
        if($student->lop_quan_ly == '') array_push($error,7);
        return $error;
    }
    /* process student */
    public function processStudent($request,$id)
    {
        $name      = $request->ho_va_ten;
        $lastname  = substr($name,0,strripos($name," "));
        $firstname = substr($name,strripos($name," ")+1);
        $data      = $request;
        if(is_object($request->ngay_sinh)) {
            $password         = Carbon::parse($request->ngay_sinh)->format('dmY');
            $data['birthday'] = Carbon::parse($request->ngay_sinh)->format('Y-m-d');
        }
        else {
            $password         = Carbon::createFromFormat('d/m/Y', $request->ngay_sinh)->format('dmY');
            $data['birthday'] = Carbon::createFromFormat('d/m/Y', $request->ngay_sinh)->format('Y-m-d');
        }
        switch ($request->gioi_tinh) {
            case 'Nam':
            $data['sex'] = 1;
            break;
            case 'Nữ':
            $data['sex'] = 0;
            break;
            default:
            $data['sex'] = 2;
            break;
        }
        $data['firstname'] = $firstname;
        $data['lastname']  = $lastname;
        $data['password']  = Hash::make($password);
        $data['uni_id']    = $id;
        $data['stdcode']   = $request->ma_sv;
        return $data->toArray();
    }

    /* process info student */
    public function processInfoStudent($data)
    {
        $profile = Auth::user()->profile->first();
        $key = explode(",",$profile->key);
        $value = explode(",",$profile->value);
        for ($i=0; $i < count($key); $i++) { 
            switch ($key[$i]) {
                case 'name_uni':
                $data['uni'] = $value[$i];
                break;
                case 'name_department':
                $data['dept'] = $value[$i];
                break;
                case 'name_major':
                $data['majo'] = $value[$i];
                break;
            }
        }
        if(is_object($data['nam_vao_truong'])) $data['start_date'] = Carbon::parse($data['nam_vao_truong'])->format('Y-m-d');
        else $data['start_date'] = Carbon::createFromFormat('d/m/Y', $data['nam_vao_truong'])->format('Y-m-d');
        $data['course'] = substr($data['lop_quan_ly'],strrpos($data['lop_quan_ly'],'_')+1);
        $data['class'] = $data['lop_quan_ly'];
        return $data;
    }
}
