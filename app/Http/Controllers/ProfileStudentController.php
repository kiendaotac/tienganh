<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Hash;
use Auth;
use Response;
use App\Functions;
use App\Unis;
use App\Student;
use App\StudentInfo;
use Validator;
class ProfileStudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $url = getURL();
        $currentFunction = Functions::where('route', $url)->where('state', 1)->orderBy('id', 'desc')->first();
        $unis = Unis::where([['states','1'],['parent_id',0]])->orderBy('ordering', 'desc')->get();
        $department = Unis::where('parent_id',Auth::guard('student')->user()->uni_id)->get();
        
        $listdep = [];
        foreach ($department as $key) {
            foreach ($key->department as $value) {
                $listdep[$value->name]=$value->id;
            }
        }
        $listmj = [];
        foreach ($department as $key) {
            foreach ($key->department as $value) {
                foreach ($value->department as $value1) {
                    $listmj[$value1->name]=$value1->id;
                }
            }
        }
        $studentinfo = StudentInfo::where('stdcode',Auth::guard('student')->user()->stdcode)->first();
        return view('frontend.profile',compact('unis','listmj','listdep','studentinfo','currentFunction'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if ($id == 'course') {
            return view('frontend.coursing');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        if (strrpos($id,"a")==true) {
            return "";
        }else{
            if (strpos($id,'unis')==true) {
                $id = substr($id, 0,-4);
                $major = Unis::find($id)->department;
                return $major; 
            }
            if (strpos($id, 'dep')==true) {
                $id = substr($id, 0,-3);
                $major = Unis::find($id)->department;
                return $major;
            }
        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        if ($request->ajax()) {
            $check = $this->processValidator($request);
            if (empty($check)) {
                $number = strlen(trim(strrchr($request->name," "))) +1;
                $student = Student::find($id);
                $student->lastname = trim(strrchr($request->name," "));
                $student->firstname = substr($request->name, 0,-$number);
                $student->birthday = $request->birthday;
                $student->sex = $request->sex;
                $student->email = $request->email;
                $student->stdcode = $request->stdcode;
                $student->phone = $request->phone;
                if ($request->newpassword !="") {
                    $student->password=Hash::make($request->newpassword);
                }
                $student->save();
                $studentinfo = StudentInfo::find($request->stdstudentinfo);
                $studentinfo->uni = Unis::find($request->uni)->name;
                $studentinfo->dept = Unis::find($request->dept)->name;
                $studentinfo->majo = Unis::find($request->majo)->name;
                $studentinfo->course = $request->course;
                $studentinfo->stdcode = $request->stdcode;
                $studentinfo->facebook = $request->facebook;
                $rqclass = Unis::find($request->majo)->name . " - " . $request->course;
                $studentinfo->class = $rqclass;
                $studentinfo->save();
                $path=0;
                if ($request->hasFile('newavatar')) {
                    $path=1;
                }
                // $path = $request->file('newavatar')->store('public/img');
                $check = ['Sửa thành công'];
                return Response::json([
                'type' => $request->all(),
                'title' => 'Thành công!',
                'content' => $check,
            ]);
            }else{
            return Response::json([
                'type' => 'error',
                'title' => 'Lỗi',
                'content' => $check,
            ]);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function processValidator($request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'stdcode' => 'required',
            'birthday' => 'required',
            'email' => 'required',
            'phone' => 'required|numeric',
            'uni' => 'required|numeric',
            'dept' => 'required|numeric',
            'majo' => 'required|numeric',
            'course' => 'required',
        ], [
            'name.required' => 'Tên không được để trống.',
            'stdcode.required' => 'Mã sinh viên không được để trống.',
            'birthday.required' => 'Ngày sinh không được để trống.',
            'email.required' => 'Email không được để trống.',
            'phone.required' => 'Số điện thoại không được để trống.',
            'phone.numeric' => 'Số điện thoại phải là số.',
            'uni.required' => 'Trường không được để trống.',
            'dept.required' => 'Ngành học không được để trống.',
            'majo.required' => 'Lớp không được để trống.',
            'course.required' => 'Khóa học không được để trống.',
        ]);
        if ($validator->fails()) {
            return $validator->errors()->all();
        }
        return '';
    }
}
