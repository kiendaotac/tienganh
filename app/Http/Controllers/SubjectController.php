<?php

namespace App\Http\Controllers;

use App\Functions;
use App\TestSubject;
use Illuminate\Http\Request;
use Validator;

class SubjectController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $url                =   getURL();
        $currentFunction    =   Functions::where('route',$url)->where('state',1)->orderBy('id','desc')->first();
        $subjects           =   TestSubject::where('state',1)->where('parent_id',0)->get();
        return view('backend.questionbank.subjects.main', compact('currentFunction','subjects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->ajax()) {
            $validator  =   Validator::make($request->all(), [
                'parent_id'     =>  'required',
                'name'          =>  'required',
                'desc'          =>  'nullable|string',
                'state'         =>  'required|numeric',
            ]);
            if($validator->fails()){
                return response()->json([
                    'type'      =>  'error',
                    'title'     =>  'Lỗi!',
                    'content'   =>  $validator->errors()->all()]);
            } else{
                $this->addItem($request);
                return response()->json([
                    'type'      => 'success',
                    'title'     => 'Thành công!',
                    'content'   => 'Thêm chủ đề thành công !!'
                ]);
            }
        } else {
            return response()->json([
                'type'      => 'error',
                'title'     => 'Lỗi!',
                'content'   => 'Không phải ajax request']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        if ($request->ajax()) {
            if ($id === 'getDatatable'){
//                $subject    =   $this->sortSubject();
                $subject    =   TestSubject::sortSubject();
                return datatables($subject)->make(true);
            }
        } else {
            return response()->json([
                'type'      => 'error',
                'title'     => 'Lỗi!',
                'content'   => 'Không phải ajax request']);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->ajax()) {
            $validator  =   Validator::make($request->all(), [
                'parent_id'     =>  'required',
                'name'          =>  'required',
                'desc'          =>  'nullable|string',
                'state'         =>  'required|numeric',
            ]);
            if($validator->fails()){
                return response()->json([
                    'type'      =>  'error',
                    'title'     =>  'Lỗi!',
                    'content'   =>  $validator->errors()->all()]);
            } else{
                $this->editItem($request,$id);
                return response()->json([
                    'type'      => 'success',
                    'title'     => 'Thành công!',
                    'content'   => 'Sửa chủ đề thành công !!'
                ]);
            }
        } else {
            return response()->json([
                'type'      => 'error',
                'title'     => 'Lỗi!',
                'content'   => 'Không phải ajax request']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->ajax()) {
            TestSubject::destroy($id);
            return response()->json([
                'type'      => 'success',
                'title'     => 'Thành công!',
                'content'   => 'Xóa chủ đề thành công !!'
            ]);
        } else {
            return response()->json([
                'type'      => 'error',
                'title'     => 'Lỗi!',
                'content'   => 'Không phải ajax request']);
        }
    }

    public function addItem($request){
        TestSubject::create($request->all());
    }
    public function editItem($request, $id){
        $subject    =   TestSubject::find($id);
        $subject->fill($request->all());
        $subject->save();
    }

//    public function sortSubject(){
//        $subjects   =   TestSubject::where('parent_id',0)->get();
//        $sort       =   array();
//        foreach ($subjects as $subject){
//            array_push($sort,$subject);
//            foreach ($subject->childs as $child){
//                array_push($sort,$child);
//            }
//        }
//        return $sort;
//    }
}
