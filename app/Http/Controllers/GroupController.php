<?php

namespace App\Http\Controllers;

use App\File;
use App\Functions;
use App\QuestionType;
use App\TestGroup;
use App\TestGroupQuestion;
use App\TestSubject;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Validator;

class GroupController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        abort(404);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->ajax()) {
            $validator  =   Validator::make($request->all(), [
                'test_subject_id'   =>  'required|numeric',
                'name'              =>  'required',
                'content'           =>  'required|string',
                'media_type'        =>  'required|numeric',
                'media_source'      =>  'required|string',
                'question_type'     =>  'required|numeric',
                'level'             =>  'required|numeric',
                'state'             =>  'required|numeric',
            ]);
            if($validator->fails()){
                return response()->json([
                    'type'      =>  'error',
                    'title'     =>  'Lỗi!',
                    'content'   =>  $validator->errors()->all()]);
            }
            if ($request->question_type == 8){
                $group_id       =   $this->addGroupAndQuestionInParagraph($request);
            } else{
                $group_id       =   $this->addItem($request);
            }
            return response()->json([
                'type'      => 'success',
                'title'     => 'Thành công!',
                'content'   => 'Thêm group thành công !!',
                'group_id'  =>  $group_id,
                'question_type'=>$request->question_type
            ]);
        } else {
            return response()->json([
                'type'      => 'error',
                'title'     => 'Lỗi!',
                'content'   => 'Không phải ajax request']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        if ($id === 'subject'){
            $subject_id         =   $request->id;
            $subject            =   TestSubject::find($subject_id);
            if ($subject->parent_id <= 0){
                abort(404);
            }
            if ($subject->state == 0){
                abort(404);
            }
            $url                =   getURL();
            $currentFunction    =   Functions::where('route',$url)->where('state','>=',1)->orderBy('id','desc')->first();
            $subjects           =   TestSubject::sortSubject();
            $question_type      =   QuestionType::where('state',1)->get();
            $maxsize = 0;
            $post_max_size = str_replace("M", "", ini_get('post_max_size'));
            $upload_max_filesize = str_replace("M", "", ini_get('upload_max_filesize'));
            $maxsize = ($post_max_size > $upload_max_filesize ? $post_max_size : $upload_max_filesize);
            $month = File::orderBy('created_at', 'desc')->groupBy('year', 'month')->get();
            $year = File::orderBy('created_at', 'desc')->groupBy('year')->get();
            return view('backend.questionbank.groups.main', compact('currentFunction','subjects','question_type','maxsize','month','year','subject_id'));
        }
        if ($request->ajax()) {
            if ($id == 'getDatatable'){
                $subject_id =   $request->subject_id;
                return datatables(TestGroup::where('state',1)->where('test_subject_id',$subject_id)->get())->make(true);
            }
        } else {
            return response()->json([
                'type'      => 'error',
                'title'     => 'Lỗi!',
                'content'   => 'Không phải ajax request']);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->ajax()) {
            $validator  =   Validator::make($request->all(), [
//                'test_subject_id'   =>  'required|numeric',
                'name'              =>  'required',
                'content'           =>  'required|string',
                'media_type'        =>  'required|numeric',
                'media_source'      =>  'required|string',
//                'question_type'     =>  'required|numeric',
                'level'             =>  'required|numeric',
                'state'             =>  'required|numeric',
            ]);
            if($validator->fails()){
                return response()->json([
                    'type'      =>  'error',
                    'title'     =>  'Lỗi!',
                    'content'   =>  $validator->errors()->all()]);
            }
            $question_type  =   TestGroup::find($id)->question_type;
            if ($question_type == 8){
                $this->editGroupAndQuestionInParagraph($request,$id);
            } else{
                $this->editItem($request, $id);
            }
            return response()->json([
                'type'      => 'success',
                'title'     => 'Thành công!',
                'content'   => 'Sửa group thành công !!'
            ]);
        } else {
            return response()->json([
                'type'      => 'error',
                'title'     => 'Lỗi!',
                'content'   => 'Không phải ajax request']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->ajax()) {
           $this->deleteAllQuestionInGroup($id);
           TestGroup::destroy($id);
            return response()->json([
                'type'      => 'success',
                'title'     => 'Thành công!',
                'content'   => 'Xóa group thành công !!'
            ]);
        } else {
            return response()->json([
                'type'      => 'error',
                'title'     => 'Lỗi!',
                'content'   => 'Không phải ajax request']);
        }
    }

    public function addItem($request){
        $group                  =   new TestGroup();
        $group->test_subject_id =   $request->test_subject_id;
        $group->name            =   $request->name;
        $group->content         =   $request->content;
        $group->media_type      =   $request->media_type;
        $group->media_source    =   $request->media_source;
        $group->question_type   =   $request->question_type;
        $group->question_shuff  =   $request->has('question_shuff') ? 1 : 0;
        $group->level           =   $request->level;
        $group->state           =   $request->state;
        $group->save();
        return $group->id;
    }

    public function editItem($request, $id){
        $group                  =   TestGroup::find($id);
//        $group->test_subject_id =   $request->test_subject_id;
        $group->name            =   $request->name;
        $group->content         =   $request->content;
        $group->media_type      =   $request->media_type;
        $group->media_source    =   $request->media_source;
//        $group->question_type   =   $request->question_type;
        $group->question_shuff  =   $request->has('question_shuff') ? 1 : 0;
        $group->level           =   $request->level;
        $group->state           =   $request->state;
        $group->save();
    }

    public function addGroupAndQuestionInParagraph($request){
        /* Lưu group */
        $group                  =   new TestGroup();
        $group->test_subject_id =   $request->test_subject_id;
        $group->name            =   $request->name;
        $group->content         =   $request->content;
        $group->media_type      =   $request->media_type;
        $group->media_source    =   $request->media_source;
        $group->question_type   =   $request->question_type;
        $group->question_shuff  =   $request->has('question_shuff') ? 1 : 0;
        $group->level           =   $request->level;
        $group->state           =   $request->state;
        $group->save();

        /* Sử lý phân tách câu hỏi */
        $content                =   $request->media_source;
        $regex                  =   '#[^\\\]{(.*?[^\\\])}#';
        $replacement            =   array();
        $check                  =   preg_match_all($regex,$content,$matches);
        if ($check){
            foreach ($matches[1] as $item){
                $arrAnswer                  =   explode('\\',$item);
                $question                   =   new TestGroupQuestion();
                $question->group_id         =   $group->id;
                $question->question         =   'Lưu tạm tý nữa sửa =))';
                $arr_answer_options         =   array();
                $id                         =   1;
                foreach ($arrAnswer as $answer){
                    $element    =   array([
                        'id'        =>  $id,
                        'type'      =>  'text',
                        'content'   =>  $answer
                    ]);
                    array_push($arr_answer_options,$element[0]);
                    $id++;
                }
                $question->answer_options   =   json_encode($arr_answer_options);
                $question->correct_answer   =   1;
                $question->answer_shuff     =   1;
                $question->explain          =   'hoangkien151092@gmail.com';
                $question->hint             =   'hoangkien151092@gmail.com';
                $question->state            =   1;
                $question->save();
                array_push($replacement,'{$question_'.$question->id.'}');
            }
        }
        $result =   str_replace(array_values($matches[1]),$replacement,$content);
        /* Điền câu hỏi đã dc sử lý vào phần câu hỏi*/
        $tests                  =   TestGroupQuestion::where('group_id', $group->id)->get();
        foreach ($tests as $test){
            $test->question =   $result;
            $test->save();
        }
        return $group->id;
    }

    /* Sửa group Điền từ và cụm từ với đoạn văn cho trước */
    public function editGroupAndQuestionInParagraph($request, $id){
        /* Lưu group */
        $group                  =   TestGroup::find($id);
//        $group->test_subject_id =   $request->test_subject_id;
        $group->name            =   $request->name;
        $group->content         =   $request->content;
        $group->media_type      =   $request->media_type;
        $group->media_source    =   $request->media_source;
//        $group->question_type   =   $request->question_type;
        $group->question_shuff  =   $request->has('question_shuff') ? 1 : 0;
        $group->level           =   $request->level;
        $group->state           =   $request->state;
        $group->save();


        /* Sử lý phân tách câu hỏi */
        $content                =   $request->media_source;
        $regex                  =   '#[^\\\]{(.*?[^\\\])}#';
        $replacement            =   array();
        $check                  =   preg_match_all($regex,$content,$matches);
        if ($check){
            /* Xóa hết câu hỏi cũ của group này */
            $this->deleteAllQuestionInGroup($id);

            foreach ($matches[1] as $item){
                $arrAnswer                  =   explode('\\',$item);
                $question                   =   new TestGroupQuestion();
                $question->group_id         =   $group->id;
                $question->question         =   'Lưu tạm tý nữa sửa =))';
                $arr_answer_options         =   array();
                $id                         =   1;
                foreach ($arrAnswer as $answer){
                    $element    =   array([
                        'id'        =>  $id,
                        'type'      =>  'text',
                        'content'   =>  $answer
                    ]);
                    array_push($arr_answer_options,$element[0]);
                    $id++;
                }
                $question->answer_options   =   json_encode($arr_answer_options);
                $question->correct_answer   =   1;
                $question->answer_shuff     =   1;
                $question->explain          =   'hoangkien151092@gmail.com';
                $question->hint             =   'hoangkien151092@gmail.com';
                $question->state            =   1;
                $question->save();
                array_push($replacement,'{$question_'.$question->id.'}');
            }
        }
        $result =   str_replace(array_values($matches[1]),$replacement,$content);
        /* Điền câu hỏi đã dc sử lý vào phần câu hỏi*/
        $tests                  =   TestGroupQuestion::where('group_id', $group->id)->get();
        foreach ($tests as $test){
            $test->question =   $result;
            $test->save();
        }
//        return $group->id;
    }

    private function deleteAllQuestionInGroup($group_id){
        TestGroupQuestion::where('group_id',$group_id)->delete();
    }
}
