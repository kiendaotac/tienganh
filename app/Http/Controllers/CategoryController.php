<?php

namespace App\Http\Controllers;

use App\Category;
use App\File;
use App\Functions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Response;
use Validator;

class CategoryController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		$url = getURL();
		$currentFunction = Functions::where('route', $url)->where('state', 1)->orderBy('id', 'desc')->first();
		$categories = Category::where('states', 1)->get();
		$maxsize = 0;
		$post_max_size = str_replace("M", "", ini_get('post_max_size'));
		$upload_max_filesize = str_replace("M", "", ini_get('upload_max_filesize'));
		$maxsize = ($post_max_size > $upload_max_filesize ? $post_max_size : $upload_max_filesize);
		$month = File::orderBy('created_at', 'desc')->groupBy('year', 'month')->get();
		$year = File::orderBy('created_at', 'desc')->groupBy('year')->get();
		return view('backend.cate_manager.category.main', compact('currentFunction', 'categories', 'maxsize', 'month', 'year'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		if ($request->ajax()) {
			$valiData = $this->processValidator($request);
			if ($valiData == '') {
				$category = Category::create([
					'name' => $request->name,
					'slug' => str_slug($request->name, '-'),
					'icon' => $request->icon,
					'ordering' => ($request->ordering) ? $request->ordering : 100,
					'desc' => $request->desc,
					'parent_id' => $request->parent_id,
					'img' => $request->img,
				]);
				if ($category) {
					return Response::json([
						'type' => 'success',
						'title' => 'Thành công!',
						'content' => 'Thêm thể loại thành công.',
						'id' => $category->id,
						'name' => $category->name,
						'parent_id' => $category->parent_id,
					]);
				}
			} else {
				return Response::json([
					'type' => 'error',
					'title' => 'Lỗi!',
					'content' => $valiData,
				]);
			}
		} else {
			return Response::json([
				'type' => 'error',
				'title' => 'Lỗi!',
				'content' => 'Không phải ajax request',
			]);
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show(Request $request, $id) {
		if ($request->ajax()) {
			if ($id === 'getDatatable') {
				return datatables($this->getDataSorted())->make(true);
			}
		} else {
			return response()->json([
				'type' => 'error',
				'title' => 'Lỗi!',
				'content' => 'Không phải ajax request',
			]);
		}
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		if ($request->ajax()) {
			$valiData = $this->processValidator($request);
			if ($valiData == '') {
				$category = Category::find($id);
				$data = $request->all();
				if ($request->img == '') {
					$data['img'] = $category->img;
				}
				$data['slug'] = str_slug($request->name);
				$category->fill($data);
				if ($category->save()) {
					return Response::json([
						'type' => 'success',
						'title' => 'Thành công!',
						'content' => 'Sửa thể loại thành công.',
					]);
				} else {
					return Response::json([
						'type' => 'warning',
						'title' => 'Cảnh báo!',
						'content' => 'Gặp sự cố, thử lại sau.',
					]);
				}

			} else {
				return Response::json([
					'type' => 'error',
					'title' => 'Lỗi!',
					'content' => $valiData,
				]);
			}
		} else {
			return Response::json([
				'type' => 'error',
				'title' => 'Lỗi!',
				'content' => 'Không phải ajax request',
			]);
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Request $request, $id) {
		if ($request->ajax()) {
			if (Category::destroy($id)) {
				return Response::json([
					'type' => 'success',
					'title' => 'Thành công!',
					'content' => 'Xóa thể loại thành công.',
				]);
			} else {
				return Response::json([
					'type' => 'warning',
					'title' => 'Cảnh báo!',
					'content' => 'Gặp sự cố, thử lại sau.',
				]);
			}
		} else {
			return Response::json([
				'type' => 'error',
				'title' => 'Lỗi!',
				'content' => 'Không phải ajax request',
			]);
		}
	}
	/* get data category */
	public function getDataSorted() {
		$categories = Category::orderBy('ordering', 'asc')->get();
		$listdata = [];
		foreach ($categories as &$item) {
			if ($item->parent_id == 0) {
				array_push($listdata, $item);
				if (count($item->childs)) {
					foreach ($item->childs->sortBy('ordering') as $i) {
						array_push($listdata, $i);
					}
				}
			}
		}
		return $listdata;
	}
	/* process validator category */
	public function processValidator($request) {
		$validator = Validator::make($request->all(), [
			'name' => 'required',
			'ordering' => 'numeric|min:0|max:9999|nullable',
		], [
			'name.required' => 'Tên thể loại không được để trống.',
			'ordering.numeric' => 'ordering phải là số.',
			'ordering.min' => 'ordering phải lớn hơn 0.',
			'ordering.max' => 'ordering phải là nhỏ hơn 9999.',
		]);
		if ($validator->fails()) {
			return $validator->errors()->all();
		}

		return '';
	}
}
