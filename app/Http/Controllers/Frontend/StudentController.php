<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Hash;
use Auth;
use Response;
use App\Functions;
use App\Unis;
use App\Student;
use App\StudentInfo;
use App\File;
use App\StudentCurriculum;
use Validator;
use App\User;
use Illuminate\Support\Facades\Storage;
use App\Curriculum;
use Intervention\Image\Facades\Image;
class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if (Auth::guard('student')->check()) {
            $url = 'students';
            $currentFunction = Functions::where('route', $url)->where('state', 1)->orderBy('id', 'desc')->first();
            $unis = Unis::where([['states','1'],['parent_id',0]])->orderBy('ordering', 'desc')->get();
            // var_dump($currentFunction);
            $department = Unis::where('parent_id',Auth::guard('student')->user()->uni_id)->get();
            $uploaded = File::where('owner_id',Auth::guard('student')->user()->id)->orderBy('updated_at','desc')->get();
            //adddropzone
            $maxsize = 0;
            $post_max_size = str_replace("M", "", ini_get('post_max_size'));
            $upload_max_filesize = str_replace("M", "", ini_get('upload_max_filesize'));
            $maxsize = ($post_max_size > $upload_max_filesize ? $post_max_size : $upload_max_filesize);
            $month = File::orderBy('created_at', 'desc')->groupBy('year', 'month')->get();
            $year = File::orderBy('created_at', 'desc')->groupBy('year')->get();
            $studentinfo = StudentInfo::where('stdcode',Auth::guard('student')->user()->stdcode)->first();
            $listdep = [];
            foreach ($department as $key) {
                $listdep[$key->name]=$key->id;
            }
            $listmj = [];
            foreach ($department as $key) {
                foreach ($key->department as $value) {
                    $listmj[$value->name]=$value->id;
                }
            }
            return view('frontend.profile',compact('unis','listmj','listdep','studentinfo','currentFunction','uploaded','month','year','maxsize'));
        }else{
            return redirect()->route('get.student.login')->with('notierror','Bạn cần đăng nhập trước khi thực hiện thao tác');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if ($request->ajax()) {
            if ($request->hasFile('file')) {
                $file = $request->file('file');
                // set destination path
                $folderDir = 'public/uploads/' . date('Y/m');
                //$destinationPath = base_path() . '/' . $folderDir;
                // this form uploads multiple files
                $exe_flag = false;
                foreach ($request->file('file') as $fileKey => $fileObject) {
                    // make sure each file is valid
                    if ($fileObject->isValid()) {
                        // make destination file name
                        $destinationFileName = time() . '-' . str_slug(pathinfo($fileObject->getClientOriginalName(), PATHINFO_FILENAME), '-');
                        $extension = $fileObject->getClientOriginalExtension();
                        if ($extension == 'exe') {
                            $exe_flag = true;
                        }
                        // move the file from tmp to the destination path
                        $store = Storage::putFileAs($folderDir, $fileObject, $destinationFileName . '.' . $extension);
                        // Check image if iamge then make thumbnail
                        $IMGextensions = array('jpg', 'JPG', 'png', 'PNG', 'jpeg', 'JPEG', 'bmp', 'BMP', 'gif', 'GIF');
                        // save the the destination filename
                        if ($store) {
                            $file = new File();
                            $file->title = $fileObject->getClientOriginalName();
                            $file->file_name = $destinationFileName;
                            $file->extension = $extension;
                            $file->size = $fileObject->getClientSize();
                            $file->year = date('Y');
                            $file->month = date('m');
                            $file->creator_id = Auth::guard('student')->user()->id;
                            $file->owner_id = Auth::guard('student')->user()->id;
                            $file->states = 1;
                            $file->save();
                            if (in_array($extension, $IMGextensions)) {
                                $thumb = 'storage/uploads/' . $file->year . '/' . $file->month;
                                if (!file_exists($thumb . '/thumbs')) {
                                    mkdir($thumb . '/thumbs', 0777, true);
                                }
                                Image::make($thumb . '/' . $file->file_name . '.' . $file->extension)->resize(198, 132)->save($thumb . '/thumbs/' . $file->id . '.' . $file->extension);
                            }
                        }
                    }
                }
                if ($exe_flag) {
                    return response()->json([
                        'type' => 'error',
                        'title' => 'Thất bại!',
                        'content' => 'Không thể upload file exe !!',
                    ]);
                }
                return Response::json([
                    'type' => 'success',
                    'title' => 'Thành công!',
                    'content' => 'Upload hoàn tất!!',
                ]);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        if(Auth::guard('student')->check()){
            if ($id == 'course') {
                $course = StudentCurriculum::where([['stdcode',Auth::guard('student')->user()->stdcode],['states',1]])->get();
                return view('frontend.coursing',compact('course'));
            }
            elseif($id =='get-img'){
                $uploaded = File::where('owner_id',Auth::guard('student')->user()->id)->orderBy('updated_at','desc')->get();
                return view('frontend/layout/data',compact('uploaded'));
            }elseif(strpos($id,'-')){
                $data = Curriculum::find(substr(strrchr($id,'-'),1));
                return view('frontend/detail',compact('data'));
            }
        }else{
            return redirect()->route('get.student.login');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        if (strpos($id, 'earch-')==1){
            if(strlen($id)==7) return "";
            $tearch = User::where('display_name','like',"%".substr($id, 7)."%")->get();
            $course = Curriculum::where('name','like',"%".substr($id, 7)."%")->orderBy('ordering','desc')->get();
            $tearchcourse = [];
            foreach ($course as $key => $value) {
                $tearchcourse[$value->id] = $value->user->display_name;
            }
            if(count($course->toArray())==0 && count($tearch->toArray())==0){
                return "nodata";
            }
            return Response::json([
                "course" => $course,
                "tearch" => $tearch,
                'tearchcourse' => $tearchcourse
            ]);
        }
        if (strrpos($id,"a")==true) {
            return "";
        }
        if (strpos($id,'unis')==true) {
            $id = substr($id, 0,-4);
            $major = Unis::find($id)->department;
            return $major; 
        }
        if (strpos($id, 'dep')==true) {
            $id = substr($id, 0,-3);
            $major = Unis::find($id)->department;
            return $major;
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        if ($request->ajax()) {
            if ($request->link) {
                $avatarnew = Student::find($id);
                $avatarnew->avatar = $request->link;
                $avatarnew->save();
                return Response::json([
                'type' => 'success',
                'title' => 'Thành công!',
                'content' => 'Cập nhật ảnh đại diện thành công',
                'link' => $request->link
                ]);
            }
            $check = $this->processValidator($request);
            if (empty($check)) {
                $number = strlen(trim(strrchr($request->name," "))) +1;
                $student = Student::find($id);
                $student->lastname = trim(strrchr($request->name," "));
                $student->firstname = substr($request->name, 0,-$number);
                $student->birthday = $request->birthday;
                $student->sex = $request->sex;
                $student->email = $request->email;
                $student->stdcode = $request->stdcode;
                $student->phone = $request->phone;
                $student->uni_id = $request->uni;
                if ($request->newpassword !="") {
                    $student->password=Hash::make($request->newpassword);
                }
                $student->save();
                $studentinfo = StudentInfo::find($request->stdstudentinfo);
                $studentinfo->uni = Unis::find($request->uni)->name;
                $studentinfo->dept = Unis::find($request->dept)->name;
                $studentinfo->majo = Unis::find($request->majo)->name;
                $studentinfo->course = $request->course;
                $studentinfo->stdcode = $request->stdcode;
                $studentinfo->facebook = $request->facebook;
                $rqclass = Unis::find($request->majo)->name . " - " . $request->course;
                $studentinfo->class = $rqclass;
                $studentinfo->save();
                return Response::json([
                'type' => 'success',
                'title' => 'Thành công!',
                'content' => ['Cập nhật thông tin thành công!'],
            ]);
            }else{
            return Response::json([
                'type' => 'error',
                'title' => 'Lỗi',
                'content' => $check,
            ]);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $value = File::find($id);
        $data = "storage/uploads/".$value->year."/".$value->month."/".$value->file_name.".".$value->extension;
        unlink($data);
        $value->delete();
        return Response::json([
            'type' => 'success',
            'title' => 'Thành công',
            'content' => "Xóa ảnh thành công",
        ]);
    }
    public function processValidator($request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'stdcode' => 'required',
            'birthday' => 'required',
            'email' => 'required',
            'phone' => 'required|numeric',
            'uni' => 'required|numeric',
            'dept' => 'required|numeric',
            'majo' => 'required|numeric',
            'course' => 'required',
        ], [
            'name.required' => 'Tên không được để trống.',
            'stdcode.required' => 'Mã sinh viên không được để trống.',
            'birthday.required' => 'Ngày sinh không được để trống.',
            'email.required' => 'Email không được để trống.',
            'phone.required' => 'Số điện thoại không được để trống.',
            'phone.numeric' => 'Số điện thoại phải là số.',
            'uni.required' => 'Trường không được để trống.',
            'dept.required' => 'Ngành học không được để trống.',
            'majo.required' => 'Lớp không được để trống.',
            'course.required' => 'Khóa học không được để trống.',
        ]);
        if ($validator->fails()) {
            return $validator->errors()->all();
        }
        return '';
    }
}
