<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Curriculum;
use Response;
use Auth;
use App\Lesson;
use App\StudentCurriculum;
class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $curri = Curriculum::where([['featured','1'],['states','1']])->orderBy('ordering','desc')->limit(4)->get();
        return view('frontend.welcome',compact('curri'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $data = Curriculum::find(substr(strrchr($id,'-'),1));
        $lessondata=Lesson::where([['curriculum_id',substr(strrchr($id,'-'),1)],['parent_id',0]])->get();
        // if(Auth::guard('student')->check()){
        //     $pass = StudentCurriculum::where([['stdcode',Auth::guard('student')->user()->stdcode],[]])->get();

        //     echo "<pre>";
        //     var_dump($pass->toArray());
        // }
       
        // die;
        return view('frontend/detail',compact('data','lessondata'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
