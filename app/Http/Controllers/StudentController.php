<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Functions;
use App\Student;
use App\StudentCurriculum;
use App\ClassCurriculumStudent;
use App\StudentInfo;    
use Auth;
use Carbon\Carbon;
use Validator;
use Response;
use Hash;

class StudentController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $url = getURL();
        $currentFunction = Functions::where('route', $url)->where('state', 1)->orderBy('id', 'desc')->first();
        return view('backend.student.main', compact('currentFunction'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->ajax()) {
            if($this->validatorStudent($request) != '') return $this->validatorStudent($request);
            $data = $this->processStudent($request);
            $student = Student::create($data);
            if($student) {
                $data = $this->processInfoStudent($data);
                $studentInfo = StudentInfo::create($data);
                if($studentInfo) {
                    return response()->json([
                        'type' => 'success',
                        'title' => 'Thành công',
                        'content' => 'Cập nhật sinh viên thành công',
                    ]);
                }
                else return $this->returnWarning();
            }
            else return $this->returnWarning();
        }
        else return $this->returnNotAjax();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        if ($request->ajax()) {
            if ($id === 'getDatatable') {
                return datatables($this->getDataSorted())->make(true);
            }
        } else {
            $this->returnNotAjax();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->ajax()) {
            if($this->validatorStudent($request) != '') return $this->validatorStudent($request);
            $student = Student::find($id);
            $studentInfo = StudentInfo::where('stdcode',$student->stdcode)->first();
            $data = $this->processStudent($request);
            $student->fill($data);
            if($student->save()){
                $data = $this->processInfoStudent($data);
                $studentInfo->fill($data);
                if($studentInfo->save()) {
                    return response()->json([
                        'type'    => 'success',
                        'title'   => 'Thành công',
                        'content' => 'Cập nhật sinh viên thành công',
                    ]);
                }
                else return $this->returnWarning();
            }
            else return $this->returnWarning();
        }
        else return $this->returnNotAjax();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if($request->ajax()) {
            $student = Student::find($id);
            $stdcode = $student->stdcode;
            if($student->delete()) {
                StudentInfo::where('stdcode',$stdcode)->first()->delete();
                return response()->json([
                    'type'    => 'success',
                    'title'   => 'Thành công',
                    'content' => 'Xóa sinh viên thành công',
                ]);
            }
            else return $this->returnWarning();
        }
        else return $this->returnNotAjax();
    }

    /* get data student */
    public function getDataSorted() {
        $students = [];
        if(Auth::user()->hasRole('Admin')) {
            $profile = Auth::user()->profile->first();
            $uni_id = explode(',',$profile->value)[0];
            $students = Student::where('uni_id',$uni_id)->latest('created_at')->get();
        }
        else $students = Student::latest('created_at')->get();
        foreach ($students as &$student) {
            $student['info'] = $student->info;
        }
        return $students->toArray();
    }

    /* validator student */
    public function validatorStudent($request)
    {
        $validator = Validator::make($request->all(), [
            'stdcode'    => 'required',
            'name'       => 'required',
            'birthday'   => 'required',
            'sex'        => 'required',
            'course'     => 'required',
            'start_date' => 'required',
            'class'      => 'required',
            'email'      => 'required|email',
            'phone'      => 'numeric|nullable'

        ]);
        if($validator->fails()) {
            return Response::json([
                'type'    => 'warning',
                'title'   => 'Cảnh báo!',
                'content' => $validator->errors()->all()[0],
            ]);
        }
        return '';
    }

    /* process student */
    public function processStudent($request)
    {
        $name      = $request->name;
        $lastname = substr($name,0,strripos($name," "));
        $firstname  = substr($name,strripos($name," ")+1);
        $password = Carbon::parse($request->birthday)->format('dmY');
        $data              = $request->all();
        $data['firstname'] = $firstname;
        $data['lastname']  = $lastname;
        $data['password']  = Hash::make($password);
        return $data;
    }

    /* process info student */
    public function processInfoStudent($data)
    {
        $profile = Auth::user()->profile->first();
        $key = explode(",",$profile->key);
        $value = explode(",",$profile->value);
        for ($i=0; $i < count($key); $i++) { 
            switch ($key[$i]) {
                case 'name_uni':
                $data['uni'] = $value[$i];
                break;
                case 'name_department':
                $data['dept'] = $value[$i];
                break;
                case 'name_major':
                $data['majo'] = $value[$i];
                break;
            }
        }
        return $data;
    }

}
