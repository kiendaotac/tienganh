<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Functions;
use App\Curriculum;
use App\ClassCurriculums;
use App\StudentCurriculum;
use App\ClassCurriculumStudent;
use App\Lesson;
use App\StudentCurriculumLessonStatus;

class StudentReportController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $url = getURL();
        $currentFunction = Functions::where('route', $url)->where('state', 1)->orderBy('id', 'desc')->first();
        $curriculums = Curriculum::belong()->oldest('ordering')->oldest('created_at')->get();
        return view('backend.report.student_report.main', compact('currentFunction','curriculums'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        if($request->ajax()) {
            switch ($id) {
                case 'getClass':
                $class = ClassCurriculums::where('curriculum_id',$request->curriculum_id)->oldest('created_at')->get();
                return response()->json($class);
                break;
                case 'getStudent':
                if($request->class_id == 'not') {
                    $students = StudentCurriculum::where('curriculum_id',$request->curriculum_id)->oldest('created_at')->paginate(10);
                }
                else {
                    $students = ClassCurriculumStudent::where('class_curriculum_id',$request->class_id)->oldest('created_at')->paginate(10);
                    foreach ($students as &$item) {
                       $item['curriculum'] = $item->studentCurriculum;
                    }
                }

                $lessons = Lesson::where('curriculum_id', $request->curriculum_id)->get();
                foreach ($students as &$item) {
                    $id = ($request->class_id == 'not') ? $item->id : $item->curriculum->id;
                    $status = StudentCurriculumLessonStatus::where([
                        ['student_curriculum_id', $id],
                        ['pass',1]
                    ])->get();
                    $item["pass"] = ceil(count($status) / count($lessons) * 100);
                    $item->info;
                    $item->student;
                }
                return response()->json($students);
                break;
            }
        }
        else return $this->returnNotAjax();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
