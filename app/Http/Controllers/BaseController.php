<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Notifications\InvoicePaid;
use App\User;
use Response;
use Auth;

class BaseController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }
    
	/* send notifications */
	public function sendNotifications($data,$users)
	{
		\Notification::send($users, new InvoicePaid($data));
	}

	/* return notify warning */
	public function returnWarning(){
		return Response::json([
			'type'    => 'warning',
			'title'   => 'Cảnh báo!',
			'content' => 'Gặp sự cố, thử lại sau.',
		]);
	}

	/* return not ajax */
	public function returnNotAjax(){
		return response()->json([
			'type'    => 'error',
			'title'   => 'Lỗi!',
			'content' => 'Không phải ajax request',
		]);
	}
}
