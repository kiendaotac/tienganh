<?php

namespace App\Http\Controllers;

use App\Category;
use App\Curriculum;
use App\File;
use App\Functions;
use App\User;
use Illuminate\Http\Request;
use App\Lesson;
use App\LessonQuicktest;
use App\LessonQuicktestQuestion;
use App\LessonContent;
use Response;
use Auth;
use Validator;
use Carbon\Carbon;
use DB;

class CurriculumController extends BaseController {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		$url = getURL();
		$currentFunction = Functions::where('route', $url)->where('state', 1)->orderBy('id', 'desc')->first();
		$curriculums = Curriculum::where('states', 1)->get();
		$categories = $this->getCategory();
		return view('backend.course.curriculum.main', compact('currentFunction', 'curriculums', 'categories'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		if ($request->ajax()) {
			if($request->states != 0) {
				if($this->processValidator($request) != '') return $this->processValidator($request);
			} 
			$curriculum = Curriculum::create([
				'cate_id'          => $request->cate_id,
				'name'             => ($request->name == null) ? 'Bản nháp' : $request->name,
				'slug'             => str_slug($request->name,'-'),
				'short_desc'       => substr($request->desc, 0, 255),
				'desc'             => $request->desc,
				'img'              => $request->img,
				'featured'         => $request->featured,
				'logintolearn'     => $request->logintolearn,
				'maximum'          => $request->maximum,
				'percent_pass'     => $request->percent_pass,
				'result_assesment' => $request->result_assesment,
				'user_id'          => Auth::user()->id,
				'states'           => $request->states,
				'ordering'         => $request->ordering,
			]);
			if($curriculum) {
				if(!Auth::user()->isOwner()) {
					$noti = [
						'icon' => 'fa fa-book',
						'mess' => '<b>'.Auth::user()->display_name.'</b> đã đăng khóa học '.'<b>'.$curriculum->name.'</b>',
						'link' => './curriculum'
					];
					$users = User::listowner()->get();
					$this->sendNotifications($noti,$users);
				}
				return Response::json([
					'type'    => 'success',
					'title'   => 'Thành công!',
					'content' => 'Thành công.',
					'id'      => $curriculum->id,
				]);
			}
			else {
				return $this->returnWarning();
			}
		} else {
			return $this->returnNotAjax();
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show(Request $request, $id) {
		if ($request->ajax()) {
			if ($id === 'getDatatable') {
				return datatables($this->getDataSorted())->make(true);
			}
		} else {
			return $this->returnNotAjax();
		}
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		if ($request->ajax()) {
			if($request->validator) {
				if($this->processValidator($request) != '') return $this->processValidator($request);
			}
			$curriculum = Curriculum::find($id);
			if($request->states == 1 && $curriculum->states == 2) {
				if(Auth::user()->isOwner()) {
					$noti = [
						'icon' => 'fa fa-bullhorn',
						'mess' => '<b>'.Auth::user()->display_name.'</b> đã duyệt khóa học '.'<b>'.$curriculum->name.'</b>',
						'link' => './curriculum'
					];
					$users = User::listadmin()->where('id',$curriculum->user_id)->get();
					$this->sendNotifications($noti,$users);
				}
			}
			$data = $request->all();
			if ($request->img == '') {
				$data['img'] = $curriculum->img;
			}
			$data['slug'] = str_slug($request->name);
			$curriculum->fill($data);
			/* update states lesson in curriculum */
			if ($curriculum->save()) {
				// DB::table('lessons')->update(['states' => $curriculum->states]);
				return Response::json([
					'type' => 'success',
					'title' => 'Thành công!',
					'content' => 'Thành công.',
				]);
			} else {
				return $this->returnWarning();
			} 
		} 
		else {
			return $this->returnNotAjax();
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Request $request, $id) {
		if ($request->ajax()) {
			if (Curriculum::destroy($id)) {
				$lessons = Lesson::where('curriculum_id',$id)->get();
				foreach ($lessons as $lesson) {
					LessonContent::where('lesson_id',$lesson->id)->delete();
					$lessonQuicktests = LessonQuicktest::where('lesson_id',$lesson->id)->get();
					foreach ($lessonQuicktests as $lessonQuicktest) {
						LessonQuicktestQuestion::where('quicktest_id',$lessonQuicktest->id)->delete();
						$lessonQuicktest->delete();
					}
					$lesson->delete();
				}
				return Response::json([
					'type' => 'success',
					'title' => 'Thành công!',
					'content' => 'Xóa khóa học thành công.',
				]);
			} else {
				return $this->returnWarning();
			}
		} else {
			return $this->returnNotAjax();
		}
	}

	/* get data curriculum */
	public function getDataSorted() {
		$curriculums = Curriculum::belong()->latest('ordering')->latest('created_at')->get();
		foreach ($curriculums as &$curriculum) {
			$curriculum['category'] = $curriculum->categories->name;
			$curriculum->students;
		}
		return $curriculums->toArray();
	}

	/* get data category */
	public function getCategory() {
		$categories = Category::latest('ordering')->get();
		$listdata = [];
		foreach ($categories as $item) {
			if ($item->parent_id == 0) {
				array_push($listdata, $item);
				if (count($item->childs)) {
					foreach ($item->childs->sortBy('ordering') as $i) {
						array_push($listdata, $i);
					}
				}
			}
		}
		return $listdata;
	}

	/* process validator */
	public function processValidator($request) {
		$validator = Validator::make($request->all(), [
			'cate_id'      => 'required|numeric|min:1',
			'name'         => 'required',
			'desc'         => 'required',
			'img'          => 'required',
			'maximum'      => 'required|numeric',
			'percent_pass' => 'required|numeric'
		], 
		[
			'cate_id.required'      => 'Bạn chưa chọn thể loại.',
			'cate_id.numeric'       => 'Thể loại không hợp lệ.',
			'cate_id.min'           => 'Thể loại không hợp lệ.',
			'name.required'         => 'Bạn chưa nhập tên khóa học.',
			'desc.required'         => 'Bạn chưa nhập mô tả khóa học.',
			'img.required'          => 'Bạn chưa chọn hình ảnh minh họa.',
			'maximum.required'      => 'Bạn chưa nhập số lượng tối đa học viên.',
			'maximum.numeric'       => 'Số lượng tối đá không hợp lệ.',
			'percent_pass.required' => 'Bạn chưa nhập điều kiện hoàn thành khóa học.',
			'percent_pass.numeric'  => 'Điều kiện hoàn thành không hợp lệ.',
		]);
		if($validator->fails()) {
			return Response::json([
				'type'    => 'warning',
				'title'   => 'Cảnh báo!',
				'content' => $validator->errors()->all()[0],
			]);
		}
		return '';
	}
}
