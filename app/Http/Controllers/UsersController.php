<?php

namespace App\Http\Controllers;

use App\Functions;
use App\User;
use App\Unis;
use App\UserProfile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Validator;

class UsersController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $checkUni = false;
        if(Auth::user()->isAdmin()) {
            $checkUni = $this->checkUni();
        } 
        $url                =   getURL();
        $currentFunction    =   Functions::where('route',$url)->where('state',1)->orderBy('id','desc')->first();
        $unis = Unis::where('parent_id',0)->active()->oldest('ordering')->oldest('created_at')->get();
        return view('backend.system.users.main', compact('currentFunction','checkUni','unis'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->ajax()) {
            $validator  =   Validator::make($request->all(), [
                'name'          =>  'required|unique:users,name',
                'email'         =>  'required|email|unique:users,email',
                'display_name'  =>  'nullable|string',
                'avatar'        =>  'nullable|string',
                'password'      =>  'required|string|min:6',
                'state'         =>  'nullable|numeric'
            ]);
            if($validator->fails()){
                return response()->json([
                    'type'      =>  'error',
                    'title'     =>  'Lỗi!',
                    'content'   =>  $validator->errors()->all()]);
            } else{
                $this->addItem($request);
                return response()->json([
                    'type'      => 'success',
                    'title'     => 'Thành công!',
                    'content'   => 'Thêm user thành công !!'
                ]);
            }
        } else {
            return response()->json([
                'type'      => 'error',
                'title'     => 'Lỗi!',
                'content'   => 'Không phải ajax request']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        if ($request->ajax()) {
            if ($id === 'getDatatable'){
                return datatables($this->getData())->make(true);
            }
        } else {
            return response()->json([
                'type'      => 'error',
                'title'     => 'Lỗi!',
                'content'   => 'Không phải ajax request']);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Chưa check admin quyền thấp sửa ad min quyền cao hơn
        if ($request->ajax()) {
            $validator  =   Validator::make($request->all(), [
                'name'          =>  'required',Rule::unique('user','name')->ignore($id),
                'email'         =>  'required|email',Rule::unique('user','email')->ignore($id),
                'display_name'  =>  'nullable|string',
                'avatar'        =>  'nullable|string',
                'password'      =>  'required|string|min:6',
                'state'         =>  'nullable|numeric'
            ]);
            if($validator->fails()){
                return response()->json([
                    'type'      =>  'error',
                    'title'     =>  'Lỗi!',
                    'content'   =>  $validator->errors()->all()]);
            } else{
                $currentUser    =   Auth::user();
                $user           =   User::find($id);
                if ($request->state == 0){
                    if ($user->hasRole('Owner') && $currentUser->id != $id){
                        return response()->json([
                            'type'      => 'error',
                            'title'     => 'Lỗi!!',
                            'content'   => 'Xin lỗi bạn quá đen, bạn không thể disable người tạo ra trang web này <3']);
                    }
                }
                $this->editItem($request,$id);
                return response()->json([
                    'type'      => 'success',
                    'title'     => 'Thành công!',
                    'content'   => 'Sửa user thành công !!'
                ]);
            }
        } else {
            return response()->json([
                'type'      => 'error',
                'title'     => 'Lỗi!',
                'content'   => 'Không phải ajax request']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $currentUser    =   Auth::user();
        $deleUser       =   User::find($id);
        if ($id == $currentUser->id){
            return response()->json([
                'type'      => 'error',
                'title'     => 'Lỗi!!',
                'content'   => 'Bạn không thể xóa chính mình']);
        }
        if ($deleUser->hasRole('Owner')){
            return response()->json([
                'type'      => 'error',
                'title'     => 'Lỗi!!',
                'content'   => 'Xin lỗi bạn quá đen, bạn không thể xóa người tạo ra trang web này <3']);
        }
        $deleUser->delete();
        return response()->json([
            'type'      => 'success',
            'title'     => 'Thành công!!',
            'content'   => 'Xóa role thành công']);
    }
    public function addItem($request){
        $user           =   new User();
        $user->name     =   $request->name;
        $user->email    =   $request->email;
        if (!$request->get('display_name')){
            $user->display_name = $request->name;
        } else {
            $user->display_name = $request->display_name;
        }
        $user->avatar = $request->avatar;
        /*if (!$request->get('avatar')){
            $user->avatar = 'img/noavatar.png';
        } else {
            $user->avatar = $request->avatar;
        }*/
        $user->password =   bcrypt($request->password);
        $user->state    =   $request->state;
        $user->save();
        UserProfile::create([
            'user_id' => $user->id,
            'key'     => $request->key,
            'value'   => $request->value,
        ]);
    }

    public function editItem($request, $id){
        $user           =   User::find($id);
        $user->name     =   $request->name;
        $user->email    =   $request->email;
        if (!$request->get('display_name')){
            $user->display_name = $request->name;
        } else {
            $user->display_name = $request->display_name;
        }
        $user->avatar = $request->avatar;
        /*if (!$request->get('avatar')){
            $user->avatar = 'img/noavatar.png';
        } else {
            $user->avatar = $request->avatar;
        }*/
        $user->password =   bcrypt($request->password);
        $user->state    =   $request->state;
        $user->save();
        $userProfile = $user->profile[0];
        $userProfile->fill($request->all());
        $userProfile->save();
    }

    /* get list data user */
    public function getData()
    {
        $users = User::all();
        foreach ($users as $item) {
            $item->profile;
        }
        return $users;
    }

}
