<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Unis;
use App\Functions;
use Response;
use Validator;
use Auth;
class MajorController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $url = getURL();
        $currentFunction = Functions::where('route', $url)->where('state', 1)->orderBy('id', 'desc')->first();
        $unis = Unis::where([['states', 1],['parent_id',0]])->orderBy('ordering','desc')->get();
        $list=[];
        foreach ($unis as $key) {
            foreach ($key->department as $value) {
                array_push($list, $value);
            }
        }
        return view('backend/cate_manager/majo.main', compact('currentFunction','unis','list'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if ($request->ajax()) {
            $valiData = $this->processValidator($request);
            if ($valiData == '') {
                $unis = Unis::create([
                    'name' => $request->name,
                    'uni_key' => $request->uni_key,
                    'desc' => $request->desc,
                    'parent_id' => $request->depart_id,
                    'states' => $request->states,
                    'ordering' => $request->ordering
                ]);
                if ($unis) {
                    return Response::json([
                        'type' => 'success',
                        'title' => 'Thành công!',
                        'content' => 'Thêm ngành thành công.',
                    ]);
                }
            } else {
                return Response::json([
                    'type' => 'error',
                    'title' => 'Lỗi!',
                    'content' => $valiData,
                ]);
            }
        } else {
            return Response::json([
                'type' => 'error',
                'title' => 'Lỗi!',
                'content' => 'Không phải ajax request',
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        if ($request->ajax()) {
            if ($id === 'getDatatable') {
                return datatables($this->getDataSorted())->make(true);
            }
            if($id === 'getMajor') return $this->getDepart($request->id);
            else return $this->getDepart($id);
        } else {
            return response()->json([
                'type' => 'error',
                'title' => 'Lỗi!',
                'content' => 'Không phải ajax request',
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        if ($id ==0) {
            return '';
        }
        $unis = Unis::find($id);
        $department = $unis->department;
        return $department;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        if ($request->ajax()) {
            $valiData = $this->processValidator($request);
            if ($valiData == '') {
                $unis = Unis::find($id);
                $data = $request->all();
                $data['parent_id'] = $request->depart_id;
                $unis->fill($data);
                if ($unis->save()) {
                    return Response::json([
                        'type' => 'success',
                        'title' => 'Thành công!',
                        'content' => 'Sửa ngành thành công.',
                    ]);
                } else {
                    return Response::json([
                        'type' => 'warning',
                        'title' => 'Cảnh báo!',
                        'content' => 'Gặp sự cố, thử lại sau.',
                    ]);
                }
            }
            else {
                return Response::json([
                    'type' => 'error',
                    'title' => 'Lỗi!',
                    'content' => $valiData,
                ]);
            }
        }
        else {
            return Response::json([
                'type' => 'error',
                'title' => 'Lỗi!',
                'content' => 'Không phải ajax request',
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        //
        if ($request->ajax()) {
            if (Unis::destroy($id)) {
                return Response::json([
                    'type' => 'success',
                    'title' => 'Thành công!',
                    'content' => 'Xóa ngành thành công.',
                ]);
            } else {
                return Response::json([
                    'type' => 'warning',
                    'title' => 'Cảnh báo!',
                    'content' => 'Gặp sự cố, thử lại sau.',
                ]);
            }
        } else {
            return Response::json([
                'type' => 'error',
                'title' => 'Lỗi!',
                'content' => 'Không phải ajax request',
            ]);
        }
    }
    /* get data category */
    public function getDataSorted() {
        $department = Unis::where([['states','1'],['parent_id','0']])->get();
        $listDepartment=[];
        foreach ($department as $key) {
            foreach ($key->department as $value) {
                foreach ($value->department as $data) {
                    $data['parents_id'] = $key->id;
                    array_push($listDepartment, $data);
                }
            }
        }
        return $listDepartment;
    }
    /* process validator university */
    public function processValidator($request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'uni_key' => 'required',
        ], [
            'name.required' => 'Tên trường không được để trống.',
            'uni_key.required' => 'Mã trường không được để trống.',
        ]);
        if ($validator->fails()) {
            return $validator->errors()->all();
        }
        return '';
    }
    public function loadunis($id){
        return Unis::select('parent_id')->find($id);
    }

    /* get data department */
    public function getDepart($id)
    {
        return Response::json(Unis::where('parent_id',$id)->active()->oldest('ordering')->oldest('created_at')->get());
    }
}
