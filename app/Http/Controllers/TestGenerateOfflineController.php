<?php

namespace App\Http\Controllers;

use App\TestShift;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Redis;

class TestGenerateOfflineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->ajax()) {
            $shift_id   =   $request->shift_id;
            $shift      =   TestShift::with('Testform')->find($shift_id);
            $process    =   $shift->process;
            $state      =   $shift->state;
            $time_start =   Carbon::parse($shift->time_active);
            $time_end   =   $time_start->addMinutes($shift->testform->time);
            if ($state != 1){
                return response()->json([
                    'type'      => 'error',
                    'title'     => 'Lỗi!',
                    'content'   => 'Ca thi đang ở trạng thái không kích hoạt !!',
                ]);
            }
            switch ($process){
                case '1':
                    return response()->json([
                    'type'      => 'error',
                    'title'     => 'Lỗi!',
                    'content'   => 'Ca thi đang trong quá trình tạo đề thi !!',
                ]);
                case '2':
                    return response()->json([
                        'type'      => 'error',
                        'title'     => 'Lỗi!',
                        'content'   => 'Ca thi đã tạo xong đề thi !!',
                    ]);
            }
//            $shift->process =   1;
//            $shift->save();
            $redis          =   Redis::connection();
            $redis->publish('shift.generate_off', json_encode([
                'student_id'    =>  0,
                'shift'         =>  $shift,
                'details'       =>  $shift->Testform->Details,
                'time_start'    =>  $time_start->toDateTimeString(),
                'time_end'      =>  $time_end->toDateTimeString()
            ]));
            return response()->json([
                'type'      => 'success',
                'title'     => 'Thành công!',
                'content'   => 'Đang tạo đề thi !!',
            ]);
        } else {
            return response()->json([
                'type'      => 'error',
                'title'     => 'Lỗi!',
                'content'   => 'Không phải ajax request']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
