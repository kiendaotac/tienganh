<?php

namespace App\Http\Controllers;

use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Illuminate\Http\Request;
use App\Functions;
use App\User;
use Validator;
use Hash;
use Auth;

class ChangePasswordController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $url = getURL();
        $currentFunction = Functions::where('route', $url)->where('state', 1)->orderBy('id', 'desc')->first();
        return view('backend.profile.change-password', compact('currentFunction'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->ajax()) {
            if($this->validatorPassword($request) != '') return $this->validatorPassword($request);
            if($request->password_new != $request->password_repeat) {
                return response()->json([
                    'type'    => 'error',
                    'title'   => 'Lỗi',
                    'content' => 'Mật khẩu nhập lại không khớp',
                ]);
            }
            $user = User::find($id);
            if(!Hash::check($request->password_old,$user->password)) {
                return response()->json([
                    'type'    => 'error',
                    'title'   => 'Lỗi',
                    'content' => 'Mật khẩu cũ không đúng',
                ]);
            }
            $user->password = Hash::make($request->password_new);
            if($user->save()) {
                Auth::logout();
                return response()->json([
                    'type'    => 'success',
                    'title'   => 'Thành công',
                    'content' => 'Đổi mật khẩu thành công',
                ]);
            }
            else return $this->returnWarning();
        }
        else return $this->returnNotAjax();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /* process validator password */
    public function validatorPassword($request)
    {
        $validator = Validator::make($request->all(), [
            'password_old' => 'required',
            'password_new' => 'required',
            'password_repeat' => 'required',
        ]);
        if($validator->fails()) {
            return response()->json([
                'type'    => 'warning',
                'title'   => 'Cảnh báo',
                'content' => $validator->errors->all()[0],
            ]);
        }
        else return '';
    }
}
