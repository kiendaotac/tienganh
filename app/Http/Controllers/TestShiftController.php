<?php

namespace App\Http\Controllers;

use App\Functions;
use App\TestForm;
use App\TestShift;
use App\TestSubject;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;
use Validator;

class TestShiftController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $url                =   getURL();
        $currentFunction    =   Functions::where('route',$url)->where('state',1)->orderBy('id','desc')->first();
        $testforms          =   TestForm::where('state',1)->get();
        $subjects           =   TestSubject::where('state',1)->get();
        return view('backend.questionbank.testshift.main', compact('currentFunction', 'testforms', 'subjects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->ajax()) {
            $data       =   $request->all();
            $input      =   $request->date.' '.$request->time;
            $format     =   'd/m/Y H:i';
            $datetime   =   Carbon::createFromFormat($format,$input);
            unset($data['date']);
            unset($data['time']);
            $data['time_active']    =   $datetime->format('Y-m-d H:i:s');
            $validator  =   Validator::make($data, [
                'name'          =>  'required|string',
                'test_form_id'  =>  'required|numeric:min:1',
                'test_numbers'  =>  'required|numeric|min:1',
                'tester_type'   =>  'required|numeric',
                'time_active'   =>  'required|date|after:'.Carbon::now('Asia/Ho_Chi_Minh')->addMinutes(30)->format('Y-m-d H:i:s'),
                'time_late'     =>  'required|numeric',
                'state'         =>  'required|numeric',
            ]);
            if($validator->fails()){
                return response()->json([
                    'type'      =>  'error',
                    'title'     =>  'Lỗi!',
                    'content'   =>  $validator->errors()->all()]);
            }
            $this->addTestShift($data);
            return response()->json([
                'type'      => 'success',
                'title'     => 'Thành công',
                'content'   => 'Thêm ca thi thành công']);
        } else {
            return response()->json([
                'type'      => 'error',
                'title'     => 'Lỗi!',
                'content'   => 'Không phải ajax request']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        if ($request->ajax()) {
            if ($id === 'getDatatable'){
                return DataTables::eloquent(TestShift::query())
                    ->addColumn('form', function ($shift){
                        return TestForm::find($shift->test_form_id)->name;
                    })
                    ->make(true);
            }
        } else {
            return response()->json([
                'type'      => 'error',
                'title'     => 'Lỗi!',
                'content'   => 'Không phải ajax request']);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->ajax()) {
            $data       =   $request->all();
            $input      =   $request->date.' '.$request->time;
            $format     =   'd/m/Y H:i';
            $datetime   =   Carbon::createFromFormat($format,$input);
            unset($data['date']);
            unset($data['time']);
            $data['time_active']    =   $datetime->format('Y-m-d H:i:s');
            $validator  =   Validator::make($data, [
                'name'          =>  'required|string',
                'test_form_id'  =>  'required|numeric:min:1',
                'test_numbers'  =>  'required|numeric|min:1',
                'tester_type'   =>  'required|numeric',
                'time_active'   =>  'required|date|after:'.Carbon::now('Asia/Ho_Chi_Minh')->addMinutes(30)->format('Y-m-d H:i:s'),
                'time_late'     =>  'required|numeric',
                'state'         =>  'required|numeric',
            ]);
            if($validator->fails()){
                return response()->json([
                    'type'      =>  'error',
                    'title'     =>  'Lỗi!',
                    'content'   =>  $validator->errors()->all()]);
            }
            $this->editTestShift($data, $id);
            return response()->json([
                'type'      => 'success',
                'title'     => 'Thành công',
                'content'   => 'Sửa ca thi thành công']);
        } else {
            return response()->json([
                'type'      => 'error',
                'title'     => 'Lỗi!',
                'content'   => 'Không phải ajax request']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->ajax()) {
            TestShift::where('id',$id)->delete();
            return response()->json([
                'type'      => 'success',
                'title'     => 'Thành công!',
                'content'   => 'Xóa test ca thi thành công !!',
            ]);
        } else {
            return response()->json([
                'type'      => 'error',
                'title'     => 'Lỗi!',
                'content'   => 'Không phải ajax request']);
        }
    }

    private function addTestShift($data){
        $item               =   new TestShift();
        $item->name         =   $data['name'];
        $item->test_form_id =   $data['test_form_id'];
        $item->test_numbers =   $data['test_numbers'];
        $item->tester_type  =   $data['tester_type'];
        $item->time_active  =   $data['time_active'];
        $item->time_late    =   $data['time_late'];
        $item->online       =   isset($data['online']) ? 1 : 0;
        $item->process      =   0;
        $item->state        =   $data['state'];
        $item->save();
    }

    private function editTestShift($data, $id){
        $item               =   TestShift::find($id);
        $item->name         =   $data['name'];
        $item->test_form_id =   $data['test_form_id'];
        $item->test_numbers =   $data['test_numbers'];
        $item->tester_type  =   $data['tester_type'];
        $item->time_active  =   $data['time_active'];
        $item->time_late    =   $data['time_late'];
        $item->online       =   isset($data['online']) ? 1 : 0;
        $item->state        =   $data['state'];
        $item->save();
    }
}
