<?php

namespace App\Http\Controllers\Auth;

use App\Student;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Validator;

class StudentLoginController extends Controller
{
    public function showStudentLoginForm(){
        return view('auth.student-login');
    }

    public function showStudentRegisterForm(){
        return view('auth.student-register');
    }

    public function loginStudent(Request $request){
        $rules      =   [
            'username'  =>  'required|string',
            'password'  =>  'required|string|min:6'
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()){
            return response()->json([
                'type'      =>  'error',
                'title'     =>  'Lỗi!',
                'content'   =>  $validator->errors()->all()]);
        }
        $isEmail    = Validator::make($request->all(), [
            'username'  =>  'required|email'
        ]);

        if ($isEmail->fails()){
            $credentials=   ['stdcode'=>$request->username, 'password'=>$request->password];
        } else {
            $credentials=   ['email'=>$request->username, 'password'=>$request->password];
        }
        if (Auth::guard('student')->attempt($credentials,$request->remember)){
            return redirect('/');
        }
        return redirect()->route('get.student.login');
    }

    public function registerStudent(Request $request){
        $rules      =   [
            'firstname' =>  'required|string',
            'lastname'  =>  'required|string',
            'email'     =>  'required|email|unique:students,email',
            'password'  =>  'required|string|confirmed|min:6'
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()){
            return response()->json([
                'type'      =>  'error',
                'title'     =>  'Lỗi!',
                'content'   =>  $validator->errors()->all()]);
        }
        $this->createStuden($request);
        return response()->json([
            'type'      =>  'success',
            'title'     =>  'Thành công!',
            'content'   =>  'Tạo tài khoản thành công']);
    }


    public function logoutStudent(){
        Auth::guard('student')->logout();
        return redirect('/');
    }

    private function createStuden($request){
        $student            =   new Student();
        $student->stdcode   =   $request->email;
        $student->email     =   $request->email;
        $student->firstname =   $request->firstname;
        $student->lastname  =   $request->lastname;
        $student->birthday  =   '';
        $student->sex       =   1;
        $student->phone     =   '';
        $student->uni_id    =   1;
        $student->avatar    =   '';
        $student->state     =   1;
        $student->password  =   bcrypt($request->password);
        $student->save();
    }
}
