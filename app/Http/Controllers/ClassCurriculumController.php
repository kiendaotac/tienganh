<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Functions;
use App\User;
use App\Curriculum;
use App\ClassCurriculums;
use App\ClassCurriculumStudent;
use Validator;

class ClassCurriculumController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $url = getURL();
        $currentFunction = Functions::where('route', $url)->where('state', 1)->orderBy('id', 'desc')->first();
        $curriculums = Curriculum::belong()->oldest('created_at')->get();
        return view('backend.student.class_curriculum.main', compact('currentFunction','curriculums'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->ajax()) {
            if($this->validatorClassCurriculum($request) != '') return $this->validatorClassCurriculum($request);
            $classCurriculum = ClassCurriculums::create($request->all());
            if($classCurriculum) {
                return response()->json([
                    'type' => 'success',
                    'title' => 'Thành công',
                    'content' => 'Cập nhật lớp thành công',
                ]);
            }
            else return $this->returnWarning();
        } 
        else {
            return $this->returnNotAjax();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        if ($request->ajax()) {
            if ($id === 'getDatatable') {
                return datatables($this->getDataSorted())->make(true);
            }
            else return response()->json(ClassCurriculums::where([
                ['curriculum_id',$request->curriculum_id],
                ['parent_id',0]
            ])->get());
        } 
        else {
            return $this->returnNotAjax();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->ajax()) {
            if($this->validatorClassCurriculum($request) != '') return $this->validatorClassCurriculum($request);
            $classCurriculum = ClassCurriculums::find($id);
            $classCurriculum->fill($request->all());
            if($classCurriculum->save()) {
                return response()->json([
                    'type' => 'success',
                    'title' => 'Thành công',
                    'content' => 'Cập nhật lớp thành công',
                ]);
            }
            else return $this->returnWarning();
        } 
        else {
            return $this->returnNotAjax();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        if ($request->ajax()) {
            if(ClassCurriculums::find($id)->delete()) {
                ClassCurriculumStudent::where('class_curriculum_id',$id)->delete();
                return response()->json([
                    'type' => 'success',
                    'title' => 'Thành công',
                    'content' => ($request->parent_id != 0) ? 'Xóa lớp thành công' : 'Xóa khóa thành công',
                ]);
            }
            else return $this->returnWarning();
        } 
        else {
            return $this->returnNotAjax();
        }
    }

    /* get data */
    public function getDataSorted() {
        $classCurriculums = ClassCurriculums::belong()->oldest('created_at')->get();
        $listdata = [];
        foreach ($classCurriculums as &$item) {
            $item['curriculum'] = $item->curriculum;
            $item['students'] = $item->students;
            if ($item->parent_id == 0) {
                array_push($listdata, $item);
                if (count($item->childs)) {
                    foreach ($item->childs->sortBy('ordering') as $i) {
                        $i['curriculum'] = $i->curriculum;
                        $i['students'] = $i->students;
                        array_push($listdata, $i);
                    }
                }
            }
        }
        return $listdata;
    }

    /* validator class curriculum */
    public function validatorClassCurriculum($request)
    {
        $validator = Validator::make($request->all(), [
            'name'          => 'required',
            'curriculum_id' => 'required',
        ]);
        if($validator->fails()) {
            return response()->json([
                'type' => 'error',
                'title' => 'Lỗi',
                'content' => $validator->errors()->all()[0],
            ]);
        }
        else return '';
    }
}
