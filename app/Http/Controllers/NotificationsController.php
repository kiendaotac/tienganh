<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Carbon\Carbon;

class NotificationsController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        if($request->ajax()){
            if($id == 'getNotifi'){
                $countNotify = 0;
                $notifications = Auth::user()->notifications;
                foreach ($notifications as &$notification) {
                    if($notification->unread()) $countNotify++;
                    $t1 = Carbon::parse($notification->created_at);
                    $t2 = Carbon::parse(Carbon::now());
                    $arrTime = $t1->diff($t2);
                    $arrDate = [' năm',' tháng',' ngày',' giờ',' phút',' giây'];
                    $i = 0;
                    $stringTime = '';
                    foreach ($arrTime as $key => $value) {
                        if($value > 0) {
                            $stringTime = $value.$arrDate[$i];
                            break;
                        }
                        $i++;
                        if($i >= 6) break; 
                    }
                    if($stringTime != '') $stringTime .= ' trước';
                    $notification->time = $stringTime;
                }
                return response()->json([
                    'notifications' => $notifications,
                    'count'         => $countNotify
                ]); 
            }
            if($id == 'readNotifi'){
                Auth::user()->unreadNotifications->markAsRead();
            }
        }
        else return $this->returnNotAjax();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
