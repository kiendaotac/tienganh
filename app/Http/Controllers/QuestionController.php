<?php

namespace App\Http\Controllers;

use App\TestGroup;
use App\TestGroupQuestion;
use Illuminate\Http\Request;
use Validator;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->ajax()) {
            $validator  =   Validator::make($request->all(), [
                'group_id'          =>  'required|numeric',
                'question'          =>  'required|string',
                'answer_options'    =>  'required',
                'correct_answer'    =>  'required|numeric|min:1',
                'question_type'     =>  'required|numeric',
                'explain'           =>  'nullable|string',
                'hint'              =>  'nullable|string',
                'state'             =>  'required|numeric',
                'answer_shuff'      =>  'required|numeric'
            ]);
            if($validator->fails() && $request->question_type != 3){
                return response()->json([
                    'type'      =>  'error',
                    'title'     =>  'Lỗi!',
                    'content'   =>  $validator->errors()->all()]);
            }
            switch ($request->question_type){
                case 3  :   $this->addMatchingQuestion($request); break;
                default :   $this->addQuestionNormal($request); break;
            }
            return response()->json([
                'type'      => 'success',
                'title'     => 'Thành công!',
                'content'   => 'Thêm câu hỏi thành công !!',
            ]);
        } else {
            return response()->json([
                'type'      => 'error',
                'title'     => 'Lỗi!',
                'content'   => 'Không phải ajax request']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        if ($request->ajax()) {
            if ($id == 'getDatatable'){
                $group_id   =   $request->group_id;
                if ($group_id) {
                    $group = TestGroup::find($group_id);
                    if ($group->question_type == 8) {
                        $questions = TestGroupQuestion::where('group_id', $group_id)->get();// order by id
//                        $html_question  =   $questions[0]->question;
//                        $find           =   array();
//                        $replace        =   array();
//                        foreach ($questions as $question){
//                            array_push($find,'{{$question_'.$question->id.'}}');
//                            $select     =   '<select class="form-control" id="question_'.$question->id.'">'
//                                            .'<option value="0">------</option>';
//                            $json_answer=   json_decode($question->answer_options);
//                            foreach ($json_answer as $item){
//                                $select .=  '<option value='.$item->id.'>'.$item->content.'</option>';
//                            }
//                            $select     .=  '</select>';
//                            array_push($replace,$select);
//                        }
//                        $html_question  =   str_replace($find, $replace, $html_question);
                        return datatables($questions)
                            ->removeColumn('answer_options')
                            ->addColumn('answers', function ($question) {
                                $answers = json_decode($question->answer_options);
                                $ol = '<ol>';
                                foreach ($answers as $answer) {
                                    $ol .= '<li>' . $answer->content . '</li>';
                                }
                                return $ol .= '</ol>';
                            })->addColumn('correct', function ($question) {
                                $answers = json_decode($question->answer_options);
                                $correct = $question->correct_answer;
                                foreach ($answers as $answer) {
                                    if ($correct == $answer->id) {
                                        return $answer->content;
                                    }
                                }
                            })
                            ->editColumn('question', function ($question){
                                $answers    =   json_decode($question->answer_options);
                                $quest      =   '';
                                foreach ($answers as $answer){
                                    $quest  .=  $answer->content.'\\';
                                }
                                return '{'.trim($quest,'\\').'}';
                            })
                            ->make(true);
                    }
                }
                /* Nếu question type khác 8 thì hiển thị như bình thường @@ */
                $questions  =   TestGroupQuestion::where('group_id', $group_id)->get();
                return datatables($questions)
                    ->addColumn('answers',function ($question){
                    $answers    =   json_decode($question->answer_options);
                    $ol         =   '<ol>';
                    foreach ($answers as $answer){
                        $ol .=  '<li>'.$answer->content.'</li>';
                    }
                    return $ol.='</ol>';
                    })
                    ->editColumn('answer_options', function ($question){
                        return base64_encode($question->answer_options);
                    })
                    ->addColumn('correct', function ($question){
                    $answers    =   json_decode($question->answer_options);
                    $correct    =   $question->correct_answer;
                    foreach ($answers as $answer){
                        if ($correct == $answer->id){
                            return $answer->content;
                        }
                    }
                })
                ->make(true);
            }
        } else {
            return response()->json([
                'type'      => 'error',
                'title'     => 'Lỗi!',
                'content'   => 'Không phải ajax request']);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->ajax()) {
            $validator  =   Validator::make($request->all(), [
                'group_id'          =>  'required|numeric',
                'question'          =>  'required|string',
                'answer_options'    =>  'required',
                'correct_answer'    =>  'required|numeric|min:1',
                'question_type'     =>  'required|numeric',
                'explain'           =>  'nullable|string',
                'hint'              =>  'nullable|string',
                'state'             =>  'required|numeric',
                'answer_shuff'      =>  'required|numeric'
            ]);
            if($validator->fails() && $request->question_type != 3){
                return response()->json([
                    'type'      =>  'error',
                    'title'     =>  'Lỗi!',
                    'content'   =>  $validator->errors()->all()]);
            }
            switch ($request->question_type){
                case 3  :   $this->editMatchingQuestion($request); break;
                default :   $this->editQuestionNormal($request); break;
            }
            return response()->json([
                'type'      => 'success',
                'title'     => 'Thành công!',
                'content'   => 'Sửa câu hỏi thành công !!',
            ]);
        } else {
            return response()->json([
                'type'      => 'error',
                'title'     => 'Lỗi!',
                'content'   => 'Không phải ajax request']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function addQuestionNormal($request){
        $question                   =   new TestGroupQuestion();
        $question->group_id         =   $request->group_id;
        $question->question         =   $request->question;
        $question->answer_options   =   json_encode($request->answer_options);
        $question->correct_answer   =   $request->correct_answer;
        $question->answer_shuff     =   $request->answer_shuff;
        $question->explain          =   $request->explain;
        $question->hint             =   $request->hint;
        $question->state            =   $request->state;
        $question->save();
    }

    public function addMatchingQuestion($request){
        $QnA                            =   $request->question;
        foreach ($QnA as $item){
            $question                   =   new TestGroupQuestion();
            $question->group_id         =   $request->group_id;
            $question->question         =   $item['question'];
            $arr_answer_options         =   array([
                'id'        =>  1,
                'type'      =>  $request->answer_type,
                'content'   =>  $item['answer']
            ]);
            $question->answer_options   =   json_encode($arr_answer_options);
            $question->correct_answer   =   1;
            $question->answer_shuff     =   $request->answer_shuff;
            $question->explain          =   $request->explain;
            $question->hint             =   $request->hint;
            $question->state            =   $request->state;
            $question->save();
        }
    }

    public function editQuestionNormal($request){
        $question                   =   new TestGroupQuestion();
        $question->group_id         =   $request->group_id;
        $question->question         =   $request->question;
        $question->answer_options   =   json_encode($request->answer_options);
        $question->correct_answer   =   $request->correct_answer;
        $question->answer_shuff     =   $request->answer_shuff;
        $question->explain          =   $request->explain;
        $question->hint             =   $request->hint;
        $question->state            =   $request->state;
        $question->save();
    }

    public function editMatchingQuestion($request){
//        $QnA                            =   $request->question;
//        foreach ($QnA as $item){
//            $question                   =   new TestGroupQuestion();
//            $question->group_id         =   $request->group_id;
//            $question->question         =   $item['question'];
//            $arr_answer_options         =   array([
//                'id'        =>  1,
//                'type'      =>  $request->answer_type,
//                'content'   =>  $item['answer']
//            ]);
//            $question->answer_options   =   json_encode($arr_answer_options);
//            $question->correct_answer   =   1;
//            $question->answer_shuff     =   $request->answer_shuff;
//            $question->explain          =   $request->explain;
//            $question->hint             =   $request->hint;
//            $question->state            =   $request->state;
//            $question->save();
//        }
    }
}
