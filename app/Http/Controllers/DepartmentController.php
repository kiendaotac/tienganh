<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Unis;
use Response;
use Validator;
use App\Functions;
use Auth;
class DepartmentController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $url = getURL();
        $currentFunction = Functions::where('route', $url)->where('state', 1)->orderBy('id', 'desc')->first();
        $unis = Unis::where('states', 1)->get();
        return view('backend.cate_manager.department.main', compact('currentFunction','unis'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if ($request->ajax()) {
            $valiData = $this->processValidator($request);
            if ($valiData == '') {
                $unis = Unis::create([
                    'name' => $request->name,
                    'uni_key' => $request->uni_key,
                    'desc' => $request->desc,
                    'parent_id' => $request->parent_id,
                    'states' => $request->states,
                    'ordering' => $request->ordering ? $request->ordering :0,
                ]);
                if ($unis) {
                    return Response::json([
                        'type' => 'success',
                        'title' => 'Thành công!',
                        'content' => 'Thêm trường thành công.',
                    ]);
                }
            } else {
                return Response::json([
                    'type' => 'error',
                    'title' => 'Lỗi!',
                    'content' => $valiData,
                ]);
            }
        } else {
            return Response::json([
                'type' => 'error',
                'title' => 'Lỗi!',
                'content' => 'Không phải ajax request',
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        //
        if ($request->ajax()) {
            if ($id === 'getDatatable') {
                return datatables($this->getDataSorted())->make(true);
            }
            else return $this->getDepartment($request->id);
        } else {
            return response()->json([
                'type' => 'error',
                'title' => 'Lỗi!',
                'content' => 'Không phải ajax request',
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        if ($request->ajax()) {
            $valiData = $this->processValidator($request);
            if ($valiData == '') {
                $unis = Unis::find($id);
                $data = $request->all();
                $unis->fill($data);
                if ($unis->save()) {
                    return Response::json([
                        'type' => 'success',
                        'title' => 'Thành công!',
                        'content' => 'Sửa khoa thành công.',
                    ]);
                } else {
                    return Response::json([
                        'type' => 'warning',
                        'title' => 'Cảnh báo!',
                        'content' => 'Gặp sự cố, thử lại sau.',
                    ]);
                }
            }
            else {
                return Response::json([
                    'type' => 'error',
                    'title' => 'Lỗi!',
                    'content' => $valiData,
                ]);
            }
        }
        else {
            return Response::json([
                'type' => 'error',
                'title' => 'Lỗi!',
                'content' => 'Không phải ajax request',
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        //
        if ($request->ajax()) {
            if (Unis::destroy($id)) {
                return Response::json([
                    'type' => 'success',
                    'title' => 'Thành công!',
                    'content' => 'Xóa khoa thành công.',
                ]);
            } else {
                return Response::json([
                    'type' => 'warning',
                    'title' => 'Cảnh báo!',
                    'content' => 'Gặp sự cố, thử lại sau.',
                ]);
            }
        } else {
            return Response::json([
                'type' => 'error',
                'title' => 'Lỗi!',
                'content' => 'Không phải ajax request',
            ]);
        }
    }
    /* get data category */
    public function getDataSorted() {
        $department = Unis::where([['states','1'],['parent_id','0']])->get();
        $listDepartment = [];
        foreach ($department as $key) {
            foreach ($key->department as $value) {
                array_push($listDepartment, $value);
            }
            
        }
        return $listDepartment;
    }
    /* process validator university */
    public function processValidator($request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'uni_key' => 'required',
        ], [
            'name.required' => 'Tên trường không được để trống.',
            'uni_key.required' => 'Mã trường không được để trống.',
        ]);
        if ($validator->fails()) {
            return $validator->errors()->all();
        }
        return '';
    }

    /* get data department */
    public function getDepartment($id)
    {
        return Response::json(Unis::where('parent_id',$id)->active()->oldest('ordering')->oldest('created_at')->get());
    }
}
