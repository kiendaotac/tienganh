<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Functions;
use App\UserProfile;
use App\Unis;
use Auth;

class ProfileController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $url = getURL();
        $currentFunction = Functions::where('route', $url)->where('state', 1)->orderBy('id', 'desc')->first();
        $unis = [];
        $userProfile = UserProfile::where('user_id',Auth::user()->id)->first();
        $userProfile->key = '"'.$userProfile->key.'"';
        $userProfile->value = '"'.$userProfile->value.'"';
        if(Auth::user()->hasRole('Admin')) {
            $id_unis = substr(explode(',',$userProfile->value)[0],1);
            $unis = Unis::where('parent_id',$id_unis)->active()->oldest('ordering')->oldest('created_at')->get();
        }
        return view('backend.profile.main', compact('currentFunction','userProfile','unis'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        if($request->ajax()){
            if($id == 'checkProfile') {
                $check = true;
                $unis = [];
                $url = getURL();
                $currentFunction = Functions::where('route', $url)->where('state', 1)->orderBy('id', 'desc')->first();
                if(Auth::user()->hasRole('Admin')) {
                    $userProfile = UserProfile::where('user_id',Auth::user()->id)->first();
                    $idUnis = substr(explode(',',$userProfile->value)[0],1);
                    $unis = \App\Unis::where('parent_id',$idUnis)->active()->oldest('ordering')->oldest('created_at')->get(); 
                    if(count(explode(',',Auth::user()->profile->first()->key)) <= 2) $check = false;
                }
                return response()->json([
                    'check' => $check,
                    'unis'  => $unis
                ]);
            }
        }
        else return $this->returnNotAjax();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->ajax()) {
            $userProfile = UserProfile::where('user_id',Auth::user()->id)->first(); 
            $data        = $request->all();
            $key         = $userProfile->key;
            $value       = $userProfile->value;
            if(count(explode(',',$userProfile->key)) > 2) {
                $key = explode(',',$key);
                $key = $key[0].','.$key[1];
                $value = explode(',',$value);
                $value = $value[0].','.$value[1];
            }
            $data["key"] = $key . $data["key"];
            $data["value"] = $value . $data["value"];
            $userProfile->fill($data);
            $userProfile->save();
            if($userProfile) {
                return response()->json([
                    'type'    => 'success',
                    'title'   => 'Thành công',
                    'content' => 'Cập nhật thông tin cá nhân thành công'
                ]);
            }
        }   
        else return $this->returnNotAjax();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
