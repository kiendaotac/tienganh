<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lesson;
use App\Functions;
use App\Curriculum;
use App\File;
use App\LessonQuicktest;
use App\LessonQuicktestQuestion;
use App\LessonContent;
use App\Document;
use Response;
use Validator;
use Auth;
use DB;

class LessonController extends BaseController {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		$url = getURL();
		$currentFunction = Functions::where('route', $url)->where('state', 1)->orderBy('id', 'desc')->first();
		$curriculums = Curriculum::where('user_id', Auth::user()->id)->orderBy('ordering', 'asc')->get();
		$lessons = Lesson::where([
			['user_id', Auth::user()->id],
			['parent_id', 0]
		])->orderBy('ordering', 'asc')->get();
		return view('backend.course.lesson.main', compact('currentFunction', 'curriculums', 'lessons'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		if ($request->ajax()) {
			if($request->check == null) {
				if ($this->processValidator($request) != '') return $this->processValidator($request);
				$curriculum = Curriculum::find($request->curriculum_id);
				$lesson = $this->addLesson($request);
				if($request->document != null) $this->addDocument($request,$lesson->id);
				switch ($request->typeLesson) {
					// thêm bài học là chương
					case 'title':
					if($lesson) {
						if($request->type == 'test') {
							$data = [
								'lesson_id'          => $lesson->id,
								'percent_pass'       => 0,
								'show_hint'          => 0,
								'review_question'    => 0,
								'showcorrect_answer' => 0,
								'showcheck_answer'   => 0,
								'states'             => $lesson->states
							];
							$this->addQuicktest($data,$lesson,null,'store');
						}
						return Response::json([
							'type'    => 'success',
							'title'   => 'Thành công!',
							'content' => ($request->type == 'title' || $request->type == 'lesson') ? 'Thêm bài học thành công.' : 'Thêm bài kiểm tra thành công.',
							'lesson'  => $lesson
						]);
					}
					else return $this->returnWarning();
					break;
					// thêm bài học với các kiểu: text/audio/video
					case 'lesson':
					if($this->addContent($request->all(),$lesson,null,'store')) {
						return Response::json([
							'type'    => 'success',
							'title'   => 'Thành công!',
							'content' => 'Thêm bài học thành công.',
						]);
					}
					else return $this->returnWarning();
					break;
					// thêm bài học là quicktest
					case 'test':
					if($this->addQuicktest($request->all(),$lesson,null,'store')) {
						return Response::json([
							'type'    => 'success',
							'title'   => 'Thành công!',
							'content' => 'Thêm bài kiểm tra thành công.',
						]);
					}
					else return $this->returnWarning();
					break;
				} 
			}
			else {
				if($this->validatorQuestion($request) != '') return $this->validatorQuestion($request);
				else {
					switch ($request->check) {
						// tự động lưu lại câu hỏi khi người dùng thêm câu kèm theo thêm lesson và quicktest
						case 'lesson':
						$lesson = $this->addLesson($request);
						$lessonQuicktest = $this->addQuicktest($request->all(),$lesson,null,'store');
						$data = $request->all();
						$data['quicktest_id'] = $lessonQuicktest->id;
						$lessonQuicktestQuestion = LessonQuicktestQuestion::create($data);
						if($lessonQuicktestQuestion){
							return Response::json([
								'type'                    => 'success',
								'title'                   => 'Thành công!',
								'content'                 => 'Thêm câu hỏi thành công.',
								'lesson'                  => $lesson,
								'lessonQuicktest'         => $lessonQuicktest,
								'lessonQuicktestQuestion' => $lessonQuicktestQuestion
							]);
						}
						else return $this->returnWarning();
						break;
						// tự động thêm câu hỏi với lesson và quick test đã tồn tại rồi
						case 'question': 
						$lessonQuicktestQuestion = LessonQuicktestQuestion::create($request->all());
						if($lessonQuicktestQuestion){
							return Response::json([
								'type'                    => 'success',
								'title'                   => 'Thành công!',
								'content'                 => 'Thêm câu hỏi thành công.',
								'lessonQuicktestQuestion' => $lessonQuicktestQuestion
							]);
						}
						else return $this->returnWarning();
						default:
						return $this->returnWarning();
						break;
					}
				}
			}
		} 
		else {
			$this->returnNotAjax();
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show(Request $request, $id) {
		if ($request->ajax()) {
			if($id != 'getDatatable'){
				switch ($id) {
					case 'getQuestion':
					return Response::json($this->getQuestion($request->quicktest_id));
					break;
					case 'getLesson':
					return Response::json($this->getLesson($request->curriculum_id));
					break;
					default:
					return Response::json($this->getDataSorted($id));
					break;
				}
			}
			else return datatables($this->getDataSorted(0))->make(true);
		} 
		else {
			$this->returnNotAjax();
		}
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		if ($request->ajax()) {
			$curriculum = Curriculum::find($request->curriculum_id);
			$request->states = $curriculum->states;
			$lesson = Lesson::find($id);
			$lesson->fill($request->all());
			$lesson->save();
			switch ($request->typeLesson) {
				case 'updateDesc':
				//$lesson->desc = $request->desc;
				if($lesson->save()) {
					return Response::json([
						'type'    => 'success',
						'title'   => 'Thành công!',
						'content' => 'Cập nhật mô tả thành công.'
					]);
				}
				else {
					return $this->returnWarning();
				}
				break;
				case 'test':
				if($request->document != null) {
					Document::where([
						['object_key','lesson'],
						['object_id',$id]
					])->delete();
					$this->addDocument($request,$id);
				}
				$lessonQuicktest = LessonQuicktest::where('lesson_id',$lesson->id)->first();
				if($lessonQuicktest) {
					$lessonQuicktest = $this->addQuicktest($request->all(),$lesson,$lessonQuicktest,'update');
				}
				else $lessonQuicktest = $this->addQuicktest($request->all(),$lesson,null,'store');
				if($lessonQuicktest){
					return Response::json([
						'type'    => 'success',
						'title'   => 'Thành công!',
						'content' => 'Cật nhật bài kiểm tra thành công'
					]);
				}
				else {
					return $this->returnWarning();
				}
				break;
				case 'lesson':
				Document::where([
					['object_key','lesson'],
					['object_id',$id]
				])->delete();
				$this->addDocument($request,$id);
				$lessonContent = LessonContent::where('lesson_id',$lesson->id)->first();
				if($lessonContent) {
					$lessonContent = $this->addContent($request->all(),$lesson,$lessonContent,'update');
				}
				else $lessonContent = $this->addContent($request->all(),$lesson,null,'store');
				if($lessonContent){
					return Response::json([
						'type'    => 'success',
						'title'   => 'Thành công!',
						'content' => 'Cập nhật bài học thành công'
					]);
				}
				else {
					return $this->returnWarning();
				}
				break;
			}
		}
		else {
			$this->returnNotAjax();
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Request $request, $id) {
		if ($request->ajax()) {
			switch ($request->type) {
				case 'question':
				if(LessonQuicktestQuestion::destroy($id)) {
					return Response::json([
						'type'    => 'success',
						'title'   => 'Thành công!',
						'content' => 'Xóa thành công.'
					]);
				}
				else return $this->returnWarning();
				break;
				case 'lesson':
				if(Lesson::destroy($id)) {
					Document::where([
						['object_key','lesson'],
						['object_id',$id]
					])->delete();
					LessonContent::where('lesson_id',$id)->delete();
					$lessonQuicktests = LessonQuicktest::where('lesson_id',$id)->get();
					foreach ($lessonQuicktests as $lessonQuicktest) {
						LessonQuicktestQuestion::where('quicktest_id',$lessonQuicktest->id)->delete();
						$lessonQuicktest->delete();
					}
					return Response::json([
						'type'    => 'success',
						'title'   => 'Thành công!',
						'content' => 'Xóa thành công.'
					]);
				}
				else {
					return $this->returnWarning();
				}
				break;
			}
		}
		else {
			$this->returnNotAjax();
		}
	}

	/* get data lesson */
	public function getDataSorted($id) {
		$lessons = ($id == 0) ? Lesson::belong()->latest('ordering')->latest()->get() : Lesson::belong()->where('curriculum_id',$id)->latest('ordering')->latest()->get();
		$listdata = [];
		foreach ($lessons as &$item) {
			$item['curriculum'] = ($item->curriculums != null) ? $item->curriculums->name : 'Không xác định';
			$item['documents'] = $item->documents;
			if ($item->parent_id == 0) {
				foreach ($item->quicktest as &$value) {
					$value->media = json_decode($value->media);
				}
				foreach ($item->content as &$value) {
					$value->desc = json_decode($value->desc);
				}
				array_push($listdata, $item);
				if (count($item->childs)) {
					foreach ($item->childs->sortBy('ordering') as &$i) {
						// $i['curriculum'] = ($i->curriculums != null) ? $i->curriculums->name : 'Không xác định';
						$i['curriculum'] = '';
						foreach ($i->quicktest as &$value) {
							$value->media = json_decode($value->media);
						}
						foreach ($i->content as &$value) {
							$value->desc = json_decode($value->desc);
						}
						$i['documents'] = $i->documents;
						array_push($listdata, $i);
					}
				}
			}
		}
		return $listdata;
	}

	/* process validator */
	public function processValidator($request) {
		$validator = Validator::make($request->all(), [
			'name'          => 'required',
			'curriculum_id' => 'required|numeric',
			'type'          => 'required'
		], 
		[
			'curriculum_id.required' => 'Bạn chưa chọn khóa học.',
			'curriculum_id.numeric'  => 'Bạn chưa chọn khóa học.',
			'name.required'          => 'Bạn chưa nhập tên bài học.',
			'type.required'          => 'Bạn chưa nhập loại bài học.',
		]);
		if($validator->fails()) {
			return Response::json([
				'type'    => 'warning',
				'title'   => 'Cảnh báo!',
				'content' => $validator->errors()->all()[0],
			]);
		}
		return '';
	}
	public function validatorQuestion($request) {
		$validator = Validator::make($request->all(), [
			'question'          => 'required',
		], 
		[
			'question.required' => 'Bạn chưa nhập nội dung câu hỏi.',
		]);
		if($validator->fails()) {
			return Response::json([
				'type'    => 'warning',
				'title'   => 'Cảnh báo!',
				'content' => $validator->errors()->all()[0],
			]);
		}
		return '';
	}
	public function validatorQuicktest($request) {
		$validator = Validator::make($request, [
			'percent_pass'       => 'required|numeric',
			'show_hint'          => 'required|numeric',
			'review_question'    => 'required|numeric',
			'showcorrect_answer' => 'required|numeric',
			'showcheck_answer'   => 'required|numeric'
		], 
		[
			'percent_pass.required|show_hint.required|review_question.required|showcorrect_answer.required|showcheck_answer.required' => 'Giá trị nhận vào bị thiếu.',
			'percent_pass.numeric|show_hint.numeric|review_question.numeric|showcorrect_answer.numeric|showcheck_answer.numeric' => ' Giá trị nhận vào không đúng định dạng.'
		]);
		if($validator->fails()) {
			return Response::json([
				'type'    => 'warning',
				'title'   => 'Cảnh báo!',
				'content' => $validator->errors()->all()[0],
			]);
		}
		return '';
	}
	public function validatorContent($request){
		$validator = Validator::make($request, [
			'desc'       => 'required',
		], 
		[
			'desc.required' => 'Vui lòng nhập nội dung bài học.',
		]);
		if($validator->fails()) {
			return Response::json([
				'type'    => 'warning',
				'title'   => 'Cảnh báo!',
				'content' => $validator->errors()->all()[0],
			]);
		}
		return '';
	}
	/* add lesson */
	function addLesson($request){
		return $lesson = Lesson::create([
			'name'          => ($request->name == '') ? 'Bản nháp' : $request->name,
			'curriculum_id' => ($request->curriculum_id == null) ? 0 : $request->curriculum_id,
			'parent_id'     => $request->parent_id,
			'desc'          => $request->desc,
			'slug'          => str_slug($request->name,'-'),
			'type'          => $request->type,
			'review'        => $request->review,
			'user_id'       => Auth::user()->id,
			'states'        => $request->states,
			'ordering'      => ($request->ordering == null) ? 100 : $request->ordering
		]);
	}
	/* add document */
	public function addDocument($request,$id)
	{
		foreach (json_decode($request->document) as $value) {
			$nameDoc = substr($value, strrpos($value,'/')+1);
			$data = [
				'object_key' => 'lesson',
				'object_id' => $id,
				'title' => $nameDoc,
				'link' => $value
			];
			Document::create($data);
		}
	}
	/* get data question */
	function getQuestion($id){
		return LessonQuicktestQuestion::where('quicktest_id', $id)->oldest()->get();	
	}
	/* get data lesson */
	function getLesson($id){
		return Lesson::where([
			['curriculum_id', $id],
			['parent_id', 0]
		])->oldest()->get();	
	}
	/* add quicktest */
	function addQuicktest($request,$lesson,$lessonQuicktest,$type){
		if($this->validatorQuicktest($request) != '') return $this->validatorQuicktest($request);
		$data = $request;
		// biến đổi arr request để thêm lesson quicktest
		$data['lesson_id'] = $lesson->id;
		if($type == 'store') $lessonQuicktest = LessonQuicktest::create($data);
		else {
			$lessonQuicktest->fill($data);
			$lessonQuicktest->save();
			$lessonQuicktest->save();
		}
		return $lessonQuicktest;
	}

	/* add lesson content */
	public function addContent($request,$lesson,$lessonContent,$type)
	{
		if ($this->validatorContent($request) != '') return $this->validatorContent($request);
		$data = $request;
		// biến đổi arr request để thêm lesson content
		$data['lesson_id'] = $lesson->id;
		$data['type'] = $request['type_content'];
		if($request['type_content'] != 'text') $data['desc'] = $request['media'];
		if($type == 'store') {
			$lessonContent = LessonContent::create($data);
		}
		else {	
			$lessonContent->fill($data);
			$lessonContent->save();
		}
		return $lessonContent;
	}
}
