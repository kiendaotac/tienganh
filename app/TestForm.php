<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TestForm extends Model
{
    protected $table = 'test_forms';
    protected $fillable = [
        'subject_id', 'name', 'time', 'show_result', 'show_explain', 'allow_review', 'show_hint', 'user_id', 'state'
    ];

    public function Details(){
        return $this->hasMany('App\TestFormDetail','form_id','id');
    }
}
