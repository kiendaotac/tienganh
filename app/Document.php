<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
	protected $table = "documents";
	protected $fillable = ['object_key', 'object_id', 'title', 'link', 'icon'];
}
