<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TestSubject extends Model
{
    protected $table = 'test_subjects';
    protected $fillable = ['name', 'desc', 'parent_id', 'state'];

    public function parent(){
        return $this->belongsTo('App\TestSubject','parent_id','id');
    }

    public function childs(){
        return $this->hasMany('App\TestSubject','parent_id','id');
    }

    public function TestGroup(){
        return $this->hasMany('App\TestGroup', 'test_subject_id', 'id');
    }

    public static function sortSubject(){
        $subjects   =   TestSubject::where('parent_id',0)->get();
        $sort       =   array();
        foreach ($subjects as $subject){
            array_push($sort,$subject);
            foreach ($subject->childs as $child){
                array_push($sort,$child);
            }
        }
        return $sort;
    }
}
