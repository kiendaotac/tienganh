<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LessonQuicktest extends Model
{	
	protected $table = "lesson_quicktests";
	protected $fillable=[ 'lesson_id', 'media_type', 'media', 'percent_pass', 'show_hint', 'review_question', 'showcorrect_answer', 'showcheck_answer', 'states', 'ordering' ];

	/* get lesson */
	public function lesson()
	{
		return $this->belongsTo('App\Lesson','lesson_id','id');
	}
}
