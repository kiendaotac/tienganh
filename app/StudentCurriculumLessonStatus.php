<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StudentCurriculumLessonStatus extends Model
{
	protected $table = "student_curriculum_lesson_status";
	protected $fillable = [
        'id','student_curriculum_id', 'lesson_id', 'pass', 'correct', 'total','note','options'
    ];
}
