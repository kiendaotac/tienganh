<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class ClassCurriculums extends Model
{
    protected $table = "class_curriculums";
	protected $fillable=[ 'id', 'curriculum_id', 'name', 'parent_id','desc','user_id'];

	public function curriculum()
	{
		return $this->belongsTo('App\Curriculum','curriculum_id','id');
	}

	public function childs() {
        return $this->hasMany('App\ClassCurriculums','parent_id','id') ;
    }

    public function students() {
        return $this->hasMany('App\ClassCurriculumStudent','class_curriculum_id','id') ;
    }

    /* get data role */
	public function scopeBelong($query)
	{
		if(Auth::user()->roles->first()->name === 'Admin') return $query->where('user_id', Auth::user()->id);
		return $query;
	}
}
