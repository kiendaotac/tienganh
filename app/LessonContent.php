<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LessonContent extends Model
{
	protected $table = "lesson_contents";
	protected $fillable=[ 'lesson_id', 'name', 'type', 'desc', 'states'];
}
