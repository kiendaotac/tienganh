<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use StudentCurriculum;

class Student extends Authenticatable
{
    use Notifiable;

    protected $guarded = 'student';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'stdcode', 'email', 'firstname', 'lastname', 'password', 'birthday', 'sex', 'phone', 'uni_id', 'avatar', 'state'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    public function unis(){
        return $this->belongsTo('App\Unis','uni_id');
    }

    /* university */
    public function university()
    {
        return $this->belongsTo('App\Unis','uni_id','id');
    }

    /* student info */
    public function info()
    {
        return $this->hasOne('App\StudentInfo','stdcode','stdcode');
    }

}
