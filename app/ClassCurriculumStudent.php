<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClassCurriculumStudent extends Model
{
    protected $table = "class_curriculum_students";
	protected $fillable=[ 'id', 'class_curriculum_id','stdcode'];

	/* get student */
	public function student()
	{
		return $this->hasOne('App\Student','stdcode','stdcode');
	}

	/* get curriculum student */
	public function studentCurriculum()
	{
		return $this->hasOne('App\StudentCurriculum','stdcode','stdcode');
	}

	/* student info */
    public function info()
    {
        return $this->hasOne('App\StudentInfo','stdcode','stdcode');
    }
}
