<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Unis extends Model
{
	protected $table = "unis";
	protected $fillable=[ 'name', 'uni_key', 'desc', 'parent_id', 'states', 'ordering' ];
	/* where unis is active */
	public function scopeActive($query)
	{
		return $query->where('states',1);
	}

	public function department(){
		return $this->hasMany('App\Unis','parent_id','id');
	}
}
