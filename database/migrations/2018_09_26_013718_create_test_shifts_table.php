<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTestShiftsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('test_shifts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('test_form_id');
            $table->integer('test_numbers');
            $table->tinyInteger('tester_type');
            $table->dateTime('time_active');
            $table->integer('time_late');
            $table->tinyInteger('online');
            $table->tinyInteger('process');
            $table->tinyInteger('state');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('test_shifts');
    }
}
