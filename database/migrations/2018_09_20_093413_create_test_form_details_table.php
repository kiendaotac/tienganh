<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTestFormDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('test_form_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('form_id');
            $table->integer('subject_id');
            $table->tinyInteger('question_type');
            $table->string('skill');
            $table->tinyInteger('level');
            $table->tinyInteger('number_group');
            $table->tinyInteger('total_point');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('test_form_details');
    }
}
