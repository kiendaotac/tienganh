<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFunctionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('functions', function (Blueprint $table) {
            $table->collation   =   'utf8_general_ci';
            $table->charset     =   'utf8';
            $table->increments('id');
            $table->integer('parent_id')->default(0);
            $table->string('name');
            $table->string('description')->nullable();
            $table->string('route')->nullable()->unique();
            $table->string('controller')->nullable()->unique();
            $table->integer('ordering')->default(100);
            $table->string('icon')->default('glyphicon glyphicon-menu-right');
            $table->tinyInteger('state')->default(1);
            $table->tinyInteger('access')->default(3);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('functions');
    }
}
