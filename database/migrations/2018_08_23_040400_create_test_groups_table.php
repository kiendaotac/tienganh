<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTestGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('test_groups', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('test_subject_id');
            $table->string('name');
            $table->text('content');
            $table->tinyInteger('media_type');
            $table->text('media_source');
            $table->tinyInteger('question_type');
            $table->tinyInteger('question_shuff');
            $table->tinyInteger('level');
            $table->tinyInteger('state');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('test_groups');
    }
}
