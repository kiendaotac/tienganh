<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTestGenerateQuestionOfflinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('test_generate_question_offlines', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('test_generate_group_id');
            $table->integer('question_id');
            $table->text('question');
            $table->text('answer_options');
            $table->text('answer');
            $table->tinyInteger('correct_answer');
            $table->tinyInteger('answer_shuff');
            $table->text('explain');
            $table->text('hint');
            $table->tinyInteger('result');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('test_generate_question_offlines');
    }
}
