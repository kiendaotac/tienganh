<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentCurriculumsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_curriculums', function (Blueprint $table) {
            $table->increments('id');
            $table->string('stdcode',45);
            $table->integer('curriculum_id');
            $table->string('curriculums_name');
            $table->integer('payment')->default(0)->comment('số tiền đã thanh toán');
            $table->date('active_date');
            $table->date('expire_date')->nullable();
            $table->tinyInteger('states')->default(1)->comment('0: inactive 1: active_unlimit 2: active_limit_time (expire_date &gt;0) -1: expired');
            $table->integer('ordering')->default(100)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_curriculums');
    }
}
