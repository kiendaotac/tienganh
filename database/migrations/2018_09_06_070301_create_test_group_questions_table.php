<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTestGroupQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('test_group_questions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('group_id');
            $table->text('question');
            $table->text('answer_options');
            $table->tinyInteger('correct_answer');
            $table->tinyInteger('answer_shuff');
            $table->text('explain');
            $table->text('hint');
            $table->tinyInteger('state');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('test_group_questions');
    }
}
