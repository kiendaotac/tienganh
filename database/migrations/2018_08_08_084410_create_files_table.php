<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('files', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title',200);
            $table->string('file_name',200);
            $table->string('extension',50);
            $table->text('content');
            $table->bigInteger('size');
            $table->integer('year');
            $table->string('month',2);
            $table->integer('creator_id');
            $table->integer('owner_id');
            $table->tinyInteger('states');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('files');
    }
}
