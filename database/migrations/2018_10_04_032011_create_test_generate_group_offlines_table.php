<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTestGenerateGroupOfflinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('test_generate_group_offlines', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('generate_id');
            $table->integer('group_id');
            $table->string('name');
            $table->text('content');
            $table->tinyInteger('media_type');
            $table->text('media_source');
            $table->tinyInteger('question_type');
            $table->tinyInteger('question_shuff');
            $table->tinyInteger('show_explain');
            $table->tinyInteger('show_hint');
            $table->integer('total_point');
            $table->integer('result');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('test_generate_group_offlines');
    }
}
