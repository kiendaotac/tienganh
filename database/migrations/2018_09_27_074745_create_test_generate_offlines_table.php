<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTestGenerateOfflinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('test_generate_offlines', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('shift_id');
            $table->integer('student_id')->nullable();;
            $table->dateTime('time_start');
            $table->dateTime('time_end');
            $table->tinyInteger('state');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('test_generate_offlines');
    }
}
