<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentCurriculumLessonStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_curriculum_lesson_status', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('student_curriculum_id');
            $table->integer('lesson_id');
            $table->tinyInteger('pass')->default(0)->comment('0: chưa hoàn thành 1: đã hoàn thành Với bài học, học viên cần đánh dấu là đã hoàn thành thì trường này sẽ đổi giá tri Với bàn test thì giá trị sẽ thay đổi khi tỷ lệ câu hỏi đúng/tổng câu hỏi &gt;= giá trị quy định ở bảng quicktest');
            $table->integer('correct')->default(0)->comment('số câu trả lời đúng');
            $table->integer('total')->default(0)->comment('tổng số câu');
            $table->text('note')->comment('lưu những ghi chú của học viên trong quá trình học');
            $table->integer('ordering')->default(100)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_curriculum_lesson_status');
    }
}
