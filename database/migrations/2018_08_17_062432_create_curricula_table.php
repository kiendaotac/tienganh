<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCurriculaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('curriculums', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cate_id');
            $table->string('name')->nullable();
            $table->string('slug')->nullable();
            $table->text('short_desc')->nullable();
            $table->text('desc')->nullable();
            $table->string('img')->nullable();
            $table->integer('price')->nullable();
            $table->integer('discount')->nullable();
            $table->tinyInteger('featured')->nullable()->comment('nổi bật');
            $table->tinyInteger('logintolearn')->nullable()->comment('Bắt buộc đăng nhập để học không? 1-có;0-không');
            $table->integer('maximum')->nullable()->comment('giới hạn số lượng học viên - 0: không giới hạn');
            $table->integer('enrolled')->nullable()->comment('số lượng học viên đã học');
            $table->tinyInteger('result_assesment')->nullable()->comment('cách đánh giá hoàn thành khóa học - 1: Thông qua hoàn thành số lượng bài học trên tổng số bài: vd: 5/10 => hoàn thành 50% - 2: Đánh giá qua kết quả bài test: xem có bao nhiêu bài test hoàn thành trên tổng số bài test có.');
            $table->integer('percent_pass')->nullable()->comment('Điều kiện hoàn thành khóa học: nếu: Result_assesment = 1 thì là % số bài học hoặc bài kiểm tra hoàn thành');
            $table->integer('user_id');
            $table->tinyInteger('states')->default(1)->comment('1: kích hoạt 0: không kích hoạt 2: Đang duyệt 3: xóa');
            $table->integer('ordering')->default(100)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('curriculums');
    }
}
