<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLessonQuicktestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lesson_quicktests', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('lesson_id');
            $table->text('desc')->nullable();
            $table->string('media_type',45)->nullable()->comment('video/audio');
            $table->text('media')->nullable()->comment('Media trường hợp media_type: video/audio Chứa đường dẫn file audio hoặc video thì media lưu chuỗi json dạng: { type: &quot;video/audio&quot;, src: &quot;đường dẫn trỏ đến video/audio (nếu là youtube thì chỉ cần ID&quot;, provider: &quot;youtube / vimeo &quot; }; audio thì provider=“NULL” Tham khảo cấu trúc trên: https://github.com/sampotts/plyr ');
            $table->integer('percent_pass')->comment('Bao nhiều % đúng thì hoàn thành? ');
            $table->tinyInteger('show_hint')->comment('Có hiển thị gợi ý không? 1: có; 0:không ');
            $table->tinyInteger('review_question')->comment('Có cho phép xem lại các câu trả lời khi hoàn thành bài thi không? ');
            $table->tinyInteger('showcorrect_answer')->comment('hiển thị câu trả lời đúng khi xem lại ');
            $table->tinyInteger('showcheck_answer')->comment('hiển thị nút yêu cầu máy trả lời đúng ');
            $table->tinyInteger('states')->default(1);
            $table->integer('ordering')->default(100)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lesson_quicktests');
    }
}
