<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTestFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('test_forms', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('subject_id');
            $table->string('name');
            $table->integer('time');
            $table->tinyInteger('show_result');
            $table->tinyInteger('show_explain');
            $table->tinyInteger('allow_review');
            $table->tinyInteger('show_hint');
            $table->integer('user_id');
            $table->tinyInteger('state');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('test_forms');
    }
}
