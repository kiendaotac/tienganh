<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLessonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lessons', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('curriculum_id');
            $table->integer('parent_id');
            $table->integer('user_id');
            $table->string('name');
            $table->string('slug');
            $table->text('desc')->nullable();
            $table->string('type',45)->comment('title lesson test 3 cái này tương ứng với các icon của 3 loại nếu là lesson thì sau hiển thị nội dung lesson ở content nếu là test thì hiển thị nội dung ở quicktest ');
            $table->tinyInteger('review')->comment('học viên có thể học thwur bài này nếu review =1 0: không được học thử 1: được học thử (không cần đăng nhập hoặc thanh toán) ');
            $table->tinyInteger('states')->default(1);
            $table->integer('ordering')->default(100)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lessons');
    }
}
