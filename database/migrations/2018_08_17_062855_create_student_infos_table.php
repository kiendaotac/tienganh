<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_info', function (Blueprint $table) {
            $table->increments('id');
            $table->string('stdcode',45)->comment('Liên kết 1-1 với bảng students');
            $table->date('start_date')->comment('năm vào trường');
            $table->string('uni',100)->comment('tên trường');
            $table->string('dept',100)->comment('tên khoa');
            $table->string('majo',100)->comment('tên ngành');
            $table->string('course',45)->comment('Tên khóa học ví dụ: Khóa 43, hoặc K43');
            $table->string('class',100)->comment('Tên lớp quản lý: ví dụ “Toán tin - K43”');
            $table->text('facebook')->nullable();
            $table->text('avatar')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_info');
    }
}
