<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLessonContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lesson_contents', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('lesson_id');
            $table->string('name')->comment('tự động lấy name từ lesson và không cho sửa trường này ');
            $table->string('type',45)->comment('text/video/audio');
            $table->text('desc')->nullable()->comment('nội dung của bài học trường hợp type: text ==&gt; nội dung là dạng bài viết text có hình ảnh,… Type:video/audio Chứa đường dẫn file audio hoặc video lưu chuỗi json dạng: { type: &quot;video/audio&quot;, src: &quot;đường dẫn trỏ đến video/audio (nếu là youtube thì chỉ cần ID&quot;, provider: &quot;youtube / vimeo &quot; }; audio thì provider=“NULL” Tham khảo cấu trúc trên: https://github.com/sampotts/plyr ');
            $table->tinyInteger('states')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lesson_contents');
    }
}
