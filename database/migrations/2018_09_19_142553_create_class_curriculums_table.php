<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassCurriculumsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('class_curriculums', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('curriculum_id');
            $table->string('name');
            $table->text('desc')->nullable();
            $table->integer('parent_id');
            $table->integer('user_id');
            $table->tinyInteger('states')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('class_curriculums');
    }
}
