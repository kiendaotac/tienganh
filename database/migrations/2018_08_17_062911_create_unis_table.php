<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('unis', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->comment('Tên trường, khoa, ngành');
            $table->string('uni_key',45)->comment('Mã trường, khoa, ngành');
            $table->text('desc')->nullable();
            $table->integer('parent_id')->default(0);
            $table->tinyInteger('states')->default(1);
            $table->integer('ordering')->default(100)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('unis');
    }
}
