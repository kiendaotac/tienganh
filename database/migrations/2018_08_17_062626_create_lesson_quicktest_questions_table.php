<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLessonQuicktestQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lesson_quicktest_questions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('quicktest_id')->nullable();
            $table->text('question');
            $table->text('options')->comment('Lưu thị các phương án trả lời dưới dạng json, mỗi thành phần là 1 phương án,  ');
            $table->text('correct')->comment('Lưu nội dung của phương án đúng ');
            $table->text('explain')->comment('Giải thích tại sao lại chọn đáp án này đúng, tại sao sai ');
            $table->text('hint')->comment('Gợi ý trả lời ');
            $table->tinyInteger('states')->default(1);
            $table->integer('ordering')->default(100)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lesson_quicktest_questions');
    }
}
