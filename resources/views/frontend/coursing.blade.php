@extends('../frontend/layout.main')
@section('content')
<div class="list-course-banner p-0 text-white">
		<div class="container" >
			<div class="row h-100">
				<div class="col-md-12 list-course-info-user">
					<div class="row">
						<div class="avatar-user position-absolute">
							<img class="w-100 h-100" src="{{Auth::guard('student')->user()->avatar ? Auth::guard('student')->user()->avatar : config('custom.noavatar')}}" alt="">
						</div>
						<div class="info-user col-md-7">
							<p class="m-0 font-weight-bold">{{Auth::guard('student')->user()->firstname}} {{Auth::guard('student')->user()->lastname}}</p>
							<span class="fs14">(Học viên)</span>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="border-bottom bg-light">
			<div class="container">
				<div class="row">
					<div class="col-md-2 p-0"></div>
					<div class="col-md-10 col-sm-12 fs14 list-course-menu">
						<ul class="nav float-left">
						  <li class="nav-item border-left border-right">
						    <a class="fs14 nav-link text-dark" href="/students">Thông tin hồ sơ</a>
						  </li>
						  <li class="nav-item border-right">
						    <a class="fs14 nav-link text-dark" href="/students/course">Khóa học đã đăng ký</a>
						  </li>
						  <li class="nav-item">
						    <a class="fs14 nav-link border-right text-dark h-100" href="#">Luyện thi A2</a>
						  </li>
						  <li class="nav-item">
						    <a class="fs14 nav-link border-right text-dark h-100" href="#">Luyện thi B1</a>
						  </li>
						  <li class="nav-item">
						    <a class="fs14 nav-link border-right text-dark h-100" href="#">Kết quả luyện thi</a>
						  </li>
						</ul>
						<ul class="nav float-right h-100">
						  <li class="nav-item border-left border-right">
						    <a class="fs14 nav-link text-dark" href="#">Hướng dẫn sử dụng</a>
						  </li>
						</ul>
					</div>
				</div>
			</div>
			
		</div>
	</div>
	<div class="list-course-content container mt-4 mb-4">
		<div class="row">
			<div class="list-course-content-title col-md-12">
				<h3 class="fs14 font-weight-bold border-bottom pb-2"><i class="fa fa-book mr-2 ml-2"></i>KHÓA HỌC CỦA TÔI</h3>
			</div>
			<div class="list-course-content-detail col-md-12">
				<div class="row">
					<div class="col-md-12">
						<div class="row">
							@foreach($course as $value)
							<div class="col-md-3 mt-3">
								<div class="border list-course-content-item position-relative">
									<img class="w-100" height="150px" src="{{$value->curriculum->img}}" alt="">
									<div class="p-2">
										<a class="fs14 font-weight-bold text-justify" href="students/{{$value->curriculum->slug}}-{{$value->curriculum->id}}">{{$value->curriculum->name}}</a>
										<p class="text-justify">{{$value->curriculum->short_desc}}</p>
										<p class="font-italic fs12 text-right m-0">{{$value->curriculum->user->display_name}}</p>
									</div>
									<div style="height: 10px" class="progress ml-2 mr-2">
									  <div class="progress-bar bg-success" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
									</div>
									<div class="p-2 clearfix">
										<div class="float-left mt-1 fs14 text-warning" title="265/265"><i class="fa fa-star "></i><i class="fa fa-star "></i><i class="fa fa-star "></i><i class="fa fa-star "></i><i class="fa fa-star "></i> <span class="text-dark fs12">(5)</span></div>
										<div class="float-right"><button title="25/60" class="btn border-success font-italic fs12 p-1">Bắt đầu học</button></div>
									</div>
								</div>
							</div>
							@endforeach
						</div>
					</div>
				</div>
			</div>
			
		</div>
	</div>
	<script type="text/javascript">
		jQuery(document).ready(function($) {
			$('body').on('click', '.menu-mobile-btn', function() {
				let content = '<div class="menu-mobile">'
				+ '<div class="menu-mobile-search">'
				+ '<div class="form-group position-relative">'
				+ '<form><input class="form-control" placeholder="Search">'
				+ '<button class="position-absolute menu-mobile-search-btn border-0" type="submit">'
				+ '<i class="fas fa-search"></i>'
				+ '</button>' 
				+ '</form>'
				+ '</div>'
				+ '</div>'
				+ '<ul>'
				+ '<li class="border-top"><a href="students"><i class="fas fa-id-card"></i> Thông tin hồ sơ</a></li>'
				+ '<li class="border-top"><a href="students/course"><i class="fas fa-check-square"></i> Khóa học đã đăng ký</a></li>'
				+ '<li class="border-top"><a href="#"><i class="fab fa-leanpub"></i> Luyện thi A2</a></li>'
				+ '<li class="border-top"><a href="#"><i class="fab fa-leanpub"></i> Luyện thi B1</a></li>'
				+ '<li class="border-top"><a href="#"><i class="fab fa-centercode"></i> Kết quả luyện thi</a></li>'
				+ '<li class="border-top"><a href="#"><i class="fas fa-info-circle"></i> Hướng dẫn sử dụng</a></li>'
				+ '<li class="border-top border-bottom"><a href="#"><i class="fas fa-sign-in-alt"></i> Đăng xuất</a></li>'
				+ '</ul>'
				+ '<div class="text-center">'
				+ '<i class="fab fa-facebook"></i>'
				+ '<i class="fab ml-3 mr-3 fa-youtube-square"></i>'
				+ '<p class="fs14"> &copy; Trung tâm phát triển phần mềm CNTT & TT</p>'
				+ '</div>'
				+ '</div>'

				$('.top').append(content);
				$('body').addClass('bg-page');
				$('.menu-mobile').animate({right: '0'},"fast");
				$('body').css({
                    height: '100%',
                    overflow: 'hidden',
                    width: '100%',
                    position: 'fixed',
                });
			});
			$('body').click(function(e){	
				// let menuMobile = e.target.closest('#menu-mobile');
				let btnmenuMobile = e.target.closest('.menu-mobile-btn');
				let headerSearch = e.target.closest('.menu-mobile-search');
				// let headerMenu = e.target.closest('.header-menu');
				if(btnmenuMobile != null || headerSearch != null) return false;
				else {
					$('.menu-mobile').animate({right: "-1000px"});
					setTimeout(function(){
						$('.menu-mobile').remove();
						$('body').removeClass('bg-page');
						$('body').css({
                            height: 'auto',
                            overflow: 'auto',
                            width: 'auto',
                            position: 'unset',
                        });
					}, 500);
				}
			});
			if(window.location.pathname == "/students/course"){
				$(".list-course-menu li").each(function(index, el) {
					$(this).removeClass('active');
				});
				$(".list-course-menu li:nth-child(2)").first().addClass('active');
			}
		});
		$('input[name=search]').on('input', function(event) {
                let input = $(this).val();
                $.get('students/search-'+input+'/edit', function(data) {
                    console.log(data);
                    $('#result-search').find('li,p').remove();
                    let value = JSON.parse(JSON.stringify(data));
                    if(data != "" && data != 'nodata'){
                        if(value.course.length!=0){
                            $('#result-search').append('<p class="fs14 font-weight-bold m-0 pt-2 pb-2 result-search border-bottom w-100">Khóa học</p>');
                            for (var i = 0; i < value.course.length; i++) {
                                if(value.course[i]['short_desc'].length>60){
                                    $sub=value.course[i]['short_desc'].substring(0, 60);
                                    value.course[i]['short_desc']=value.course[i]['short_desc'].substring(0, $sub.lastIndexOf(" "))+"...";
                                }
                                $code = '<li class="result-search border-bottom">'
                                       +'<a href="">'
                                       +'<div class="h-100 d-flex justify-content-center align-items-center mr-2 float-left" style="width: 50px">'
                                       +'<img width="45" height="45" src="'+value.course[i]['img']+'" alt="">'
                                       +'</div>'
                                       +'<div class="pt-1"><p class="fs14 m-0">'+value.course[i]['name']+'</p><p class="font-italic fs10 m-0 text-info">'+value.tearchcourse[value.course[i]['id']]+'</p></div></a></li>'
                                $('#result-search').append($code);
                            }
                        }
                        if(value.tearch !=0 ){
                            $('#result-search').append('<p class="fs14 font-weight-bold m-0 pt-2 pb-2 result-search border-bottom w-100">Giáo viên</p>');
                            for (var i = 0; i < value.tearch.length; i++) {
                                $code = '<li class="result-search border-bottom">'
                                       +'<a href="">'
                                       +'<div class="h-100 d-flex justify-content-center align-items-center mr-2 float-left" style="width: 50px">'
                                       +'<img width="45" height="45" src="'+value.tearch[i]['avatar']+'" alt="">'
                                       +'</div>'
                                       +'<div class="pt-1"><p class="fs14 m-0"><p class="font-italic fs12 m-0 text-info">'+value.tearch[i]['display_name']+'</p></div></a></li>'
                                $('#result-search').append($code);
                            }
                        }
                    }else if(data == 'nodata'){
                    	$('#result-search').append('<p class="fs14 m-0 pt-2 pb-2 result-search border-bottom w-100">Không tìm thấy dữ liệu về "'+input+'"</p>');
                    }
                });
            });
	</script>
@endsection