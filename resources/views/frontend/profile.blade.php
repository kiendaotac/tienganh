@extends('../frontend/layout.main')
@section('style')
	<link href="{{asset('css/plugins/datapicker/datepicker3.css')}}" rel="stylesheet">
	    <!-- Toastr style -->
    <link href="{!! asset('css/plugins/toastr/toastr.min.css') !!}" rel="stylesheet">
    <link href="{!! asset('css/animate.css') !!}" rel="stylesheet">
    <!-- Sweet Alert -->
    <link href="{!! asset('css/plugins/sweetalert/sweetalert.css') !!}" rel="stylesheet">
	<link href="css/plugins/dropzone/basic.css" rel="stylesheet">
    <link href="css/plugins/dropzone/dropzone.css" rel="stylesheet">
    <style>
        /* style css code tại đây */
        .dropzone .dz-message {
            margin: 47px 0!important;
        }
        .del-img-uploaded{
        	width: 20px;height: 20px;top:2px;right: 2px;display: none;
        }
        
        .item-img:hover .del-img-uploaded{
        	display: block;
        }
    </style>
@endsection
@section('content')
<div class="list-course-banner p-0 text-white">
		<div class="container" >
			<div class="row h-100">
				<div class="col-md-12 list-course-info-user">
					<div class="row">
						<div class="avatar-user position-absolute">
							<img class="w-100 h-100" src="{{Auth::guard('student')->user()->avatar ? Auth::guard('student')->user()->avatar : config('custom.noavatar')}}" alt="">
						</div>
						<div class="info-user col-md-7">
							<p class="m-0 font-weight-bold">{{Auth::guard('student')->user()->firstname}} {{Auth::guard('student')->user()->lastname}}</p>
							<span class="fs14">(Học viên)</span>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="border-bottom bg-light">
			<div class="container">
				<div class="row">
					<div class="col-md-2 p-0"></div>
					<div class="col-md-10 col-sm-12 fs14 list-course-menu">
						<ul class="nav float-left">
						  <li class="nav-item border-left border-right">
						    <a class="fs14 nav-link text-dark" href="/students">Thông tin hồ sơ</a>
						  </li>
						  <li class="nav-item border-right">
						    <a class="fs14 nav-link text-dark" href="/students/course">Khóa học đã đăng ký</a>
						  </li>
						  <li class="nav-item">
						    <a class="fs14 nav-link border-right text-dark h-100" href="#">Luyện thi A2</a>
						  </li>
						  <li class="nav-item">
						    <a class="fs14 nav-link border-right text-dark h-100" href="#">Luyện thi B1</a>
						  </li>
						  <li class="nav-item">
						    <a class="fs14 nav-link border-right text-dark h-100" href="#">Kết quả luyện thi</a>
						  </li>
						</ul>
						<ul class="nav float-right h-100">
						  <li class="nav-item border-left border-right">
						    <a class="fs14 nav-link text-dark" href="#">Hướng dẫn sử dụng</a>
						  </li>
						</ul>
					</div>
				</div>
			</div>
			
		</div>
	</div>
	<div class="list-course-content container mt-4 mb-4">
		<div class="row">
			<div class="list-course-content-title col-md-12 mb-5 mt-3">
				<h3 class="fs14 font-weight-bold border-bottom pb-2"><i class="fa fa-book mr-2 ml-2"></i>THÔNG TIN CÁ NHÂN</h3>
			</div>
			<form name="form-edit-info" class="col-md-12" enctype="multipart/form-data">
				@csrf
				<div class="row">
					<div class="col-md-4">
						<div class="row">
							<div class="col-md-12">
								<div class="text-center mb-3">
									<p>Ảnh đại diện</p>
									<div class="avatar-old">
										<img class="rounded-circle" width="150px" height="150px" src="{{Auth::guard('student')->user()->avatar ? Auth::guard('student')->user()->avatar : config('custom.noavatar')}}" alt="">
										<div class="change-avatar">
											<label for="files" class="m-0"><i class="fas fa-cloud-upload-alt"></i></label>
      										<input id="files" name="newavatar" style="visibility: hidden;" type="button">
										</div>
									</div>
									<p class="font-italic fs12">Kích cỡ ảnh nhỏ hơn 100kb</p>
								</div>
								<div class="form-group">
									<label for="" class="m-0">Mật khẩu mới:</label>
									<input type="password" name="passnew" class="form-control" value="">
								</div>
								<div class="form-group">
									<label for="" class="m-0">Nhập lại mật khẩu mới:</label>
									<div class="position-relative">
										<input name="newpassword" type="password" class="form-control">
										<p class="position-absolute m-0 checkrepassfail1" style="display: none;">Nhập lại mật khẩu sai</p> 
										<i title="Nhập lại mật khẩu sai" class="fas fa-times checkrepassfail position-absolute" style="display: none;right: 15px;"></i>
										<i title="Nhập lại mật khẩu đúng" class="fas fa-check checkrepasstrue position-absolute" style="display: none;right: 15px;"></i>
									</div>
									
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
									<label for="" class="m-0">Họ và tên: <span class="text-danger">*</span></label>
									<input type="text" name="name" class="form-control" value="@if(Auth::guard('student')->user()->lastname !=''){{Auth::guard('student')->user()->firstname}} {{Auth::guard('student')->user()->lastname}}@endif">
								</div>
								<div class="form-group">
									<label for="" class="m-0">Mã sinh viên:<span class="text-danger">*</span></label>
									<input type="text" name="stdcode" class="form-control" value="{{Auth::guard('student')->user()->stdcode}}">
								</div>
								<div class="row">
									<div class="form-group col-md-6">
										<label for="" class="m-0">Ngày sinh:<span class="text-danger">*</span></label>
										<input type="text" data-date-format='yyyy-mm-dd' name="birthday" class="form-control" value="{{Auth::guard('student')->user()->birthday}}">
									</div>
									
									<div class="form-group col-md-6">
										<label for="" class="m-0">Giới tính:<span class="text-danger">*</span></label>
										<select class="form-control" name="sex">
											<option class="sex" value="1">Nam</option>
											<option class="sex" value="0">Nữ</option>
										</select>
										<input type="hidden" id="studentsex" value="{{Auth::guard('student')->user()->sex}}">
									</div>
								</div>
						<div class="form-group">
							<label for="" class="m-0">Email{{-- <a href="#">[Xác minh]</a> --}}:<span class="text-danger">*</span></label>
							<input type="hidden" name="id" value="{{Auth::guard('student')->user()->id}}">
							<input type="email" name="email" class="form-control" value="{{Auth::guard('student')->user()->email}}">
							{{-- <p class="alert alert-success p-1 mt-1">Gửi email thành công. Vui lòng mở email và làm theo hướng dẫn</p> --}}
						</div>
						<div class="form-group">
							<label for="" class="m-0">Số điện thoại:<span class="text-danger">*</span></label>
							
							<input type="text" name="phone" class="form-control" value="{{Auth::guard('student')->user()->phone}}">
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label for="" class="m-0">Tên trường:<span class="text-danger">*</span></label>
							<select class="form-control" name="uni" id="unis">
								<option value="aa" selected="">Chọn trường</option>
								@foreach($unis as $key)
									<option class="unis" value="{{$key->id}}">{{$key->name}}</option>
								@endforeach
								<input type="hidden" id="studentunis" value="{{$studentinfo->uni}}">
							</select>
						</div>
						<div class="form-group position-relative">
							<label for="" class="m-0">Tên ngành học:<span class="text-danger">*</span></label>
							<select class="form-control position-relative" name="dept" id="department">
								<option value="aa" selected="">Chọn ngành</option>
								@if($studentinfo->dept!="")
									<option class="department" value="{{$listdep[$studentinfo->dept]}}">{{$studentinfo->dept}}</option>
								@endif
								<input type="hidden" id="studentdepartment" value="{{$studentinfo->dept}}">
								<input type="hidden" name="stdstudentinfo" value="{{$studentinfo->id}}">
							</select>
							<img class="position-absolute loadingdep" width="20px" height="20px" src="{{ asset('img/student/loading-animated.gif') }}">	

						</div>
						<div class="form-group position-relative">
							<label for="" class="m-0">Lớp:<span class="text-danger">*</span></label>
							<select class="form-control" name="majo" id="major">
								<option value="aa" selected="">Chọn lớp</option>
								
								@if($studentinfo->majo!="")
									<option value="{{$listmj[$studentinfo->majo]}}" class="major" selected>{{$studentinfo->majo}}</option>
								@endif
							</select>
							<img class="position-absolute loadingmajo" width="20px" height="20px" src="{{ asset('img/student/loading-animated.gif') }}">

						</div>
						<div class="form-group">
							<label for="" class="m-0">Khóa:<span class="text-danger">*</span></label>
							<input type="text" name="course" class="form-control" value="{{$studentinfo->course}}">
						</div>
						<div class="form-group">
							<label for="" class="m-0">Facebook:</label>
							<input type="text" class="form-control" name="facebook" value="{{$studentinfo->facebook}}">
						</div>
					</div>
					
				</div>
				<div class="col-md-12 text-center">
					<button class="btn btn-success" id="save" type="button">Cập nhật</button>
				</div>
			</form>
		</div>
	</div>
	@include('frontend/layout/modal-upload')
	<!-- Sweet alert -->
    <script src="{!! asset('js/plugins/sweetalert/sweetalert.min.js') !!}"></script>
	<!-- Toastr -->
    <script src="{!! asset('js/plugins/toastr/toastr.min.js') !!}"></script>
	<!-- Data picker -->
    <script src="js/plugins/datapicker/bootstrap-datepicker.js"></script>
	<script type="text/javascript">
		let mainUrl = '{{$currentFunction->route}}';
		jQuery(document).ready(function($) {
			$('.del-img-uploaded').click(function(event) {
				let value =$(this).data('id');
				swal({
	            title: "Nhắc nhở",
	            text: "Bạn có chắc muốn xóa ảnh này?",
	            type: "warning",
	            showCancelButton: true,
	            confirmButtonColor: "#28a745",
	            confirmButtonText: "Có",
	            cancelButtonColor:"#dc3545",
	            cancelButtonText: "Không",
	            closeOnConfirm: true,
	            closeOnCancel: true
		        }, function(config){
		            if(config){
						type    =   'DELETE',
		                $.ajax({
		                    url: mainUrl + '/' + value,
		                    type: type,
		                    data    :   {
		                        _token  :   '{{csrf_token()}}',
		                        id      :   value
		                    },
		                    success: function(data){
		                        let value = JSON.parse(JSON.stringify(data));
		                        notification(value.type, value.title, value.content);
		                        loaddata();
		                    }
		                });
		            }
		        });
			});
			$('input[name=search]').on('input', function(event) {
                let input = $(this).val();
                $.get('students/search-'+input+'/edit', function(data) {
                    $('#result-search').find('li,p').remove();
                    let value = JSON.parse(JSON.stringify(data));
                    if(data != "" && data != 'nodata'){
                        if(value.course.length!=0){
                            $('#result-search').append('<p class="fs14 font-weight-bold m-0 pt-2 pb-2 result-search border-bottom w-100">Khóa học</p>');
                            for (var i = 0; i < value.course.length; i++) {
                                if(value.course[i]['short_desc'].length>60){
                                    $sub=value.course[i]['short_desc'].substring(0, 60);
                                    value.course[i]['short_desc']=value.course[i]['short_desc'].substring(0, $sub.lastIndexOf(" "))+"...";
                                }
                                $code = '<li class="result-search border-bottom">'
                                       +'<a href="home/'+value.course[i]['slug']+'-'+value.course[i]['id']+'">'
                                       +'<div class="h-100 d-flex justify-content-center align-items-center mr-2 float-left" style="width: 50px">'
                                       +'<img width="45" height="45" src="'+value.course[i]['img']+'" alt="">'
                                       +'</div>'
                                       +'<div class="pt-1"><p class="fs14 m-0">'+value.course[i]['name']+'</p><p class="font-italic fs10 m-0 text-info">'+value.tearchcourse[value.course[i]['id']]+'</p></div></a></li>'
                                $('#result-search').append($code);
                            }
                        }
                        if(value.tearch !=0 ){
                            $('#result-search').append('<p class="fs14 font-weight-bold m-0 pt-2 pb-2 result-search border-bottom w-100">Giáo viên</p>');
                            for (var i = 0; i < value.tearch.length; i++) {
                                $code = '<li class="result-search border-bottom">'
                                       +'<a href="">'
                                       +'<div class="h-100 d-flex justify-content-center align-items-center mr-2 float-left" style="width: 50px">'
                                       +'<img width="45" height="45" src="'+value.tearch[i]['avatar']+'" alt="">'
                                       +'</div>'
                                       +'<div class="pt-1"><p class="fs14 m-0"><p class="font-italic fs12 m-0 text-info">'+value.tearch[i]['display_name']+'</p></div></a></li>'
                                $('#result-search').append($code);
                            }
                        }
                    }else if(data == 'nodata'){
                    	$('#result-search').append('<p class="fs14 m-0 pt-2 pb-2 result-search border-bottom w-100">Không tìm thấy dữ liệu về "'+input+'"</p>');
                    }
                });
            });
           	
			$('input[name=newavatar]').on('click',function () {
                $('#modal-upload').modal('show');
            });
        
			$('body').on('click', '.menu-mobile-btn', function() {
				let content = '<div class="menu-mobile">'
				+ '<div class="menu-mobile-search">'
				+ '<div class="form-group position-relative">'
				+ '<form><input class="form-control" placeholder="Search">'
				+ '<button class="position-absolute menu-mobile-search-btn border-0" type="submit">'
				+ '<i class="fas fa-search"></i>'
				+ '</button>' 
				+ '</form>'
				+ '</div>'
				+ '</div>'
				+ '<ul>'
				+ '<li class="border-top"><a href="student"><i class="fas fa-id-card"></i> Thông tin hồ sơ</a></li>'
				+ '<li class="border-top"><a href="student/course"><i class="fas fa-check-square"></i> Khóa học đã đăng ký</a></li>'
				+ '<li class="border-top"><a href="#"><i class="fab fa-leanpub"></i> Luyện thi A2</a></li>'
				+ '<li class="border-top"><a href="#"><i class="fab fa-leanpub"></i> Luyện thi B1</a></li>'
				+ '<li class="border-top"><a href="#"><i class="fab fa-centercode"></i> Kết quả luyện thi</a></li>'
				+ '<li class="border-top"><a href="#"><i class="fas fa-info-circle"></i> Hướng dẫn sử dụng</a></li>'
				+ '<li class="border-top border-bottom"><a href="#"><i class="fas fa-sign-in-alt"></i> Đăng xuất</a></li>'
				+ '</ul>'
				+ '<div class="text-center">'
				+ '<i class="fab fa-facebook"></i>'
				+ '<i class="fab ml-3 mr-3 fa-youtube-square"></i>'
				+ '<p class="fs14"> &copy; Trung tâm phát triển phần mềm CNTT & TT</p>'
				+ '</div>'
				+ '</div>'
				$('.top').append(content);
				$('body').addClass('bg-page');
				$('.menu-mobile').animate({right: '0'},"fast");
				$('body').css({
                    height: '100%',
                    overflow: 'hidden',
                    width: '100%',
                    position: 'fixed',
                });
			});
			$('body').click(function(e){
				$('input[name=search]').val("");
				$('#result-search').find('li,p').remove();
				let btnmenuMobile = e.target.closest('.menu-mobile-btn');
				let headerSearch = e.target.closest('.menu-mobile-search');
				if(btnmenuMobile != null || headerSearch != null) return false;
				else {
					$('.menu-mobile').animate({right: "-1000px"});
					setTimeout(function(){
						$('.menu-mobile').remove();
			
						if($('body').hasClass('bg-page')==true){
							$('body').removeClass('bg-page');
							$('body').css({
	                            height: 'auto',
	                            overflow: 'auto',
	                            width: 'auto',
	                            position: 'unset',
	                        });
						}
						
					}, 500);
				}
			});
			$(window).resize(function(event) {
				if ($(window).width()<992) {
					$('.info-user').removeClass('col-md-7');
					$('.info-user').addClass('col-md-12');
				}
			});
			if(window.location.pathname == "/students"){
				$(".list-course-menu li").each(function(index, el) {
					$(this).removeClass('active');
				});
				$(".list-course-menu li").first().addClass('active');
			}
			$('.unis').each(function(index, el) {
				if ($(this).text()==$('#studentunis').val()) {
					$(this).closest('option').attr('selected', '');
				}
			});
			$('.department').each(function(index, el) {
				if ($(this).text()==$('#studentdepartment').val()) {
					$(this).closest('option').attr('selected', '');
				}
			});
			$('.sex').each(function(index, el) {
				if ($(this).val()==$("#studentsex").val()) {
					$(this).closest('option').attr('selected', '');
				}
			});
			$('#unis').on('change blur', function() {
				$id = $(this).val();
				$url = '{{$currentFunction->route}}';
				$('.loadingdep').css('display', 'block');
				$.get($url+'/'+$id+'unis/edit', function(data) {
		            if (data =='') {
		            	$('.major').closest('option').remove();
		                $('.department').closest('option').remove();
		            }
		            let value = JSON.parse(JSON.stringify(data));
		            $('.department').closest('option').remove();
		            for (var i = 0; i < value.length; i++) {
		                $html = '<option class="department" value="'+value[i].id+'">'+value[i].name+'</option>';
		                $('#department').append($html);
		            }
		            $('.loadingdep').css('display', 'none');
		        });
			});		
			$('#department').on('change blur', function() {
				$id = $(this).val();
				$('.loadingmajo').css('display', 'block');
				$url = '{{$currentFunction->route}}';
				$.get($url+'/'+$id+'dep/edit', function(data) {
		            if (data =='') {
		                $('.major').closest('option').remove();
		            }
		            let value = JSON.parse(JSON.stringify(data));
		            $('.major').closest('option').remove();
		            for (var i = 0; i < value.length; i++) {
		                $html = '<option class="major" value="'+value[i].id+'">'+value[i].name+'</option>';
		                $('#major').append($html);
		            }
		             $('.loadingmajo').css('display', 'none');
		        });
			});
			$("input[name=passnew]").val("");
			function checkrepass(){
				$(".checkrepassfail,.checkrepassfail1,.checkrepasstrue").css({
					display: 'none',
				});
				if ($('input[name=newpassword]').val() == $('input[name=passnew]').val()) {
					if ($('input[name=newpassword]').val() != "" && $('input[name=passnew]').val()!="") {
						$(".checkrepasstrue").css({
							display: 'block',
							color: 'green',
							top: '10px',
	    					fontSize: '15px'
						});
					}
					return 1;
				}else {
					$(".checkrepassfail").css({
						display: 'block',
						color: 'red',
						top: '10px',
    					fontSize: '15px'
					});
					$(".checkrepassfail1").css({
						display: 'block',
						color: 'red',
					});
					return 0;
				}
			}
			$('input[name=passnew]').click(function() {
				$(this).val("");
				$('input[name=newpassword]').val("");
				$(".checkrepassfail,.checkrepassfail1,.checkrepasstrue").css({
					display: 'none',
				});
			});
			$('input[name=newpassword]').change(function() {
				checkrepass();
			});

			$('.ImgUploaded').click(function() {
				let src = $(this).data('src');
				swal({
	            title: "Nhắc nhở",
	            text: "Bạn có chắc muốn chọn ảnh này làm ảnh đại diện?",
	            type: "warning",
	            showCancelButton: true,
	            confirmButtonColor: "#28a745",
	            confirmButtonText: "Có",
	            
	            cancelButtonColor:"#dc3545",
	            cancelButtonText: "Không",
	            closeOnConfirm: true,
	            closeOnCancel: true
		        }, function(config){
		            if(config){
		            	let mainUrl = '{{$currentFunction->route}}',
						type    =   'PUT',
						value 	= 	$(this).data('src');
						id      =   $('input[name=id]').val();
		                $.ajax({
		                    url: mainUrl + '/' + id,
		                    type: type,
		                    data    :   {
		                        _token  :   '{{csrf_token()}}',
		                        link      :   src
		                    },
		                    success: function(data){
		                        let value = JSON.parse(JSON.stringify(data));
		                        notification(value.type, value.title, value.content);
		                        $('.avatar-user img').attr('src', value.link);
		                        $('.avatar-old img').attr('src', value.link);
		                        $('#modal-upload').modal('hide');
		                    }
		                });
		            }
		        });
				
			});
			$("#save").click(function(event) {
				if (checkrepass()==1) {
					data = $('form[name=form-edit-info]').serialize(),
					type    =   'PUT',
					id      =   $('input[name=id]').val();
					$.ajax({
						url: mainUrl + '/' + id,
						type: type,
						dataType: 'json',
						data: data,
						success: function (data){
							let value = JSON.parse(JSON.stringify(data));
							for (var i = 0; i < value.content.length; i++) {
								notification(value.type, value.title, value.content[i]);
							}
						},
						error:function(data){
							let value = JSON.parse(JSON.stringify(data));
                			notification(value.type, value.title, value.content);

						}
					})
				}else {
					notification("warning", "Thông báo", "Nhập lại mật khẩu sai");
				}
			});	
			$('input[name=birthday]').datepicker({
                dateFormat: 'yy-mm-dd',
                todayBtn: "linked",
                keyboardNavigation: false,
                forceParse: false,
                calendarWeeks: true,
                autoclose: true,
            });
			function notification(type, title, content) {
            title = '';
            setTimeout(function () {
                toastr.options = {
                    closeButton: true,
                    progressBar: true,
                    showMethod: 'slideDown',
                    timeOut: 4000
                };
                switch (type) {
                    case 'success'  :
                    toastr.success(content, title);
                    break;
                    case 'error'    :
                    toastr.error(content, title);
                    break;
                    case 'warning'  :
                    toastr.warning(content, title);
                    break;
                    default         :
                    toastr.warning('Không xác định được thông báo', 'Cảnh báo!');
                    break;
                }
            }, 1300);
        	}
		});
	</script>
	@section('script-upload-user')
    <!-- DROPZONE -->
    <script src="js/plugins/dropzone/dropzone.js"></script>
    <script type="text/javascript">
        Dropzone.options.myDropzone={
            url: 'students',
            headers: {
                'X-CSRF-TOKEN': '{!! csrf_token() !!}'
            },
            autoProcessQueue: true,
            uploadMultiple: true,
            parallelUploads: 5,
            maxFiles: 10,
            maxFilesize: {!! $maxsize !!},
            dictDefaultMessage: "<strong>Kéo thả files vào đây để upload lên máy chủ </strong></br> (Hoặc click chuột để chọn files upload.)",
            dictFileTooBig: 'Image is bigger than 5MB',
            addRemoveLinks: false,
            previewsContainer: null,
            hiddenInputContainer: "body",
            success: function (file, respon) {
                notification(respon.type, respon.title, respon.content);
                loaddata();
            }
        }
        function notification(type, title, content) {
            title = '';
            setTimeout(function () {
                toastr.options = {
                    closeButton: true,
                    progressBar: true,
                    showMethod: 'slideDown',
                    timeOut: 4000
                };
                switch (type) {
                    case 'success'  :
                    toastr.success(content, title);
                    break;
                    case 'error'    :
                    toastr.error(content, title);
                    break;
                    case 'warning'  :
                    toastr.warning(content, title);
                    break;
                    default         :
                    toastr.warning('Không xác định được thông báo', 'Cảnh báo!');
                    break;
                }
            }, 1300);
        	}
        function loaddata(){
        	let url = 'students/get-img';
        	$.ajax({
        		url: url,
        		type: 'GET',
        		success:function(data){
        			$('.list-img').html(data);
        		}
        	})
        }
    </script>
	@stop
@endsection