<div class="col-md-12 pb-2 text-center">
	<h3 class="fs14 float-left">Ảnh đã tải lên trước đây</h3>
	<br>
	<br>
	@if($uploaded->isEmpty())
		<p>Bạn chưa từng tải ảnh nào lên cả!</p>
	@else
		@foreach($uploaded as $value)
		<div class="d-inline-block position-relative item-img">
			<img width="80" height="80" class="mx-auto border ImgUploaded mb-1"
			data-src='storage/uploads/{{$value->year}}/{{$value->month}}/{{$value->file_name}}.{{$value->extension}}'
    		 src="storage/uploads/{{$value->year}}/{{$value->month}}/{{$value->file_name}}.{{$value->extension}}" title="{{Carbon\Carbon::parse($value->created_at)->format('d/m/Y')}}">
    		<form action="">
    			<button type="button" data-id="{{$value->id}}" class="text-white position-absolute bg-dark border-0 rounded-circle del-img-uploaded">X</button>
    		</form>
		</div>
    	@endforeach
	@endif
</div>