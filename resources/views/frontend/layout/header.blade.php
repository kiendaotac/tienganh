<div class="top">
	<div class="container">
		<div class="row">
			<div class="col-md-12 header">
				<div class="row">
					<div class="col-md-4 col-sm-6 col-6">
						<a href="/" class="logo">
							<img height="40px" src="{{asset('img/student/logo-ictuedu-cam.png')}}" alt="">
						</a>
					</div>
					<div class="col-md-4 search-menu position-relative">
						<form action="">
							<input class="form-control" name="search" style="height: 30px" placeholder="Nhập tên khóa học" type="text">
							<button type="submit" class="mt-1"><i class="fas fa-search"></i></button>
							<ul class="nav bg-white rounded-bottom pl-1" id="result-search">
				                
				            </ul>
					    </form>
					</div>
					<div class="col-md-4 login-header fs12 d-lg-flex justify-content-end fs14 align-items-center text-right">
						<i class="fab fa-facebook"></i>
						<i class="fab ml-3 mr-3 fa-youtube-square"></i>
						@if(Auth::guard('student')->check())
						Chào {{Auth::guard('student')->user()->lastname}}<a title="Đăng xuất" href="/logout"><i class="ml-2 fas fa-sign-out-alt"></i></a>
						@else
						<button class="btn bg-white btn-sm border-danger mr-1"><a class="text-dark" href="">Đăng ký</a></button>
						<a class="text-dark" href="/login"><button class="btn bg-white btn-sm border-success">Đăng nhập</button></a>
						@endif
						
					</div>
					<div class="col-md-4 col-sm-6 col-6 text-right" id="menu-mobile">
						<i class="fas fa-bars mt-3 menu-mobile-btn"></i>
					</div>
					
				</div>
			</div>
		</div>
	</div>
</div>
