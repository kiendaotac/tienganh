<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>abc</title>

	<base href="{{asset('path')}}">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="{{ asset('css/student/student.css') }}">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	@yield('style')
</head>
<body>
	@include('../frontend/layout.header')
	@yield('content')
	@include('../frontend/layout/footer')
	<script type="text/javascript">
		jQuery(document).ready(function($) {
			$('.menu').click(function(event) {
				$('.submenu').css({
					opacity: '1',
					left: '0'
				});
			});
			$('.submenu').after().click(function(event) {
		
				$('.submenu').css({
					left: '-200px',
					opacity: '0'
				});
			});
		});
	</script>
	@yield('script-upload-user')
</body>
</html>