<div class="modal fade bd-example-modal-lg" id="modal-upload" tabindex="-1" role="dialog" aria-labelledby="modal upload" aria-hidden="true" style="height: 625px;overflow: none !important;padding: 0">
    <div class="modal-dialog modal-lg" style="width: auto;">
        <div class="modal-content">
            <div class="dropzone" id="my-dropzone" name="myDropzone" style="padding: 0; margin: 10px">
            </div>
            <div class="list-img">
            	@include('frontend/layout/data')
            </div>
            
        </div>
    </div>
</div>
