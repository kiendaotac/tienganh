@extends('../frontend/layout.main')
@section('style')
	<!-- Sweet Alert -->
    <link href="{!! asset('css/plugins/sweetalert/sweetalert.css') !!}" rel="stylesheet">
@endsection
@section('content')
	<div class="banner">
		<div class="container h-100">
			<div class="row">
				<div class="col-md-5 banner-img">
					<img class="w-100 h-100" src="{{$data->img}}" alt="">
				</div>
				<div class="col-md-7 banner-detail">
					<h3 class="mb-5">{{$data->name}}</h3>
					{{-- <button class="mb-4">Tiếp tục học bài 28</button>
					<a href="#">Hướng dẫn học</a>
					<p>Bạn đã hoàn thành <strong>27</strong> trong <strong>30</strong></p>
					<div class="progress">
					  <div class="progress-bar bg-success" role="progressbar" style="width: 75%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">75%</div>
					</div> --}}
					<input type="hidden" id="logined" value="@if(Auth::guard('student')->check()) {{1}}@endif">
					<button class="btn btn-danger mb-4 tolearn">Học miễn phí</button>
					<input type="hidden" class="checktolearn" value="{{$data->logintolearn}}">
					<a href="#">Hướng dẫn học</a>
					
					@if(Auth::guard('student')->check())
					<p>Bạn đã học <strong>27</strong> trong <strong>30</strong> bài học</p>
					<div class="progress">
					  <div class="progress-bar progress-bar-striped progress-bar-animated bg-danger" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 75%">75%</div>
					</div>
					@endif
					
				</div>
			</div>
		</div>
	</div>
	<div class="menu-course">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<ul class="nav">
						<li class="nav-item border-left border-right active"><a class="nav-link" href="">Tổng quan</a></li>
						{{-- <li class="nav-item border-right"><a class="nav-link" href="">Bài học</a></li> --}}
						<li class="nav-item border-right"><a class="nav-link" href="">Hỏi & Đáp</a></li>
					</ul>
				</div>
				
			</div>
			
		</div>
	</div>
	<div class="content-course pb-3 pt-3">
		<div class="container">
			<div class="row">
				<div class="col-md-7 pt-3 pb-3">
					<div class="bg-white p-3 border rounded">
						<h3 class="fs20">Chào mừng đến với khóa học {{$data->name}}</h3>
						<p>
							{{$data->desc}}
						</p>
					</div>
					<div class="bg-white mt-3 p-3 border rounded">
						<h3 class="fs20">Nội dung khóa học</h3>
						<div class="row">
							<div class="col-md-12">
								@foreach($lessondata as $value)
									<ul class="nav rounded mt-2 lesson-content">
											<li class="nav-item w-100 border p-2 border-bottom-0 bg-light rounded-top"><strong><i class="fas fa-book"></i> {{$value->name}}</strong></li>
									@foreach($value->childs as $key)
											@if($key->childsContent->type == 'video')
 											<li class="nav-item w-100 border p-2 pl-3 border-bottom-0"><i class="far fa-play-circle"></i>  {{$key->name}}</li>
 											@elseif($key->childsContent->type == 'text')
 												<li class="nav-item w-100 border p-2 pl-3 border-bottom-0"><i class="far fa-file-alt"></i>  {{$key->name}}</li>
 												@else
 												<li class="nav-item w-100 border p-2 pl-3 border-bottom-0"><i class="fas fa-volume-up"></i> {{$key->name}}</li>
											@endif
									@endforeach
									</ul>
								@endforeach
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-5 pt-3 pb-3">
					<div class="bg-white p-3 border rounded">
						<h3 class="fs20">Thông tin giảng viên</h3>
						<div class="row">
							<div class="col-md-7 detail-teacher text-center">
								<div>
									<img src="{{$data->user->avatar}}" alt="">
								</div>
								<p class="m-0"><strong>{{$data->user->display_name}}</strong></p>
								<span>Giảng viên</span>
							</div>
							<div class="col-md-5 content-course-detail- d-flex justify-content-center align-self-center">
								<div class="content-course-detail">
									<p><i class="fas fa-star mr-2"></i> 5 đánh giá</p>
									<p><i class="fas fa-users mr-2"></i> 255 học viên</p>
									<p><i class="far fa-play-circle mr-2"></i> 3 khóa học</p>
									<p><i class="fas fa-comment-alt mr-2"></i> 222 bình luận</p>
								</div>
							</div>
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</div>
	<!-- Sweet alert -->
    <script src="{!! asset('js/plugins/sweetalert/sweetalert.min.js') !!}"></script>
	<script type="text/javascript">
		jQuery(document).ready(function($) {
			$('.tolearn').click(function(event) {
				if ($('.checktolearn').val()==1 && $('#logined').val()=="") {
					swal("Thông báo", "Bạn phải đăng nhập để học bài này!", "error")
				}else{
					// window.location.href=window.location.href+'/abc';
				}
			});
			$(".lesson-content li:last-child").addClass('border-bottom');
			$(".lesson-content li:last-child").removeClass('border-bottom-0');
			$( window ).resize(function() {
				if ($(window).width()<=991) {
					$('.banner-img').removeClass('col-md-5');
					$('.banner-img').addClass('col-md-12');
					$('.banner-detail').removeClass('col-md-7');
					$('.detail-teacher').removeClass('col-md-7');
					$('.detail-teacher').addClass('col-md-12');
					$('.content-course-detail-').removeClass('col-md-5');
					$('.content-course-detail-').addClass('col-md-12');
				}else {
					$('.banner-img').removeClass('col-md-12');
					$('.banner-img').addClass('col-md-5');
					$('.banner-detail').addClass('col-md-7');
					$('.detail-teacher').addClass('col-md-7');
					$('.detail-teacher').removeClass('col-md-12');
					$('.content-course-detail-').addClass('col-md-5');
					$('.content-course-detail-').removeClass('col-md-12');
				}
			});
			if ($(window).width()<=991) {
				$('.banner-img').removeClass('col-md-5');
				$('.banner-img').addClass('col-md-12');
				$('.banner-detail').removeClass('col-md-7');
				$('.detail-teacher').removeClass('col-md-7');
				$('.detail-teacher').addClass('col-md-12');
				$('.content-course-detail-').removeClass('col-md-5');
				$('.content-course-detail-').addClass('col-md-12');
			}
			if($('body').height() < $(window).height()){
				$(".footer").css({
					position: 'fixed',
					bottom: '0',
					width : '100%'
				});
				console.log('ok');
			}else{
				$(".footer").css({
					position: 'sticky',
				});
			}
			$('body').on('click', '.menu-mobile-btn', function() {
				let content = '<div class="menu-mobile">'
				+ '<div class="menu-mobile-search">'
				+ '<div class="form-group position-relative">'
				+ '<form><input class="form-control" placeholder="Search">'
				+ '<button class="position-absolute menu-mobile-search-btn border-0" type="submit">'
				+ '<i class="fas fa-search"></i>'
				+ '</button>' 
				+ '</form>'
				+ '</div>'
				+ '</div>'
				+ '<ul>'
				+ '<li class="border-top"><a href="#"><i class="fas fa-id-card"></i> Thông tin hồ sơ</a></li>'
				+ '<li class="border-top"><a href="#"><i class="fas fa-check-square"></i> Khóa học đã đăng ký</a></li>'
				+ '<li class="border-top"><a href="#"><i class="fab fa-leanpub"></i> Luyện thi A2</a></li>'
				+ '<li class="border-top"><a href="#"><i class="fab fa-leanpub"></i> Luyện thi B1</a></li>'
				+ '<li class="border-top"><a href="#"><i class="fab fa-centercode"></i> Kết quả luyện thi</a></li>'
				+ '<li class="border-top"><a href="#"><i class="fas fa-info-circle"></i> Hướng dẫn sử dụng</a></li>'
				+ '<li class="border-top border-bottom"><a href="#"><i class="fas fa-sign-in-alt"></i> Đăng xuất</a></li>'
				+ '</ul>'
				+ '<div class="text-center">'
				+ '<i class="fab fa-facebook"></i>'
				+ '<i class="fab ml-3 mr-3 fa-youtube-square"></i>'
				+ '<p class="fs14"> &copy; Trung tâm phát triển phần mềm CNTT & TT</p>'
				+ '</div>'
				+ '</div>'
				$('.top').append(content);
				$('body').addClass('bg-page');
				$('.menu-mobile').animate({right: '0'},"fast");
				$('body').css('overflow', 'hidden');
			});
			$('body').click(function(e){	
				$('input[name=search]').val("");
				$('#result-search').find('li,p').remove();
				let btnmenuMobile = e.target.closest('.menu-mobile-btn');
				let headerSearch = e.target.closest('.menu-mobile-search');
				// let headerMenu = e.target.closest('.header-menu');
				if(btnmenuMobile != null || headerSearch != null) return false;
				else {
					$('.menu-mobile').animate({right: "-1000px"});
					setTimeout(function(){
						$('.menu-mobile').remove();
						$('body').removeClass('bg-page');
					}, 500);
				}
			});
			$('input[name=search]').on('input', function(event) {
                let input = $(this).val();
                $.get('students/search-'+input+'/edit', function(data) {
                    $('#result-search').find('li,p').remove();
                    let value = JSON.parse(JSON.stringify(data));
                    if(data != "" && data != 'nodata'){
                        if(value.course.length!=0){
                            $('#result-search').append('<p class="fs14 font-weight-bold m-0 pt-2 pb-2 result-search border-bottom w-100">Khóa học</p>');
                            for (var i = 0; i < value.course.length; i++) {
                                if(value.course[i]['short_desc'].length>60){
                                    $sub=value.course[i]['short_desc'].substring(0, 60);
                                    value.course[i]['short_desc']=value.course[i]['short_desc'].substring(0, $sub.lastIndexOf(" "))+"...";
                                }
                                $code = '<li class="result-search border-bottom">'
                                       +'<a href="home/'+value.course[i]['slug']+'-'+value.course[i]['id']+'">'
                                       +'<div class="h-100 d-flex justify-content-center align-items-center mr-2 float-left" style="width: 50px">'
                                       +'<img width="45" height="45" src="'+value.course[i]['img']+'" alt="">'
                                       +'</div>'
                                       +'<div class="pt-1"><p class="fs14 m-0">'+value.course[i]['name']+'</p><p class="font-italic fs10 m-0 text-info">'+value.tearchcourse[value.course[i]['id']]+'</p></div></a></li>'
                                $('#result-search').append($code);
                            }
                        }
                        if(value.tearch !=0 ){
                            $('#result-search').append('<p class="fs14 font-weight-bold m-0 pt-2 pb-2 result-search border-bottom w-100">Giáo viên</p>');
                            for (var i = 0; i < value.tearch.length; i++) {
                                $code = '<li class="result-search border-bottom">'
                                       +'<a href="">'
                                       +'<div class="h-100 d-flex justify-content-center align-items-center mr-2 float-left" style="width: 50px">'
                                       +'<img width="45" height="45" src="'+value.tearch[i]['avatar']+'" alt="">'
                                       +'</div>'
                                       +'<div class="pt-1"><p class="fs14 m-0"><p class="font-italic fs12 m-0 text-info">'+value.tearch[i]['display_name']+'</p></div></a></li>'
                                $('#result-search').append($code);
                            }
                        }
                    }else if(data == 'nodata'){
                    	$('#result-search').append('<p class="fs14 m-0 pt-2 pb-2 result-search border-bottom w-100">Không tìm thấy dữ liệu về "'+input+'"</p>');
                    }
                });
            });
		});
	</script>
@endsection