<script>
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
	/* lesson active */
	$("#lesson li:not('.chapter'):eq(0)").addClass('lesson-active');
	$("#lesson li.lesson-active").trigger('click');
	/* load lesson */
	let idOld = 0,
	countTest = 0, // bến để kiếm tra xem đã trả lời hết câu hỏi hay chưa
	quicktestId = 0,
	liCurrent = null;
	/* chọn bài đầu tiên khi bắt đầu học */
	setTimeout(function(){
		$("#lesson li:not('.chapter'):eq(0)").trigger('click');
	},0);
	$(document).on('click', "#lesson li:not('.chapter')", function(){
		// active
		liCurrent = $(this),
		index  = parseInt($(this).index()) - 1;
		if(index == 0) idLessonPrev = 0;
		else {
			idLessonPrev = searchLi(index).data('id');
		}
		if(liCurrent.find('i.fa-check').length <= 0) {
			if(index != 0 && searchLi(index).find('i.fa-check').length <= 0) {
				swal('Bạn chưa hoàn thành bài học trước đó');
				idOld = 0;
			}
			else loadContent(liCurrent);
		}
		else loadContent(liCurrent);
		
	});
	/* load content lesson */
	function loadContent(li){
		removeActive();
		li.addClass('lesson-active');
		if(li.data('id') != idOld) {
			idOld = li.data('id');
		// load
		$('.lesson-content').html('<div class="text-center" style="width: 100%; height: 100%;"><i class="fa fa-refresh load"></i></div>');
		$.ajax({
			url: 'learning/' + idOld,
			type: 'PUT',
			dataType: 'JSON',
			data: {
				type: 'init',
				idLessonPrev: idLessonPrev
			},
			success: function(data){
				$('.lesson-content').html('');
				let value = JSON.parse(JSON.stringify(data));
				if(typeof value.lesson === 'undefined') {
					notification(value.type,value.title,value.content);
					idOld = 0;
				}
				else {
					if(value.lesson.type === 'lesson') {
						if(value.status.pass == 0) {
							liCurrent.append('<input type="checkbox" name="pass" value="'+value.status.id+'">');
							/* i check */
							$('input[name=pass]').iCheck({
								checkboxClass: 'icheckbox_square-green check-pass',
							});
						}
						switch(value.data.type) {
							case 'video':
							$('.lesson-content').html('@include('frontend.learning.o_learning.inc.video')');
							let desc = JSON.parse(value.data.desc);
							$('#player').attr('data-plyr-provider',desc.provider);
							$('#player').attr('data-plyr-embed-id',desc.src);
							new Plyr('#player');
							break;
							case 'audio':
							$('.lesson-content').html('@include('frontend.learning.o_learning.inc.audio')');
							$('.lesson-content audio source').attr('src',JSON.parse(value.data.desc).src);
							new Plyr('#player');
							break;
							case 'text':
							$('.lesson-content').html('@include('frontend.learning.o_learning.inc.text')');
							$('.lesson-text > div').html(value.data.desc);
							break;
						}
					}
					if(value.lesson.type === 'test'){
						let quicktest = value.quicktest,
						questions     = value.questions;
						quicktestId   = quicktest.id;
						countTest     = questions.length;
						$('.lesson-content').html('@include('frontend.learning.o_learning.inc.quicktest')');
						$.each(questions, function(index, val) {
							let temp = parseInt(index)+1;
							$('.quicktest-list-answer ul').append('<li>'+temp+'</li>');
							let content = '<div class="quicktest-item">' +
							'<div class="quicktest-name"><span>'+temp+',</span> <span>'+val.question+'</span></div>' +
							'<div class="quicktest-answer">' +
							'<div class="row">';
							$.each(JSON.parse(val.options), function(i, v) {
								content += '<div class="col-lg-12">' +
								'<label><input type="radio" data-id="'+val.id+'" value="'+v+'" name="options-'+temp+'">'+v+'</label>' +
								'</div>';
							});
							content += '</div></div>';
							if(quicktest.show_hint == 1) {
								content += '<div class="quicktest-hint none">'+val.hint+'</div>';
							}
							$('.quicktest-slide').append(content);
						});
						$('.quicktest-list-answer ul li:eq(0)').addClass('active');
						$('.total-question').text($('.quicktest-item').length);
						$('#correct-force').text(value.correctForce);
						if(typeof value.status !== 'undefined') {
							let options = JSON.parse(value.status.options);
							$.each(options, function(index, val) {
								let input = $('input[data-id='+index+']');
								$.each(input, function(i, v) {
									if(v.value === val) $(v).attr('checked','checked');
								});
							});
						}
						if(typeof value.correct !== 'undefined') {
							let correct = value.correct.original;
							$.each(correct, function(index, val) {
								let input = $('input[data-id='+index+']');
								$.each(input, function(i, v) {
									if(v.value === val) {
										let div = $(v).closest('div');
										div.append('<i class="fa fa-check pass-success" style="margin-left: 10px"></i>');
									}
								});
							});
						}
						quicktestResult();
						$(window).trigger('resize');
					}
					/* load document */
					$('#file > ul').html('');
					$.each(value.documents, function(index, val) {
						let content = '<li>' +
						'<div>' +
						'<i class="fa fa-file"></i>' +
						'<span>'+val.title+'</span>' +
						'</div>' +
						'<div>' +
						'<a href='+val.link+'>' +
						'<i class="fa fa-download"></i>' +
						'</a>' +
						'</div>' +
						'</li>';
						$('#file > ul').append(content);
					});
					/* update data id status add note */
					$('.note-add > i').attr('data-id',value.status.id);
					/* load note */
					$('.note-list > ul').html('');
					$.each(value.notes, function(index, val) {
						let date = index.substr(0,index.lastIndexOf('-')),
						time = index.substr(index.lastIndexOf('-')+1);
						let content = '<li>'+
						'<i class="fa fa-comment"></i>'+
						'<span>'+val+'</span>'+
						'<span class="note-date">'+removeSeconds(date)+'<i class="fa fa-trash delete-note" data-id="'+value.status.id+'" data-date="'+time+'"></i></span>'+
						'</li>';
						$('.note-list > ul').append(content);
					});
					console.log(value);
				}
			}
		});
}

}
/* save note */
$(document).on('click', '.note-add > i', function(){
	let note = $(this).closest('div').find('textarea').val(),
	id       = $(this).data('id');
	if(note === '') swal('Bạn chưa nhập ghi chú');
	else {
		$.ajax({
			url: 'learning/'+id,
			type: 'PUT',
			dataType: 'JSON',
			data: {
				type: 'writeNote',
				note: note,
			},
			success: function(data){
				let value = JSON.parse(JSON.stringify(data));
				console.log(value);
				notification(value.type,value.title,value.content);
				if(value.type == 'success') {
					let content = '<li>'+
					'<i class="fa fa-comment"></i>'+
					'<span>'+note+'</span>'+
					'<span class="note-date">'+removeSeconds(value.date)+'<i class="fa fa-trash delete-note" data-id="'+value.status.id+'" data-date="'+value.time+'"></i></span>'+
					'</li>';
					$('.note-list > ul').html(content + $('.note-list > ul').html());
				}
			}
		})
	}
});	
/* delete note */
$(document).on('click','.delete-note', function(){
	let id = $(this).data('id'),
	date   = $(this).data('date'),
	li     = $(this).closest('li');
	swal({
		title: "Bạn có muốn xóa ghi chú này không?",
		text: "Khi xóa sẽ không thể khôi phục lại",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "OK",
		cancelButtonText: "Không",
		closeOnConfirm: true,
		closeOnCancel: true
	}, function(config){
		if(config){
			$.ajax({
				url: 'learning/'+id,
				type: 'PUT',
				dataType: 'JSON',
				data: {
					type: 'deleteNote',
					date: date,
				},
				success: function(data){
					let value = JSON.parse(JSON.stringify(data));
					notification(value.type,value.title,value.content);
					if(value.type == 'success') {
						li.remove();
					}
				}
			})
		}
	});
});
/* remove seconds in date note */
function removeSeconds(seconds){
	let s = seconds.substr(0,seconds.lastIndexOf(':'));
	return s;
}
/* search li prev */
function searchLi(index) {
	while($('#lesson li:eq('+index+')').hasClass('chapter')) {
		index--;
	}
	return $('#lesson li:eq('+index+')');
}
/* remove active */
function removeActive(){
	$('#lesson li').each(function(){
		$(this).removeClass('lesson-active');
	});
}
/* submit test */
$(document).on('click','.bg-submit', function(){
	let button = $(this);
	let count = 0,
	question  = [],
	correct   = [];
	options = '{';
	$(".quicktest-item input[type='radio']").each(function(){
		if($(this).is(":checked")){
			count++;
			options += '"'+$(this).data('id')+'":"'+$(this).val()+'",';
			question.push($(this).data('id'));
			correct.push($(this).val());
		}
	});
	options = options.substr(0,options.lastIndexOf(','));
	options += '}';
	if(count != countTest) swal('Bạn chưa trả lời hết câu hỏi');
	else {
		$(this).append('<i class="fa fa-refresh load" style="margin-left: 5px; font-size: 12px;"></i>');
		liCurrent.find('i.pass-success').remove();
		$.ajax({
			url: 'learning',
			type: 'POST',
			dataType: 'JSON',
			data: {
				quicktest_id: quicktestId,
				lesson_id: idOld,
				correct: JSON.stringify(correct),
				question: JSON.stringify(question),
				options: options,
				type: 'submitTest'
			},
			success: function(data){
				button.find('i').remove();
				let value = JSON.parse(JSON.stringify(data)),
				title = 'Chưa hoàn thành bài thi';
				if(value.pass == 1) {
					title = 'Hoàn thành bài thi';
					liCurrent.append('<i class="fa fa-check pass-success"></i>');
				}
				swal({
					title: title,
					text: "Số câu đúng: "+value.correct+'/'+value.total
				});
			}
		})
	}
});
/* click success lessson */
$(document).on('ifChanged','.check-pass', function(){
	liCurrent = $(this).closest('li');
	if($('input[name=pass]').is(':checked')) {
		let id = $('input[name=pass]').val();
		$.ajax({
			url: 'learning/'+id,
			type: 'PUT',
			dataType: 'JSON',
			data: {
				type: 'pass'
			},
			success: function(data){
				let value = JSON.parse(JSON.stringify(data));
				notification(value.type,value.title,value.content);
				if(value.type == 'success') {
					liCurrent.append('<i class="fa fa-check pass-success"></i>');
					liCurrent.find('.check-pass').remove();
				}
			}
		})
	}
});
let w_width = $(window).width();
/* count result */
$('.quicktest-list-answer li:eq(0)').addClass('active');
function quicktestResult(){
	let countResult = 0;
	$(".quicktest-item input[type='radio']").each(function(){
		if($(this).is(":checked")){
			countResult++;
			let index_quicktest_item = $(this).closest('.quicktest-item').index();
			$('.quicktest-list-answer li:eq(' +index_quicktest_item+')').addClass('answered');
		}
	});
	$('#number-result').text(countResult);
}
$(document).on('click',".quicktest-item input[type='radio']",function(){
	quicktestResult();
});	
/* quicktest hint */
$(document).on('click',".bg-hint",function(){
	if($('.quicktest-hint').length > 0) checkHint();
	else swal('Bài thi không cho gợi ý');
});
function checkHint() {
	$('.quicktest-hint').toggleClass('none');
}
/* perfect bar */
if($('#lesson')[0]) new PerfectScrollbar($('#lesson')[0]);
$(document).on('click',".nav-tabs a",function(){
	let value = $(this).attr('href');
	new PerfectScrollbar($(''+value)[0]);
});
$(document).on('click','.quicktest-list-answer li', function(){
	nth = $(this).index();
	clearAvtive();
	if($(this).hasClass('backview')) $(this).removeClass('backview');
	$(this).addClass('active');
	let num = $(this).index();
	move = num * w_value;
	$('.quicktest-slide').css('margin-left','-'+move+'px');
});
/* back view */
$(document).on('click','.bg-back-view', function(){
	$('.quicktest-list-answer li:nth('+nth+')').toggleClass('active');
	$('.quicktest-list-answer li:nth('+nth+')').toggleClass('backview');
});
setHeight();
function setHeight(){
	let h_video = $('.plyr').height();
	let w_height = $(window).height();
	let w_width = $(window).width();
	if(w_width < 768) {
		$('.lesson-list').removeClass('lesson-show');
		$('.lesson-list').hide();
	}
	let height_lesson_menu = $('.lesson-menu').height();
	if(w_width <= 1200) height_lesson_menu += 10;
	$('.box-lesson').css('cssText', 'height: ' + w_height + 'px !important');
	$('.lessons .tab-content > div').css('height', (w_height -  height_lesson_menu - $('.nav-tabs').height()) + 'px');
}
let w_value = null;
let max = null;
let move = 0;
$(window).resize(function(){
	setHeight();
	sizeQuicktet();
	if($(window).width() > 768) {
		$('.lesson-list').removeClass('lesson-hide');
		$('.lesson-list').addClass('lesson-show');
		setCol();
		$('.lesson-list').show();
	}
});
/* custom size */
sizeQuicktet();
function sizeQuicktet() {
	if($('.lesson-quicktest')[0]) {
		let w_lesson_content = $('.lesson-content').width();
		let col = $('.lesson-quicktest > div').attr('class').split(" ")[0].split("-")[2];
		w_value = (w_lesson_content / 12) * col;
		if(w_lesson_content < 768) w_value = (w_lesson_content / 12) * 11;
		/* process w_value if is number decimal*/
		let string_value = w_value.toString();
		if(string_value.indexOf('.') >= 0) {
			string_value = string_value.substr(0, string_value.indexOf('.'));
			w_value = parseInt(string_value);
		}
		$('.quicktest-list, .quicktest-item').css('width', w_value);
		/* process slide */
		let li_index = $('.quicktest-list-answer li.active').index();
		let value_margin = li_index * w_value;
		move = value_margin;
		$('.quicktest-slide').css('margin-left', '-'+value_margin+'px');
		max = $('.quicktest-item').length * w_value - w_value;
	}
}
/* quicktest slide */
			let nth = 0 // nth of li quicktest-list-answer
			function clearAvtive(){
				$('.quicktest-list-answer li').each(function(){
					$(this).removeClass('active');
				});
			}
			function active(val){
				nth = (val / w_value);
				$('.quicktest-list-answer li:nth('+nth+')').addClass('active');
			}
			function nextQuestion(){
				clearAvtive();
				if(move == max) move = 0;
				else move += w_value;
				$('.quicktest-slide').css('margin-left','-'+move+'px');
				active(move);
			}
			function prevQuestion(){
				clearAvtive();
				if(move == 0) move = max;
				else move -= w_value;
				$('.quicktest-slide').css('margin-left','-'+move+'px');
				active(move);
			}
			$(document).on('click','#quicktest-next',function(){
				nextQuestion();
			});
			$(document).on('click','#quicktest-prev',function(){
				prevQuestion();
			});
			/* set col class box-lesson */
			function setCol(){
				let box_lesson = $('.box-lesson');
				let lesson_list = $('.lesson-list');
				if(lesson_list.hasClass('lesson-hide')){
					box_lesson.removeClass('col-lg-9');
					box_lesson.addClass('col-lg-12');
				}
				else {
					box_lesson.removeClass('col-lg-12');
					box_lesson.addClass('col-lg-9');
				}
				sizeQuicktet();
			}
			/* set hide or show class lesson-list */
			function setLessonList(){
				let w_width = $(window).width();
				let lesson_list = $('.lesson-list');
				if(w_width > 768) {
					lesson_list.show();
					if(lesson_list.hasClass('lesson-show')){
						lesson_list.removeClass('lesson-show');
						lesson_list.addClass('lesson-hide');
					}
					else {
						lesson_list.removeClass('lesson-hide');
						lesson_list.addClass('lesson-show');
					}
				}
				else {
					if($('.lesson-quicktest')[0]) $('.quicktest-list-answer').toggleClass('none');
					else $('.lesson-list').slideToggle();
				}
			}
			/* menu bar */
			$(document).on('click','.lesson-toggle',function(){	
				setLessonList();
				setCol();
			});
			$(document).on('click','#lesson-next',function(){
				let w_width = $(window).width();
				if(w_width <= 768){
					nextQuestion();
				}
				else {
					let index = liCurrent.index();
					$('#lesson > ul li:not(".chapter"):eq('+index+')').trigger('click');
				}
			});
			$(document).on('click','#lesson-prev',function(){
				let w_width = $(window).width();
				if(w_width <= 768){
					prevQuestion();
				}
				else {
					let index = liCurrent.index();
					if(index > 1) {
						index-= 2;
						$('#lesson > ul li:not(".chapter"):eq('+index+')').trigger('click');
					}
				}
			});
		</script>