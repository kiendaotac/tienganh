
<div class="col-lg-3 col-sm-4 col-sm-12 lesson-list lesson-show">
	<div class="lessons">
		<ul class="nav nav-tabs">
			<li class="active"><a data-toggle="tab" href="#lesson"><i class="fa fa-leanpub"></i><span>Bài giảng</span></a></li>
			<li><a data-toggle="tab" href="#file"><i class="fa fa-file"></i><span>Tài liệu</span></a></li>
			<li><a data-toggle="tab" href="#note"><i class="fa fa-comment"></i><span>Ghi chú</span></a></li>
		</ul>
		<div class="tab-content">
			<div id="lesson" class="tab-pane fade in active">
				<ul>
					@foreach ($listLesson as $item)
					@if ($item->parent_id == 0)
					<li class="chapter"><div><i class="fa fa-bars"></i><span>{{$item->name}}</span></div></li>
					@else
					<li data-id="{{$item->id}}"><div>@php
					switch ($item->type) {
						case 'lesson':
						if(isset($item->content[0])) {
							switch($item->content[0]->type){
								case 'video':
								echo '<i class="fa fa-play-circle"></i>';
								break;
								case 'audio':
								echo '<i class="fa fa-volume-up"></i>';
								break;
								case 'text':
								echo '<i class="fa fa-book"></i>';
								break;
							}
						}
						break;
						case 'test':
						echo '<i class="fa fa-clock-o"></i>';
						break;
					}
					@endphp<span>{{$item->name}}</span></div>@if ($item->status['pass'] == 1)
					<i class="fa fa-check pass-success"></i>
				@endif</li>
				@endif
				@endforeach
			</ul>
		</div>
		<div id="file" class="tab-pane fade">
			<ul>
			</ul>
		</div>
		<div id="note" class="tab-pane fade">
			<div class="note-add">
				<i class="fa fa-plus"></i>
				<textarea placeholder="Nhập ghi chú và nhấn nút +"></textarea>
			</div>
			<div class="note-list">
				<ul>
				</ul>
			</div>
		</div>
	</div>
</div>
</div>