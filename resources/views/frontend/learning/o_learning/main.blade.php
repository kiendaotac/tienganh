
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>{{ $curriculum->name . ' - ' .\Illuminate\Support\Facades\Crypt::decryptString(config('custom.app_name'))}}</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="csrf-token" content="{{ csrf_token() }}" />
	<base href="{{ asset('path') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/o_learning.css') }}">
	<link href="{!! asset('css/bootstrap.min.css') !!} " rel="stylesheet">
	<link href="{!! asset('font-awesome/css/font-awesome.css') !!}" rel="stylesheet">
	<link rel="stylesheet" href="https://cdn.plyr.io/3.3.23/plyr.css">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet"
	type="text/css"/>
	<link rel="stylesheet" type="text/css" href="{{ asset('css/perfect-scrollbar.css') }}">
	<!-- Sweet Alert -->
	<link href="{!! asset('css/plugins/sweetalert/sweetalert.css') !!}" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/plugins/iCheck/custom.css') }}">
	<link href="{!! asset('css/plugins/toastr/toastr.min.css') !!}" rel="stylesheet">
	<script src="{!! asset('js/jquery-3.1.1.min.js') !!}"></script>
	<script src="{!! asset('js/bootstrap.min.js') !!}"></script>
	<script src="https://cdn.plyr.io/3.3.23/plyr.js"></script>
	<!-- Perfect scroll -->
	<script src="{{ asset('js/perfect-scrollbar.min.js') }}"></script>
	<!-- Sweet alert -->
	<script src="{!! asset('js/plugins/sweetalert/sweetalert.min.js') !!}"></script>
	<script src="{!! asset('js/plugins/iCheck/icheck.min.js') !!}"></script>
	<!-- Toastr -->
	<script src="{!! asset('js/plugins/toastr/toastr.min.js') !!}"></script>
</head>
<body> 
	<section class="learn">
				<!-- <div class="logo">
					<img src="logo-ictuedu.png" alt="ictuedu">
				</div> -->
				<div class="container-fluid">
					<div class="row">
						<div class="col-lg-9 box-lesson">
							<div class="lesson-content"></div>
							<div class="lesson-menu">
								<div>
									<i class="fa fa-home" title="Về trang chủ"></i>
								</div>
								<div>
									<i class="fa fa-chevron-circle-left" title="Bài trước" id="lesson-prev"></i>
									<i class="fa fa-chevron-circle-right" title="Bài tiếp" id="lesson-next"></i>
								</div>
								<div>
									<i class="fa fa-bars lesson-toggle" title="Ẩn/hiện bài giảng"></i>
								</div>
							</div>
						</div>
						@include('frontend.learning.o_learning.inc.list', ['lessons' => $listLesson])
					</div>
				</div>
			</section>
			<script>
				function notification(type, title, content) {
					title = '';
					setTimeout(function () {
						toastr.options = {
							closeButton: true,
							progressBar: true,
							showMethod: 'slideDown',
							timeOut: 4000
						};
						switch (type) {
							case 'success'  :
							toastr.success(content, title);
							break;
							case 'error'    :
							toastr.error(content, title);
							break;
							case 'warning'  :
							toastr.warning(content, title);
							break;
							default         :
							toastr.warning('Không xác định được thông báo', 'Cảnh báo!');
							break;
						}

					}, 1300);
				}
			</script>
			@include('frontend.learning.o_learning.inc.custom_js')
		</body>
		</html>