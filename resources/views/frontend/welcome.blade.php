<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="UTF-8">
    <title>Home</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('css/student/welcome.css') }}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</head>
<body>
    <div class="header container-fluid">
        <div class="row h-100">
            @if(Auth::guard('student')->check())
                <input type="hidden" id="nameuser" value="{{Auth::guard('student')->user()->firstname}} {{Auth::guard('student')->user()->lastname}}">
                <input type="hidden" id="uniuser" value="{{Auth::guard('student')->user()->unis->name}}">
            @endif
            <div class="logo col-md-2 col-sm-6 col-4 d-flex align-items-center">
                <a href="/home"><img src="{{ asset('img/student/logo.png') }}" alt=""></a>      
            </div>
            <div class="col-md-7 d-flex align-items-center header-menu">
                <ul class="nav">
                    <li class="nav-item"><a class="nav-link" href="">trang chủ</a></li>
                    <li class="nav-item"><a class="nav-link gotoIntro" href="#">giới thiệu</a></li>
                    <li class="nav-item"><a id="gotoLearn" class="nav-link" href="#">học tiếng anh theo chủ đề</a></li>
                    <li class="nav-item"><a  class="nav-link" href="">luyện thi</a></li>
                    <li class="nav-item"><a id="gotoContact" class="nav-link" href="#">liên hệ</a></li>
                </ul>
            </div>
            <div class="col-md-3 col-sm-6 col-8 d-flex align-items-center header-button">
                <ul class="nav ml-auto">
                    <li class="nav-item d-flex align-items-center pr-2" id="search"><i class="fas fa-search"></i></li>
                    <li class="nav-item d-flex align-items-center"><a class="nav-link" href="#"><i class="fas fa-question-circle"></i> Trợ giúp</a></li>
                    @if(Auth::guard('student')->check())
                     <li class="nav-item d-flex align-items-center logged">
                        <a href="/students" style="text-decoration: none;" class="fs-12 text-dar" id="linkInfo">
                            <img width="30" height="30" class="rounded-circle" src="{{Auth::guard('student')->user()->avatar}}" alt="">
                            {{Auth::guard('student')->user()->lastname}}
                        </a>
                        
                        <a title="Đăng xuất" class="ml-1" href="/logout"><i class="fas fa-sign-out-alt"></i></a></li> 
                    @else
                    <li class="nav-item"><a class="nav-link" href="{{ route('get.student.login') }}"><i class="far fa-user-circle"></i> Đăng nhập</a></li>
                    @endif
                   
                    <li class="nav-item pl-3 button-menu"><i class="fas fa-bars"></i></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="banner">
        <div class="banner-detail container">
            <div class="row h-100 d-flex justify-content-center align-items-center">
                <div class="pt-5">
                    <h3 class="mb-4 mt-5 font-weight-bold">LUYỆN TIẾNG ANH <span>ONLINE</span> CÙNG <span>ICT</span><span>eDu</span> </h3>
                    <div class="text-banner mb-5">
                        <p class="m-0 font-italic">Giúp sinh viên trang bị kiến thức, tự tin khi bước vào các kỳ thi chuẩn năng lực ngoại ngữ theo khung CEFR và NLNN 6 bậc</p>
                        <p class="m-0 font-italic mb-5">Còn chờ gì nữa, hãy đăng ký và cùng nhau ôn luyện thôi</p>
                    </div>
                    <a href=""><button class="btn mt-5">Bạn chưa có tài khoản? <span>CLICK</span> để đăng ký</button></a>
                </div>
                
            </div>
        </div>
    </div>
    <div class="introduce" id="intro">
        <div class="container banner-item">
            <div class="row">
                <div class="col-md-4 d-flex align-items-center flex-column justify-content-center text-center">
                    <img class="mb-4" src="{{ asset('img/student/bn-1.png') }}">
                    <h3>giảng viên có nhiều kinh nghiệm</h3>
                    <p>Giảng viên là những người có kinh nghiệm sẵn sàng giúp đỡ sinh viên khi gặp khó khăn trong quá trình ôn luyện trên hệ thống</p>
                </div>
                <div class="col-md-4 d-flex align-items-center flex-column justify-content-center text-center">
                    <img class="mb-4" src="{{ asset('img/student/bn-2.png') }}">
                    <h3>thư viện bài giảng chuẩn</h3>
                    <p>Kho bài giảng được xây dụng theo các chủ đề giúp sinh viên dễ dàng ôn luyện nắm vững ngữ pháp nâng cao kỹ năng trong giao tiếp tiếng anh</p>
                </div>
                <div class="col-md-4 d-flex align-items-center flex-column justify-content-center text-center">
                    <img class="mb-4" src="{{ asset('img/student/bn-3.png') }}">
                    <h3>ngân hàng đề thi phong phú</h3>
                    <p>Hệ thống ngân hàng đề thi được thiết kế chuẩn theo Khung Châu Âu (CEFR) và theo Khung NLNN 6 bậc dành cho Việt Nam</p>
                </div>
            </div>
        </div>
        <div class="container mt-5 pt-3 pb-3 ">
            <div class="row">
                <div  id="logouniversity" class="carousel slide col-md-12 " data-ride="carousel">
                  <div class="carousel-inner border-top">
                    <div class="carousel-item active">
                      <div class="col-md-12">
                        <ul class="nav w-100">
                            <li class="nav-item"><a class="nav-link" href="#"><img src="{{ asset('img/student/intro-1.png') }}"></a></li>
                            <li class="nav-item"><a class="nav-link" href="#"><img src="{{ asset('img/student/intro-2.png') }}"></a></li>
                            <li class="nav-item"><a class="nav-link" href="#"><img src="{{ asset('img/student/intro-3.png') }}"></a></li>
                            <li class="nav-item"><a class="nav-link" href="#"><img src="{{ asset('img/student/intro-4.png') }}"></a></li>
                            <li class="nav-item"><a class="nav-link" href="#"><img src="{{ asset('img/student/intro-1.png') }}"></a></li>
                            <li class="nav-item"><a class="nav-link" href="#"><img src="{{ asset('img/student/intro-2.png') }}"></a></li>
                        </ul>
                      </div>
                    </div>
                    <div class="carousel-item">
                      <div class="col-md-12">
                        <ul class="nav w-100">
                            <li class="nav-item"><a class="nav-link" href="#"><img src="{{ asset('img/student/intro-1.png') }}"></a></li>
                            <li class="nav-item"><a class="nav-link" href="#"><img src="{{ asset('img/student/intro-2.png') }}"></a></li>
                            <li class="nav-item"><a class="nav-link" href="#"><img src="{{ asset('img/student/intro-3.png') }}"></a></li>
                            <li class="nav-item"><a class="nav-link" href="#"><img src="{{ asset('img/student/intro-4.png') }}"></a></li>
                            <li class="nav-item"><a class="nav-link" href="#"><img src="{{ asset('img/student/intro-1.png') }}"></a></li>
                            <li class="nav-item"><a class="nav-link" href="#"><img src="{{ asset('img/student/intro-2.png') }}"></a></li>
                        </ul>
                      </div>
                    </div>
                    <div class="carousel-item">
                      <div class="col-md-12">
                        <ul class="nav w-100">
                            <li class="nav-item"><a class="nav-link" href="#"><img src="{{ asset('img/student/intro-1.png') }}"></a></li>
                            <li class="nav-item"><a class="nav-link" href="#"><img src="{{ asset('img/student/intro-2.png') }}"></a></li>
                            <li class="nav-item"><a class="nav-link" href="#"><img src="{{ asset('img/student/intro-3.png') }}"></a></li>
                            <li class="nav-item"><a class="nav-link" href="#"><img src="{{ asset('img/student/intro-4.png') }}"></a></li>
                            <li class="nav-item"><a class="nav-link" href="#"><img src="{{ asset('img/student/intro-1.png') }}"></a></li>
                            <li class="nav-item"><a class="nav-link" href="#"><img src="{{ asset('img/student/intro-2.png') }}"></a></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content pt-5 pb-5">
        <div class="container">
            <div class="row">
                <div class="col-md-12 mb-2">
                    <h3>Chủ đề học tiêu biểu</h3>
                </div>
                <div class="col-md-12">
                    <div class="row">
                        @foreach($curri as $value)
                            <div class="col-md-6 col-lg-3 mt-4">
                                <div class="border content-item">
                                    <a href="home/{{$value->slug}}-{{$value->id}}">
                                    <div class="content-img-item">
                                        <img src="{{$value->img}}" alt="">
                                    </div>
                                    </a>
                                    <div class="content-info-item pl-3 pr-3 pt-2 pb-2">
                                        <div class="mb-2"><a href="home/{{$value->slug}}-{{$value->id}}">{{$value->name}}</a></div>
                                        <a class="fs-14" href="/{{$value->lesson['slug']}}-{{$value->lesson['id']}}">{{$value->lesson['name']}}</a>
                                    </div>
                                    <div class="content-footer clearfix border-top">
                                        <div class="float-left h-100 d-flex align-items-center pl-3">
                                            <p class="m-0 text-success"><i class="far fa-user"></i>{{$value->user->display_name}}</p>  
                                        </div>
                                        <div class="float-right h-100 d-flex align-items-center amount pr-3">
                                            <i class="fas fa-eye mr-1"></i><span> 255</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="col-md-12 mt-3 text-right">
                    <a href="#"><button class="btn border-0">Xem tất cả các chủ đề <i class="fas fa-long-arrow-alt-right"></i></button></a>
                </div>
            </div>
        </div>
    </div>
    <div class="reason pt-5 pb-5">
        <div class="container">
            <div class="row h-100">
                <div class="col-md-12 text-center reason-title mb-3">
                    <h3>CẢM NHẬN TỪ HỌC VIÊN</h3>
                    <p class="font-italic">Cảm nhận của các học viên đã từng học tập, ôn luyện tiếng anh để nâng cao trình độ tiếng Anh của mình</p>
                </div>
                <div id="carouselExampleIndicators" class="carousel slide col-md-12" data-ride="carousel">
                  <ol class="carousel-indicators reason-slide-button">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                  </ol>
                  <div class="carousel-inner">
                    <div class="carousel-item active">
                        <div class="row">
                            <div class="col-md-4 student-comment-item text-center">
                                <div class="avt-student-comment mx-auto d-flex justify-content-center align-items-center rounded-circle">
                                    <img class="rounded-circle" src="{{ asset('img/student/a6.jpg') }}" alt="">
                                </div>
                                <div>
                                    <p class="mb-3">Thực sự mình thấy nguồn tài nguyên tại đây có nội dung rất sát với khối kiến thức chuẩn đầu ra A2 mà mình đã thi</p>
                                    <p class="mb-4"><span class="font-weight-bold">Nguyễn Anh</span> <br> SV Trường Đại học Sư Phạm Thái Nguyên</p>
                                </div>
                                <div class="clear-box"></div>
                            </div>
                            <div class="col-md-4 student-comment-item text-center">
                                <div class="avt-student-comment mx-auto d-flex justify-content-center align-items-center rounded-circle">
                                    <img class="rounded-circle" src="{{ asset('img/student/a6.jpg') }}" alt="">
                                </div>
                                <div>
                                    <p>Mình đã thi đỗ chuẩn đầu ra với kết quả cao nhờ việc chăm chỉ ôn luyện tiếng anh cùng ICT-eDu</p>
                                    <p class="mb-4"><span class="font-weight-bold">Nguyễn Anh</span> <br> SV Trường Đại học Công nghiệp</p>
                                </div>
                            </div>
                            <div class="col-md-4 student-comment-item text-center">
                                <div class="avt-student-comment mx-auto d-flex justify-content-center align-items-center rounded-circle">
                                    <img class="rounded-circle" src="{{ asset('img/student/a6.jpg') }}" alt="">
                                </div>
                                <div>
                                    <p>Toàn bộ bài giảng được các thầy cô dựng theo chủ đề và quay video trực quan giúp mình dễ ôn luyện hơn</p>
                                    <p class="mb-4"><span class="font-weight-bold">Nguyễn Anh</span> <br> SV Trường Đại học Y</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="row">
                            <div class="col-md-4 student-comment-item text-center">
                                <div class="avt-student-comment mx-auto d-flex justify-content-center align-items-center rounded-circle">
                                    <img class="rounded-circle" src="{{ asset('img/student/a6.jpg') }}" alt="">
                                </div>
                                <div>
                                    <p>Thực sự mình thấy nguồn tài nguyên tại đây có nội dung rất sát với khối kiến thức chuẩn đầu ra A2 mà mình đã thi</p>
                                    <p class="mb-4"><span class="font-weight-bold">Nguyễn Anh</span> <br> SV Trường Đại học Sư Phạm Thái Nguyên</p>
                                </div>
                            </div>
                            <div class="col-md-4 student-comment-item text-center">
                                <div class="avt-student-comment mx-auto d-flex justify-content-center align-items-center rounded-circle">
                                    <img class="rounded-circle" src="{{ asset('img/student/a6.jpg') }}" alt="">
                                </div>
                                <div>
                                    <p>Mình đã thi đỗ chuẩn đầu ra với kết quả cao nhờ việc chăm chỉ ôn luyện tiếng anh cùng ICT-eDu</p>
                                    <p class="mb-4"><span class="font-weight-bold">Nguyễn Anh</span> <br> SV Trường Đại học Công nghiệp</p>
                                </div>
                            </div>
                            <div class="col-md-4 student-comment-item text-center">
                                <div class="avt-student-comment mx-auto d-flex justify-content-center align-items-center rounded-circle">
                                    <img class="rounded-circle" src="{{ asset('img/student/a6.jpg') }}" alt="">
                                </div>
                                <div>
                                    <p>Toàn bộ bài giảng được các thầy cô dựng theo chủ đề và quay video trực quan giúp mình dễ ôn luyện hơn</p>
                                    <p class="mb-4"><span class="font-weight-bold">Nguyễn Anh</span> <br> SV Trường Đại học Y</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="row">
                            <div class="col-md-4 student-comment-item text-center">
                                <div class="avt-student-comment mx-auto d-flex justify-content-center align-items-center rounded-circle">
                                    <img class="rounded-circle" src="{{ asset('img/student/a6.jpg') }}" alt="">
                                </div>
                                <div>
                                    <p>Thực sự mình thấy nguồn tài nguyên tại đây có nội dung rất sát với khối kiến thức chuẩn đầu ra A2 mà mình đã thi</p>
                                    <p class="mb-4"><span class="font-weight-bold">Nguyễn Anh</span> <br> SV Trường Đại học Sư Phạm Thái Nguyên</p>
                                </div>
                            </div>
                            <div class="col-md-4 student-comment-item text-center">
                                <div class="avt-student-comment mx-auto d-flex justify-content-center align-items-center rounded-circle">
                                    <img class="rounded-circle" src="{{ asset('img/student/a6.jpg') }}" alt="">
                                </div>
                                <div>
                                    <p>Mình đã thi đỗ chuẩn đầu ra với kết quả cao nhờ việc chăm chỉ ôn luyện tiếng anh cùng ICT-eDu</p>
                                    <p class="mb-4"><span class="font-weight-bold">Nguyễn Anh</span> <br> SV Trường Đại học Công nghiệp</p>
                                </div>
                            </div>
                            <div class="col-md-4 student-comment-item text-center">
                                <div class="avt-student-comment mx-auto d-flex justify-content-center align-items-center rounded-circle">
                                    <img class="rounded-circle" src="{{ asset('img/student/a6.jpg') }}" alt="">
                                </div>
                                <div>
                                    <p>Toàn bộ bài giảng được các thầy cô dựng theo chủ đề và quay video trực quan giúp mình dễ ôn luyện hơn</p>
                                    <p class="mb-4"><span class="font-weight-bold">Nguyễn Anh</span> <br> SV Trường Đại học Y</p>
                                </div>
                            </div>
                        </div>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
    <div class="info">
        <div class="container">
            <div class="row">
                <div class="col-md-4 text-white pt-5 pb-5">
                    <h3 class="mb-4">GIỚI THIỆU</h3>
                    <p>
                        Nguồn học liệu hỗ trợ dậy và học tiếng anh dành cho các đối tượng sinh viên khung chuyên ngữ, được xây dựng bơi đội ngũ Giảng viên tiếng Anh của các trường Đại học thành viên thuộc Đại học Thái Nguyên.
                    </p>
                    <p>
                        Website và phần mềm thi được thiết kế và xây dựng bởi <span class="font-weight-bold font-italic">Trung tâm Phát triển phần mềm</span>, sản phẩm được tài trợ nguồn kinh phí từ dự án Ngoại ngữ 2020 của Bộ Giáo dục và Đào tạo.
                    </p>
                    <p class="pt-2">
                        <img class="mr-3" src="{{ asset('img/student/fb.png') }}" alt="">
                        <img src="{{ asset('img/student/yt.png') }}" alt="">
                    </p>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-3 text-white pt-5 pb-5">
                    <h3 class="mb-4">LIÊN KẾT</h3>
                    <ul class="nav ">
                        <li class="nav-item w-100">
                            <a class="nav-link pt-0" href="#">- Trang chủ</a>
                        </li>
                        <li class="nav-item w-100">
                            <a class="nav-link gotoIntro" href="#">- Giới thiệu</a>
                        </li>
                        <li class="nav-item w-100">
                            <a class="nav-link" href="">- Học tiếng anh theo chủ đề</a>
                        </li>
                        <li class="nav-item w-100">
                            <a class="nav-link" href="">- Luyện thi chuẩn NLNN 6 bậc</a>
                        </li>
                        <li class="nav-item w-100">
                            <a class="nav-link" href="">- Đăng ký tài khoản</a>
                        </li>
                        <li class="nav-item w-100">
                            <a class="nav-link" href="">- Hướng dẫn sử dụng</a>
                        </li>
                        <li class="nav-item w-100">
                            <a class="nav-link" href="">- Điều khoản sử dụng</a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4 text-white pt-5 pb-5">
                    <h3 class="mb-4">LIÊN HỆ VỚI CHÚNG TÔI</h3>
                    <p class="mb-2"><span class="font-weight-bold">Trung tâm Phát triển phần mềm, Trường Đại học CNTT và Truyền thông.</span></p>
                    <p class="mb-2">-<span class="font-italic">Đ/c: Xã Quyết Thắng,TP. Thái Nguyên</span></p>
                    <p class="mb-2">-<span class="font-italic">Email:nvsu@ictu.edu.vn</span></p>
                    <p>-<span>Phone:0280 6 559968</span></p>

                    <p class="mb-2 mt-3"><span class="font-weight-bold">Văn phòng đề án Ngoại ngữ</span></p>
                    <p class="mb-2">-<span class="font-italic">Đ/c: Tổ xxx, Phường YYY - TP. Thái Nguyên</span></p>
                    <p class="mb-2">-<span class="font-italic">Email:vpnn2020@tnu.edu.vn</span> </p>
                    <p>-<span>Phone:02803 33333x</span></p>
                </div>
            </div>
        </div>
    </div>
    <div class="footer bg-white">
        <div class="container h-100">
            <div class="row h-100 text-dark">
                <div class="col-md-12 h-100 d-flex justify-content-center align-items-center">2017-2018 &copy; Trung tâm phần mềm</div>
            </div>
        </div>
    </div>
    <form class="search">
        <div class="d-flex justify-content-center align-items-end h-50">
            <input class="pl-2" type="text" placeholder="Nhập tên khóa học hoặc giảng viên bạn muốn tìm" name="search">
            <button><i class="fas fa-search"></i></button>
        </div>
        
        <div class="d-flex justify-content-center align-items-start h-50">
            <ul class="nav bg-white rounded-bottom pl-1" id="result-search">
                
            </ul>
        </div>
    </form>
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            
            $(".gotoIntro").click(function (){
                $('html, body').animate({
                    scrollTop: $(".banner-item").offset().top-50
                }, 500);
            });
            $("#gotoLearn").click(function (){
                $('html, body').animate({
                    scrollTop: $(".content").offset().top-50
                }, 500);
            });
            $("#gotoContact").click(function (){
                $('html, body').animate({
                    scrollTop: $(".info").offset().top
                }, 500);
            });
            
            
            $(window).scroll(function(event) {
                if ($(window).scrollTop()>50) {
                    $(".header").css({
                        opacity: '1',
                    });
                }else {
                    $(".header").css({
                        opacity: '0.9',
                    });
                }
            });
            $("#search").click(function() {
                $(".search").css({
                    transform: 'scale(1)',
                });
                $('body').css({
                    height: '100%',
                    overflow: 'hidden',
                    width: '100%',
                    position: 'fixed',
                });
            });
            $(".search").mouseover(function() {
                $("form").css('cursor','url(../../img/student/cursor.png),auto');
            });
            $(".search").click(function(e) {
                let input = e.target.closest('.search input');
                if(input != null) return false;
                else {
                    $(".search").css({transform: 'scale(0)',});
                    $('body').css({
                            height: 'auto',
                        overflow: 'auto',
                        width: 'auto',
                        position: 'unset',
                      });
                }
                
            });

            $('body').on('click', '.button-menu', function() {
                let content = '<div class="menu-mobile">'
                + '<div class="avt-user-menumobile">'
                // + '<i class="fas fa-user-circle"></i>'
                // + '<p class="m-0 fs-12 mt-3">Bạn chưa đăng nhập!</p>'
                + '<div class="mx-auto bg-white border-avt-user-menumobile rounded-circle d-flex align-items-center justify-content-center">'
                + '<img width="60" height="60" class="rounded-circle" src="{{ asset('img/student/a6.jpg') }}" alt="">'
                + '</div>'
                + '<p class="m-0 fs-12 font-weight-bold">Đỗ Khương Duy</p>'
                + '<p class="m-0 fs-12 font-italic">Trường ĐH CNTT & TT</p>'
                + '</div>'
                + '<ul class="menuMobileItem">'
                + '<li class="border-top"><a href="/student"><i class="fas fa-id-card"></i> Thông tin hồ sơ</a></li>'
                + '<li class="border-top"><a href="#"><i class="fas fa-check-square"></i> Khóa học đã đăng ký</a></li>'
                + '<li class="border-top"><a href="#"><i class="fab fa-leanpub"></i> Học theo chủ đề</a></li>'
                + '<li class="border-top"><a href="#"><i class="fab fa-leanpub"></i> Luyện thi</a></li>'
                + '<li class="border-top"><a href="#"><i class="fab fa-centercode"></i> Kết quả luyện thi</a></li>'
                + '<li class="border-top"><a href="#"><i class="fas fa-info-circle"></i> Hướng dẫn sử dụng</a></li>'

                + '<li class="border-top"><a href="#"><i class="fas fa-sign-in-alt"></i> Đăng nhập</a></li>'
                + '<li class="border-top border-bottom"><a href="#"><i class="fas fa-sign-out-alt"></i> Đăng xuất</a></li>'
                + '</ul>'
                + '<div class="text-center">'
                + '<i class="fab fa-facebook"></i>'
                + '<i class="fab ml-3 mr-3 fa-youtube-square"></i>'
                + '<p class="fs-12"> 2017-2018 &copy;Trung tâm phát triển phần mềm CNTT & TT</p>'
                + '</div>'
                + '</div>'
                $('body').append(content);
                $('body').addClass('bg-page');
                $('.menu-mobile').animate({right: '0'},"fast");
                $('body').css({
                    height: '100%',
                    overflow: 'hidden',
                    width: '100%',
                    position: 'fixed',
                });
            });

            document.addEventListener("touchstart", function(e){
                let btnmenuMobile = e.target.closest('.button-menu');
                let menuMoblie = e.target.closest('.menu-moblie');
                let avt = e.target.closest('.avt-user-menumobile');
                let search = e.target.closest('#search');
                let menuLi = e.target.closest('.menuMobileItem');
                if(btnmenuMobile != null || menuMoblie != null || menuLi!=null || avt !=null || search !=null) return false;
                else {
                    $('.menu-mobile').animate({right: "-500px"});
                    setTimeout(function(){
                        $('.menu-mobile').remove();
                        $('body').removeClass('bg-page');
                        $('body').css({
                            height: 'auto',
                            overflow: 'auto',
                            width: 'auto',
                            position: 'unset',
                        });
                    },0);
                }
            })
            $(window).resize(function() {
                if ($(window).width()<517) {
                    $('.content-info-item>a').each(function(index, el) {
                        if ($(this).text().length>120) {
                            $sub=$(this).text().substring(0, 120);
                            $(this).text($(this).text().substring(0, $sub.lastIndexOf(" "))+"...");
                        }
                    });
                }else {
                    if ($(window).width()<768) {
                        $('.content-info-item>a').each(function(index, el) {
                            if ($(this).text().length>270) {
                                $sub=$(this).text().substring(0, 270);
                                $(this).text($(this).text().substring(0, $sub.lastIndexOf(" "))+"...");
                            }
                        });
                    }else {
                        if ($(window).width()<992) {
                            $('.content-info-item>a').each(function(index, el) {
                                if ($(this).text().length>165) {
                                    $sub=$(this).text().substring(0, 165);
                                    $(this).text($(this).text().substring(0, $sub.lastIndexOf(" "))+"...");
                                }
                            });
                        }else {
                            if ($(window).width()<1200) {
                                $('.content-info-item>a').each(function(index, el) {
                                    if ($(this).text().length>90) {
                                        $sub=$(this).text().substring(0, 90);
                                        $(this).text($(this).text().substring(0, $sub.lastIndexOf(" "))+"...");
                                    }
                                });
                            }else {
                                if ($(window).width()>1200) {
                                    $('.content-info-item>a').each(function(index, el) {
                                        if ($(this).text().length>120) {
                                            $sub=$(this).text().substring(0, 120);
                                            $(this).text($(this).text().substring(0, $sub.lastIndexOf(" "))+"...");
                                        }
                                    });
                                }
                            }
                        }
                    }
                }
                if ($(window).width() < 767) {
                    $('.text-banner').removeClass('mb-5');
                    $('.text-banner p').removeClass('mb-5');
                    $('.banner pt-5').addClass('pt-3')
                    $('.banner>.banner-detail>.row>.pt-5').addClass('pt-3');
                    $('.banner>.banner-detail>.row>.pt-5').removeClass('pt-5');
                }else {
                    if ($(window).width() < 855) {
                        $('.banner-detail button').removeClass('mt-5');
                        $('.banner-detail button').addClass('mt-3');
                        $('.header-menu li').removeClass('mr-3');
                    }else{
                        if($(window).width() < 909){
                            $('.header-menu li').removeClass('mr-3');
                            console.log('a');  
                        }else{
                            if ($(window).width() < 992) {
                            $('.header-menu li').removeClass('mr-3');   
                            }else {
                                $('.header-menu li').addClass('mr-3');
                            }
                        }
                    }
                }
            });
            if ($(window).width() < 767) {
                $('.text-banner').removeClass('mb-5');
                $('.text-banner p').removeClass('mb-5');
                $('.banner pt-5').addClass('pt-3')
                $('.banner>.banner-detail>.row>.pt-5').addClass('pt-3');
                $('.banner>.banner-detail>.row>.pt-5').removeClass('pt-5');
            }else {
                if ($(window).width() < 855) {
                    $('.banner-detail button').removeClass('mt-5');
                    $('.banner-detail button').addClass('mt-3');
                    $('.header-menu li').removeClass('mr-3');
                }else{
                    if($(window).width() < 909){
                        $('.header-menu li').removeClass('mr-3');   
                    }else{
                        if ($(window).width() < 992) {
                        $('.header-menu li').removeClass('mr-3');   
                        }else {
                            $('.header-menu li').addClass('mr-3');
                        }
                    }
                    
                }
            }
            // if ($(window).width() < 767) {
            //     $('.text-banner').removeClass('mb-5');
            //     $('.text-banner p').removeClass('mb-5');
            //     $('.banner pt-5').addClass('pt-3')
            //     $('.banner>.banner-detail>.row>.pt-5').addClass('pt-3');
            //     $('.banner>.banner-detail>.row>.pt-5').removeClass('pt-5');
            // }else {
            //     if ($(window).width() < 855) {
            //         $('.banner-detail button').removeClass('mt-5');
            //         $('.banner-detail button').addClass('mt-3');
            //         $('.header-menu li').removeClass('mr-4');
            //     }else{
            //         if ($(window).width() < 992) {
            //             $('.header-menu li').removeClass('mr-4');    
            //         }else {
            //             $('.header-menu li').addClass('mr-3');
            //         }
            //     }
            // }
            if ($(window).width()<517) {
                    $('.content-info-item>a').each(function(index, el) {
                        if ($(this).text().length>120) {
                            $sub=$(this).text().substring(0, 120);
                            $(this).text($(this).text().substring(0, $sub.lastIndexOf(" "))+"...");
                        }
                    });
            }else {
                if ($(window).width()<768) {
                    $('.content-info-item>a').each(function(index, el) {
                        if ($(this).text().length>270) {
                            $sub=$(this).text().substring(0, 270);
                            $(this).text($(this).text().substring(0, $sub.lastIndexOf(" "))+"...");
                        }
                    });
                }else {
                    if ($(window).width()<992) {
                        $('.content-info-item>a').each(function(index, el) {
                            if ($(this).text().length>165) {
                                $sub=$(this).text().substring(0, 165);
                                $(this).text($(this).text().substring(0, $sub.lastIndexOf(" "))+"...");
                            }
                        });
                    }else {
                        if ($(window).width()<1200) {
                            $('.content-info-item>a').each(function(index, el) {
                                if ($(this).text().length>90) {
                                    $sub=$(this).text().substring(0, 90);
                                    $(this).text($(this).text().substring(0, $sub.lastIndexOf(" "))+"...");
                                }
                            });
                        }else {
                            if ($(window).width()>1200) {
                                $('.content-info-item>a').each(function(index, el) {
                                    if ($(this).text().length>120) {
                                        $sub=$(this).text().substring(0, 120);
                                        $(this).text($(this).text().substring(0, $sub.lastIndexOf(" "))+"...");
                                    }
                                });
                            }
                        }
                    }
                }
            }
            $('input[name=search]').on('input', function(event) {
                let input = $(this).val();
                $.get('students/search-'+input+'/edit', function(data) {
                    console.log(data);
                    $('#result-search').find('li,p').remove();
                    let value = JSON.parse(JSON.stringify(data));
                    if(data != "" && data != 'nodata'){
                        if(value.course.length!=0){
                            $('#result-search').append('<p class="fs14 font-weight-bold m-0 pt-2 pb-2 result-search border-bottom w-100">Khóa học</p>');
                            for (var i = 0; i < value.course.length; i++) {
                                if(value.course[i]['short_desc'].length>60){
                                    $sub=value.course[i]['short_desc'].substring(0, 60);
                                    value.course[i]['short_desc']=value.course[i]['short_desc'].substring(0, $sub.lastIndexOf(" "))+"...";
                                }
                                $code = '<li class="result-search border-bottom">'
                                       +'<a href="home/'+value.course[i]['slug']+'-'+value.course[i]['id']+'">'
                                       +'<div class="h-100 d-flex justify-content-center align-items-center mr-2 float-left" style="width: 50px">'
                                       +'<img width="45" height="45" src="'+value.course[i]['img']+'" alt="">'
                                       +'</div>'
                                       +'<div class="pt-1"><p class="fs14 m-0">'+value.course[i]['name']+'</p><p class="font-italic fs10 m-0 text-info">'+value.tearchcourse[value.course[i]['id']]+'</p></div></a></li>'
                                $('#result-search').append($code);
                            }
                        }
                        if(value.tearch !=0 ){
                            $('#result-search').append('<p class="fs14 font-weight-bold m-0 pt-2 pb-2 result-search border-bottom w-100">Giáo viên</p>');
                            for (var i = 0; i < value.tearch.length; i++) {
                                $code = '<li class="result-search border-bottom">'
                                       +'<a href="">'
                                       +'<div class="h-100 d-flex justify-content-center align-items-center mr-2 float-left" style="width: 50px">'
                                       +'<img width="45" height="45" src="'+value.tearch[i]['avatar']+'" alt="">'
                                       +'</div>'
                                       +'<div class="pt-1"><p class="fs14 m-0"><p class="font-italic fs12 m-0 text-info">'+value.tearch[i]['display_name']+'</p></div></a></li>'
                                $('#result-search').append($code);
                            }
                        }
                    }
                    if(data=='nodata'){
                        $('#result-search').append('<p class="fs14 m-0 pt-2 pb-2 result-search border-bottom w-100">Không tìm thấy dữ liệu về "'+input+'"</p>');
                    }
                });
            });
        });
    </script>
</body>
</html>
