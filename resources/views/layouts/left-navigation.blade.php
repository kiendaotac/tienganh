@php
if (\Illuminate\Support\Facades\Auth::check()){
    $user       =   \Illuminate\Support\Facades\Auth::user();
    if ($user->hasRole('Owner')){
        $funcIds=   \App\Functions::where('state',1)->select('id')->get()->toArray();
        $funcIds=   array_column($funcIds,'id');
    } else {
        $funcIds=   \App\User::funcIds();
    }
    $functions  =   \App\Functions::orderBy('ordering','asc')->find($funcIds);
} else {
    $funcIds    =   \App\Functions::where('state',1)->where('access',3)->select('id')->get()->toArray();
    $funcIds    =   array_column($funcIds,'id');
    $functions  =   \App\Functions::orderBy('ordering','asc')->find($funcIds);
}
    $rootSubject = \App\TestSubject::where('parent_id',0)->where('state',1)->get();
@endphp
<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                @auth()
                <div class="dropdown profile-element"> <span>
                            <img alt="image" class="img-circle" src="{!! asset($user->avatar ? $user->avatar : config('custom.noavatar'))!!}" style="width: 52px"/>
                             </span>
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear" data-id="{{ $user->id }}"> <span class="block m-t-xs"> <strong class="font-bold">{{$user->display_name}}</strong>
                             </span> <span class="text-muted text-xs block">{{$user->roles->first() ? $user->roles->first()->display_name : 'User chưa được phân quyền'}} <b class="caret"></b></span> </span> </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        {{--<li><a href="profile.html">Profile</a></li>--}}
                        {{--<li><a href="contacts.html">Contacts</a></li>--}}
                        {{--<li><a href="mailbox.html">Mailbox</a></li>--}}
                        <li class="divider"></li>
                        <li><a href="{!! asset('logout') !!}">Logout</a></li>
                    </ul>
                </div>
                @endauth
                <div class="logo-element">
                    MCQ
                </div>
            </li>
            @foreach($functions as $function)
                @if($function->parent_id == 0)
                    {{-- Menu sinh ra từ các subject trang bàng test_subject --}}
                    {{-- id = 29 function: Subject, 27 function: Group --}}
                    @if ($function->id == 29) {{-- database mà thay đổi phải thay đổi chỗ này--}}
                        <li class="{{strcmp(\Request::path(),\App\Functions::find(27)->route) ? '' : 'active'}}">
                            <a><i class="{{$function->icon}}"></i> <span class="nav-label">{{$function->name}}</span><span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level collapse">
                                    @foreach($function->childs->where('state',1)->sortBy('ordering') as $item)
                                        @if(in_array($item->id,$funcIds))
                                            <li class="{{strcmp(\Request::path(),$item->route) ? '' : 'active'}}"><a href="{{asset($item->route)}}"><i class="{{$item->icon}}" style="font-size: 10px"></i> {{$item->name}}</a></li>
                                        @endif
                                    @endforeach
                                @if (count($rootSubject))
                                    @foreach($rootSubject as $item)
                                        @if (count($item->childs))
                                            <li style="margin-bottom: 0px">
                                                <a><i class="fa fa-book"></i>{{$item->name}} <span class="fa arrow"></span></a>
                                                <ul class="nav nav-third-level">
                                                    @foreach($item->childs as $child)
                                                    <li class="{{strcmp(basename($_SERVER['REQUEST_URI']),"subject?id=".$child->id) ? '' : 'active'}}">
                                                        <a href="{{asset(\App\Functions::find(27)->route.'/subject?id='.$child->id)}}" style="padding-left: 40px"><i class="fa fa-plus"></i>{{$child->name}}</a>
                                                    </li>
                                                    @endforeach
                                                </ul>
                                            </li>
                                        @else
                                            <li style="margin-bottom: 0px"><a><i class="fa fa-book"></i>{{$item->name}}</a></li>
                                        @endif
                                    @endforeach
                                @endif
                                </ul>
                        </li>
                    @else
                        {{-- Menu sinh ra từ bảng functions --}}
                        <li>
                            <a><i class="{{$function->icon}}"></i> <span class="nav-label">{{$function->name}}</span>
                                @foreach($function->childs as $child)
                                    @if(in_array($child->id,$funcIds))
                                        <span class="fa arrow"></span>
                                        @break
                                    @endif
                                @endforeach
                            </a>
                            @foreach($function->childs as $child)
                                @if(in_array($child->id,$funcIds))
                                    <ul class="nav nav-second-level collapse">
                                        @foreach($function->childs->where('state',1)->sortBy('ordering') as $item)
                                            @if(in_array($item->id,$funcIds))
                                                <li class="{{strcmp(\Request::path(),$item->route) ? '' : 'active'}}"><a href="{{asset($item->route)}}"><i class="{{$item->icon}}" style="font-size: 10px"></i> {{$item->name}}</a></li>
                                            @endif
                                        @endforeach
                                    </ul>
                                    @break
                                @endif
                            @endforeach
                        </li>
                    @endif
                @endif
            @endforeach
        </ul>
    </div>
</nav>
