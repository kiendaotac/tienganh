<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    @yield('title')

    <link href="{!! asset('css/bootstrap.min.css') !!} " rel="stylesheet">
    <link href="{!! asset('font-awesome/css/font-awesome.css') !!}" rel="stylesheet">
    <link rel="icon" href="{!! asset('favicon.ico') !!}">

    <!-- Toastr style -->
    <link href="{!! asset('css/plugins/toastr/toastr.min.css') !!}" rel="stylesheet">
    <link href="{!! asset('css/animate.css') !!}" rel="stylesheet">
    <link href="{!! asset('css/style.css') !!}" rel="stylesheet">

    <!-- Sweet Alert -->
    <link href="{!! asset('css/plugins/sweetalert/sweetalert.css') !!}" rel="stylesheet">

    <!-- Perfect scroll -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/perfect-scrollbar.css') }}">

    <!-- Custom styles -->
    <style>
    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        -moz-appearance: none;
        appearance: none;
        margin: 0;
    }
</style>
@yield('style')

</head>

<body>
    <div id="wrapper">
        <!-- Left navigation -->
        @include('layouts.left-navigation')

        <!-- Page wraper -->
        <div id="page-wrapper" class="gray-bg">

            <!-- Header -->
            @include('layouts.header')

            <!-- Content -->
            @yield('content')

            <!-- Footer -->
            @include('layouts.footer')

        </div>

        <!-- right sidebar -->
        {{--@include('layouts.right-sidebar')--}}
    </div>
    <!-- Mainly scripts -->
    <script src="{!! asset('js/jquery-3.1.1.min.js') !!}"></script>
    <script src="{!! asset('js/bootstrap.min.js') !!}"></script>
    <script src="{!! asset('js/plugins/metisMenu/jquery.metisMenu.js') !!}"></script>
    <script src="{!! asset('js/plugins/slimscroll/jquery.slimscroll.min.js') !!}"></script>

    <!-- Custom and plugin javascript -->
    <script src="{!! asset('js/inspinia.js') !!}"></script>
    <script src="{!! asset('js/plugins/pace/pace.min.js') !!}"></script>

    <!-- jQuery UI -->
    <script src="{!! asset('js/plugins/jquery-ui/jquery-ui.min.js') !!}"></script>

    <!-- Toastr -->
    <script src="{!! asset('js/plugins/toastr/toastr.min.js') !!}"></script>

    <!-- Sweet alert -->
    <script src="{!! asset('js/plugins/sweetalert/sweetalert.min.js') !!}"></script>

  <!-- Perfect scroll -->
  <script src="{{ asset('js/perfect-scrollbar.min.js') }}"></script>

  <!-- Custom notification  -->
  <script src="{{ asset('js/notification.js') }}"></script>

  {{-- <!-- Custom js -->
  <script src="{{ asset('js/custom.js') }}"></script> --}}

  <!-- Custom scripts -->
  <script>
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $(document).ready(function () {
      $('li.active').parent('ul').addClass('in').parent('li').addClass('active');
      $(document).ajaxComplete(function (event, xhr, settings) {
        if (xhr.status == 550) {
          notification(xhr.responseJSON.type, xhr.responseJSON.title, xhr.responseJSON.content);
        }

            })
        });

        function notification(type, title, content) {
            title = '';
            if (Array.isArray(content)){
                let string  =   '';
                $.each(content, function (index, item) {
                    string  +=  item + '<br/>';
                });
                content =   string;
            }
            setTimeout(function () {
                toastr.options = {
                    closeButton: true,
                    progressBar: true,
                    showMethod: 'slideDown',
                    timeOut: 4000
                };
                switch (type) {
                    case 'success'  :
                    toastr.success(content, title);
                    break;
                    case 'error'    :
                    toastr.error(content, title);
                    break;
                    case 'warning'  :
                    toastr.warning(content, title);
                    break;
                    default         :
                    toastr.warning('Không xác định được thông báo', 'Cảnh báo!');
                    break;
                }

      }, 1300);
    }
    /* check profile */
    let path = window.location.href;
    let name = path.substr(path.lastIndexOf('/')+1);
    if(name != 'profile') {
      $.ajax({
      url: 'profile/checkProfile',
      type: 'GET',
      success: function(data){
        let value = JSON.parse(JSON.stringify(data));
        if(!value.check) {
          let pathNext = path.substr(0,path.lastIndexOf('/')) + '/profile';
          window.location.assign(pathNext);
        }
      }
    });
    }
    
  </script>
  @yield('script-upload')
  @yield('script-lesson')
  @yield('script')
</body>
</html>
