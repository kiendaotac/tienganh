@extends('layouts.app')
@section('title')
<title>{{$currentFunction->name}} - {{\Illuminate\Support\Facades\Crypt::decryptString(config('custom.app_name'))}}</title>
@endsection
@section('style')
<style>
.table-custom-title{
  background-color: #dddddd6b;
  border-top: #dddddd63 solid 1px;
  border-bottom: #dddddd63 solid 1px;
  margin-top: 20px;
  padding-top: 10px;
  padding-bottom: 10px;
  font-weight: bold;
}
.avatar{
  width: 50px;  
  height: 50px;
  vertical-align: middle;
  border-radius: 50px;
  margin-right: 5px;
}
.col-name, .row-item{
  display: flex;
  align-items: center;
  margin-bottom: 10px;
}
.table-custom-content{
  margin-top: 20px;
}
.pagi{
  text-align: right;
}
</style>
@endsection
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-10">
    <h2>{{ __('admin.title_page') }} {{mb_strtolower($currentFunction->name)}}</h2>
  </div>
  <div class="col-lg-2">

  </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">
        <div class="ibox-content">
          <div class="row">
            <div class="col-lg-3 div-chose-curriculum">
              <label>
                {{ __('admin.lesson.curriculum') }}
              </label>
              <div class="chose-curriculum-title">
                <span id="id-curriculum" value="not">
                  {{ __('admin.lesson.id_curriculum') }}
                </span>
                <i class="fa fa-caret-down">
                </i>
              </div>
              <div class="chose-curriculum-content-report">
                <div>
                  <input class="form-control" id="search-curriculum" placeholder="{{ __('admin.lesson.search_curriculum_placeholder') }}" type="text">
                </div>
                <ul id="list-curriculum-class" class="list-curriculum">
                  @foreach ($curriculums as $item)
                  <li data-id="{{ $item->id }}">
                    {{ $item->name }}
                  </li>
                  @endforeach
                </ul>
              </div>
            </div>
            <div class="col-lg-3 div-chose-curriculum">
              <label>
                {{ __('admin.student_report.class') }}
              </label>
              <div class="chose-curriculum-title">
                <span id="id-class" value="not">
                  {{ __('admin.student_report.id_class') }}
                </span>
                <i class="fa fa-caret-down">
                </i>
              </div>
              <div class="chose-curriculum-content class-content">
                <span class="list-lesson-notify">
                  {{ __('admin.lesson.list_lesson_notify') }}
                </span>
              </div>
            </div>
          </div>
          <div class="table-custom">
            <div class="container-fluid">
              <div class="row table-custom-title hidden-xs hidden-sm">
                <div class="col-lg-4">
                  {{ __('admin.student_report.table_name') }}
                </div>
                <div class="col-lg-3">
                  {{ __('admin.student_report.table_dept') }}
                </div>
                <div class="col-lg-3">
                  {{ __('admin.student_report.table_dept') }}
                </div>
                <div class="col-lg-2">
                  {{ __('admin.student_report.table_pass') }}
                </div>
              </div>
              <div class="row table-custom-content">
                <div class="col-lg-12">
                </div>
              </div>
            </div>
            <div class="pagi">
              <ul class="pagination">
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('script')
<script src={{ asset('js/custom.js') }}></script>
<script>
  $(document).on('click','#list-curriculum-class li', function(){
    if($(this).closest('ul')[0].className == 'list-curriculum') {
      $('#id-class').attr('value','not');
      $('#id-class').text('-- Chọn lớp học phần --');
    }
    let url = 'student-report/getStudent';
    $('.pagination').html('');
    loadStudent(url);
  });

  /* ajax Pagination  */
  $(document).on('click', '.pagination li', function(data){
    let page = $(this).find('a').text(),
    url = 'student-report/getStudent?page='+page;
    $('.pagination').html('');
    loadStudent(url);
  });

  /* ajax load student report */
  function loadStudent(url){
    $('.table-custom-content > div').html('<i class="fa fa-refresh load"></i>');
    $('.table-custom-content > div').addClass('text-center');
    $.ajax({
      url: url,
      type: 'GET',
      dataType: 'JSON',
      data: {
        curriculum_id: $('#id-curriculum').attr('value'),
        class_id: $('#id-class').attr('value'),
      },
      success: function(data){
        $('.table-custom-content > div').removeClass('text-center');
        let value= JSON.parse(JSON.stringify(data));
        if(value.last_page >= 2) {
          for (let i = 1; i <= value.last_page; i++) {
            $('.pagination').append('<li><a href="#">'+i+'</a></li>');
          }
          let current_page = parseInt(value.current_page) - 1;
          $('.pagination li:eq('+current_page+')').addClass('active');
        }
        $('.table-custom-content > div').html('');
        $.each(value.data, function(index, val) {
          let avatar = 'img/noavatar.png';
          if(val.info.avatar != null) avatar = val.info.avatar;
          let content = '<div class="row row-item">' +
          '<div class="col-lg-4 col-name">' +
          '<img src="'+avatar+'" class="avatar">' +
          '<span>'+val.student.lastname+' '+val.student.firstname+'</span>' +
          '</div>' +
          '<div class="col-lg-3">' + val.info.dept +
          '</div>' +
          '<div class="col-lg-3">' + val.info.majo +
          '</div>' +
          '<div class="col-lg-2">' + val.pass + '%' +
          '</div>' +
          '</div>';
          $('.table-custom-content > div').append(content);
        });
      }
    })
  }
</script>
@endsection