@extends('layouts.app')
@section('title')
<title>{{$currentFunction->name}} - {{\Illuminate\Support\Facades\Crypt::decryptString(config('custom.app_name'))}}</title>
@endsection

@section('style')
<link href="css/plugins/dataTables/datatables.min.css" rel="stylesheet">
<style>
table.dataTables-users > thead > tr > th.stt {
    width: 15px;
}
table.dataTables-users > thead > tr > th.avatar {
    width: 70px;
}
table.dataTables-users > thead > tr > th.name {
    width: 20%;
}
table.dataTables-users > thead > tr > th.email {
    width: 20%;
}
table.dataTables-users > thead > tr > th.action {
    width: 60px;
}
table.dataTables-users > thead > tr > th.state {
    width: 90px;
}
table.dataTables-users > tbody > tr > td > img.avatar {
    width: 60px;
    height: 60px;
}
table.dataTables-users > tbody > tr > td {
    vertical-align: middle;
}
table.dataTables-users {
    width: 100% !important;
}

</style>
@endsection
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Quản lý {{mb_strtolower($currentFunction->name)}}</h2>
    </div>
    <div class="col-lg-2">

    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-users">
                            <thead>
                                <tr>
                                    <th class="stt">STT</th>
                                    <th class="avatar">Avatar</th>
                                    <th class="name">User name</th>
                                    <th class="email">Email</th>
                                    <th class="display_name">Display name</th>
                                    <th class="state">Trạng Thái</th>
                                    <th class="action">Thao tác</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal add new function -->
@include('backend.system.users.form')
@endsection

@section('script')
<script src="js/plugins/dataTables/datatables.min.js"></script>
<script src={{ asset('js/custom.js') }}></script>
<!-- Page-Level Scripts -->
<script>
   let mainUrl = '{{$currentFunction->route}}';
   $(document).ready(function(){
    let Table = $('.dataTables-users').DataTable({
        bSort: false,
        bInfo: true,
        bLengthChange: false,
        processing: true,
        serverSide: true,
        searching: true,
        paging: true,
        pageLength: 15,
        responsive: true,
        ajax: mainUrl + '/getDatatable',
        dom: '<"row"<"col-sm-6"Bi><"col-sm-6"f>>rtp',
        buttons: {
            buttons: [
            {
                text: 'Thêm mới',
                action: function ( e, dt, node, config ) {
                    resetForm();
                    $('#modal-add-user').modal({backdrop: 'static', keyboard: false, show : true});
                }
            }
            ],
            dom: {
                button: {
                    tag: "button",
                    className: "btn btn-primary"
                },
                buttonLiner: {
                    tag: null
                }
            }
        },
        language: {
            "lengthMenu": "Hiển thị _MENU_ bản ghi trên 1 trang",
            "zeroRecords": "Không có dữ liệu",
            "info": "Hiển thị trang _PAGE_ trong tổng số _PAGES_ trang",
            "infoEmpty": "Không có dữ liệu tồn tại",
            "infoFiltered": "(Lọc từ _MAX_ bản ghi)",
            "search": "Tìm kiếm:",
            "paginate": {
                "first":      "Trang đầu",
                "last":       "Trang cuối",
                "next":       "Trang tiếp theo",
                "previous":   "Trang trước"
            },
        },
        columns     :   [
        {
            data    :   'id',
            className:  'text-center',
            render  :   function (data, type, row, meta) {
                return meta.row +1;
            }
        },
        {
            data    :   'avatar',
            render  : function (data, type, row, meta) {
                data = data ? data : '{{config('custom.noavatar')}}';
                return '<img title="Avatar" class="avatar" alt="Avatar" src="'+data+'">';
            }
        },
        {   data    :   'name'},
        {   data    :   'email'},
        {   data    :   'display_name'},
        {
            data    :   'state',
            render  :   function (data, type, row, meta) {
                let state;
                switch (parseInt(data)){
                    case 0: state = 'Không kích hoạt'; break;
                    case 1: state = 'Kích hoạt'; break;
                    case 2: state = 'Kích hoạt ẩn'; break;
                    default:state = 'Chưa xác định'; break;
                }
                return state;
            }
        },
        {
            data    :   'id',
            className: 'text-center',
            render  :   function (data, type, row, meta) {
                return '@permission("Edit-2")<button name="btn-edit" class="btn btn-primary btn-flat btn-xs" title="Sửa" data-data=\''+JSON.stringify(row)+'\'><i class="fa fa-edit"></i></button>@endpermission @permission("Delete-2")<button name="btn-delete" class="btn btn-remove btn-danger btn-flat btn-xs" title="Xóa" data-id='+data+'><i class="fa fa-remove"></i></button>@endpermission';
            }
        }
        ]
    });

    /* Add new/ edit User */
    $('button[name=add-user]').on('click', function () {
        if($('select[name=uni_id]').val() <= 0) swal('Vui lòng chọn trường');
        else {
            let key = 'id_uni,name_uni';
            let value = $('select[name=uni_id]').val()+','+$('select[name=uni_id] option:selected').text();
            let data = $('form[name=form-add-user]').serialize(),
            id      =   $('input[name=id]').val(),
            type    =   'POST',
            url     =   mainUrl;
            if (parseInt(id)){
                type    =   'PUT';
                url     =   mainUrl + '/' + id;
            }
            data += '&key='+key+'&value='+value;
            $.ajax(url,{
                type    :   type,
                data    :   data,
                success :   function (respon) {
                    notification(respon.type, respon.title, respon.content);
                    if(respon.type === 'success'){
                        Table.ajax.reload();
                        $('#modal-add-user').modal('hide');
                    }
                }
            })
        }
    });

    /* Edit function */
    $(document).on('click', 'button[name=btn-edit]', function () {
        let data = $(this).data('data');
        let uniId = data.profile[0].value.substr(0,data.profile[0].value.indexOf(','));
        $('select[name=uni_id] option[value='+uniId+']').attr('selected', 'selected');
        $.each(data, function (index, item) {
            $('[name='+index+']').val(item);
        });
        $('#modal-add-user').modal({backdrop: 'static', keyboard: false, show : true});
    });

    /* Delete function */
    $(document).on('click', 'button[name=btn-delete]', function () {
        let id  = $(this).data('id');
        let url =   mainUrl + '/' + id;
        swal({
            title: "Bạn có muốn xóa user này không??",
            text: "Khi xóa user sẽ không thể khôi phục lại",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "OK",
            cancelButtonText: "Không",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function (config) {
            if (config){
                $.ajax(url,{
                    type    :   'DELETE',
                    data    :   {
                        _token  :   '{{csrf_token()}}'
                    },
                    success :   function (respon) {
                        notification(respon.type, respon.title, respon.content);
                        if(respon.type === 'success'){
                            Table.ajax.reload();
                        }
                    }
                })
            }
        });
    });
});

function resetForm() {
    $('form[name=form-add-user]').trigger('reset');
    $('input[name=id]').val(0);
}
</script>
@endsection
