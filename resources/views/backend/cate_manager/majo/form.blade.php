<!-- Modal -->
<div id="modal-add-majo" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><i class="fa fa-plus" style="margin-right: 10px;"></i>{{ __('admin.major.modal_title') }}</h4>
      </div>
      <div class="modal-body">
        <form id="f-add-majo" name="form-add-majo">
          @csrf
          <input type="hidden" name="id" value="0">
          <div class="row">
            <div class="col-lg-12">
              <div class="row">
                <div class="col-lg-6">
                  <div class="form-group">
                    <label for="group">{{ __('admin.major.group') }} <span class="contraint">*</span></label>
                    <select class="form-control" name="uni_id" id="unis group">
                      <option value="0" selected>{{ __('admin.major.select_group_option') }}</option>
                      @foreach ($unis as $unis)
                      @if ($unis->parent_id == 0)
                      <option name="unis" value="{{ $unis->id }}">{{ $unis->name }}</option>
                      @endif
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="form-group">
                    <label for="department">{{ __('admin.major.department') }} <span class="contraint">*</span></label>
                    <select class="form-control" id="department" name="depart_id">
                      <option value="0" selected>{{ __('admin.major.select_department_option') }}</option>
                    </div>
                  </select>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label for="uni_key">{{ __('admin.major.uni_key') }} <span class="contraint">*</span></label>
              <input type="text" class="form-control input-check" id="uni_key" name="uni_key" data-error="{{ __('admin.major.uni_key_error') }}">
            </div>
            <div class="form-group">
              <label for="name">{{ __('admin.major.name') }} <span class="contraint">*</span></label>
              <input type="text" class="form-control input-check" id="name" name="name" data-error="{{ __('admin.major.name_error') }}">
            </div>
            <div class="form-group">
              <label for="desc">{{ __('admin.desc') }}</label>
              <textarea class="form-control" id="desc" name="desc"></textarea>
            </div>
            <div class="row">
              <div class="col-lg-6">
                <div class="form-group">
                  <label for="states">{{ __('admin.states') }} <span class="contraint">*</span></label>
                  <select class="form-control" id="states" name="states">
                    <option value="1">{{ __('admin.states_active') }}</option>
                    <option value="0">{{ __('admin.states_not_active') }}</option>
                  </select>
                </div>
              </div>
              <div class="col-lg-6">
                <div class="form-group">
                  <label for="ordering">Ordering:</label>
                  <input type="number" class="form-control" id="ordering" name="ordering" value="100">
                </div>
              </div>
            </div>
          </div>
        </div>
      </form>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-primary" id="save">{{ __('admin.save') }}</button>
      <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('admin.close') }}</button>
    </div>
  </div>

</div>
</div>
