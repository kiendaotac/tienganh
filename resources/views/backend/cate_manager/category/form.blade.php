
<!-- Modal -->
<div id="modal-add-category" class="modal fade" role="dialog" style="overflow-y: auto">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><i class="fa fa-plus"></i>{{ __('admin.category.modal_title') }}</h4>
      </div>
      <div class="modal-body">
        <form id="f-add" name="form-add-category">
          <input type="hidden" name="id" value="0">
          <input type="hidden" name="img" value="">
          <div class="row">
            <div class="col-lg-4 chose-image">
              <div>
                <img src="http://admin.localhost:8000/storage/uploads/2018/08/thumbs/56.png" alt="{{ __('admin.category.image_alt') }}">
              </div>
              <span style="color: #898989; font-size: 12px; margin-top: 10px">{{ __('admin.category.image_title') }}</span>
            </div>
            <div class="col-lg-8">
              <div class="form-group">
                <label for="group">{{ __('admin.category.group') }}</label>
                <select class="form-control" id="group" name="parent_id">
                  <option value="0" selected>{{ __('admin.category.select_group_option') }}</option>
                  @foreach ($categories as $category)
                  @if ($category->parent_id == 0)
                  <option value="{{ $category->id }}">{{ $category->name }}</option>
                  @endif
                  @endforeach
                </select>
              </div>
              <div class="form-group">
                <label for="name">{{ __('admin.category.name') }} <span class="contraint">*</span></label>
                <input type="text" class="form-control input-text-category" id="name" name="name">
              </div>
              <div class="row">
                <div class="col-lg-6">
                  <div class="form-group">
                    <label for="icon">Icon:</label>
                    <input type="text" class="form-control" id="icon" name="icon">
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="form-group">
                    <label for="ordering">Ordering:</label>
                    <input type="number" class="form-control" id="ordering" name="ordering">
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label for="desc">{{ __('admin.desc') }}</label>
                <textarea class="form-control" id="desc" name="desc"></textarea>
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="save">{{ __('admin.save') }}</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('admin.close') }}</button>
      </div>
    </div>

  </div>
</div>