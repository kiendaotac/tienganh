@extends('layouts.app')
@section('title')
<title>{{$currentFunction->name}} - {{\Illuminate\Support\Facades\Crypt::decryptString(config('custom.app_name'))}}</title>
@endsection
@section('style')
<link href="css/plugins/dataTables/datatables.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/plugins/dropzone/dropzone.css">
<link rel="stylesheet" type="text/css" href="css/plugins/summernote/summernote.css">
<style>
table.dataTables-categories > thead > tr > th.stt {
  width: 50px;
}
table.dataTables-categories > thead > tr > th.name {
  width: 20%;
}
table.dataTables-categories > thead > tr > th.icon {
  width: 100px;
}
table.dataTables-categories > thead > tr > th.action {
  width: 100px;
}
table.dataTables-categories > thead > tr > th.state {
  width: 200px;
}
table.dataTables-categories {
  width: 100% !important;
}
#modal-add-category i{
  margin-right: 10px;
}
.dz-preview{
  margin: 0 !important;
  margin-bottom: 20px !important;
  width: 100% !important;
}
.chose-image{
  height: 325px;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
}
.chose-image div{
  height: 50%;
  border: #ddd dashed 1px;
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 16px;
  cursor: pointer;
}
.chose-image img{
  width: 90%;
  height: 90%;
}
</style>
@endsection
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-10">
    <h2>{{ __('admin.title_page') }} {{mb_strtolower($currentFunction->name)}}</h2>
  </div>
  <div class="col-lg-2">

  </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">
        <div class="ibox-content">
          <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover dataTables-categories">
              <thead>
                <tr>
                  <th class="stt">{{ __('admin.table_stt') }}</th>
                  <th>{{ __('admin.category.name') }}</th>
                  <th>{{ __('admin.table_desc') }}</th>
                  <th class="icon">Icon</th>
                  <th class="icon">Ordering</th>
                  <th class="state">{{ __('admin.table_state') }}</th>
                  <th class="action">{{ __('admin.table_action') }}</th>
                </tr>
              </thead>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal add new function -->
@include('backend.cate_manager.category.form')
@include('backend.system.file.module')
@endsection
@section('script')
<script src="js/plugins/dataTables/datatables.min.js"></script>
<script>
  /* category */
  let mainUrl = '{{$currentFunction->route}}';
  /* datatable category*/
  let Table = $('.dataTables-categories').DataTable({
    bSort: false,
    bInfo: true,
    bLengthChange: false,
    processing: true,
    serverSide: true,
    searching: true,
    paging: true,
    pageLength: 15,
    responsive: true,
    ajax: mainUrl + '/getDatatable',
    dom: '<"row"<"col-sm-6"Bi><"col-sm-6"f>>rtp',
    buttons: {
      buttons: [
      {
        text: 'Thêm mới',
        action: function ( e, dt, node, config ) {
          resetForm();
          $('#modal-add-category').modal({backdrop: 'static', keyboard: false, show : true});
        }
      }
      ],
      dom: {
        button: {
          tag: "button",
          className: "btn btn-primary"
        },
        buttonLiner: {
          tag: null
        }
      }
    },
    language: {
      "lengthMenu": "Hiển thị _MENU_ bản ghi trên 1 trang",
      "zeroRecords": "Không có dữ liệu",
      "info": "Hiển thị trang _PAGE_ trong tổng số _PAGES_ trang",
      "infoEmpty": "Không có dữ liệu tồn tại",
      "infoFiltered": "(Lọc từ _MAX_ bản ghi)",
      "search": "Tìm kiếm:",
      "paginate": {
        "first":      "Trang đầu",
        "last":       "Trang cuối",
        "next":       "Trang tiếp theo",
        "previous":   "Trang trước"
      },
    },
    columns     :   [
    {
      data    :   'id',
      className:  'text-center',
      render  :   function (data, type, row, meta) {
        return meta.row +1;
      }
    },
    {
      data    :   'name',
      render  : function (data, type, row, meta) {
        if (row.parent_id == 0){
          return '<b>'+data+'</b>';
        } else {
          return '&emsp;'+data;
        }
      }
    },
    {   data    :   'desc'},
    {
      data    :   'icon',
      className: 'text-center',
      render  :   function (data, type, row, meta) {
        return '<i class="'+data+'"></i>';
      }
    },
    {
      data    :   'ordering',
      className: 'text-center'
    },
    {
      data    :   'states',
      className: 'text-center',
      render  :   function (data, type, row, meta) {
        if(data == 0) return 'Không kích hoạt';
        return 'Kích hoạt';
      }
    },
    {
      data    :   'id',
      className: 'text-center',
      render  :   function (data, type, row, meta) {
        let button  =   '@permission("Edit-9")<button name="btn-edit" class="btn btn-primary btn-flat btn-xs" title="{{ __('admin.title_btn_edit') }}" data-data=\''+JSON.stringify(row)+'\'><i class="fa fa-edit"></i></button>@endpermission @permission("Delete-9")<button name="btn-delete" class="btn btn-remove btn-danger btn-flat btn-xs" title="{{ __('admin.title_btn_delete') }}" data-id='+data+'><i class="fa fa-remove"></i></button>@endpermission';
        return button;
      }
    }
    ]
  });
  /* add or edit category */
  $('#save').on('click', function(){
    let data = $('form[name=form-add-category]').serialize(),
    id      =   $('input[name=id]').val(),
    type    =   'POST',
    url     =   mainUrl;
    if (parseInt(id)){
      type    =   'PUT';
      url     =   mainUrl + '/' + id;
    }
    if($('.input-text-category').val() === '') swal('Vui lòng nhập vào tên thể loại.');
    else {
      $(this).append('<i class="fa fa-refresh load" style="margin-left: 5px"></i>');
      $.ajax({
        url : url,
        type: type,
        dataType: 'json',
        data: data,
        success: function (data) {
          $('#save i').remove();
          let value = JSON.parse(JSON.stringify(data));
          notification(value.type, value.title, value.content);
          if(value.type == 'success') {
            $('#modal-add-category').modal('hide');
            Dropzone.forElement(".dropzone").removeAllFiles(true);
            Table.ajax.reload();
          }
          let idOption = null,
          nameOption = null;
          if(value.id != null) {
            idOption = value.id;
            nameOption = value.name;
          }
          else{
            idOption = $("input[name='id']").val();
            nameOption = $("input[name='name']").val();
            $('option[value='+idOption+']').remove();
          }
          if(value.parent_id == 0 || $('select[name=parent_id]').val() == 0){
            let content = '<option value="'+ idOption+'">'+nameOption+'</option>';
            $('select[name=parent_id]').append(content);
          }
        },
        error: function(data){
          notification('warning', 'Cảnh báo!', 'Gặp sự cố, thử lại sau.');
        }
      });
    }
    
  });
  /* edit category */
  $(document).on('click', 'button[name=btn-edit]', function(){
    let data = $(this).data('data');
    $('select[name=parent_id] option[value=' + data.id + ']').hide();
    let imgPath = data.img;
    let imgElement = null;
    if(imgPath != '' && imgPath != null) imgElement = '<img src="'+imgPath+'" alt="hình ảnh mô tả thể loại" />';
    else imgElement = 'Nhấn vào để chọn ảnh';
    $('.chose-image > div').html(imgElement);
    $.each(data, function (index, item) {
      $('[name='+index+']').val(item);
    });
    $('#modal-add-category').modal({backdrop: 'static', keyboard: false, show : true});
  });
  /* delete category */
  $(document).on('click', 'button[name=btn-delete]', function(){
    let id = $(this).data('id');
    swal({
      title: "Bạn có muốn xóa chức năng này không??",
      text: "Khi xóa chức năng sẽ không thể khôi phục lại",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "OK",
      cancelButtonText: "Không",
      closeOnConfirm: true,
      closeOnCancel: true
    }, function(config){
      if(config){
        $.ajax({
          url: mainUrl + '/' + id,
          type: 'DELETE',
          dataType: 'json',
          success: function(data){
            let value = JSON.parse(JSON.stringify(data));
            notification(value.type, value.title, value.content);
            if(value.type == 'success') {
              $('select[name=parent_id] option[value=' + id + ']').remove();
              Table.ajax.reload();
            }
          }
        });
      }
    });
  });
  function resetForm() {
    $('#f-add').trigger('reset');
    $('input[name=id]').val(0);
    $('input[name=img]').val('');
    $('.chose-image > div').html('Nhấn vào để chọn ảnh');
    $('select[name=parent_id] option').show();
  }
  /* check chose is image */
  function checkChoseFile(extension){
    let arrExtension = ['png', 'jpg', 'jpeg', 'gif'];
    if(arrExtension.indexOf(extension) == -1) return false;
    return true;
  }
  /* show modal upload file */
  $(document).on('click', '.chose-image', function(){
    $('#module-file').modal();
  });
  $(document).on('click', '#module-file .file-box', function(){
    let imgPath = $(this).find('.file').data('src');
    let fileExtension = imgPath.substr(imgPath.lastIndexOf('.')+1).toLowerCase();
    if(checkChoseFile(fileExtension)) {
      $('input[name=img]').val(imgPath);
      let imgElement = '<img src="'+imgPath+'" alt="hình ảnh mô tả thể loại" />'
      $('.chose-image > div').html(imgElement);
      $('#module-file').modal('hide');
    }
    else swal('File này không phải là hình ảnh.');
  });
  $(document).on('click', '#modal-upload', function(){
    let classList = $(this)[0].classList;
    if(classList[classList.length-1] != 'in') {
      $('#module-file').modal('show');
    }
  });
</script>
@endsection