@php
$checkUni = true;
if(Auth::user()->isAdmin()) {
  if(count(Auth::user()->profile) <= 0) $checkUni = false;
}
$unis = \App\Unis::where('parent_id',0)->active()->oldest('ordering')->oldest('created_at')->get();
@endphp
<div id="modal-chose-uni" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title"><i class="fa fa-university" style="margin-right: 5px"></i>{{ __('admin.university.form_chose_uni_title') }}</h4>
      </div>
      <div class="modal-body">
        <div class="chose-uni">
          <label>Chọn trường:</label>
          <div class="chose-uni-title">
            <span id="id-uni" value="not">-- Chọn trường --</span>
            <i class="fa fa-caret-down rt"></i>
          </div>
          <div class="chose-uni-content">
            <div><input class="form-control" id="search-university" placeholder="Tìm kiếm trường" type="text"></div>
            <ul id="list-uni">
              @foreach ($unis as $uni)
              <li data-id="{{ $uni->id }}">{{ $uni->name }}</li>
              @endforeach
            </ul>
          </div>
        </div>
        <div class="chose-uni">
          <label>Chọn khoa:</label>
          <div class="chose-uni-title">
            <span id="id-department" value="not">-- Chọn khoa --</span>
            <i class="fa fa-caret-down rt"></i>
          </div>
          <div class="chose-uni-content">
            <span class="list-department-notify">
              Hãy họn trường trước
            </span>
            <ul id="list-department">
            </ul>
          </div>
        </div>
        <div class="chose-uni">
          <label>Chọn ngành:</label>
          <div class="chose-uni-title">
            <span id="id-major" value="not">-- Chọn ngành --</span>
            <i class="fa fa-caret-down rt"></i>
          </div>
          <div class="chose-uni-content">
            <span class="list-major-notify">
              Hãy họn khoa trước
            </span>
            <ul id="list-major">
            </ul>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="save-uni">{{ __('admin.save') }}</button>
      </div>
    </div>
  </div>
</div>
@section('script-lesson')
<script>
  @if (!$checkUni)
  $('#modal-chose-uni').modal({backdrop: 'static', keyboard: false});
  @endif
  /* save chose uni */
  $(document).on('click','#save-uni',function(){
    let check = true;
    $('.chose-uni-title span').each(function(){
      if($(this).attr('value') == 'not') {
        check = false;
        return true;
      }
    });
    if(!check) swal('Vui lòng chọn đầy đủ thông tin');
    else{
      let key = 'MA_TRUONG,TEN_TRUONG,MA_KHOA,TEN_KHOA,MA_NGANH,TEN_NGANH';
      let value = $('#id-uni').attr('value')+','+$('#id-uni').text()+','+$('#id-department').attr('value')+','+$('#id-department').text()+','+$('#id-major').attr('value')+','+$('#id-major').text();
      $.ajax({
        url: 'user-profile',
        type: 'POST',
        dataType: 'JSON',
        data: {
          key: key,
          value: value,
          user_id: {{Auth::user()->id}}
        },
        success: function(data){

        }
      });
    }
  });
</script>
@stop