@extends('layouts.app')
@section('title')
<title>{{$currentFunction->name}} - {{\Illuminate\Support\Facades\Crypt::decryptString(config('custom.app_name'))}}</title>
@endsection
@section('style')
<link href="css/plugins/dataTables/datatables.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/plugins/dropzone/dropzone.css">
<style>
table.dataTables-universities > thead > tr > th.stt {
    width: 23px;
}
table.dataTables-universities > thead > tr > th.order{
    width: 60px;
}
table.dataTables-universities > thead > tr > th.action {
    width: 100px;
}
table.dataTables-universities > thead > tr > th.state {
    width: 100px;
}
table.dataTables-universities {
    width: 100% !important;
}
</style>
@endsection
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>{{ __('admin.title_page') }} {{mb_strtolower($currentFunction->name)}}</h2>
    </div>
    <div class="col-lg-2">

    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-universities">
                            <thead>
                                <tr>
                                    <th class="stt">{{ __('admin.table_stt') }}</th>
                                    <th>{{ __('admin.university.uni_key') }}</th>
                                    <th>{{ __('admin.university.name') }}</th>
                                    <th class="order">Ordering</th>
                                    <th class="state">{{ __('admin.table_state') }}</th>
                                    <th class="action">{{ __('admin.table_action') }}</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal add new function -->
@include('backend.cate_manager.university.form')
@endsection
@section('script')
<script src="js/plugins/dataTables/datatables.min.js"></script>
<script src="js/plugins/dropzone/dropzone.js"></script>
<!-- Page-Level Scripts -->
<script>
    let mainUrl = '{{$currentFunction->route}}';
    /* datatable */
    let Table = $('.dataTables-universities').DataTable({
        bSort: false,
        bInfo: true,
        bLengthChange: false,
        processing: true,
        serverSide: true,
        searching: true,
        paging: true,
        pageLength: 15,
        responsive: true,
        ajax: mainUrl + '/getDatatable',
        dom: '<"row"<"col-sm-6"Bi><"col-sm-6"f>>rtp',
        buttons: {
            buttons: [
            {
                text: 'Thêm mới',
                action: function ( e, dt, node, config ) {
                    resetForm();
                    $('#modal-add-university').modal({backdrop: 'static', keyboard: false, show : true});
                }
            }
            ],
            dom: {
                button: {
                    tag: "button",
                    className: "btn btn-primary"
                },
                buttonLiner: {
                    tag: null
                }
            }
        },
        language: {
            "lengthMenu": "Hiển thị _MENU_ bản ghi trên 1 trang",
            "zeroRecords": "Không có dữ liệu",
            "info": "Hiển thị trang _PAGE_ trong tổng số _PAGES_ trang",
            "infoEmpty": "Không có dữ liệu tồn tại",
            "infoFiltered": "(Lọc từ _MAX_ bản ghi)",
            "search": "Tìm kiếm:",
            "paginate": {
                "first":      "Trang đầu",
                "last":       "Trang cuối",
                "next":       "Trang tiếp theo",
                "previous":   "Trang trước"
            },
        },
        columns     :   [
        {
            data    :   'id',
            className:  'text-center',
            render  :   function (data, type, row, meta) {
                return meta.row +1;
            }
        },
        {
            data:'uni_key'
        },
        {
            data    :   'name',
        },
        {
            data    :   'ordering',
            className: 'text-center'
        },
        {
            data    :   'states',
            className: 'text-center',
            render  :   function (data, type, row, meta) {
                if(data == 0) return 'Không kích hoạt';
                return 'Kích hoạt';
            }
        },
        {
            data    :   'id',
            className: 'text-center',
            render  :   function (data, type, row, meta) {
                let button  =   '@permission("Edit-10")<button name="btn-edit" class="btn btn-primary btn-flat btn-xs" title="Sửa" data-data=\''+JSON.stringify(row)+'\'><i class="fa fa-edit"></i></button>@endpermission @permission("Delete-10")<button name="btn-delete" class="btn btn-remove btn-danger btn-flat btn-xs" title="Xóa" data-id='+data+'><i class="fa fa-remove"></i></button>@endpermission';
                return button;
            }
        }
        ]
    });
    /* add or edit university */
    $('#save').on('click', function(){
        if(checkNotValue('input-check')) {
            let data = $('form[name=form-add-university]').serialize(),
            id      =   $('input[name=id]').val(),
            type    =   'POST',
            url     =   mainUrl;
            if (parseInt(id)){
                type    =   'PUT';
                url     =   mainUrl + '/' + id;
            }
            $(this).append('<i class="fa fa-refresh load" style="margin-left: 5px;"></i>');
            $.ajax({
                url : url,
                type: type,
                dataType: 'json',
                data: data,
                success: function (data) {
                    $('#save i').remove();
                    let value = JSON.parse(JSON.stringify(data));
                    notification(value.type, value.title, value.content);
                    if(value.type == 'success') {
                        $('#modal-add-university').modal('hide');
                        Table.ajax.reload();
                    }
                },
                error: function(data){
                    notification('warning', 'Cảnh báo!', 'Gặp sự cố, thử lại sau.');
                }
            });
        }
        else return checkNotValue('input-check');
    });
    /* edit university */
    $(document).on('click', 'button[name=btn-edit]', function(){
        let data = $(this).data('data');
        $('select[name=parent_id] option[value=' + data.id + ']').hide();
        $.each(data, function (index, item) {
            $('[name='+index+']').val(item);
        });
        $('#modal-add-university').modal({backdrop: 'static', keyboard: false, show : true});
    });
    /* delete category */
    $(document).on('click', 'button[name=btn-delete]', function(){
        let id = $(this).data('id'),
        button = $(this);    
        swal({
            title: "Bạn có chắc chắn muốn xóa?",
            text: "Khi xóa dữ liệu sẽ không thể khôi phục lại",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Có",
            cancelButtonText: "Không",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function(config){
            if(config){
                button.html('<i class="fa fa-refresh load"></i>');
                $.ajax({
                    url: mainUrl + '/' + id,
                    type: 'DELETE',
                    dataType: 'json',
                    success: function(data){
                        let value = JSON.parse(JSON.stringify(data));
                        notification(value.type, value.title, value.content);
                        if(value.type == 'success') {
                            $('select[name=parent_id] option[value=' + id + ']').remove();
                            Table.ajax.reload();
                        }
                    }
                });
            }
        });
    });
    function resetForm() {
        $('#f-add-university').trigger('reset');
        $('input[name=id]').val(0);
    }
    /* check input options is have value */
    function checkNotValue(className){
      let check = true;
      let input = [];
      $('.'+className).each(function(index, item){
        input.push($(this));
    });
      input = input.reverse();
      $.each(input, function(index, item){
        if(item.val() === '' || item.val() === '<p><br></p>') check = swal($(this).data('error'));
    })
      return check;
  }
</script>
@endsection