@extends('layouts.app')
@section('title')
<title>{{$currentFunction->name}} - {{\Illuminate\Support\Facades\Crypt::decryptString(config('custom.app_name'))}}</title>
@endsection
@section('style')
<style>
.form-group{
	position: relative !important;
}
.view-pass{
	position: absolute;
	top: 42px;
	right: 9px;
	cursor: pointer;
	color: #898989;
	display: none;
}
.form-group input{
	padding-right: 30px !important;
}
</style>
@endsection
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-10">
		<h2>{{mb_strtolower($currentFunction->name)}}</h2>
	</div>
	<div class="col-lg-2">

	</div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox float-e-margins">
				<div class="ibox-content">
					<div class="row">
						<div class="col-lg-6 col-lg-push-3 col-lg-pull-3">
							<div class="form-group">
								<label for="password-old">{{ __('admin.change_password.password_old') }} <span class="contraint">*</span></label>
								<input type="password" class="form-control input-text-password" name="password_old" id="password-old" data-error="{{ __('admin.change_password.password_old_error') }}">
								<i class="fa fa-eye view-pass"></i>
							</div>
							<div class="form-group">
								<label for="password-new">{{ __('admin.change_password.password_new') }} <span class="contraint">*</span></label>
								<input type="password" class="form-control input-text-password" name="password_new" id="password-new" data-error="{{ __('admin.change_password.password_new_error') }}">
								<i class="fa fa-eye view-pass"></i>
							</div>
							<div class="form-group">
								<label for="password-repeat">{{ __('admin.change_password.password_repeat') }} <span class="contraint">*</span></label>
								<input type="password" class="form-control input-text-password" name="password_repeat" id="password-repeat" data-error="{{ __('admin.change_password.password_repeat_error') }}">
								<i class="fa fa-eye view-pass"></i>
							</div>
							<div class="form-group" style="text-align: right;">
								<button type="button" id="save" class="btn btn-primary">{{ __('admin.save') }}</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('script')
<script>
	$(document).on('click','#save',function(){
		if(checkNotValue('input-text-password')) {
			if($('input[name=password_new]').val() != $('input[name=password_repeat]').val()) swal('Nhập lại mật khẩu mới không khớp');
			else {
				$(this).append('<i class="fa fa-refresh load" style="margin-left: 5px"></i>');
				$.ajax({
					url: 'change-password/'+{{ Auth::user()->id }},
					type: 'PUT',
					dataType: 'JSON',
					data: {
						password_old: $('input[name=password_old]').val(),
						password_new: $('input[name=password_new]').val(),
						password_repeat: $('input[name=password_repeat]').val()
					},
					success: function(data){
						$('#save i').remove();
						let value = JSON.parse(JSON.stringify(data));
						notification(value.type, value.title, value.content);
						if(value.type == 'success') {
							window.location = $('#logout a').attr('href');
						}
					}
				});
			}
		}
		else return checkNotValue('input-text-password');
	});
	/* show view pass */
	$(document).on('focus','input',function(){
		$(this).closest('.form-group').find('i').show();
	});
	/* view password */
	$(document).on('click','.view-pass',function(){
		let i = $(this);
		let input = $(this).closest('.form-group').find('input');
		let type = input.attr('type');
		if(type == 'password') input.attr('type','text');
		else input.attr('type','password');
		setTimeout(function(){
			i.show();
		},200);
	});
	/* hide view pass */
	$(document).on('blur','input',function(){
		let input = $(this);
		setTimeout(function(){
			input.closest('.form-group').find('i').hide();
		},200);	
	});
</script>
@endsection
