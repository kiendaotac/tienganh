@extends('layouts.app')
@section('title')
<title>{{$currentFunction->name}} - {{\Illuminate\Support\Facades\Crypt::decryptString(config('custom.app_name'))}}</title>
@endsection
@section('style')
<link href="css/plugins/dataTables/datatables.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/plugins/dropzone/dropzone.css">
<link rel="stylesheet" type="text/css" href="css/plugins/summernote/summernote.css">
<link rel="stylesheet" type="text/css" href="{{ asset('css/plugins/switchery/switchery.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/plugins/iCheck/custom.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/custom.css') }}">
@endsection
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-10">
		<h2>{{mb_strtolower($currentFunction->name)}}</h2>
	</div>
	<div class="col-lg-2">

	</div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox float-e-margins">
				<div class="ibox-content" style="display: flex;">
					<div class="box-avatar">
						<div class="avatar">
							<img src="img/noavatar.png" alt="avatar" id="avatar-profile">
						</div>
						<div class="box-chose-avatar">
							<div class="chose-avatar">
								<button class="btn btn-danger" type="button">Chọn ảnh đại diện</button>
							</div>
							<div class="delete-avatar">
								<button class="btn btn-default" type="button">Xóa ảnh</button>
							</div>
						</div>
					</div>
					<div class="box-info-student">
						<div style="width: 100%">	
							<div class="chose-uni">
								<label>{{ __('admin.chose_department') }} <span class="contraint">*</span></label>
								<div class="chose-uni-title chose-uni-profile-title">
									<span id="id-department" value="not" name="id_department">{{ __('admin.chose_department') }}</span>
									<i class="fa fa-caret-down rt"></i>
								</div>
								<div class="chose-uni-content">
									<div><input type="text" id="search-department" class="form-control" placeholder="Tìm kiếm khoa"></div>
									<ul id="list-department">
										@foreach ($unis as $uni)
										<li data-id="{{ $uni->id }}">{{ $uni->name }}</li>
										@endforeach
									</ul>
								</div>
							</div>
							<div class="chose-uni">
								<label>{{ __('admin.chose_major') }} <span class="contraint">*</span></label>
								<div class="chose-uni-title chose-uni-profile-title">
									<span id="id-major" value="not" name="id_major">{{ __('admin.chose_major') }}</span>
									<i class="fa fa-caret-down rt"></i>
								</div>
								<div class="chose-uni-content">
									<span class="list-major-notify">
										{{ __('admin.profile.notify_chose_major') }}
									</span>
									<ul id="list-major">
									</ul>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label for="name-profile">{{ __('admin.profile.name') }} <span class="contraint">*</span></label>
										<input type="text" name="name_profile" class="form-control input-text" id="name-profile" data-error="{{ __('admin.profile.name_error') }}">
									</div>
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label for="educational-background">{{ __('admin.profile.educational_background') }} <span class="contraint">*</span></label>
										<input type="text" name="educational_background" class="form-control input-text" id="educational-background" data-error="{{ __('admin.profile.educational_background_error') }}">
									</div>
								</div>
							</div>
							<div class="form-group">
								<label for="desc-profile">{{ __('admin.desc') }}</label>
								<textarea class="form-control input-text" name="desc_profile" id="desc-profile" data-error="{{ __('admin.desc_error') }}"></textarea>
							</div>
							<div class="box-btn">
								<button type="button" class="btn btn-primary" id="save-profile">{{ __('admin.save') }}</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@include('backend.system.file.module')
@endsection
@section('script')
<script src={{ asset('js/plugins/summernote/summernote.min.js') }}></script>
<script src={{ asset('js/custom.js') }}></script>
<script>
	/* save profile */
	$(document).on('click','#save-profile',function(){
		let check = true;
		if(typeof mainUrl === 'undefined' || mainUrl != 'profile') {
			$('.chose-uni-profile-modal-title span').each(function(){
				if($(this).attr('value') == 'not') {
					check = false;
					return true;
				}
			});
		}
		else {
			$('.chose-uni-profile-title span').each(function(){
				if($(this).attr('value') == 'not') {
					check = false;
					return true;
				}
			});
		}
		if(!check) swal('Vui lòng chọn đầy đủ khoa-ngành');
		else {
			if(checkNotValue('input-text')) {
				$(this).append('<i class="fa fa-refresh load" style="margin-left: 5px"></i>');
				let key = ',id_department,name_department,id_major,name_major,avatar,name_profile,educational_background,desc_profile';
				let value = ','+$('#id-department').attr('value')+','+
				$('#id-department').text()+','+
				$('#id-major').attr('value')+','+
				$('#id-major').text()+','+
				$('#avatar-profile').attr('src')+','+
				$('input[name=name_profile]').val()+','+
				$('input[name=educational_background]').val()+','+
				$('textarea[name="desc_profile"]').val();
				$.ajax({
					url: 'profile/'+$('.clear').data('id'),
					type: 'PUT',
					dataType: 'JSON',
					data: {
						key: key,
						value: value,
						user_id: $('.clear').data('id')
					},
					success: function(data){
						$('#save-profile i').remove();
						let value = JSON.parse(JSON.stringify(data));
						notification(value.type, value.title, value.content);
						if(value.type=="success") {
							$('#modal-profile').modal('hide');
						}
					}
				});
			}
			else return checkNotValue('input-text');
		}
	}); 
	/* summernote */
	$('textarea[name=desc_profile]').summernote({
		height: 150,
	});
	let mainUrl = '{{$currentFunction->route}}';
	@if ($userProfile != null)
	let key = {!! $userProfile->key !!};
	let value = {!! $userProfile->value !!};
	key = key.split(',');
	value = value.split(',');
	for (let i = 0; i < key.length; i++) {
		$('span[name="'+key[i]+'"]').attr('value',value[i]);
		$('span[name="'+key[i]+'"]').text(value[i+1]);
		switch(key[i]){
			case 'id_department':
			laodMajor(value[i]);
			break;
			case 'desc_profile':
			$('textarea[name="'+key[i]+'"]').val(value[i]);
			$('textarea[name="'+key[i]+'"]').closest('.form-group').find('.note-editable').html(value[i]);
			break;
			case 'avatar':
			$('#avatar-profile').attr('src',value[i]);
			break;
		}
		$('input[name='+key[i]+']').val(value[i]);
	}
	@endif
</script>
@endsection
