<div class="box-avatar">
  <div class="avatar">
    <img src="img/noavatar.png" alt="avatar" id="avatar-student">
  </div>
  <div class="box-chose-avatar">
    <div class="chose-avatar">
      <button class="btn btn-danger" type="button">Chọn ảnh đại diện</button>
    </div>
    <div class="delete-avatar">
      <button class="btn btn-default" type="button">Xóa ảnh</button>
    </div>
  </div>
</div>
<form id="form-import-student">
  <input type="hidden" name="id" value="0">
  <div class="box-info-student">
    <div>
      <div class="form-group">
        <label for="stdcode">{{ __('admin.student.stdcode') }} <span class="contraint">*</span></label>
        <input type="text" name="stdcode" class="form-control input-text-student" id="stdcode" data-error="{{ __('admin.student.stdcode_error') }}">
      </div>
      <div class="form-group">
        <label for="name">{{ __('admin.student.name') }} <span class="contraint">*</span></label>
        <input type="text" name="name" class="form-control input-text-student" id="name" data-error="{{ __('admin.student.name_error') }}">
      </div>
      <div class="row">
        <div class="col-lg-4">
          <div class="form-group">
            <label for="birthday">{{ __('admin.student.birthday') }} <span class="contraint">*</span></label>
            <input type="date" name="birthday" class="form-control input-text-student" id="birthday" data-error="{{ __('admin.student.birthday_error') }}">
          </div>
        </div>
        <div class="col-lg-3">
          <div class="form-group">
            <label for="sex">{{ __('admin.student.sex') }} <span class="contraint">*</span></label>
            <select class="form-control" id="sex" name="sex">
              <option value="1">Nam</option>
              <option value="0">Nữ</option>
              <option value="2">Khác</option>
            </select>
          </div>
        </div>
        <div class="col-lg-5">
          <div class="form-group">
            <label for="phone">{{ __('admin.student.phone') }}</label>
            <input type="text" name="phone" class="form-control" id="phone">
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-7">
          <div class="form-group">
            <label for="email">Email:</label>
            <input type="text" name="email" class="form-control input-text-student" id="email" data-error="{{ __('admin.student.email_error') }}">
          </div>
        </div>
        <div class="col-lg-5">
          <div class="form-group">
            <label for="start_date">{{ __('admin.student.start_date') }} <span class="contraint">*</span></label>
            <input type="date" name="start_date" class="form-control input-text-student" id="start_date" data-error="{{ __('admin.student.start_date_error') }}">
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-2">
          <div class="form-group">
            <label for="course">{{ __('admin.student.course') }} <span class="contraint">*</span></label>
            <input type="text" name="course" class="form-control input-text-student" id="course" data-error="{{ __('admin.student.course_error') }}">
          </div>
        </div>
        <div class="col-lg-5">
          <div class="form-group">
            <label for="class">{{ __('admin.student.class') }} <span class="contraint">*</span></label>
            <input type="text" name="class" class="form-control input-text-student" id="class" data-error="{{ __('admin.student.class_error') }}">
          </div>
        </div>
        <div class="col-lg-5">
          <div class="form-group">
            <label for="state">{{ __('admin.student.state') }}</label>
            <select class="form-control" id="state" name="state">
              <option value="1">{{ __('admin.student.active') }}</option>
              <option value="0">{{ __('admin.student.not_active') }}</option>
            </select>
          </div>
        </div>
      </div>
    </div>
  </div>
</form>
@section('script-lesson')
<script>
  /* reset form import student */
  $(document).on('click','#refresh-form', function(){
    $('#form-import-student').trigger('reset');
    $('.avatar img').attr('src','img/noavatar.png');
  });
  /* save student */
  $(document).on('click','#save-student',function(){
    let url = 'student',
    id      = $('input[name=id]').val(),
    type    = 'POST';
    if(parseInt(id)) {
      url  = url + '/' + id;
      type = 'PUT';
    }
    if(checkNotValue('input-text-student')) {
      if(checkEmail($('input[name=email]').val())) {
        if(checkPhone($('input[name=phone]').val())) {
          $(this).append('<i class="fa fa-refresh load" style="margin-left: 5px"></i>');
          $.ajax({
            url: url,
            type: type,
            dataType: 'JSON',
            data: {
              stdcode: $('input[name=stdcode]').val(),
              email: $('input[name=email]').val(),
              name: $('input[name=name]').val(),
              birthday: $('input[name=birthday]').val(),
              sex: $('select[name=sex]').val(),
              phone: $('input[name=phone]').val(),
              avatar: $('#avatar-student').attr('src'),
              course: $('input[name=course]').val(),
              start_date: $('input[name=start_date]').val(),
              class: $('input[name=class]').val(),
              state: $('select[name=state]').val(),
            },
            success: function(data){
              $('#save-student i').remove();
              let value = JSON.parse(JSON.stringify(data));
              notification(value.type, value.title, value.content);
              if(value.type == 'success') {
                Table.ajax.reload();
                $('#modal-student').modal('hide');
              }
            }
          });
        } else swal('Số điện thoại không đúng');
      }
      else swal('Email không đúng');
    }
    else return checkNotValue('input-text-student');
  });

  /* check email */
  function checkEmail(email){
    if(email == '') return true;
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }
  /* check phone */
  function checkPhone(phone){
    if(phone == '') return true;
    if(isNaN(phone)) return false;
    return true;
  }
</script>
@stop