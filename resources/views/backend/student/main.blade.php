@extends('layouts.app')
@section('title')
<title>{{$currentFunction->name}} - {{\Illuminate\Support\Facades\Crypt::decryptString(config('custom.app_name'))}}</title>
@endsection
@section('style')
<link href="css/plugins/dataTables/datatables.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/plugins/dropzone/dropzone.css">
<link rel="stylesheet" type="text/css" href="css/plugins/summernote/summernote.css">
<link rel="stylesheet" type="text/css" href="{{ asset('css/plugins/switchery/switchery.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/plugins/iCheck/custom.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/custom.css') }}">
<style>
td.td-name{
  display: flex;
  align-items: center;
}
table.dataTables-students > tbody > tr > td{
  vertical-align: middle ;
}
td.td-name img{
  width: 50px;
  height: 50px;
  vertical-align: middle;
  border-radius: 50px;
}
td.td-name span{
  flex: 1;
  margin-left: 5px;
}
#modal-student .modal-dialog{
  width: 80%;
}
</style>
@endsection
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-10">
    <h2>{{ __('admin.title_page') }} {{mb_strtolower($currentFunction->name)}}</h2>
  </div>
  <div class="col-lg-2">

  </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">
        <div class="ibox-content">
          <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover dataTables-students">
              <thead>
                <tr>
                  <th width="30">{{ __('admin.table_stt') }}</th>
                  <th>{{ __('admin.student.name') }}</th>
                  <th>{{ __('admin.student.stdcode') }}</th>
                  <th class="text-center" width="50">{{ __('admin.student.sex') }}</th>
                  <th>{{ __('admin.student.table_uni') }}</th>
                  <th>{{ __('admin.student.table_depart') }}</th>
                  <th>{{ __('admin.student.table_major') }}</th>
                  <th class="text-center">{{ __('admin.table_state') }}</th>
                  <th>{{ __('admin.table_action') }}</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal add new function -->
@include('backend.student.modal-form')
@include('backend.system.file.module')
@endsection
@section('script')
<script src="js/plugins/dataTables/datatables.min.js"></script>
<script src={{ asset('js/custom.js') }}></script>
<script>
  let mainUrl = '{{$currentFunction->route}}';
  /* datatable student*/
  let Table = $('.dataTables-students').DataTable({
    bSort: false,
    bInfo: true,
    bLengthChange: false,
    processing: true,
    serverSide: true,
    searching: true,
    paging: true,
    pageLength: 15,
    responsive: true,
    ajax: mainUrl + '/getDatatable',
    dom: '<"row"<"col-sm-6"Bi><"col-sm-6"f>>rtp',
    buttons: {
      buttons: [
      {
        text: 'Thêm mới',
        action: function ( e, dt, node, config ) {
          resetForm();
          $('#modal-student').modal({backdrop: 'static', keyboard: false, show : true});
        }
      }
      ],
      dom: {
        button: {
          tag: "button",
          className: "btn btn-primary"
        },
        buttonLiner: {
          tag: null
        }
      }
    },
    language: {
      "lengthMenu": "Hiển thị _MENU_ bản ghi trên 1 trang",
      "zeroRecords": "Không có dữ liệu",
      "info": "Hiển thị trang _PAGE_ trong tổng số _PAGES_ trang",
      "infoEmpty": "Không có dữ liệu tồn tại",
      "infoFiltered": "(Lọc từ _MAX_ bản ghi)",
      "search": "Tìm kiếm:",
      "paginate": {
        "first":      "Trang đầu",
        "last":       "Trang cuối",
        "next":       "Trang tiếp theo",
        "previous":   "Trang trước"
      },
    },
    columns     :   [
    {
      data    :   'id',
      className:  'text-center',
      render  :   function (data, type, row, meta) {
        return meta.row +1;
      }
    },
    {
      data    :   'id',
      className: 'td-name',
      render  :   function (data, type, row, meta) {
        let avatar = row.avatar;
        if(avatar == null) avatar = 'img/noavatar.png';
        return '<img src="'+avatar+'" alt="no-img"><span>'+row.lastname +' '+row.firstname+'</span>';
      }
    },
    { data    :   'stdcode'},
    {
      data    :   'sex',
      className: 'text-center',
      render  :   function (data, type, row, meta) {
        if(data == 1) return 'Nam';
        if(data == 0) return 'Nữ';
        return 'Khác';
      }
    },
    { 
      data    :   'info',
      render  :   function (data, type, row, meta) {
        return row.info.uni;
      }
    },
    { 
      data    :   'info',
      render  :   function (data, type, row, meta) {
        return row.info.dept;
      }
    },
    { 
      data    :   'info',
      render  :   function (data, type, row, meta) {
        return row.info.majo;
      }
    },
    {
      data    :   'state',
      className: 'text-center',
      render  :   function (data, type, row, meta) {
        if(data == 0) return 'Không kích hoạt';
        return 'Kích hoạt';
      }
    },
    {
      data    :   'id',
      className: 'text-center',
      render  :   function (data, type, row, meta) {
        let button  =   '@permission("Edit-20")<button name="btn-edit" class="btn btn-primary btn-flat btn-xs" title="{{ __('admin.title_btn_edit') }}" data-data=\''+JSON.stringify(row)+'\'><i class="fa fa-edit"></i></button>@endpermission @permission("Delete-20")<button name="btn-delete" class="btn btn-remove btn-danger btn-flat btn-xs" title="{{ __('admin.title_btn_delete') }}" data-id='+data+'><i class="fa fa-remove"></i></button>@endpermission';
        return button;
      }
    }
    ]
  });
  /* edit student */
  $(document).on('click','button[name=btn-edit]',function(){
    let data = $(this).data('data'),
    name = data.lastname + ' ' + data.firstname,
    info = data.info,
    avatar = data.avatar;
    if(avatar == null) avatar = 'img/noavatar.png';
    $.each(data, function(index, val) {
     $('input[name='+index+']').val(val);
   });
    $.each(info, function(index, val) {
      if(index != 'id') $('input[name='+index+']').val(val);
    });
    $('input[name=name]').val(name);
    $('select[name=sex] option[value='+data.sex+']').prop('selected', 'selected');
    $('select[name=state] option[value='+data.state+']').prop('selected', 'selected');
    $('#avatar-student').attr('src', avatar);
    $('#modal-student').modal();
  });
  /* delete student */
  $(document).on('click','button[name=btn-delete]',function(){
    let element = $(this).find('i'),
    id = $(this).data('id');
    swal({
      title: "Bạn có muốn xóa sinh viên này không?",
      text: "Khi xóa sinh viên sẽ không thể khôi phục lại",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "OK",
      cancelButtonText: "Không",
      closeOnConfirm: true,
      closeOnCancel: true
    }, function(config){
      if(config){
        element.removeClass('fa fa-trash');
        element.addClass('fa fa-refresh load');
        $.ajax({
          url: 'student/' + id,
          type: 'DELETE',
          dataType: 'json',
          success: function(data){
            let value = JSON.parse(JSON.stringify(data));
            notification(value.type, value.title, value.content);
            if(value.type == 'success') {
              Table.ajax.reload();
            }
          }
        });
      }
    });
  });
  function resetForm() {
    $('input[name=id]').val(0);
    $('#form-import-student').trigger('reset');
    $('.avatar img').attr('src','img/noavatar.png');
  }
</script>
@endsection
