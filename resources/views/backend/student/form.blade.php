
@extends('layouts.app')
@section('title')
<title>{{$currentFunction->name}} - {{\Illuminate\Support\Facades\Crypt::decryptString(config('custom.app_name'))}}</title>
@endsection
@section('style')
<link href="css/plugins/dataTables/datatables.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/plugins/dropzone/dropzone.css">
<link rel="stylesheet" type="text/css" href="css/plugins/summernote/summernote.css">
<link rel="stylesheet" type="text/css" href="{{ asset('css/plugins/switchery/switchery.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/plugins/iCheck/custom.css') }}">
@endsection
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-10">
    <h2>{{mb_strtolower($currentFunction->name)}}</h2>
  </div>
  <div class="col-lg-2">

  </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">
        <div class="ibox-content" style="display: flex;">
          @include('backend.student.form-body')
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
