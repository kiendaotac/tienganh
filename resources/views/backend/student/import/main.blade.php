@extends('layouts.app')
@section('title')
<title>{{$currentFunction->name}} - {{\Illuminate\Support\Facades\Crypt::decryptString(config('custom.app_name'))}}</title>
@endsection
@section('style')
<link href="css/plugins/dataTables/datatables.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/plugins/dropzone/dropzone.css">
<link rel="stylesheet" type="text/css" href="css/plugins/summernote/summernote.css">
<link rel="stylesheet" type="text/css" href="{{ asset('css/plugins/switchery/switchery.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/plugins/iCheck/custom.css') }}">
<style>
.chose-excel{
  cursor: pointer;
}
.success{
  color: #1ab394;
}
.error{
  color: #ec4758;
}
.count-inport-student{
  margin-bottom: 10px;
}
#info-import-student{
  font-weight: bold;
}
</style>
@endsection
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-10">
    <h2>{{mb_strtolower($currentFunction->name)}}</h2>
  </div>
  <div class="col-lg-2">

  </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">
        <div class="ibox-content">
          <form id="f-import-student">
            <input type="hidden" name="uni_id" value="{{$uniId}}">
            <div class="row">
              <div class="col-lg-6 col-lg-push-3">
                <div class="form-group">
                  <label for="chose_excel">{{ __('admin.import.chose_excel') }} <span class="contraint">*</span></label>
                  <div class="input-group">
                    <span class="input-group-addon chose-excel"><span class="fa fa-file"></span></span>
                    <input type="text" class="form-control" name="chose_excel" readonly="">
                  </div>
                </div>
                <div class="form-group text-right">
                  <button type="button" class="btn btn-primary" id="save">{{ __('admin.save') }}</button>
                  <a href="storage/uploads/2018/10/1539765831-import-student-mcq.xlsx">
                    <button type="button" class="btn btn-default">{{ __('admin.import.dowload_excel') }}</button>
                  </a>
                  
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@include('backend.system.file.module')
@include('backend.student.import.modal')
@endsection
@section('script')
<script src={{ asset('js/custom.js') }}></script>
<script>
  /* perfect scroll */
  new PerfectScrollbar($('#modal-import-student .modal-body')[0]);
  /* save import excel */
  $(document).on('click','#save', function(){
    if($('input[name=chose_excel]').val() == '') swal('Vui lòng chọn file excel để import');
    else {
      $(this).append('<i class="fa fa-refresh load" style="margin-left: 5px;"></i>')
      let data = $('#f-import-student').serialize();
      $.ajax({
        url: 'import-student',
        type: 'POST',
        dataType: 'JSON',
        data: data,
        success: function(data){
          $('#save i').remove();
          let value = JSON.parse(JSON.stringify(data));
          if(value.type == 'success') {
            $('#count-success').text(value.count);
            $('#total-student').text(value.total);
            $('.table-import-student tbody').html('');
            let i = 1;
            $.each(value.students, function(index, val) {
              let td = '',
              col        = val.col,
              stdcode    = (val.ma_sv == null) ? '' : val.ma_sv;
              name       = (val.ho_va_ten == null) ? '' : val.ho_va_ten;
              sex        = (val.gioi_tinh == null) ? '' : val.gioi_tinh;
              start_date = processDate(val.nam_vao_truong);
              birthday   = processDate(val.ngay_sinh);
              class_manager      = (val.lop_quan_ly == null) ? '' : val.lop_quan_ly;
              if(val.col == 0) td = '<td class="text-center"><i class="fa fa-check success"></i></td>';
              else td = '<td class="text-center"><i class="fa fa-times error"></i></td>';
              let tr = '<tr>' +
              '<td>'+i+'</td>' +
              '<td>'+stdcode+'</td>' +
              '<td>'+name+'</td>' +
              '<td>'+sex+'</td>' +
              '<td>'+start_date+'</td>' +
              '<td>'+birthday+'</td>' +
              '<td>'+class_manager+'</td>' + td +
              '</tr>';
              $('.table-import-student tbody').append(tr);
              $.each(col, function(i, v) {
                if(v != 0) {
                  v--;
                  $('.table-import-student tbody tr:last-child td:eq('+v+')').css({'background-color':'#ec4758','color':'#fff'});
                }
              });
              i++;
            });
            $('#modal-import-student').modal();
          }
        }
      });
    }
  });
  /* process date */
  function processDate(date){
    if(date == null) return '';
    else {
      if(typeof date === 'object') {
        let temp = date.date;
        return temp.substr(0,temp.lastIndexOf(' '));
      }
    }
    return date;
  }
</script>
@endsection
