
<div id="modal-import-student" class="modal fade modal-full" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><i class="fa fa-check"></i>{{ __('admin.import.modal_title') }}</h4>
      </div>
      <div class="modal-body">
        <div class="count-inport-student">
          Import thành công <span id="info-import-student"><span id="count-success">0</span> / <span id="total-student">0</span></span> sinh viên
        </div>
        <div class="table-responsive">
          <table class="table table-striped table-bordered table-hover table-import-student">
            <thead>
              <th width="50">STT</th>
              <th>Mã sinh viên</th>
              <th>Họ và tên</th>
              <th>Giới tính</th>
              <th>Năm vào trường</th>
              <th>Ngày sinh</th>
              <th>Lớp quản lý</th>
              <th width="100" class="text-center">Thành công</th>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('admin.close') }}</button>
      </div>
    </div>

  </div>
</div>