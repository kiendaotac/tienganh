@extends('layouts.app')
@section('title')
<title>{{$currentFunction->name}} - {{\Illuminate\Support\Facades\Crypt::decryptString(config('custom.app_name'))}}</title>
@endsection
@section('style')
<link href="css/plugins/dataTables/datatables.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/plugins/summernote/summernote.css">
<style>
#modal-class-curriculum .modal-dialog{
  width: 50%;
}
#modal-class-curriculum .modal-body{
  max-height: 400px;
  overflow-y: auto;
}
td.td-name{
  display: flex;
  align-items: center;
}
table > tbody > tr > td{
  vertical-align: middle !important;
}
td.td-name img{
  width: 50px;
  height: 50px;
  vertical-align: middle;
  border-radius: 50px;
}
td.td-name span{
  flex: 1;
  margin-left: 5px;
}
</style>
@endsection
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-10">
    <h2>{{ __('admin.title_page') }} {{mb_strtolower($currentFunction->name)}}</h2>
  </div>
  <div class="col-lg-2">

  </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">
        <div class="ibox-content">
          <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover dataTables-class-curriculum">
              <thead>
                <tr>
                  <th width="30">{{ __('admin.table_stt') }}</th>
                  <th>{{ __('admin.class_curriculum.name') }}</th>
                  <th>{{ __('admin.class_curriculum.table_curriculum') }}</th>
                  <th class="text-center" width="200">{{ __('admin.class_curriculum.table_number_student') }}</th>
                  <th width="100">{{ __('admin.table_action') }}</th>
                </tr> 
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>  
<!-- Modal add new function -->
@include('backend.student.class_curriculum.form')
@include('backend.student.class_curriculum.modal-chose-student')
@endsection
@section('script')
<script src="js/plugins/dataTables/datatables.min.js"></script>
<script>
  let class_id = null;
  new PerfectScrollbar($('.modal-body')[0]);
  let mainUrl = '{{$currentFunction->route}}';
  /* datatable class curriculum*/
  let Table = $('.dataTables-class-curriculum').DataTable({
    bSort: false,
    bInfo: true,
    bLengthChange: false,
    processing: true,
    serverSide: true,
    searching: true,
    paging: true,
    pageLength: 15,
    responsive: true,
    ajax: mainUrl + '/getDatatable',
    dom: '<"row"<"col-sm-6"Bi><"col-sm-6"f>>rtp',
    buttons: {
      buttons: [
      {
        text: 'Thêm mới',
        action: function ( e, dt, node, config ) {
          resetForm();
          $('#modal-class-curriculum').modal({backdrop: 'static', keyboard: false, show : true});
        }
      }
      ],
      dom: {
        button: {
          tag: "button",
          className: "btn btn-primary"
        },
        buttonLiner: {
          tag: null
        }
      }
    },
    language: {
      "lengthMenu": "Hiển thị _MENU_ bản ghi trên 1 trang",
      "zeroRecords": "Không có dữ liệu",
      "info": "Hiển thị trang _PAGE_ trong tổng số _PAGES_ trang",
      "infoEmpty": "Không có dữ liệu tồn tại",
      "infoFiltered": "(Lọc từ _MAX_ bản ghi)",
      "search": "Tìm kiếm:",
      "paginate": {
        "first":      "Trang đầu",
        "last":       "Trang cuối",
        "next":       "Trang tiếp theo",
        "previous":   "Trang trước"
      },
    },
    columns     :   [
    {
      data    :   'id',
      className:  'text-center',
      render  :   function (data, type, row, meta) {
        return meta.row +1;
      }
    },
    { 
      data    :   'name',
      render  : function (data, type, row, meta) {
        if (row.parent_id == 0){
          return '<b>'+data+'</b>';
        } else {
          return '&emsp;'+data;
        }
      }
    },
    {
      data    :   'curriculum',
      render  :   function (data, type, row, meta) {
        return 'abc';
      }
    },
    {
      data    :   'number_student',
      className: 'text-center',
      render  :   function (data, type, row, meta) {
        return '';
      }
    },
    {
      data    :   'id',
      className: 'text-center',
      render  :   function (data, type, row, meta) {
        let button  =   '<button style="margin-right: 3px" name="btn-plus" class="btn btn-primary btn-flat btn-xs" title="{{ __('admin.title_btn_edit') }}" data-data=\''+JSON.stringify(row)+'\'><i class="fa fa-plus"></i></button>@permission("Edit-27")<button name="btn-edit" class="btn btn-primary btn-flat btn-xs" title="{{ __('admin.title_btn_edit') }}" data-data=\''+JSON.stringify(row)+'\'><i class="fa fa-edit"></i></button>@endpermission @permission("Delete-27")<button name="btn-delete" class="btn btn-remove btn-danger btn-flat btn-xs" title="{{ __('admin.title_btn_delete') }}" data-id='+data+'><i class="fa fa-remove"></i></button>@endpermission';
        return button;
      }
    }
    ]
  });
  /* data table student chosed */
  let TableStudentChosed = $('.dataTables-students-chosed').DataTable({
    bSort: false,
    bInfo: false,
    bLengthChange: false,
    searching: true,
    paging: true,
    pageLength: 15,
    responsive: true,
    dom: '<"row"<"col-sm-6"Bi><"col-sm-6"f>>rtp',
    buttons: {
      buttons: [
      {
        text: 'Bỏ tất cả',
        action: function ( e, dt, node, config ) {
          let id = [],
          tr     = [];
          if($('input[name=chosed_student]').length <= 0) swal('Không có sinh viên nào');
          else {
            $('input[name=chosed_student]').each(function(){
              tr.push($(this).closest('tr'));
              id.push($(this).data('id'));
            });
            $.ajax({
              url: 'class-curriculum-student',
              type: 'POST',
              dataType: 'json',
              data: {
                id: JSON.stringify(id),
                type: 'unChosed',
              },
              success: function(data){
                let value = JSON.parse(JSON.stringify(data));
                notification(value.type, value.title, value.content);
                if(value.type == "success") {
                  for (let i = 0; i < id.length; i++) {
                    tr[i].find('input').attr('name','chose_student');
                    $('.dataTables-students tbody').append('<tr>'+tr[i][0].innerHTML+'</tr>');
                    tr[i].remove();
                  }
                }
              }
            });
          }
        }
      }
      ],
      dom: {
        button: {
          tag: "button",
          className: "btn btn-primary"
        },
        buttonLiner: {
          tag: null
        }
      }
    },
    language: {
      "lengthMenu": "Hiển thị _MENU_ bản ghi trên 1 trang",
      "zeroRecords": "Không có dữ liệu",
      "info": "Hiển thị trang _PAGE_ trong tổng số _PAGES_ trang",
      "infoEmpty": "Không có dữ liệu tồn tại",
      "infoFiltered": "(Lọc từ _MAX_ bản ghi)",
      "search": "Tìm kiếm:",
      "paginate": {
        "first":      "Trang đầu",
        "last":       "Trang cuối",
        "next":       "Trang tiếp theo",
        "previous":   "Trang trước"
      },
    },
  });
  /* data table student */
  let TableStudent = $('.dataTables-students').DataTable({
    bSort: false,
    bInfo: false,
    bLengthChange: false,
    searching: true,
    paging: true,
    pageLength: 15,
    responsive: true,
    dom: '<"row"<"col-sm-6"Bi><"col-sm-6"f>>rtp',
    buttons: {
      buttons: [
      {
        text: 'Chọn tất cả',
        action: function ( e, dt, node, config ) {
          let stdcode         = [],
          class_curriculum_id = [],
          tr                  = [];
          if($('input[name=chose_student]').length <= 0) swal('Không có sinh viên nào');
          else {
            $('input[name=chose_student]').each(function(){
              tr.push($(this).closest('tr'));
              stdcode.push($(this).val());
              class_curriculum_id.push($(this).data('class'));
            });
            $.ajax({
              url: 'class-curriculum-student',
              type: 'POST',
              dataType: 'json',
              data: {
                stdcode: JSON.stringify(stdcode),
                class_curriculum_id: JSON.stringify(class_curriculum_id),
                type: 'chosed',
              },
              success: function(data){
                let value = JSON.parse(JSON.stringify(data));
                notification(value.type, value.title, value.content);
                if(value.type == "success") {
                  for (let i = 0; i < value.id.length; i++) {
                    tr[i].find('input').attr('name','chosed_student');
                    tr[i].find('input').attr('data-id',value.id[i]);
                    $('.dataTables-students-chosed tbody').append('<tr>'+tr[i][0].innerHTML+'</tr>');
                    tr[i].remove();
                  }
                }
              }
            })
          }
        }
      }
      ],
      dom: {
        button: {
          tag: "button",
          className: "btn btn-primary"
        },
        buttonLiner: {
          tag: null
        }
      }
    },
    language: {
      "lengthMenu": "Hiển thị _MENU_ bản ghi trên 1 trang",
      "zeroRecords": "Không có dữ liệu",
      "info": "Hiển thị trang _PAGE_ trong tổng số _PAGES_ trang",
      "infoEmpty": "Không có dữ liệu tồn tại",
      "infoFiltered": "(Lọc từ _MAX_ bản ghi)",
      "search": "Tìm kiếm:",
      "paginate": {
        "first":      "Trang đầu",
        "last":       "Trang cuối",
        "next":       "Trang tiếp theo",
        "previous":   "Trang trước"
      },
    },
  });
  /* reset form */ 
  function resetForm(){
    $('input[name=id]').val(0);
    $('#form-class-curriculum').trigger('reset');
    $('#id-curriculum').attr('value',0);
    $('#id-curriculum').text('-- Chọn khóa học --');
    $('#id-class').attr('value',0);
    $('#id-class').text('-- Không thuộc khóa nào --');
  }
  /* chose curriculum or lesson */
  $(document).on('click', '.chose-class-curriculum-content ul li', function(){
    let spanElement = $(this).closest('.chose-class-curriculum').find('.chose-class-curriculum-title span');
    spanElement.text($(this).text());
    spanElement.attr('value', $(this).data('id'));
    $(this).closest('.chose-class-curriculum-content').slideToggle();
    if($(this).closest('ul')[0].id == 'list-class-curriculum'){
      $('.box-list-course').html('<span class="list-lesson-notify">Hãy họn khóa học trước</span>');
      $('#id-class').text('-- Không thuộc khóa nào --');
      $('#id-class').attr('value', 0);
      let divContent = $('.list-lesson-notify').closest('.chose-class-curriculum-content');
      $('.list-lesson-notify').html('<i class="fa fa-refresh load"></i>');
      $.ajax({
        url: 'class-curriculum/getCourse',
        type: 'GET',
        dataType: 'JSON',
        data: {
         curriculum_id: $(this).data('id')
       },  
       success: function(data){
        $('.list-lesson-notify').hide();
        let value = JSON.parse(JSON.stringify(data));
        divContent.html('<div><input type="text" id="search-class-curriculum" class="form-control" placeholder="Tìm kiếm khóa"></div><ul id="list-class"></ul>');
        $.each(value, function(index, item){
          let liElement = '<li data-id='+item.id+'>'+item.name+'</li>';
          divContent.find('ul').append(liElement);
        });
        if(idParent != 0) $('#list-class li[data-id='+idParent+']').trigger('click');
        $('.chose-class-curriculum-content').each(function(){
         if($(this).css('display') === 'block') {
          $(this).hide();
        }
      });
      }
    });
    }
  });
  $(document).on('click','#save',function(){
    let id = $('input[name=id]').val(),
    url = 'class-curriculum',
    type = 'POST';
    if(parseInt(id)) {
      url = url + '/' + id,
      type = 'PUT';
    }
    if($('input[name=name]').val() === '') swal($('input[name=name]').data('error'));
    else if($('#id-curriculum').attr('value') == 0) swal('Vui lòng chọn khóa học');
    else {
      $(this).append('<i class="fa fa-refresh load" style="margin-left: 5px"></i>');
      let students = [];
      $('input[name=chose_student]').each(function(){
        if($(this).is(':checked')) students.push($(this).val());
      });
      $.ajax({
        url: url,
        type: type,
        dataType: 'JSON',
        data: {
          name: $('input[name=name]').val(),
          curriculum_id: $('#id-curriculum').attr('value'),
          parent_id: 0,
          desc: $('textarea[name=desc]').val(),
          user_id: {{Auth::user()->id}},
        },
        success: function (data) {
          $('#save i').remove();
          let value = JSON.parse(JSON.stringify(data));
          notification(value.type, value.title, value.content);
          if(value.type == 'success') {
            $('#modal-class-curriculum').modal('hide');
            Table.ajax.reload();
          }
        }
      })
    }
  });
  $(document).on('click','button[name="btn-edit"]',function(){
    let data = $(this).data('data');
    idParent = data.parent_id;
    $('#list-class-curriculum li[data-id='+data.curriculum.id+']').trigger('click');
    $.each(data, function(index, val) {
     $('input[name='+index+']').val(val);
     $('textarea[name='+index+']').val(val);
   });
    $('#modal-class-curriculum').modal({backdrop: 'static', keyboard: false, show : true});
  });
  /* delete class curriculum */
  $(document).on('click','button[name=btn-delete]',function(){
    let id = $(this).data('id');
    let button = $(this);
    swal({
      title: "Bạn chắc chắn muốn xóa?",
      text: "Khi xóa không thể khôi phục lại",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "OK",
      cancelButtonText: "Không",
      closeOnConfirm: true,
      closeOnCancel: true
    }, function(config){
      if(config){
        button.find('i').removeClass('fa-times');
        button.find('i').addClass('fa-refresh load');
        $.ajax({
          url: 'class-curriculum/' + id,
          type: 'DELETE',
          dataType: 'json',
          success: function(data){
            let value = JSON.parse(JSON.stringify(data));
            notification(value.type, value.title, value.content);
            if(value.type == 'success') {
              Table.ajax.reload();
            }
          }
        });
      }
    });
  });

  $(document).on('click','button[name=btn-plus]',function(){
    let data = $(this).data('data');
    if(class_id == null || class_id != data.id) {
      $('.dataTables-students tbody').html('');
      $('.dataTables-students-chosed tbody').html('');
    }
    class_id = data.id;
    let id = data.id;
    if($('.dataTables-students tbody tr').length  <= 1) {
      $('.dataTables-students tbody tr:first-child').hide();
      $('.dataTables-students tbody').append('<tr class="text-center tr-load"><td colspan="6"><i class="fa fa-refresh load"></i></td></tr>');
      $.ajax({
        url: 'class-curriculum-student/getStudent',
        type: 'GET',
        dataType: 'json',
        data: {
          curriculum_id: data.curriculum_id,
          class_curriculum_id: id,
        },
        success: function(data){
          $('.tr-load').remove();
          let value = JSON.parse(JSON.stringify(data));
          $.each(value, function(index, val) {
            let content = '<tr><td class="text-center"><input type="checkbox" name="chose_student" value="'+val.student.stdcode+'" data-class="'+id+'"></td>'+
            '<td class="td-name"><img src="'+val.student.avatar+'" alt="no-img"><span>'+val.student.lastname +' '+val.student.firstname+'</span></td>' +
            '<td>'+val.student.stdcode+'</td>' +
            '<td class="text-center">'+val.student.sex+'</td>' +
            '<td>'+val.student.info.dept+'</td>' +  
            '<td>'+val.student.info.majo+'</td></tr>';
            $('.dataTables-students tbody').append(content);            
          });
        }
      })
    }
    if($('.dataTables-students-chosed tbody tr').length  <= 1) {
      $('.dataTables-students-chosed tbody tr:first-child').hide();
      $('.dataTables-students-chosed tbody').append('<tr class="text-center tr-load-chosed"><td colspan="6"><i class="fa fa-refresh load"></i></td></tr>');
      $.ajax({
        url: 'class-curriculum-student/getStudentChosed',
        type: 'GET',
        dataType: 'json',
        data: {
          class_curriculum_id: data.id,
        },
        success: function(data){
          $('.tr-load-chosed').remove();
          let value = JSON.parse(JSON.stringify(data));
          $.each(value, function(index, val) {
            let content = '<tr><td class="text-center"><input type="checkbox" name="chosed_student" value="'+val.student.stdcode+'" data-id="'+val.id+'" data-class="'+val.class_curriculum_id+'"></td>'+
            '<td class="td-name"><img src="'+val.student.avatar+'" alt="no-img"><span>'+val.student.lastname +' '+val.student.firstname+'</span></td>' +
            '<td>'+val.student.stdcode+'</td>' +
            '<td class="text-center">'+val.student.sex+'</td>' +
            '<td>'+val.student.info.dept+'</td>' +  
            '<td>'+val.student.info.majo+'</td></tr>';
            $('.dataTables-students-chosed tbody').append(content);            
          });
        }
      })
    }
    /* datatable student */
    $('#modal-chose-student').modal({backdrop: 'static', keyboard: false, show : true});
  });
  /* chose student */
  $(document).on('click','input[name=chose_student]',function(){
    let td = $(this).closest('td'),
    tr     = $(this).closest('tr'),
    tdHtml = td.html();
    td.html('<i class="fa fa-refresh load"><i/>');
    $.ajax({
      url: 'class-curriculum-student',
      type: 'POST',
      dataType: 'JSON',
      data: {
        stdcode: $(this).val(),
        class_curriculum_id: $(this).data('class'),
      },
      success: function(data){
        let value = JSON.parse(JSON.stringify(data));
        td.html(tdHtml);
        td.find('input').attr('name','chosed_student');
        td.find('input').attr('data-id',value.id);
        notification(value.type, value.title, value.content);
        if(value.type == 'success') {
          $('.dataTables-students-chosed tbody').append('<tr>'+tr[0].innerHTML+'</tr>');
          tr.remove();
        }
      }
    })
  });
  /* un chose student */
  $(document).on('click','input[name=chosed_student]',function(){
    let td = $(this).closest('td'),
    tr     = $(this).closest('tr'),
    tdHtml = td.html();
    td.html('<i class="fa fa-refresh load"><i/>');
    $.ajax({
      url: 'class-curriculum-student/'+$(this).data('id'),
      type: 'DELETE',
      dataType: 'JSON',
      success: function(data){
        td.html(tdHtml);
        td.find('input').attr('name','chose_student');
        let value = JSON.parse(JSON.stringify(data));
        notification(value.type, value.title, value.content);
        if(value.type == 'success') {
          $('.dataTables-students tbody').append('<tr>'+tr[0].innerHTML+'</tr>');
          tr.remove();
        }
      }
    })
  });
</script>
@endsection
