
<div id="modal-class-curriculum" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><i class="fa fa-book"></i>{{ __('admin.class_curriculum.modal_title') }}</h4>
      </div>
      <div class="modal-body">
        <form id="form-class-curriculum">
          <input type="hidden" name="id">
          <div class="chose-class-curriculum">
            <label>{{ __('admin.class_curriculum.chose_curriculum') }} <span class="contraint">*</span></label>
            <div class="chose-class-curriculum-title">
              <span id="id-curriculum" value="0" name="id_curriculum">{{ __('admin.class_curriculum.id_curriculum') }} <span class="contraint">*</span></span>
              <i class="fa fa-caret-down rt"></i>
            </div>
            <div class="chose-class-curriculum-content">
              <div>
                <input class="form-control" id="search-class-curriculum" placeholder="Tìm kiếm khóa" type="text">
              </div>
              <ul id="list-class-curriculum">
                @foreach ($curriculums as $curriculum)
                <li data-id="{{ $curriculum->id }}">{{ $curriculum->name }}</li>
                @endforeach
              </ul>
            </div>
          </div>
          <div class="form-group">
            <label for="name">{{ __('admin.class_curriculum.name') }} <span class="contraint">*</span></label>
            <input type="text" name="name" class="form-control" id="name" data-error="{{ __('admin.class_curriculum.name_error') }}">
          </div>
          <div class="form-group">
            <label for="desc">{{ __('admin.desc') }}</label>
            <textarea class="form-control" id="desc" name="desc" rows="5"></textarea>
          </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" id="save">{{ __('admin.save') }}</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('admin.close') }}</button>
        </div>
      </div>

    </div>
  </div>