
<div id="modal-chose-student" class="modal fade modal-full" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><i class="fa fa-user"></i>{{ __('admin.class_curriculum.modal_chose_title') }}</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-lg-6">
            <div class="box-content">
              <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover dataTables-students-chosed">
                  <thead> 
                    <tr>
                      <th width="30"></th>
                      <th>{{ __('admin.student.name') }}</th>
                      <th>{{ __('admin.student.stdcode') }}</th>
                      <th class="text-center" width="50">{{ __('admin.student.sex') }}</th>
                      <th>{{ __('admin.student.table_depart') }}</th>
                      <th>{{ __('admin.student.table_major') }}</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
              <span class="box-content-title">Sinh viên đã có trong lớp</span>
            </div>
          </div>
          <div class="col-lg-6">
            <div class="box-content">
              <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover dataTables-students">
                  <thead>
                    <tr>
                      <th width="30"></th>
                      <th>{{ __('admin.student.name') }}</th>
                      <th>{{ __('admin.student.stdcode') }}</th>
                      <th class="text-center" width="50">{{ __('admin.student.sex') }}</th>
                      <th>{{ __('admin.student.table_depart') }}</th>
                      <th>{{ __('admin.student.table_major') }}</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
              <span class="box-content-title">Sinh viên chưa có trong lớp</span>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('admin.close') }}</button>
      </div>
    </div>

  </div>
</div>