<div id="modal-student" class="modal fade" role="dialog" style="overflow-y: auto">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><i class="fa fa-graduation-cap" style="margin-right: 5px"></i>{{ __('admin.student.modal_title') }}</h4>
      </div>
      <div class="modal-body" style="display: flex;">
        @include('backend.student.form-body')
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="save-student">{{ __('admin.save') }}</button>
        <button type="button" class="btn btn-default" id="refresh-form">{{ __('admin.student.refresh_form') }}</button>
      </div>
    </div>
  </div>
</div>