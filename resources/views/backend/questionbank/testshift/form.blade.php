<div class="modal inmodal" id="modal-add-function" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                            class="sr-only">Close</span></button>
                <i class="{{$currentFunction->icon}} modal-icon"></i>
                <h4 class="modal-title">Thêm {{mb_strtolower($currentFunction->name)}}</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <form method="POST" name="add-test-shift">
                    <div class="col-sm-6 form-group">
                        <label>Name</label>
                        <input type="text" name="name" class="form-control" required>
                    </div>
                    <div class="col-sm-6 form-group">
                        <label>Test form</label>
                        <select name="test_form_id" class="form-control">
                            @foreach($testforms as $testform)
                                <option value="{{$testform->id}}">{{$testform->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-sm-6 form-group">
                        <label>Test number</label>
                        <input type="number" name="test_numbers" class="form-control" required min="1" step="1">
                    </div>
                    <div class="col-sm-6 form-group">
                        <label>Tester Type</label>
                        <select name="tester_type" class="form-control">
                            <option value="0">Tự do</option>
                            <option value="1">Định sẵn</option>
                        </select>
                    </div>
                    {{--<div class="col-sm-6 form-group">--}}
                    {{--<label>Time active</label>--}}
                    {{--<input type="date" name="time_active" class="form-control">--}}
                    {{--</div>--}}
                    <div class="col-sm-3 form-group">
                        <label>Date</label>
                        <div class="input-group date">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" name="date" class="form-control" value="" required>
                        </div>
                    </div>
                    <div class="col-sm-2 form-group">
                        <label>Time</label>
                        <div class="input-group clockpicker" data-autoclose="true">
                            <input type="text" name="time" class="form-control" value="" placeholder="Now" required>
                            <span class="input-group-addon">
                                <span class="fa fa-clock-o"></span>
                            </span>
                        </div>
                    </div>
                    <div class="col-sm-1 form-group">
                        <label>Online</label>
                        <input type="checkbox" name="online" class="form-control">
                    </div>
                    <div class="col-sm-3 form-group">
                        <label>Time late</label>
                        <input type="number" name="time_late" class="form-control" value="0" min="0" step="5" required>
                    </div>
                    <div class="form-group col-sm-3">
                        <label>Trạng thái</label>
                        <select name="state" class="form-control">
                            <option value="1">Kích hoạt</option>
                            <option value="0">Không kích hoạt</option>
                        </select>
                    </div>
                        <input type="hidden" name="id" value="0">
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" name="add-function"><i class="fa fa-save"></i> Save
                </button>
            </div>
        </div>
    </div>
</div>
