@extends('layouts.app')
@section('title')
    <title>{{$currentFunction->name}} - {{\Illuminate\Support\Facades\Crypt::decryptString(config('custom.app_name'))}}</title>
@endsection

@section('style')
    <link href="css/plugins/dataTables/datatables.min.css" rel="stylesheet">
    <link href="css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="css/plugins/clockpicker/clockpicker.css" rel="stylesheet">
    <link href="css/plugins/datapicker/datepicker3.css" rel="stylesheet">
    <style>
        table.dataTables-testForm > thead > tr > th.stt {
            width: 15px;
        }

        table.dataTables-testForm > thead > tr > th.time-late {
            width: 80px;
        }

        table.dataTables-testForm > thead > tr > th.time-active {
            width: 169px!important;
        }

        table.dataTables-testForm > thead > tr > th.tester-type {
            width: 138px!important;
        }

        table.dataTables-testForm > thead > tr > th.test-number {
            width: 120px;
        }

        table.dataTables-testForm > thead > tr > th.online {
            width: 50px;
        }

        table.dataTables-testForm > thead > tr > th.progress {
            width: 250px !important;
        }

        .progress {
            width: 250px !important;
            background-color: transparent;
        }
        .progress.progress-striped{
            color: #f5f5f5;
            background-color: #e2e2e2;
        }

        table.dataTables-testForm > thead > tr > th.state {
            width: 100px;
        }

        table.dataTables-testForm > thead > tr > th.action {
            width: 100px;
        }

        table.dataTables-testForm {
            width: 100% !important;
        }
        .modal-full{
            margin-right: 0;
        }
        .modal-full .modal-dialog{
            width: 100%;
            height: 100%;
            margin: 0;
        }
        .modal-full .modal-content{
            height: 100%;
            display: flex;
            flex-direction: column;
            border-radius: unset;
            border: none;
        }
        .modal-full .modal-body{
            flex: 1;
            overflow-y: auto;
            /*padding-bottom: 30px !important;*/
        }
        .inmodal .modal-header {
            padding-top: 30px;
            padding-right: 15px;
            padding-left: 15px;
        }
        li.show-li {
            position: relative;
            list-style: circle
        }
        .double-dot {
            position: absolute;
            top: 0px;
            left: 95px;
        }

        /* Clock picker */
        .popover.clockpicker-popover.bottom{
            z-index: 99999;
        }
    </style>
@endsection
@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Quản lý {{mb_strtolower($currentFunction->name)}}</h2>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-testForm">
                                <thead>
                                <tr>
                                    <th class="stt">STT</th>
                                    <th class="">Shift name</th>
                                    <th class="">Test form</th>
                                    <th class="test-number">Test number</th>
                                    <th class="tester-type">Tester type</th>
                                    <th class="time-active">Time active</th>
                                    <th class="time-late">Time late</th>
                                    <th class="online">Online</th>
                                    <th class="progress">Process</th>
                                    <th class="state">State</th>
                                    <th class="action">Thao tác</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal add new function -->
    @include('backend.questionbank.testshift.form')
@endsection

@section('script')
    <!-- iCheck -->
    <script src="js/plugins/iCheck/icheck.min.js"></script>

    <script src="js/plugins/dataTables/datatables.min.js"></script>
    <!-- Page-Level Scripts -->
    <!-- Clock picker -->
    <script src="js/plugins/clockpicker/clockpicker.js"></script>
    <!-- Data picker -->
    <script src="js/plugins/datapicker/bootstrap-datepicker.js"></script>
    {{-- Language --}}
    <script src="js/plugins/datapicker/bootstrap-datepicker.vi.min.js"></script>

    {{-- Socket.io--}}
    <script src="js/socket.io.js"></script>

    <script>
        let mainUrl =   '{{$currentFunction->route}}';
        $(document).ready(function(){
            $('.clockpicker').clockpicker({
                donetext: 'Done',
                'default': 'now'
            });
            $('.input-group.date').datepicker({
                language:'vi',
                startView: 1,
                todayBtn: "linked",
                keyboardNavigation: false,
                forceParse: false,
                autoclose: true,
                format: "dd/mm/yyyy"
            });
            let Table = $('.dataTables-testForm').DataTable({
                bSort: false,
                bInfo: true,
                bLengthChange: false,
                processing: true,
                serverSide: true,
                searching: true,
                paging: true,
                pageLength: 5,
                responsive: true,
                ajax: mainUrl + '/getDatatable',
                dom: '<"row"<"col-sm-6"Bi><"col-sm-6"f>>rtp',
                buttons: {
                    buttons: [
                        {
                            text: 'Thêm mới',
                            action: function ( e, dt, node, config ) {
                                resetTestForm();
                                $('#modal-add-function').modal({backdrop: 'static', keyboard: false, show : true});
                            }
                        }
                    ],
                    dom: {
                        button: {
                            tag: "button",
                            className: "btn btn-primary"
                        },
                        buttonLiner: {
                            tag: null
                        }
                    }
                },
                language: {
                    "lengthMenu": "Hiển thị _MENU_ bản ghi trên 1 trang",
                    "zeroRecords": "Không có dữ liệu",
                    "info": "Hiển thị trang _PAGE_ trong tổng số _PAGES_ trang",
                    "infoEmpty": "Không có dữ liệu tồn tại",
                    "infoFiltered": "(Lọc từ _MAX_ bản ghi)",
                    "search": "Tìm kiếm:",
                    "paginate": {
                        "first":      "Trang đầu",
                        "last":       "Trang cuối",
                        "next":       "Trang tiếp theo",
                        "previous":   "Trang trước"
                    },
                },
                columns     :   [
                    {
                        data    :   'id',
                        className:  'text-center',
                        render  :   function (data, type, row, meta) {
                            return meta.row +1;
                        }
                    },
                    {   data    :   'name'      },
                    {   data    :   'form'      },
                    {
                        data    :   'test_numbers',
                        render  :   function (data, type, row, meta) {
                            return Number(parseInt(data).toFixed(0)).toLocaleString() + ' Đề';
                        }
                    },
                    {
                        data    :   'tester_type',
                        render  :   function (data, type, row, meta) {
                            return parseInt(data) ? 'Định sẵn' : 'Tự do';
                        }
                    },
                    {
                        data    :   'time_active',
                        render  :   function (data, type, row, meta) {
                            let date    =   new  Date(data);
                            let seconds =   date.getSeconds() < 10 ? '0'+date.getSeconds() : date.getSeconds();
                            return $.datepicker.formatDate('dd/mm/yy', date) + ' ' + date.getHours() + ':' + date.getMinutes() + ':' + seconds;
                        }
                    },
                    {
                        data    :   'time_late',
                        render  :   function (data, type, row, meta) {
                            return Number(parseInt(data).toFixed(0)).toLocaleString() + ' Phút';
                        }
                    },
                    {
                        data    :   'online',
                        render  :   function (data, type, row, meta) {
                            return parseInt(data) ? 'Online' : 'Offline';
                        }
                    },
                    {
                        data    :   'id',
                        className:  'progress',
                        render  :   function (data, type, row, meta) {
                            let process     =   parseInt(row.process);
                            let process_bar =
                                '<small>Số đề thi đã được tạo <span id="test-complete-' + data + '">0</span>/' + row.test_numbers + '</small>' +
                                '<div class="stat-percent" id="test-complete-percent-' + data + '">0%</div>' +
                                '<div class="progress progress-striped active m-b-sm">'+
                                '<div style="width: 0%;" class="progress-bar" id="test-complete-progress-' + data + '"></div></div>';
                            if (process == 2) {
                                process_bar =
                                    '<small>Số đề thi đã được tạo <span id="test-complete-' + data + '">' + row.test_numbers + '</span>/' + row.test_numbers + '</small>' +
                                    '<div class="stat-percent" id="test-complete-percent-' + data + '">100%</div>' +
                                    '<div class="progress progress-striped active m-b-sm">'+
                                    '<div style="width: 100%;" class="progress-bar" id="test-complete-progress-' + data + '"></div></div>';
                            }
                            return process_bar;
                        }
                    },
                    {
                        data    :   'state',
                        render  :   function (data, type, row, meta) {
                            let state;
                            switch (parseInt(data)){
                                case 0: state = 'Không kích hoạt'; break;
                                case 1: state = 'Kích hoạt'; break;
                                case 2: state = 'Kích hoạt ẩn'; break;
                                default:state = 'Chưa xác định'; break;
                            }
                            return state;
                        }
                    },
                    {
                        data    :   'id',
                        className: 'text-center',
                        render  :   function (data, type, row, meta) {
                            let button  =   '<button name="btn-generate" class="btn btn-primary btn-flat btn-xs" title="Generate" data-id='+data+' data-process='+row.process+'><i class="fa fa-play"></i></button> @permission("Edit-31")<button name="btn-edit" class="btn btn-primary btn-flat btn-xs" title="Sửa" data-data=\''+JSON.stringify(row)+'\'><i class="fa fa-edit"></i></button>@endpermission @permission("Delete-31")<button name="btn-delete" class="btn btn-remove btn-danger btn-flat btn-xs" title="Xóa" data-id='+data+'><i class="fa fa-remove"></i></button>@endpermission';
                            return button;
                        }
                    }
                ]
            });
            
            /* Socket io */
            let domain  =   'http://localhost:8080';
            let socket  =   io( domain );
            socket.on('shift.generate_off', function (data) {
                let shift_id    =   data.shift_id;
                let total       =   data.total;
                let question    =   data.question;
                $('#test-complete-' + shift_id).html(question);
                let percen  =   parseInt(question) / parseInt(total) * 100;
                $('div#test-complete-progress-' + shift_id).css('width',percen + '%');
                $('div#test-complete-percent-' + shift_id).html(percen.toFixed(2) + '%');
                if (question === total) {
                    $('div#test-complete-percent-' + shift_id).html('100%');
                    notification('success', 'Thành công', 'Đã tạo xong đề thi');
                }
            });

            /* Add new/ edit test form */
            $('button[name=add-function]').on('click', function () {
                let data    =   $('form[name=add-test-shift]').serialize(),
                    id      =   $('input[name=id]').val(),
                    type    =   'POST',
                    url     =   mainUrl;
                if (parseInt(id)){
                    type    =   'PUT';
                    url     =   mainUrl + '/' + id;
                }
                $.ajax(url,{
                    type    :   type,
                    data    :   data,
                    success :   function (respon) {
                        notification(respon.type, respon.title, respon.content);
                        if(respon.type === 'success'){
                            Table.ajax.reload();
                            $('#modal-add-function').modal('hide');
                        }
                    }
                });

            });


            /* Edit function */
            $(document).on('click', 'button[name=btn-edit]', function () {
                let data        =   $(this).data('data');
                $.each(data, function (key, value) {
                    $('[name='+key+']').val(value);
                });
                let datetime    =   new Date(data.time_active);
                $('.input-group.date').datepicker('setDate',datetime);
                let hours       =   datetime.getHours();
                let minutes     =   datetime.getMinutes();
                if (minutes < 10){
                    minutes = '0'+minutes;
                }
                $('input[name=time]').val(hours+':'+minutes);
                $('input[name=online]').prop('checked',data.online);
                $('#modal-add-function').modal({backdrop: 'static', keyboard: false, show : true});
            });

            /* Delete function */
            $(document).on('click', 'button[name=btn-delete]', function () {
                let id  = $(this).data('id');
                let url =   mainUrl + '/' + id;
                swal({
                    title: "Bạn có muốn xóa ca thi này không??",
                    text: "Khi xóa ca thi sẽ không thể khôi phục lại",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "OK",
                    cancelButtonText: "Không",
                    closeOnConfirm: true,
                    closeOnCancel: true
                }, function (config) {
                    if (config){
                        $.ajax(url,{
                            type    :   'DELETE',
                            data    :   {
                                _token  :   '{{csrf_token()}}'
                            },
                            success :   function (respon) {
                                notification(respon.type, respon.title, respon.content);
                                if(respon.type === 'success'){
                                    Table.ajax.reload();
                                }
                            }
                        })
                    }
                });
            });

            /* click generate button */
            $(document).on('click','button[name=btn-generate]', function () {
                let shift_id=   $(this).data('id');
                let process =   $(this).data('process');
                let url     =   'generate';
                $.ajax(url, {
                    type    :   'POST',
                    data    :   {
                        _token  :   '{!! csrf_token() !!}',
                        shift_id :   shift_id,
                    },
                    success :   function (respon) {
                        notification(respon.type, respon.title, respon.content);
                    }
                });
            });
        });

        function resetTestForm() {
            $('form[name=add-test-shift]').trigger('reset');
            $('input[name=id]').val(0);
        }
    </script>
@endsection
