<div class="modal inmodal" id="modal-add-function" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                            class="sr-only">Close</span></button>
                <i class="{{$currentFunction->icon}} modal-icon"></i>
                <h4 class="modal-title">Thêm {{mb_strtolower($currentFunction->name)}}</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <form action="#" method="post" name="form-add-function">
                        {{csrf_field()}}
                        <div class="form-group col-sm-6">
                            <label>Tên chủ đề</label>
                            <input type="text" placeholder="Tên chủ đề" name="name" class="form-control"
                                   autocomplete="off">
                        </div>
                        <div class="form-group col-sm-3">
                            <label>Thuộc chủ đề</label>
                            <select name="parent_id" class="form-control">
                                <option value="0" style="color: black; font-weight: bold">Không thuộc chủ đề nào</option>
                                @foreach($subjects as $subject)
                                    <option value="{{$subject->id}}" style="color: black; font-weight: bold">{{$subject->name}}</option>
                                    @if (count($subject->childs))
                                        @foreach($subject->childs as $child)
                                            <option value="{{$child->id}}">&nbsp;- {{$child->name}}</option>
                                        @endforeach
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-sm-3">
                            <label>Trạng thái</label>
                            <select name="state" class="form-control">
                                <option value="1">Kích hoạt</option>
                                {{--<option value="2">Kích hoạt ẩn</option>--}}
                                <option value="0">Không kích hoạt</option>
                            </select>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Mô tả</label>
                                <textarea name="desc" class="form-control" id="" cols="30" rows="5"></textarea>
                            </div>
                        </div>
                        <input type="hidden" name="id" value="0">
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" name="add-function"><i class="fa fa-save"></i> Save
                </button>
            </div>
        </div>
    </div>
</div>
