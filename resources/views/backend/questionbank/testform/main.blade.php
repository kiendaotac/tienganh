@extends('layouts.app')
@section('title')
    <title>{{$currentFunction->name}} - {{\Illuminate\Support\Facades\Crypt::decryptString(config('custom.app_name'))}}</title>
@endsection

@section('style')
    <link href="css/plugins/dataTables/datatables.min.css" rel="stylesheet">
    <link href="css/plugins/iCheck/custom.css" rel="stylesheet">
    <style>
        table.dataTables-testForm > thead > tr > th.stt {
            width: 15px;
        }


        table.dataTables-testForm > thead > tr > th.time {
            width: 60px;
        }

        table.dataTables-testForm > thead > tr > th.show-option {
            width: 200px;
        }

        table.dataTables-testForm > thead > tr > th.creator {
            width: 100px;
        }

        table.dataTables-testForm > thead > tr > th.state {
            width: 100px;
        }

        table.dataTables-testForm > thead > tr > th.action {
            width: 55px;
        }

        table.dataTables-testForm {
            width: 100% !important;
        }
        .modal-full{
            margin-right: 0;
        }
        .modal-full .modal-dialog{
            width: 100%;
            height: 100%;
            margin: 0;
        }
        .modal-full .modal-content{
            height: 100%;
            display: flex;
            flex-direction: column;
            border-radius: unset;
            border: none;
        }
        .modal-full .modal-body{
            flex: 1;
            overflow-y: auto;
            /*padding-bottom: 30px !important;*/
        }
        .inmodal .modal-header {
            padding-top: 30px;
            padding-right: 15px;
            padding-left: 15px;
        }
        li.show-li {
            position: relative;
            list-style: circle
        }
        .double-dot {
            position: absolute;
            top: 0px;
            left: 95px;
        }
    </style>
@endsection
@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Quản lý {{mb_strtolower($currentFunction->name)}}</h2>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-testForm">
                                <thead>
                                <tr>
                                    <th class="stt">STT</th>
                                    <th class="subject">Subject</th>
                                    <th class="name">Name</th>
                                    <th class="time">Time</th>
                                    <th class="show-option">Show option</th>
                                    {{--<th class="true-false">Explain</th>--}}
                                    {{--<th class="true-false">Review</th>--}}
                                    {{--<th class="true-false">Hint</th>--}}
                                    <th class="creator">Creator</th>
                                    <th class="state">State</th>
                                    <th class="action">Thao tác</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal add new function -->
    @include('backend.questionbank.testform.form')
@endsection

@section('script')
    <!-- iCheck -->
    <script src="js/plugins/iCheck/icheck.min.js"></script>

    <script src="js/plugins/dataTables/datatables.min.js"></script>
    <!-- Page-Level Scripts -->
    <script>
        let mainUrl = '{{$currentFunction->route}}';
        $(document).ready(function(){
            let Table = $('.dataTables-testForm').DataTable({
                bSort: false,
                bInfo: true,
                bLengthChange: false,
                processing: true,
                serverSide: true,
                searching: true,
                paging: true,
                pageLength: 5,
                responsive: true,
                ajax: mainUrl + '/getDatatable',
                dom: '<"row"<"col-sm-6"Bi><"col-sm-6"f>>rtp',
                buttons: {
                    buttons: [
                        {
                            text: 'Thêm mới',
                            action: function ( e, dt, node, config ) {
                                resetTestForm();
                                let subject_id  =   $('select[name=subject_id]').val();
                                let form_id     =   $('input[name=id]').val();
                                renderFormDetails(form_id, subject_id);
                                $('#modal-add-function').modal({backdrop: 'static', keyboard: false, show : true});
                            }
                        }
                    ],
                    dom: {
                        button: {
                            tag: "button",
                            className: "btn btn-primary"
                        },
                        buttonLiner: {
                            tag: null
                        }
                    }
                },
                language: {
                    "lengthMenu": "Hiển thị _MENU_ bản ghi trên 1 trang",
                    "zeroRecords": "Không có dữ liệu",
                    "info": "Hiển thị trang _PAGE_ trong tổng số _PAGES_ trang",
                    "infoEmpty": "Không có dữ liệu tồn tại",
                    "infoFiltered": "(Lọc từ _MAX_ bản ghi)",
                    "search": "Tìm kiếm:",
                    "paginate": {
                        "first":      "Trang đầu",
                        "last":       "Trang cuối",
                        "next":       "Trang tiếp theo",
                        "previous":   "Trang trước"
                    },
                },
                columns     :   [
                    {
                        data    :   'id',
                        className:  'text-center',
                        render  :   function (data, type, row, meta) {
                            return meta.row +1;
                        }
                    },
                    {   data    :   'subject'   },
                    {   data    :   'name'      },
                    {
                        data    :   'time',
                        render  :   function (data, type, row, meta) {
                            return data + ' phút';
                        }
                    },
                    {
                        data    :   'show_result',
                        render  :   function (data, type, row, meta) {
                            let show_result     =   row.show_result ? 'True' : 'False';
                            let show_explain    =   row.show_explain ? 'True' : 'False';
                            let allow_review    =   row.allow_review ? 'True' : 'False';
                            let show_hint       =   row.show_hint ? 'True' : 'False';
                            let ul  =
                                    '<ul>'
                                +   '<li class="show-li">Show result<span class="double-dot">: &emsp;' + show_result + '</span></li>'
                                +   '<li class="show-li">Show explain<span class="double-dot">: &emsp;' + show_explain + '</span></li>'
                                +   '<li class="show-li">Allow review<span class="double-dot">: &emsp;' + allow_review + '</span></li>'
                                +   '<li class="show-li">Show hint<span class="double-dot">: &emsp;' + show_hint + '</span></li>'
                                +   '</ul>';
                            return ul;
                        }
                    },
                    {   data    :   'creator'   },
                    {
                        data    :   'state',
                        render  :   function (data, type, row, meta) {
                            let state;
                            switch (parseInt(data)){
                                case 0: state = 'Không kích hoạt'; break;
                                case 1: state = 'Kích hoạt'; break;
                                case 2: state = 'Kích hoạt ẩn'; break;
                                default:state = 'Chưa xác định'; break;
                            }
                            return state;
                        }
                    },
                    {
                        data    :   'id',
                        className: 'text-center',
                        render  :   function (data, type, row, meta) {
                            let button  =   '@permission("Edit-31")<button name="btn-edit" class="btn btn-primary btn-flat btn-xs" title="Sửa" data-data=\''+JSON.stringify(row)+'\'><i class="fa fa-edit"></i></button>@endpermission @permission("Delete-31")<button name="btn-delete" class="btn btn-remove btn-danger btn-flat btn-xs" title="Xóa" data-id='+data+'><i class="fa fa-remove"></i></button>@endpermission';
                            return button;
                        }
                    }
                ]
            });
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });

            /* Add new/ edit test form */
            $('button[name=add-function]').on('click', function () {
                submitTestForm(Table);
            });


            /* Edit function */
            $(document).on('click', 'button[name=btn-edit]', function () {
                let data = $(this).data('data');
                $.each(data, function (index, item) {
                    $('[name='+index+']').val(item);
                });
                parseInt(data.show_result)  ?   $('input[name=show_result]').iCheck('check')    :   $('input[name=show_result]').iCheck('uncheck');
                parseInt(data.show_explain) ?   $('input[name=show_explain]').iCheck('check')   :   $('input[name=show_explain]').iCheck('uncheck');
                parseInt(data.allow_review) ?   $('input[name=allow_review]').iCheck('check')   :   $('input[name=allow_review]').iCheck('uncheck');
                parseInt(data.show_hint)    ?   $('input[name=show_hint]').iCheck('check')      :   $('input[name=show_hint]').iCheck('uncheck');
                let subject_id  =   data.subject_id;
                let form_id     =   data.id;
                renderDetailsToEdit(form_id, subject_id);
                $('#modal-add-function').modal({backdrop: 'static', keyboard: false, show : true});
            });

            /* Delete function */
            $(document).on('click', 'button[name=btn-delete]', function () {
                let id  = $(this).data('id');
                let url =   mainUrl + '/' + id;
                swal({
                    title: "Bạn có muốn xóa form này không??",
                    text: "Khi xóa form sẽ không thể khôi phục lại",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "OK",
                    cancelButtonText: "Không",
                    closeOnConfirm: true,
                    closeOnCancel: true
                }, function (config) {
                    if (config){
                        $.ajax(url,{
                            type    :   'DELETE',
                            data    :   {
                                _token  :   '{{csrf_token()}}'
                            },
                            success :   function (respon) {
                                notification(respon.type, respon.title, respon.content);
                                if(respon.type === 'success'){
                                    Table.ajax.reload();
                                }
                            }
                        })
                    }
                });
            });

            /* Change subject */
            $(document).on('change','select[name=subject_id]', function () {
                let form_id     =   $('input[name=id]').val();
                let subject_id  =   $(this).val();
                renderFormDetails(form_id, subject_id);
            });

            /* Add row details */
            $(document).on('click','button[name=btn-add-row-details]', function () {
                let subject_id  =   $('select[name=subject_id]').val();
                getQuestionType(subject_id);
            });

            /* Change details_subject_id */
            $(document).on('change','select[name=details_subject_id]', function () {
                let _this               =   $(this);
                let details_subject_id  =   _this.val();
                let level               =   _this.closest('.details-row').find('select[name=level]').val();
                let url                 =   mainUrl + '/getQuestionTypeBySubjectDetails';
                $.ajax(url,{
                    type    :   'GET',
                    data    :   {
                        subject_id  :   details_subject_id,
                        level       :   level
                    },
                    success :   function (data) {
                        let type    =   '';
                        $.each(data, function (index, item) {
                            type    +=  '<option value="' + item.id + '">' + item.name + '</option>';
                        });
                        _this.closest('.details-row').find('select[name=question_type]').html(type);
                    }
                });
            });

            /* Change level */
            $(document).on('change','select[name=level]', function () {
                let _this               =   $(this);
                let level               =   _this.val();
                let details_subject_id  =   _this.closest('.details-row').find('select[name=details_subject_id]').val();
                let url                 =   mainUrl + '/getQuestionTypeBySubjectDetails';
                $.ajax(url,{
                    type    :   'GET',
                    data    :   {
                        subject_id  :   details_subject_id,
                        level       :   level
                    },
                    success :   function (data) {
                        let type    =   '';
                        $.each(data, function (index, item) {
                            type    +=  '<option value="' + item.id + '">' + item.name + '</option>';
                        });
                        _this.closest('.details-row').find('select[name=question_type]').html(type);
                    }
                });
            })

        });

        function resetTestForm() {
            $('form[name=form-add-test-form]').trigger('reset');
            $('input[name=id]').val(0);
            $('input[name=show_result]').closest('div.icheckbox_square-green').removeClass('checked');
            $('input[name=show_explain]').closest('div.icheckbox_square-green').removeClass('checked');
            $('input[name=allow_review]').closest('div.icheckbox_square-green').removeClass('checked');
            $('input[name=show_hint]').closest('div.icheckbox_square-green').removeClass('checked');
        }

        function renderFormDetails(form_id, subject_id) {
            /* Tạo mới form thì chỉ render 1 dòng */
            if (form_id == 0){
                $('.details').html('');
                getQuestionType(subject_id);
            }
        }
        /* render row ver 2 */
        function renderRow(subject_id, question_type) {
            let subject         =   JSON.parse('{!! $subjects !!}');
            let subject_options =   '';
            let type            =   '';
            $.each(question_type, function (index, item) {
                type    +=  '<option value="' + item.id + '">' + item.name + '</option>'
            });
            $.each(subject, function (index, item) {
                if (item.parent_id == subject_id) {
                    subject_options +=  '<option value="' + item.id + '">' + item.name + '</option>';
                }
            });
            let row
                =   '<div class="details-row"><div class="form-group col-sm-2">'
                // +   '<label>Kỹ năng</label>'
                +   '<select name="details_subject_id" class="form-control">'
                +   subject_options
                +   '</select>'
                +   '</div>'
                +   '<div class="form-group col-sm-2">'
                // +   '<label>Level</label>'
                +   '<select name="level" class="form-control">'
                +   '<option value="1">Dễ</option>'
                +   '<option value="2">Trung bình</option>'
                +   '<option value="3">Khó</option>'
                +   '</select>'
                +   '</div>'
                +   '<div class="form-group col-sm-4">'
                // +   '<label>Question Type</label>'
                +   '<select name="question_type" class="form-control">'
                +   type
                +   '</select>'
                +   '</div>'
                +   '<div class="form-group col-sm-2">'
                // +   '<label>Số group</label>'
                +   '<input type="number" class="form-control" name="number_groups" autocomplete="off" placeholder="Số groups">'
                +   '</div>'
                +   '<div class="form-group col-sm-2">'
                // +   '<label>Tổng điểm</label>'
                +   '<input type="number" class="form-control" name="total_point" autocomplete="off" placeholder="Tổng điểm">'
                +   '</div></div>';
            $('.details').append(row);
        }

        function renderRowToEdit(detail, question_type, subject) {
            let subject_options =   '';
            let type            =   '';
            let level           =   '';
            let level_option    =   [
                {   id      :   1,  value   :   'Dễ'    },
                {   id      :   2,  value   :   'Trung bình'    },
                {   id      :   3,  value   :   'Khó'    },
            ];
            $.each(subject, function (index, item) {
                let selected    =   '';
                if (detail.subject_id == item.id){
                    selected    =   'selected';
                }
                subject_options +=  '<option value="' + item.id + '" ' + selected + '>' + item.name + '</option>';
            });
            $.each(question_type, function (index, item) {
                let selected    =   '';
                if (detail.question_type == item.id){
                    selected    =   'selected';
                }
                if (detail.question_types.indexOf(item.id) > -1){
                    type    +=  '<option value="' + item.id + '"' + selected + '>' + item.name + '</option>';
                }
            });

            $.each(level_option, function (index, item) {
                let selected    =   '';
                if (detail.level == item.id){
                    selected    =   'selected';
                }
                level       +=  '<option value="' + item.id + '"' + selected + '>' + item.value + '</option>';
            });
            let row
                =   '<div class="details-row"><div class="form-group col-sm-2">'
                // +   '<label>Kỹ năng</label>'
                +   '<select name="details_subject_id" class="form-control">'
                +   subject_options
                +   '</select>'
                +   '</div>'
                +   '<div class="form-group col-sm-2">'
                // +   '<label>Level</label>'
                +   '<select name="level" class="form-control">'
                +   level
                +   '</select>'
                +   '</div>'
                +   '<div class="form-group col-sm-4">'
                // +   '<label>Question Type</label>'
                +   '<select name="question_type" class="form-control">'
                +   type
                +   '</select>'
                +   '</div>'
                +   '<div class="form-group col-sm-2">'
                // +   '<label>Số group</label>'
                +   '<input type="number" class="form-control" name="number_groups" autocomplete="off" placeholder="Số groups" value=' + detail.number_group + '>'
                +   '</div>'
                +   '<div class="form-group col-sm-2">'
                // +   '<label>Tổng điểm</label>'
                +   '<input type="number" class="form-control" name="total_point" autocomplete="off" placeholder="Tổng điểm" value=' + detail.total_point + '>'
                +   '</div></div>';
            $('.details').append(row);
        }
        /*
        function renderRow(item) {
            let content
                = '<div class="details">'
                + '<div class="col-sm-3">'
                + '<label>Kỹ năng</label>'
                + '<input type="text" class="form-control" name="details_subject_id" value="'+item.name+'" data-id="'+item.id+'" readonly>'
                + '</div>'
                + '<div class="col-sm-3">'
                + '<label>Level</label>'
                + '<input type="text" class="form-control" name="level" value="Dễ" data-level="1" readonly>'
                + '</div>'
                + '<div class="col-sm-3">'
                + '<label>Số group</label>'
                + '<input type="number" class="form-control detail-number_group-'+item.id+'-1" name="number_groups" autocomplete="off" placeholder="Số groups">'
                + '</div>'
                + '<div class="col-sm-3">'
                + '<label>Tổng điểm</label>'
                + '<input type="number" class="form-control detail-total_point-'+item.id+'-1" name="total_point" autocomplete="off" placeholder="Tổng điểm">'
                + '</div>'
                + '</div>'
                + '<div class="details">'
                + '<div class="col-sm-3">'
                // + '<label>Kỹ năng</label>'
                + '<input type="text" class="form-control" name="details_subject_id" value="'+item.name+'" data-id="'+item.id+'" readonly>'
                + '</div>'
                + '<div class="col-sm-3">'
                // + '<label>Level</label>'
                + '<input type="text" class="form-control" name="level" value="Trung bình" data-level="2" readonly>'
                + '</div>'
                + '<div class="col-sm-3">'
                // + '<label>Số group</label>'
                + '<input type="number" class="form-control detail-number_group-'+item.id+'-2" name="number_groups" autocomplete="off" placeholder="Số groups">'
                + '</div>'
                + '<div class="col-sm-3">'
                // + '<label>Tổng điểm</label>'
                + '<input type="number" class="form-control detail-total_point-'+item.id+'-2" name="total_point" autocomplete="off" placeholder="Tổng điểm">'
                + '</div>'
                + '</div>'
                + '<div class="details">'
                + '<div class="col-sm-3">'
                // + '<label>Kỹ năng</label>'
                + '<input type="text" class="form-control" name="details_subject_id" value="'+item.name+'" data-id="'+item.id+'" readonly>'
                + '</div>'
                + '<div class="col-sm-3">'
                // + '<label>Level</label>'
                + '<input type="text" class="form-control" name="level" value="Khó" data-level="3" readonly>'
                + '</div>'
                + '<div class="col-sm-3">'
                // + '<label>Số group</label>'
                + '<input type="number" class="form-control detail-number_group-'+item.id+'-3" name="number_groups" autocomplete="off" placeholder="Số groups">'
                + '</div>'
                + '<div class="col-sm-3">'
                // + '<label>Tổng điểm</label>'
                + '<input type="number" class="form-control detail-total_point-'+item.id+'-3" name="total_point" autocomplete="off" placeholder="Tổng điểm">'
                + '</div>'
                + '</div>';
            $('div.row.form-details').append(content);
        }
        /*
        /* submit test form */
        function renderDetailsToEdit(form_id, subject_id){
            let url =   mainUrl + '/getDetails';
            $.ajax(url, {
                type    :   'GET',
                data    :   {
                    form_id     :   form_id,
                    subject_id  :   subject_id
                },
                success :   function (data) {
                    let details =   data.details;
                    let question_type   =   data.question_type;
                    let subject         =   data.subject;
                    $('.details').html('');
                    $.each(details, function (index, item) {
                        renderRowToEdit(item, question_type, subject);
                    });
                }
            })
        }
        function submitTestForm(Table){
            let form    =   $('form[name=form-add-test-form]').serializeArray(),
                id      =   $('input[name=id]').val(),
                type    =   'POST',
                url     =   mainUrl;
            let detail  =   new Array();
            $('.row.form-details > .details > .details-row').each(function (index) {
                let element =   {
                    form_id         :   $('input[name=id]').val(),
                    subject_id      :   $(this).find('select[name=details_subject_id]').val(),
                    level           :   $(this).find('select[name=level]').val(),
                    question_type   :   $(this).find('select[name=question_type]').val(),
                    number_group    :   $(this).find('input[name=number_groups]').val(),
                    total_point     :   $(this).find('input[name=total_point]').val()
                };
                detail.push(element);
            });
            if (parseInt(id)){
                type    =   'PUT';
                url     =   mainUrl + '/' + id;
            }

            $.ajax(url,{
                type    :   type,
                data    :   {
                    form    :   form,
                    detail  :   detail
                },
                success :   function (respon) {
                    notification(respon.type, respon.title, respon.content);
                    if(respon.type === 'success'){
                        Table.ajax.reload();
                        $('#modal-add-function').modal('hide');
                        // $('input[name=id]').val(respon.id);
                    }
                }
            })
        }

        /* Ajax get question type in subject */
        function getQuestionType(subject_id) {
            let url =   'form/getQuestionType';
            $.ajax(url,{
                type    :   'GET',
                data    :   {
                    subject_id  :   subject_id,
                    level       :   1
                },
                success :   function (data) {
                    renderRow(subject_id, data);
                }
            })
        }
    </script>
@endsection
