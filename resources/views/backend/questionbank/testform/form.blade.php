<div class="modal inmodal modal-full" id="modal-add-function" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                            class="sr-only">Close</span></button>
                <i class="{{$currentFunction->icon}} modal-icon"></i>
                <h4 class="modal-title">Thêm {{mb_strtolower($currentFunction->name)}}</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="form-group col-sm-5">
                        <div class="box-content">
                            <form action="#" method="post" name="form-add-test-form">
                                {{csrf_field()}}
                                <div class="row">
                                    <div class="form-group col-sm-12">
                                        <label>Tên mẫu test</label>
                                        <input type="text" placeholder="Tên mẫu test" name="name" class="form-control" autocomplete="off">
                                    </div>
                                    <div class="form-group col-sm-12">
                                        <label>Chủ đề</label>
                                        <select name="subject_id" class="form-control">
                                            @foreach($subjects as $item)
                                                @if ($item->parent_id === 0)
                                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-sm-6">
                                        <label>Thời gian</label>
                                        <input type="number" class="form-control" name="time" autocomplete="off" placeholder="Thời gian">
                                    </div>
                                    <div class="form-group col-sm-6">
                                        <label>Trạng thái</label>
                                        <select name="state" class="form-control">
                                            <option value="1">Kích hoạt</option>
                                            <option value="2">Kích hoạt ẩn</option>
                                            <option value="0">Không kích hoạt</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-sm-6">
                                        <div class="i-checks"><label> <input type="checkbox" value="1" name="show_result"> <i></i> Show result </label></div>
                                    </div>
                                    <div class="form-group col-sm-6">
                                        <div class="i-checks"><label> <input type="checkbox" value="1" name="show_explain"> <i></i> Show explain </label></div>
                                    </div>
                                    <div class="form-group col-sm-6">
                                        <div class="i-checks"><label> <input type="checkbox" value="1" name="allow_review"> <i></i> Allow review </label></div>
                                    </div>
                                    <div class="form-group col-sm-6">
                                        <div class="i-checks"><label> <input type="checkbox" value="1" name="show_hint"> <i></i> Show hint </label></div>
                                    </div>
                                </div>
                                <input type="hidden" name="id" value="0">
                            </form>
                            <span class="box-content-title">Test form</span>
                        </div>
                    </div>
                    <div class="form-group col-sm-7">
                        <div class="box-content">
                            <div class="row form-details">
                                <div class="col-sm-2">
                                    <label>Kỹ năng</label>
                                    {{--<select name="details_subject_id" class="form-control">--}}
                                    {{--@foreach ($subjects as $subject)--}}
                                    {{--@if ($subject->parent_id == 1)--}}
                                    {{--<option value="{{$subject->id}}">{{$subject->name}}</option>--}}
                                    {{--@endif--}}
                                    {{--@endforeach--}}
                                    {{--</select>--}}
                                    {{--<input type="text" class="form-control" name="details_subject_id" value="reading" readonly>--}}
                                </div>
                                <div class="col-sm-2">
                                    <label>Level</label>
                                    {{--<select name="level" class="form-control">--}}
                                    {{--<option value="1">Dễ</option>--}}
                                    {{--<option value="2">Trung bình</option>--}}
                                    {{--<option value="3">Khó</option>--}}
                                    {{--</select>--}}
                                    {{--<input type="text" class="form-control" name="level" value="Dễ" readonly>--}}
                                </div>
                                <div class="col-sm-4">
                                    <label>Question Type</label>
                                    {{--<select name="level" class="form-control">--}}
                                    {{--@foreach($question_type as $item)--}}
                                    {{--<option value="{{$item->id}}">{{$item->name}}</option>--}}
                                    {{--@endforeach--}}
                                    {{--</select>--}}
                                </div>
                                <div class="col-sm-2">
                                    <label>Số group</label>
                                    {{--<input type="number" class="form-control" name="number_groups" autocomplete="off" placeholder="Số groups">--}}
                                </div>
                                <div class="col-sm-2">
                                    <label>Tổng điểm</label>
                                    {{--<input type="number" class="form-control" name="total_point" autocomplete="off" placeholder="Tổng điểm">--}}
                                </div>
                                <div class="details">

                                </div>
                                <div class="col-sm-12">
                                    <div class="">
                                        <button name="btn-add-row-details" class="btn btn-primary" style="border-radius: 50%; float: right"><i class="fa fa-plus"></i></button>
                                    </div>
                                </div>
                            </div>
                            <span class="box-content-title">Test form details</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" name="add-function"><i class="fa fa-save"></i> Save
                </button>
            </div>
        </div>
    </div>
</div>
