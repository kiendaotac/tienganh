<div class="box-test test-options" style="border: none; padding-top: 0px !important;">
    <div style="margin-bottom: 10px!important;"><div class="list-type-option col-sm-6">
        <div class="row">
            <label class="radio-inline"><input type="radio" name="type_option_question" value="text" checked>Văn bản</label>
            <label class="radio-inline"><input type="radio" name="type_option_question" value="image">Hình ảnh</label>
        </div>
    </div>
    <div class="list-type-option col-sm-6">
        <label class="radio-inline"><input type="radio" name="type_option_answer" value="text" checked>Văn bản</label>
        <label class="radio-inline"><input type="radio" name="type_option_answer" value="image">Hình ảnh</label>
    </div></div>
    <div class="list-options">
        <div class="row">
            <div class="col-lg-6 q">
                <div class="option-item">
                    <div class="option-name">Q</div>
                    <input type="text" name="option" class="input-text question" data-error="Vui lòng nhập đầy đủ các phương án đã tạo.">
                    <i class="fa fa-times destroy-option"></i>
                </div>
            </div>
            <div class="col-lg-6 a">
                <div class="option-item">
                    <div class="option-name">A</div>
                    <input type="text" name="option" class="input-text" data-error="Vui lòng nhập đầy đủ các phương án đã tạo.">
                    {{--<div class="correct"><input type="radio" name="correct"></div>--}}
                    <i class="fa fa-times destroy-option"></i>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="add-option-item">
                    <i class="fa fa-plus" title="Thêm câu hỏi"></i>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="add-option-item-answer">
                    <i class="fa fa-plus" title="Thêm phương án nhiễu"></i>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    /*
    *   Sử lý nút thêm phương án trả lời cho loại câu hỏi 'CHỌN' 1 phương án trả lời đúng.
    * */
    $('.add-option-item i').on('click', function(){
        // let question_type   =   $('input[name=question_type_created]').val();
        let _this           =   $(this);
        addQuestionAndAnswer(_this);
    });

    $('.add-option-item-answer i').on('click', function(){
        addAnswer();
    });

    /* destroy question and answer */
    $(document).on('click','.destroy-option', function(){
        let _this           =   $(this);
        destroyAnswerAndQestion(_this);
    });

    $(document).on('change', 'input[name=type_option_question]', function(){
        settingOption($(this));
        $('.col-lg-6.q').find('input[name=option]').val('');
    });
    $(document).on('change', 'input[name=type_option_answer]', function(){
        settingOption($(this));
        $('.col-lg-6.a').find('input[name=option]').val('');
    });

    /* chose image option */
    $(document).on('click', 'input[name=option]', function(){
        if($('input[name=type_option_question]:checked').val() === 'image' && $(this).closest('.col-lg-6').hasClass('q') || $('input[name=type_option_answer]:checked').val() === 'image' && $(this).closest('.col-lg-6').hasClass('a')) {
            nameClass = $(this);
            $('#module-file').modal('show');
        }
    });

    /* check chose is image */
    function checkChoseFile(extension){
        let arrExtension = ['png', 'jpg', 'jpeg', 'gif'];
        if(arrExtension.indexOf(extension) == -1) return false;
        return true;
    }
    /* show box file */
    
    /* chose image */
    $(document).on('click', '.file-box', function(){
        if ($('#modal-add-question').hasClass('in')) {
            let imgPath = $(this).find('.file').data('src');
            let fileExtension = imgPath.substr(imgPath.lastIndexOf('.')+1).toLowerCase();
            if(checkChoseFile(fileExtension)) {
                if(nameClass === 'note-icon-picture') {
                    $('.note-image-url').val(imgPath);
                    $('.note-image-btn').trigger('click');
                }
                else {
                    nameClass.val(imgPath);
                }
                $('#module-file').modal('hide');
            }
            else swal('File này không phải là hình ảnh.');
        }
    });

    /* add Q and A for Mactching question type */
    function addQuestionAndAnswer(_this){
        let content = '<div class="col-lg-6 q"><div class="option-item">'
            + '<div class="option-name">'
            + 'Q'
            + '</div>'
            + '<input type="text" name="option" class="input-text question" data-error="Vui lòng nhập đầy đủ các phương án đã tạo.">'
            + '<i class="fa fa-times destroy-option"></i>'
            + '</div></div>'
            + '<div class="col-lg-6 a"><div class="option-item">'
            + '<div class="option-name">'
            + 'A'
            + '</div>'
            + '<input type="text" name="option" class="input-text" data-error="Vui lòng nhập đầy đủ các phương án đã tạo.">'
            + '<i class="fa fa-times destroy-option"></i>'
            + '</div></div>';
        let addOptionItem = '<div class="col-lg-6">'
            +'<div class="add-option-item">'
            + '<i class="fa fa-plus"></i>'
            + '</div>'
            + '</div>';
        _this.closest('.col-lg-6').before(content);
        settingOption($('input[name=type_option_answer]:checked'));
        settingOption($('input[name=type_option_question]:checked'));
    }

    /* add Answer for Mactching 2 question type */
    function addAnswer(){
        let content = '<div class="col-lg-6 q"><div class="option-item">'
            + '<div class="option-name">'
            + 'Q'
            + '</div>'
            + '<input type="text" name="option" class="input-text question" data-error="Vui lòng nhập đầy đủ các phương án đã tạo." disabled>'
            + '<i class="fa fa-times destroy-option"></i>'
            + '</div></div>'
            + '<div class="col-lg-6 a"><div class="option-item">'
            + '<div class="option-name">'
            + 'A'
            + '</div>'
            + '<input type="text" name="option" class="input-text" data-error="Vui lòng nhập đầy đủ các phương án đã tạo.">'
            + '<i class="fa fa-times destroy-option"></i>'
            + '</div></div>';
        let addOptionItem = '<div class="col-lg-6">'
            +'<div class="add-option-item">'
            + '<i class="fa fa-plus"></i>'
            + '</div>'
            + '</div>';
        $('.add-option-item').closest('.col-lg-6').before(content);
        settingOption($('input[name=type_option_answer]:checked'));
        settingOption($('input[name=type_option_question]:checked'));
    }

    /* Xóa question and answer */
    function destroyAnswerAndQestion(_this) {
        let select  =   _this.closest('.col-lg-6');
        if (select.hasClass('q')){
            select.next().remove();
            select.remove();
        }
        if (select.hasClass('a')){
            select.prev().remove();
            select.remove();
        }
    }

    function settingOption(val){
        let name    =   val.attr('name');
        let input   =   '';
        if (name === 'type_option_answer'){
            input   =   $('.col-lg-6.a').find('input[name=option]');
        }
        if (name === 'type_option_question') {
            input   =   $('.col-lg-6.q').find('input[name=option]');
        }
        if(val.val() === 'image') {
            input.attr('readonly', 'readonly');
            input.attr('placeholder', 'Nhấn vào đây để chọn ảnh');
        }
        else {
            input.removeAttr('readonly');
            input.removeAttr('placeholder');
        }
    }
    
    function getData() {
        if (validate()){
            let data            =   new Object();
            data.question_type  =   $('input[name=question_type_created]').val();
            data.type_question  =   $('input[name=type_option_question]:checked').val();
            data.type_answer    =   $('input[name=type_option_answer]:checked').val();
            data.hint           =   $('textarea[name=hint]').val();
            data.explain        =   $('textarea[name=explain]').val();
            data.answer_shuff   =   $('input[name=answer_shuff]').is(':checked') ? 1 : 0;
            data.question_state =   $('select[name=question_state]').val();
            data.option         =   new Array();
            $('.col-lg-6.q').each(function () {
                let element     =   new Object();
                let answer      =   $(this).next().find('input[name=option]');
                element.id      =   1;
                element.question=   $(this).find('input[name=option]').val();
                element.answer  =   answer.val();
                answer.css('border','');
                data.option.push(element);
            });
            return data;    
        } 
        else swal('Bạn cần nhập đáp án cho câu hỏi trước!');
        return false;
    }
    
    function validate() {
        let check   =   true;
        $('.col-lg-6.q').each(function () {
            if ($(this).find('input[name=option]').val() !== '' && $(this).next().find('input[name=option]').val() === ''){
                $(this).next().find('input[name=option]').css('border','red 1px solid');
                check   =   false;
            }
        });
        return check;
    }
</script>
