<div class="form-group">
    <label for="question">Câu hỏi: </label>
    <textarea class="form-control input-text" id="question" name="question" data-error="Vui lòng nhập nội dung câu hỏi."></textarea>
</div>
<div class="box-test test-options" style="border: none; border-top: #ddd solid 1px">
    <div class="list-type-option">
        <label class="radio-inline"><input type="radio" name="type_option" value="text" checked>Văn bản</label>
        <label class="radio-inline"><input type="radio" name="type_option" value="image">Hình ảnh</label>
    </div>
    <div class="list-options">
        <div class="row">
            <div class="col-lg-6">
                <div class="option-item">
                    <div class="option-name">A</div>
                    <input type="text" name="option" class="input-text" data-error="Vui lòng nhập đầy đủ các phương án đã tạo.">
                    <div class="correct"><input type="radio" name="correct"></div>
                    <i class="fa fa-times destroy-option"></i>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="option-item">
                    <div class="option-name">B</div>
                    <input type="text" name="option" class="input-text" data-error="Vui lòng nhập đầy đủ các phương án đã tạo.">
                    <div class="correct"><input type="radio" name="correct"></div>
                    <i class="fa fa-times destroy-option"></i>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="option-item">
                    <div class="option-name">C</div>
                    <input type="text" name="option" class="input-text" data-error="Vui lòng nhập đầy đủ các phương án đã tạo.">
                    <div class="correct"><input type="radio" name="correct"></div>
                    <i class="fa fa-times destroy-option"></i>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="option-item">
                    <div class="option-name">D</div>
                    <input type="text" name="option" class="input-text" data-error="Vui lòng nhập đầy đủ các phương án đã tạo.">
                    <div class="correct"><input type="radio" name="correct"></div>
                    <i class="fa fa-times destroy-option"></i>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="add-option-item">
                    <i class="fa fa-plus" title="Thêm phương án"></i>
                </div>
            </div>
        </div>
    </div>
    <span class="add-test-box-title"
          style="padding-left: 0; left: 0">Phương án trả lời</span>
</div>
<script>
    /*
    *   Sử lý nút thêm phương án trả lời cho loại câu hỏi 'CHỌN' 1 phương án trả lời đúng.
    * */
    $('.add-option-item i').on('click', function(){
        // let question_type   =   $('input[name=question_type_created]').val();
        let _this           =   $(this);
        addAnswerOptions(_this);
    });

    /* destroy option item */
    $(document).on('click','.destroy-option', function(){
        let _this           =   $(this);
        destroyOptionNormal(_this);
    });

    $(document).on('change', 'input[name=type_option]', function(){
        settingOption($(this).val());
    });

    /* chose image option */
    $(document).on('click', 'input[name=option]', function(){
        if($('input[name=type_option]:checked').val() === 'image') {
            nameClass = $(this);
            $('#module-file').modal('show');
        }
    });

    /* check chose is image */
    function checkChoseFile(extension){
        let arrExtension = ['png', 'jpg', 'jpeg', 'gif'];
        if(arrExtension.indexOf(extension) == -1) return false;
        return true;
    }

    /* chose image */
    $(document).on('click', '.file-box', function(){
        if ($('#modal-add-question').hasClass('in')) {
            let imgPath = $(this).find('.file').data('src');
            let fileExtension = imgPath.substr(imgPath.lastIndexOf('.')+1).toLowerCase();
            if(checkChoseFile(fileExtension)) {
                if(nameClass === 'note-icon-picture') {
                    $('.note-image-url').val(imgPath);
                    $('.note-image-btn').trigger('click');
                }
                else {
                    nameClass.val(imgPath);
                }
                $('#module-file').modal('hide');
            }
            else swal('File này không phải là hình ảnh.');
        }
    });

    /* Thêm answer options */
    function addAnswerOptions(_this){
        let size = $('.option-item').length;
        let letter = $('.option-item:eq('+(size-1)+') > div').text();
        let letterNext = 'A';
        if (letter){
            letterNext = String.fromCharCode(letter.charCodeAt(0) + 1);
        }
        let content = '<div class="option-item">'
            + '<div class="option-name">'
            + letterNext
            + '</div>'
            + '<input type="text" name="option" class="input-text" data-error="Vui lòng nhập đầy đủ các phương án đã tạo.">'
            + '<div class="correct"><input type="radio" name="correct"></div>'
            + '<i class="fa fa-times destroy-option"></i>'
            + '</div>';
        let addOptionItem = '<div class="col-lg-6">'
            +'<div class="add-option-item">'
            + '<i class="fa fa-plus"></i>'
            + '</div>'
            + '</div>';
        _this.closest('.col-lg-6').before('<div class="col-lg-6">' + content + '</div>');
        // $('.list-options > div.row').append(addOptionItem);
        $('input[name=correct]').iCheck({
            radioClass: 'iradio_square-green',
        });
        settingOption($('input[name=type_option]:checked').val());
        if(letterNext == 'Z') $('.add-option-item').closest('.col-lg-6').remove();
    }

    /* Xóa answer option */
    function destroyOptionNormal(_this) {
        _this.closest('.col-lg-6').remove();
        let letter = 'A';
        $('.option-name').each(function(){
            $(this).text(letter);
            letter = String.fromCharCode(letter.charCodeAt(0) + 1);
        });
    }

    function settingOption(val){
        let input   =   $('input[name=option]');
        if(val === 'image') {
            input.attr('readonly', 'readonly');
            input.attr('placeholder', 'Nhấn vào đây để chọn ảnh');
            input.val('');
        }
        else {
            input.removeAttr('readonly');
            input.removeAttr('placeholder');
        }
    }

    function getData() {
        if (validate()){
            let data                =   new Object();
            let id                  =   1;
            data.question_type      =   $('input[name=question_type_created]').val();
            data.type_question      =   $('input[name=type_option_question]:checked').val();
            data.type_answer        =   $('input[name=type_option]:checked').val();
            data.hint               =   $('textarea[name=hint]').val();
            data.explain            =   $('textarea[name=explain]').val();
            data.answer_shuff       =   $('input[name=answer_shuff]').is(':checked') ? 1 : 0;
            data.question_state     =   $('select[name=question_state]').val();
            data.answer             =   new Array();
            $('input[name=option]').each(function () {
                let element         =   new Object();
                let answer          =   $(this);
                element.id          =   id;
                element.answer      =   answer.val();
                if (answer.parent('div.option-item').find('div.iradio_square-green').hasClass('checked')){
                    data.correct    =   id;
                }
                answer.css('border','');
                data.answer.push(element);
                id++;
            });
            return data;
        }
        return false;
    }

    function validate() {
        let check   =   true;
        let editor  =   $('.note-editor.note-frame.panel.panel-default');
        let question=   $('textarea[name=question]');
        let correct =   false;
        question.css('border','');
        editor.css('border', '');
        $('input[name=option]').css('border','');
        $('.col-lg-6').each(function () {
            if ($(this).find('input[name=option]').val() === ''){
                $(this).find('input[name=option]').css('border','red 1px solid');
                check   =   false;
                swal('Bạn cần nhập đáp án cho câu hỏi trước!');
            }
        });
        if ($(question.summernote('code')).text() === ''){
            check   =   false;
            editor.css('border', 'red 1px solid');
            swal('Bạn cần nhập câu hỏi trước!');
        }

        $('input[name=correct]').each(function () {
            if ($(this).iCheck('update')[0].checked){
                correct =   true;
            }
        });
        if (!correct && check){
            check   =   false;
            swal('Bạn phải chọn ít nhất 1 đáp án đúng!');
        }
        return check;
    }
</script>
