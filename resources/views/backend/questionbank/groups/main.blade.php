@extends('layouts.app')
@section('title')
    <title>{{$currentFunction->name}} - {{\Illuminate\Support\Facades\Crypt::decryptString(config('custom.app_name'))}}</title>
@endsection

@section('style')

    <link rel="stylesheet" type="text/css" href="{!! asset('css/plugins/dataTables/datatables.min.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! asset('css/plugins/iCheck/custom.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! asset('css/plugins/summernote/summernote.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! asset('css/perfect-scrollbar.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! asset('css/plyr.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! asset('css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css') !!}">
{{--    <link rel="stylesheet" type="text/css" href="{!! asset('css/plugins/chosen/bootstrap-chosen.css') !!}">--}}
    <style>
        table.dataTables-functions > thead > tr > th.stt {
            width: 15px;
        }
        table.dataTables-functions > thead > tr > th.name {
            width: 20%;
        }
        table.dataTables-functions > thead > tr > th.level {
            width: 40px;
        }
        table.dataTables-functions > thead > tr > th.type {
            width: 350px;
        }
        table.dataTables-functions > thead > tr > th.action {
            width: 60px;
        }
        table.dataTables-functions > thead > tr > th.state {
            width: 60px;
        }
        table.dataTables-functions {
            width: 100% !important;
        }
        .modal-full{
            margin-right: 0;
        }
        .modal-full .modal-dialog{
            width: 100%;
            height: 100%;
            margin: 0;
        }
        .modal-full .modal-content{
            height: 100%;
            display: flex;
            flex-direction: column;
            border-radius: unset;
            border: none;
        }
        .modal-full .modal-body{
            flex: 1;
            overflow-y: auto;
            /*padding-bottom: 30px !important;*/
        }
        .modal-title i{
            margin-right: 10px;
        }
        .modal-open .modal{
            overflow-y: hidden;
        }
        fieldset{
            padding: .35em .625em .75em;
            margin: 0 2px;
            border: 1px solid silver;
            padding-top: .5em;
            display: block;
            width: 100%;
            text-align: center;
        }
        fieldset .note-editable.panel-body{
            text-align: initial;
        }
        legend{
            padding: initial!important;
            display: initial!important;
            width: initial!important;
            margin-bottom: initial!important;
            font-size: 13px!important;
            line-height: initial!important;
            color: initial!important;
            border: initial!important;
            border-bottom: initial!important;
            padding-left: 5px!important;
            text-align: left;
        }
        fieldset#media-fieldset:not(textarea){
            /*padding: 20px;*/
            margin: 0 2px;
            border: 1px solid silver;
            padding-top: .5em;
            display: block;
            width: 100%;
            text-align: center;
            height: 393px;
        }
        div.media-type{
            width: 25%;
            padding-left: 0 !important;
            margin-left: 0 !important;
            margin-right: 0 !important;
            display: block;
            float: left;
        }
        .chose-image div,
        .chose-audio div{
            height: 330px;
            border: #ddd dashed 1px;
            width: 100%;
            display: flex;
            align-items: center;
            justify-content: center;
            font-size: 16px;
            cursor: pointer;
        }
        .chose-image img{
            width: 90%;
            height: 90%;
        }
        /* style radio */
        fieldset label input[type=radio]{
            display: none;
        }
        fieldset label{
            display: inline-block;
            position: relative;
            padding-left: 30px;
            cursor: pointer;
            margin-right: 50px;
        }
        fieldset label i:before{
            content: '';
            display: inline-block;
            width: 25px;
            height: 25px;
            border: 1px solid #dddddd;
            background-color: #eeeeee;
            float: left;
            position: absolute;
            left: 0;
            top: 4px;
            border-radius: 50%;
        }
        fieldset label input:checked+i:before{
            background-color: #18a689;
            border-color: #18a689;
        }
        fieldset label i:after{
            content: "\f00c";
            font-family: 'FontAwesome';
            font-size: 15px;
            color: #fff;
            position: absolute;
            left: 3px;
            top: 6px;
            opacity: 0;
            visibility: hidden;
            font-weight: normal;
        }
        fieldset label input:checked+i:after{
            opacity: 1;
            visibility: visible;
        }

        /* Modal add quesiton */
        ul > li{
            list-style: none;
        }
        .rt{
            transform: rotate(180deg);
        }
        .curriculum-content > div{
            padding: 10px 20px 10px 20px;
            display: flex;
            justify-content: center;
        }
        .chose-curriculum-content{
            border: #ddd solid 1px;
            border-top: none;
            display: none;
            max-height: 200px;
            overflow-y: auto;
            padding-bottom: 10px;
        }
        .chose-curriculum-content > div{
            width: 90%;
            margin: auto;
            padding-top: 10px;
            padding-bottom: 10px;
        }
        .chose-curriculum-content input{
            border-radius: 5px;
        }
        .chose-curriculum-content li{
            height: 40px;
            display: flex;
            align-items: center;
            padding-left: 20px;
            cursor: pointer;
        }
        .chose-curriculum-content li:hover{
            background: #dddddd73;
        }
        .chose-curriculum-content ul{
            padding-left: 0;
            margin-bottom: 0;
        }
        .chose-curriculum-title{
            height: 35px;
            border: #ddd solid 1px;
            display: flex;
            align-items: center;
            padding-left: 10px;
            padding-right: 10px;
            cursor: pointer;
        }
        .chose-curriculum-title i{
            transition: all 0.5s;
        }
        .chose-curriculum-title span{
            flex: 1;
        }
        .load{
            animation: load 2s;
            animation-iteration-count: infinite;
        }
        .test-options{
            padding-left: 0 !important;
            padding-right: 0 !important;
        }
        .test-additional{
            margin-top: 5px !important;
            border-top: #ddd solid 1px;
            position: relative;
            overflow: unset;
            font-size: 13px;
            padding-top: 15px;
        }
        @keyframes load {
            from {transform: rotate(0deg);}
            to {transform: rotate(360deg);}
        }
        .list-type-content{
            margin-bottom: 10px;
            display: none;
        }
        .media-video div, .media-audio div{
            display: flex;
            height: 40px;
            border: #ddd solid 1px;
            align-items: center;
            border-radius: 5px;
            border-top-left-radius: 10px;
            border-bottom-left-radius: 10px;
            margin-bottom: 10px;
            cursor: pointer;
        }
        .media-video div i{
            font-size: 47px;
            color: #e74c3c;
        }
        .media-audio div i{
            font-size: 20px;
        }
        .media-video div:last-child i{
            color: #3498db;
        }
        .media-video div input, .media-audio div input{
            height: 100%;
            flex: 1;
            padding-left: 10px;
            padding-right: 10px;
            outline: none;
            border: none;
        }
        .media-video label, .media-audio label{
            margin-bottom: 10px;
        }
        .media-audio div > span{
            height: 100%;
            display: flex;
            align-items: center;
            background-color: #3498db;
            color: #fff !important;
            padding-left: 15px;
            padding-right: 15px;
            border-top-left-radius: 5px;
            border-bottom-left-radius: 5px;
        }
        .media-audio div > span i{
            margin-right: 5px;
        }
        .btn-question{
            text-align: center;
            position: relative;
        }
        .modal-full{
            margin-right: 0;
            padding-right: 0 !important;
        }
        .modal-full .modal-dialog{
            width: 100%;
            height: 100%;
            margin: 0;
        }
        .modal-full .modal-content{
            height: 100%;
            display: flex;
            flex-direction: column;
            border-radius: unset;
            border: none;
        }
        .box-test, .box-title{
            border: #ddd solid 1px;
            padding: 15px;
            position: relative;
            margin-top: 30px !important;
            overflow: unset;
            font-size: 13px;
        }
        .add-test-box-title{
            position: absolute;
            top: -12px;
            left: 5px;
            background: #fff;
            padding-left: 5px;
            padding-right: 5px;
            font-weight: bold;
        }
        .test-custom-item{
            height: 40px;
            display: flex;
            align-items: center;
            margin-bottom: 20px;
        }
        .test-custom-item-input{
            margin-bottom: 0 !important;
        }
        .test-custom-item-input input{
            height: 40px;
            padding-left: 20px;
            padding-right: 30px;
            font-size: 16px;
            width: 50%;
        }
        .test-custom-item span, .test-custom-item div{
            flex: 1;
        }
        .test-custom-item div{
            text-align: center;
        }
        .test-custom-item span{
            text-align: right;
            margin-right: 50px;
        }
        .test-custom-item-input > div{
            position: relative;
        }
        .symbol{
            position: absolute;
            right: 7%;
            height: 20px;
            top: 0;
            bottom: 0;
            margin: auto;
        }
        .question-content .box-test:first-child{
            margin-top: 0 !important;
        }
        select[name=correct]{
            width: 20%;
        }
        .list-options{
            padding-top: 15px;
        }
        .option-item{
            display: flex;
            height: 35px;
            border: #ddd solid 1px;
            position: relative;
        }
        .option-item:before{
            position: absolute;
            content: "";
            width: 3%;
            height: 100%;
            bottom: 16px;
            right: 0;
        }
        .option-name{
            display: flex;
            align-items: center;
            width: 35px;
            justify-content: center;
            border-right: #ddd solid 1px;
            background: #89898933;
        }
        .correct{
            display: flex;
            align-items: center;
            padding-right: 10px;
        }
        .option-item input{
            border: none;
            padding-left: 10px;
            padding-right: 10px;
            flex: 1;
            outline: none;
        }
        .destroy-option{
            position: absolute;
            right: 0px;
            color: #c0392b;
            cursor: pointer;
            top: -15px;
            display: none;
        }
        .option-item:hover .destroy-option{
            display: block;
        }
        .add-option-item{
            text-align: center;
        }
        .add-option-item i{
            width: 35px;
            height: 35px;
            background-color: #89898933;
            line-height: 35px;
            font-size: 16px;
            border-radius: 50px;
            cursor: pointer;
        }
        .add-option-item-answer{
            text-align: center;
        }
        .add-option-item-answer i{
            width: 35px;
            height: 35px;
            background-color: #89898933;
            line-height: 35px;
            font-size: 16px;
            border-radius: 50px;
            cursor: pointer;
        }
        .list-options .col-lg-6{
            margin-bottom: 20px;
        }
        .box-image-option{
            max-width: 230px;
            max-height: 150px;
            position: fixed;
            z-index: 9999999;
            border: #ddd solid 1px;
            padding: 5px;
            border-radius: 5px;
            box-shadow: #ddd 0 0 5px;
        }
        .detail-question-title{
            background-color: #89898933;
            border-radius: 5px;
            padding: 10px;
            text-align: justify;
        }
        .detail-question-option{
            padding-left: 25px;
            padding-right: 25px;
            padding-top: 5px;
            padding-bottom: 5px;
        }
        input[name=answer]{
            margin-right: 5px;
        }
        .detail-question-option span{
            font-weight: 500;
        }
        .btn-question button:before{
            position: absolute;
            content: "";
            width: 14%;
            left: 0;
            height: 115%;
            right: 0;
            top: 0;
            bottom: 0;
            margin: auto;
            border: #1a7bb9 solid 1px;
            border-radius: 5px;
            animation: zoomButton 0.5s;
            animation-iteration-count: infinite;
        }
        @keyframes zoomButton {
            0% {
                width: 14%;
                height: 115%;
                opacity: 1;
            }
            100% {
                width: 17%;
                height: 160%;
                opacity: 0;
            }
        }
        .detail-question-option span{
            margin-left: 5px;
        }
        /* Style table question */
        table.dataTables-questions-list > tbody > tr > td.question-stt,
        table.dataTables-questions-list > thead > tr > th.question-stt{
            width: 25px !important;
            vertical-align: middle !important;
        }
        table.dataTables-questions-list > tbody > tr > td.table-question,
        table.dataTables-questions-list > thead > tr > th.table-question{
            vertical-align: middle !important;
        }
        table.dataTables-questions-list > tbody > tr > td.table-state,
        table.dataTables-questions-list > thead > tr > th.table-state{
            vertical-align: middle !important;
            width: 80px !important;
            text-align: center!important;
        }
        table.dataTables-questions-list > tbody > tr > td.table-action,
        table.dataTables-questions-list > thead > tr > th.table-action{
            vertical-align: middle !important;
            width: 80px !important;
        }
        table.dataTables-questions-list > tbody > tr > td.table-correct,
        table.dataTables-questions-list > tbody > tr > td.table-correct,
        table.dataTables-questions-list > tbody > tr > td.table-answers,
        table.dataTables-questions-list > thead > tr > th.table-answers{
            vertical-align: middle !important;
            width: 25% !important;
        }
        .dataTables_filter label{
            margin-right: 0px !important;
        }
    </style>
@endsection
@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Quản lý {{mb_strtolower($currentFunction->name)}} - {{\App\TestSubject::find($subject_id)->name}}</h2>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-functions">
                                <thead>
                                <tr>
                                    <th class="stt">STT</th>
                                    <th>Name</th>
                                    <th class="state">Type</th>
                                    <th class="type">Question type</th>
                                    <th class="state">Shuff</th>
                                    <th class="level">Level</th>
                                    <th class="state">State</th>
                                    <th class="action">Action</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal add new function -->
    @include('backend.questionbank.groups.form')
    @include('backend.questionbank.groups.modal-add-question')
    @include('backend.system.file.module')
@endsection

@section('script')
    <script src="{!! asset('js/plugins/dataTables/datatables.min.js') !!}"></script>
    <!-- Page-Level Scripts -->
    <!-- iCheck -->
    <script src="{!! asset('js/plugins/iCheck/icheck.min.js') !!}"></script>
    <script src="{!! asset('js/perfect-scrollbar.min.js') !!}"></script>
    <script src="{!! asset('js/plugins/summernote/summernote.min.js') !!}"></script>

    <!-- Plyr player -->
    <script src="{!! asset('js/plyr.js') !!}"></script>
    <script src="{!! asset('js/jquery.base64.js') !!}"></script>

    <script>
        let mainUrl         =   '{{asset('')}}' +   '{{$currentFunction->route}}';
        let subject_id      =   parseInt('{!! $subject_id !!}',10);
        let groupUrl        =   mainUrl + '/getDatatable';
        let questionUrl     =   '{!! asset('/') !!}' + 'questions/getDatatable';
        let summernoteConfig = [
            // [groupName, [list of button]]
            // ['insert',['picture']],
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough', 'superscript', 'subscript']],
            ['fontsize', ['fontsize','fontname']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']]
        ];
        $(document).ready(function(){
            /* Table groups */
            let Table = $('.dataTables-functions').DataTable({
                bSort: false,
                bInfo: true,
                bLengthChange: false,
                processing: true,
                serverSide: true,
                searching: true,
                paging: true,
                pageLength: 15,
                responsive: true,
                ajax: $.extend({
                        url: groupUrl
                    }, {
                        type: 'get'
                    },
                    {
                        data: function (d) {
                            d.subject_id    =   subject_id;
                        }
                    }),
                // ajax: mainUrl + '/getDatatable',
                dom: '<"row"<"col-sm-6"Bi><"col-sm-6"f>>rtp',
                buttons: {
                    buttons: [
                        {
                            text: 'Thêm mới',
                            action: function ( e, dt, node, config ) {
                                resetForm();
                                TableQuestion.ajax.reload();
                                $('#modal-add-function').modal({backdrop: 'static', keyboard: false, show : true});
                            }
                        }
                    ],
                    dom: {
                        button: {
                            tag: "button",
                            className: "btn btn-primary"
                        },
                        buttonLiner: {
                            tag: null
                        }
                    }
                },
                language: {
                    "lengthMenu": "Hiển thị _MENU_ bản ghi trên 1 trang",
                    "zeroRecords": "Không có dữ liệu",
                    "info": "Hiển thị trang _PAGE_ trong tổng số _PAGES_ trang",
                    "infoEmpty": "Không có dữ liệu tồn tại",
                    "infoFiltered": "(Lọc từ _MAX_ bản ghi)",
                    "search": "Tìm kiếm:",
                    "paginate": {
                        "first":      "Trang đầu",
                        "last":       "Trang cuối",
                        "next":       "Trang tiếp theo",
                        "previous":   "Trang trước"
                    },
                },
                columns     :   [
                    {
                        data    :   'id',
                        className:  'text-center',
                        render  :   function (data, type, row, meta) {
                            return meta.row +1;
                        }
                    },
                    {   data    :   'name'},
                    {
                        data    :   'media_type',
                        className:  'text-center',
                        render  :   function (data, type, row, meta) {
                            let media = parseInt(data,10);
                            switch (media) {
                                case 1  :   return 'Text';
                                case 2  :   return 'Image';
                                case 3  :   return 'Audio';
                                case 4  :   return 'Video';
                                default :   return 'Other';
                            }
                        }
                    },
                    {
                        data    :   'question_type',
                        render  :   function (data, type, row, meta) {
                            data                =   parseInt(data,10);
                            let question_type   =   JSON.parse('{!! $question_type !!}');
                            let obj             =   question_type.find(o => o.id === data);
                            return obj.name;
                        }
                    },
                    {
                        data    :   'question_shuff',
                        className:  'text-center',
                        render  :   function (data, type, row, meta) {
                            return data === 0 ? 'False' : 'True';
                        }
                    },
                    {
                        data    :   'level',
                        className:  'text-center',
                        render  :   function (data, type, row, meta) {
                            data    =   parseInt(data,10);
                            switch (data) {
                                case 1  :   return 'Dễ';
                                case 2  :   return 'Trung bình';
                                case 3  :   return 'Khó';
                                default :   return 'Khác';
                            }
                        }
                    },
                    {
                        data    :   'state',
                        render  :   function (data, type, row, meta) {
                            let state;
                            switch (parseInt(data)){
                                case 0: state = 'Không kích hoạt'; break;
                                case 1: state = 'Kích hoạt'; break;
                                // case 2: state = 'Kích hoạt ẩn'; break;
                                default:state = 'Chưa xác định'; break;
                            }
                            return state;
                        }
                    },
                    {
                        data    :   'id',
                        className: 'text-center',
                        render  :   function (data, type, row, meta) {
                            return '@permission("Edit-27")<button name="btn-edit" class="btn btn-primary btn-flat btn-xs" title="Sửa" data-data=\'' + JSON.stringify(row) + '\'><i class="fa fa-edit"></i></button>@endpermission @permission("Delete-27")<button name="btn-delete" class="btn btn-remove btn-danger btn-flat btn-xs" title="Xóa" data-id=' + data + '><i class="fa fa-remove"></i></button>@endpermission';
                        }
                    }
                ]
            });

            /* Table question */
            let TableQuestion = $('.dataTables-questions-list').DataTable({
                "aoColumnDefs": [ {
                    "aTargets": [ 1,2 ],
                    "mRender": function ( data, type, full ) {
                        return $("<div/>").html(data).text();
                    }
                } ],
                bSort: false,
                bInfo: true,
                bLengthChange: false,
                processing: true,
                serverSide: true,
                searching: true,
                paging: true,
                pageLength: 15,
                responsive: true,
                ajax: $.extend({
                        url: questionUrl
                    }, {
                        type: 'get'
                    },
                    {
                        data: function (d) {
                            d.group_id    =   $('input[name=id]').val();
                        }
                    }),
                // ajax: mainUrl + '/getDatatable',
                dom: '<"row"<"col-sm-6"Bi><"col-sm-6"f>>rtp',
                buttons: {
                    buttons: [
                        {
                            text: 'Thêm câu hỏi mới',
                            // action: function ( e, dt, node, config ) {
                            //     $(this).trigger('add-question');
                            // }
                        }
                    ],
                    dom: {
                        button: {
                            tag: "button",
                            className: "btn btn-primary btn-add-question"
                        },
                        buttonLiner: {
                            tag: null
                        }
                    }
                },
                language: {
                    "lengthMenu": "Hiển thị _MENU_ bản ghi trên 1 trang",
                    "zeroRecords": "Không có dữ liệu",
                    "info": "Hiển thị trang _PAGE_ trong tổng số _PAGES_ trang",
                    "infoEmpty": "Không có dữ liệu tồn tại",
                    "infoFiltered": "(Lọc từ _MAX_ bản ghi)",
                    "search": "Tìm kiếm:",
                    "paginate": {
                        "first":      "Trang đầu",
                        "last":       "Trang cuối",
                        "next":       "Trang tiếp theo",
                        "previous":   "Trang trước"
                    },
                },
                columns     :   [
                    {
                        data    :   'id',
                        className:  'text-center question-stt',
                        render  :   function (data, type, row, meta) {
                            return meta.row +1;
                        }
                    },
                    {
                        className:  'table-question',
                        data    :   'question'
                    },
                    {
                        className:  'table-answers',
                        data    :   'answers'
                    },
                    {
                        className:  'table-correct',
                        data    :   'correct'},
                    {
                        className:  'table-state',
                        data    :   'state',
                        render  :   function (data, type, row, meta) {
                            let state;
                            switch (parseInt(data)){
                                case 0: state = 'Không kích hoạt'; break;
                                case 1: state = 'Kích hoạt'; break;
                                case 2: state = 'Kích hoạt ẩn'; break;
                                default:state = 'Chưa xác định'; break;
                            }
                            return state;
                        }
                    },
                    {
                        data    :   'id',
                        className: 'text-center table-action',
                        render  :   function (data, type, row, meta) {
                            let answers =   row.answer_options;
                            delete row.answer_options;
                            answers     =   $.base64.atob(answers);
                            return '@permission("Edit-27")<button name="btn-edit-question" class="btn btn-primary btn-flat btn-xs" title="Sửa" data-data=\'' + JSON.stringify(row) + '\' data-answers='+answers+'><i class="fa fa-edit"></i></button>@endpermission @permission("Delete-27")<button name="btn-delete-question" class="btn btn-remove btn-danger btn-flat btn-xs" title="Xóa" data-id=' + data + '><i class="fa fa-remove"></i></button>@endpermission';
                        }
                    }
                ]
            });


            /* Add new/ edit function */
            $('button[name=add-function]').on('click', function () {
                let media_type = $('input[name=media_type]:checked').val();
                if (media_type == 1){
                    $('input[name=media_source]').val(($('textarea[name=media_source]').summernote('code')));
                }
                let data    =   $('form[name=form-add-function]').serialize(),
                    id      =   $('input[name=id]').val(),
                    type    =   'POST',
                    url     =   mainUrl;
                if (parseInt(id)){
                    type    =   'PUT';
                    url     =   mainUrl + '/' + id;
                }
                $.ajax(url,{
                    type    :   type,
                    data    :   data,
                    success :   function (respon) {
                        notification(respon.type, respon.title, respon.content);
                        if(respon.type === 'success'){
                            Table.ajax.reload();
                            if (respon.group_id > 0){
                                $('input[name=id]').val(respon.group_id);
                            }
                            $('input[name=question_type_create]').val(respon.question_type);
                            $('select[name=question_type]').prop('disabled',true);
                            TableQuestion.ajax.reload();
                        }
                    }
                })
            });

            /* Edit function */
            $(document).on('click', 'button[name=btn-edit]', function () {
                resetForm();
                let data = $(this).data('data');
                $.each(data, function (index, item) {
                    if (index !== 'media_type')
                        $('[name='+index+']').val(item);
                });
                $('input[name=question_type_created]').val(data.question_type);
                disableMediaType(data.question_type);
                $('textarea[name=content]').summernote('code',data.content);
                if (parseInt(data.question_shuff)){
                    $('input[name=question_shuff]').iCheck('check');
                } else {
                    $('input[name=question_shuff]').iCheck('uncheck');
                }
                $('input[type=radio][name=media_type][value="'+data.media_type+'"]').prop('checked',true);
                changeMediaBox(data.media_type);
                switch (data.media_type) {
                    case '1'    :   $('textarea[name=media_source]').summernote('code',data.media_source); break;
                    case '2'    :   let image = '<image src="'+'{!! asset('/') !!}'+data.media_source+'" alt="Ảnh" style="height: 345px; width: auto; max-width: 844px;">';
                                    $('div.chose-image').html(image);
                                    break;
                    case '3'    :   let media = '<div class="col-sm-12"> <button name="chose-audio" class="btn btn-primary" type="button">Chose Audio</button></div>';
                                    let audio   =   '<audio crossorigin controls style="z-index: 99; margin-top: 100px"> <source src="'+'{!! asset('/') !!}'+data.media_source+'" type="audio/mp3"> </audio>';
                                    $('div#media-box').html('<div style="margin-top: 200px" ">'+audio + media+'</div>');
                                    const player = new Plyr('audio', {});
                                    break;
                    case '4'    :   let player_html =   '<div class="container" style="width: 65%; padding-bottom: 15px"> <div id="player" data-plyr-provider="youtube" data-plyr-embed-id="'+data.media_source+'"></div> </div>';
                                    $('div#video-player').html(player_html);
                                    new Plyr('#player', {
                                        modestbranding: true
                                    });
                                    break;
                }
                $('select[name=question_type]').prop('disabled',true);
                TableQuestion.ajax.reload();
                $('#modal-add-function').modal({backdrop: 'static', keyboard: false, show : true});
            });

            /* Delete function */
            $(document).on('click', 'button[name=btn-delete]', function () {
                let id  = $(this).data('id');
                let url =   mainUrl + '/' + id;
                swal({
                    title: "Bạn có muốn xóa group này??\nToàn bộ câu hỏi của group này cũng sẽ bị xóa",
                    text: "Khi xóa sẽ không thể khôi phục lại",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "OK",
                    cancelButtonText: "Không",
                    closeOnConfirm: true,
                    closeOnCancel: true
                }, function (config) {
                    if (config){
                        $.ajax(url,{
                            type    :   'DELETE',
                            data    :   {
                                _token  :   '{{csrf_token()}}'
                            },
                            success :   function (respon) {
                                notification(respon.type, respon.title, respon.content);
                                if(respon.type === 'success'){
                                    Table.ajax.reload();
                                }
                            }
                        })
                    }
                });
            });

            /* iCheck */
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
            $('input[name=correct]').iCheck({
                radioClass: 'iradio_square-green',
            });

            changeMediaBox(1);

            /* Current value of media type */
            /* Change media type */
            $('input[name=media_type]').on('change', function () {
                let type = $(this).val();
                changeMediaBox(type)
            });

            /* destroy video player when close modal */
            $(document).on('hide.bs.modal','#modal-add-function', function () {
                $('#media-box').html('');
            });

            /* change url video */
            $(document).on('input','input[name=youtube_source]',function () {
                let url = $(this).val();
                let youtube_id = youtube_parser(url);
                if (youtube_id !== 'false'){
                    // let input       = '<input type="text" name="youtube_source" class="form-control form-group">';
                    $('input[name=media_source]').val(youtube_id);
                    let player_html =   '<div class="container" style="width: 65%; padding-bottom: 15px"> <div id="player" data-plyr-provider="youtube" data-plyr-embed-id="'+youtube_id+'"></div> </div>';
                    $('div#video-player').html(player_html);
                    const player    = new Plyr('#player', {
                        modestbranding: true
                    });
                }

            });

            $(document).on('paste','input[name=youtube_source]', function (e) {
                $(this).val('');
                // let url = e.originalEvent.clipboardData.getData('text');
                // let youtube_id = youtube_parser(url);
                // alert(youtube_id);
            });

            /* Upload ảnh */
            $(document).on('click','div.chose-image, button[name=chose-audio]', function () {
                $('#module-file').modal('show');
            });

            $(document).on('click','div.file.upload', function () {
                if (!$('#modal-add-question').hasClass('in')) {
                    let arrayImage  =   ['jpg', 'JPG', 'png', 'PNG', 'jpeg', 'JPEG', 'bmp', 'BMP', 'gif', 'GIF'];
                    let arrayAudio  =   ['mp3', 'MP3', 'OGG', 'ogg', 'WAV', 'wav'];
                    let url         =   $(this).data('src');
                    let extension   =   $(this).data('extension');
                    let media_type  =   $('input[name=media_type]:checked').val();
                    $('input[name=media_source]').val(url);
                    if ($.inArray(extension,arrayImage) >= 0  && media_type == 2){
                        let image = '<image src="'+'{!! asset('/') !!}'+ url +'" alt="Ảnh" style="height: 345px; width: auto; max-width: 844px;">';
                        $('div.chose-image').html(image);
                    }
                    if ($.inArray(extension, arrayAudio) >= 0  && media_type == 3){
                        let media = '<div class="col-sm-12"> <button name="chose-audio" class="btn btn-primary" type="button">Chose Audio</button></div>';
                        let audio   =   '<audio crossorigin controls style="z-index: 99; margin-top: 100px"> <source src="'+'{!! asset('/') !!}'+url+'" type="audio/mp3"> </audio>';
                        $('div#media-box').html('<div style="margin-top: 150px" ">'+audio + media+'</div>');
                        const player = new Plyr('audio', {});
                    }
                }
            });

           /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
            *                                                                       *
            *                   Sử lý popup modal thêm câu hỏi.                     *
            *                                                                       *
            * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
            $(document).on('click', 'button.btn-add-question', function () {
                let question_type   =   $('input[name=question_type_created]').val();
                addQuestionInput(parseInt(question_type));
                $('input[name=correct]').iCheck({
                    radioClass: 'iradio_square-green',
                });
                $('textarea[name=question]').summernote({
                    height: 100,
                });
                let group_id        =   $('input[name=id]').val();
                $('textarea[name=hint]').val('');
                $('textarea[name=explain]').val('');
                $('input[name=question_id]').val(0);
                $('input[name=answer_shuff]').iCheck('check');
                if (parseInt(group_id)){
                    $('#modal-add-question').modal('show');
                } else {
                    swal('Bạn phải tạo group trước');
                }
            });
            //
            // /* edit question */
            // $(document).on('click','button[name=btn-edit-question]', function () {
            //     let question_type   =   $('input[name=question_type_created]').val();
            //     let data_question   =   $(this).data('data');
            //     let data_answers    =   $(this).data('answers');
            //     addQuestionInput(parseInt(question_type));
            //     addAnswerForEdit(question_type, data_answers, data_question.correct_answer);
            //     $('input[name=correct]').iCheck({
            //         radioClass: 'iradio_square-green',
            //     });
            //     $('textarea[name=question]').summernote('code',data_question.question,{
            //         height: 100,
            //     });
            //     let type_option     =   data_answers[0].type;
            //     $('input[type=radio][name=type_option][value='+type_option+']').prop('checked',true);
            //     $('textarea[name=hint]').val(data_question.hint);
            //     $('textarea[name=explain]').val(data_question.explain);
            //     let check           =   data_question.answer_shuff ? 'check' : 'uncheck';
            //     $('input[name=answer_shuff]').iCheck(check);
            //     $('select[name=question_state]').val(data_question.state);
            //     $('input[name=question_id]').val(data_question.id);
            //     $('#modal-add-question').modal('show');
            // });
            //
            // function addAnswerForEdit(question_type, data_answers, correct_answer) {
            //     switch (parseInt(question_type)) {
            //         case 3  :   addAnswerMatchingForEdit(data_answers, correct_answer); break;
            //         default :   addAnswerNormalForEdit(data_answers, correct_answer); break;
            //     }
            // }
            //
            // function addAnswerNormalForEdit(data_answers, correct_answer) {
            //     let charcode        =   'A'.charCodeAt(0);
            //     let choice          =   '<div class="row">';
            //     data_answers.forEach(function (element) {
            //         let checked     =   '';
            //         if (element.id == correct_answer){
            //             checked     =   'checked';
            //         }
            //         choice  +=  '<div class="col-lg-6">'
            //                 +   '<div class="option-item">'
            //                 +   '<div class="option-name">' + String.fromCharCode(charcode)
            //                 +   '</div>'
            //                 +   '<input type="text" name="option" class="input-text" data-error="Vui lòng nhập đầy đủ các phương án đã tạo." value="'+element.content+'">'
            //                 +   '<div class="correct"><input type="radio" name="correct" '+checked+'></div>'
            //                 +   '<i class="fa fa-times destroy-option"></i>'
            //                 +   '</div>'
            //                 +   '</div>';
            //         charcode++;
            //     });
            //     choice  +=  '<div class="col-lg-6">'
            //             +   '<div class="add-option-item">'
            //             +   '<i class="fa fa-plus" title="Thêm phương án"></i>'
            //             +   '</div></div>';
            //     $('div.box-test.test-options > div.list-options').html(choice);
            // }
            let nameClass = null;
            function addQuestionInput(question_type){
                let question_input                  =   $('#question_input');
                let add_button                      =   $('.dt-buttons.btn-group');
                add_button.removeClass('hide');
                question_input.html('');
                let question_chose_the_best_answer  =   {!! json_encode(view('backend.questionbank.groups.question-chose-thebestanswer')->render()) !!};
                let matching                        =   {!! json_encode(view('backend.questionbank.groups.question-matching')->render()) !!};
                let matching2                       =   {!! json_encode(view('backend.questionbank.groups.question-matching-2')->render()) !!};
                switch (question_type) {
                    case 1  :   question_input.html(question_chose_the_best_answer); break;
                    case 2  :   question_input.html(matching2); break;
                    case 3  :   question_input.html(matching); break;
                    case 4  :   add_button.addClass('hide'); break;
                    case 5  :   add_button.addClass('hide'); break;
                    case 6  :   add_button.addClass('hide'); break;
                    case 7  :   question_input.html(matching); break;
                    case 8  :   question_input.html(matching); break;
                    case 9  :   question_input.html(matching); break;
                    default :   question_input.html(question_chose_the_best_answer); break;
                }
            }

            /* Change type question to Điền từ và cụm từ với đoạn văn cho trước */
            $(document).on('change','select[name=question_type]', function () {
                let type = parseInt($(this).val());
                disableMediaType(type);
            });
            function disableMediaType(type) {
                switch (type) {
                    case 4:
                    case 5:
                    case 6:     changeMediaBox(1);
                                $('input[type=radio]#media2').prop('disabled',true);
                                $('input[type=radio]#media3').prop('disabled',true);
                                $('input[type=radio]#media4').prop('disabled',true);
                                $('.btn-add-question').addClass('hide');
                                TableQuestion.column(5).visible(false);
                                break;
                    default:    $('input[type=radio]#media2').prop('disabled',false);
                                $('input[type=radio]#media3').prop('disabled',false);
                                $('input[type=radio]#media4').prop('disabled',false);
                                $('.btn-add-question').removeClass('hide');
                                TableQuestion.column(5).visible(true);
                                break;
                }
            }

           /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
            *                                                           *
            *               Submit question to controller               *
            *                                                           *
            * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

            /* reset form */
            $(document).on('click', '#refresh-form', function(){
                let question_type   =   $('input[name=question_type_created]').val();
                addQuestionInput(parseInt(question_type));
                $('form[name=form-add-question]').trigger('reset');
                $('textarea[name=question]').summernote('reset');
                $('input[name=type_option_question][value=text]').prop('checked');
                $('input[name=type_option_answer][value=text]').prop('checked');
                $('input[name=type_option][value=text]').prop('checked');
            });

        });

        function resetForm() {
            $('form[name=form-add-function]').trigger('reset');
            $('select[name=test_subject_id]').val(subject_id);
            $('select[name=test_subject_id]').prop( "disabled", true );
            $('select[name=question_type]').prop( "disabled", false );
            $('input[name=id]').val(0);
            $('textarea[name=content]').summernote('code','');
            $('input[name=question_shuff]').iCheck('check');
            $('.dt-buttons.btn-group').removeClass('hide');
            changeMediaBox(1);
        }
        /* summernote editer */
        $('textarea[name=content]').summernote({
            toolbar: summernoteConfig,
            height: 230
        });

        /* Change media box */
        function changeMediaBox(type = 1) {
            if (type == 1){
                let textarea = '<textarea name="media_source"></textarea> ';
                $('div#media-box').html(textarea);
                $('textarea[name=media_source]').summernote({
                    toolbar: summernoteConfig,
                    height: 300
                });
            }
            if (type == 2){
                let media = '<div class="col-sm-12"> <div class="chose-image"> <div>Nhấn vào để chọn ảnh</div> <span class="add-curriculum-box-title">Ảnh minh họa </span> </div>';
                $('div#media-box').html(media);
            }

            if (type == 3){
                let media = '<div class="col-sm-12"> <button name="chose-audio" class="btn btn-primary" type="button">Chose Audio</button></div>';
                $('div#media-box').html(media);
            }

            if (type == 4){
                let input = '<div id="video-player"></div><input type="text" name="youtube_source" class="form-control form-group" placeholder="Paste link youtube hear!!" style="width: 62%; display: inline-block">';
                $('div#media-box').html(input);
            }
        }

        /* get youtube id */
        function youtube_parser(url){
            let regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
            let match = url.match(regExp);
            return (match && match[7].length == 11) ? match[7] : false;
        }
    </script>
@endsection
