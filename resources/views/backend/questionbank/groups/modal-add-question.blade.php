<div id="modal-add-question" class="modal fade modal-full" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><i class="fa fa-clock-o"></i>Cật nhật thông tin bài kiểm tra</h4>
            </div>
            <div class="modal-body">
                <form name="form-add-question">
                    <div class="question box-test">
                        <div id="question_input"></div>
                        <div class="test-additional">
                            <div class="form-group">
                                <label for="hint">Gợi ý: </label>
                                <textarea class="form-control" name="hint" id="hint"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="explain">Giải thích đáp án: </label>
                                <textarea class="form-control" name="explain" id="explain"></textarea>
                            </div>
                            <div class="row">
                                <div class="col-sm-1">
                                    <label> </label>
                                    {{--<div class="i-checks"><label> <input type="checkbox" value="" checked="" name="answer_shuff"> <i></i> Answer Shuff </label></div>--}}
                                    <div class="checkbox checkbox-primary">
                                        <input id="answer_shuff" type="checkbox" checked name="answer_shuff">
                                        <label for="answer_shuff" style="padding-top: 0px!important;">
                                            <strong>Answer Shuff</strong>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <label>State</label>
                                    <select name="question_state" class="form-control">
                                        <option value="1">Kích hoạt</option>
                                        <option value="0">Không kích hoạt</option>
                                        <option value="2">Kích hoạt ẩn</option>
                                    </select>
                                </div>
                            </div>
                            <span class="add-test-box-title" style="padding-left: 0; left: 0">Bổ sung</span>
                        </div>
                        <span class="add-test-box-title">Thêm câu hỏi</span>
                    </div>
                    <input type="hidden" name="question_id" value="">
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="save-question">Lưu</button>
                <button type="button" class="btn btn-default" id="refresh-form">Làm mới</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
