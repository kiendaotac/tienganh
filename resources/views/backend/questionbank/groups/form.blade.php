<div class="modal inmodal modal-full" id="modal-add-function" tabindex="-1" role="dialog" aria-hidden="true" style="padding-left: initial!important;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header" style="padding-top: 5px; padding-bottom: 5px; text-align: left">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                            class="sr-only">Close</span></button>
                <h4 class="modal-title">Thêm {{mb_strtolower($currentFunction->name)}}</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <form action="#" method="post" name="form-add-function">
                        {{csrf_field()}}
                            <div class="col-md-6">
                                <div class="col-md-6">
                                    <label>{{__('groups.name_group')}} </label>
                                    <input type="text" class="form-control form-group" name="name">
                                    <label>{{__('groups.name_subject')}} </label>
                                    <select name="test_subject_id" id="" class="form-control form-group">
                                        @foreach($subjects as $subject)
                                            <option value="{{$subject->id}}"
                                                @if ($subject->parent_id ==0)
                                                    disabled
                                                    style="font-weight: bold"
                                                @endif>
                                                @if ($subject->parent_id > 0)
                                                    &nbsp;
                                                @endif
                                                    {{$subject->name}}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <label>Question Type</label>
                                    <select name="question_type" id="" class="form-control form-group">
                                        @foreach($question_type as $item)
                                            <option value="{{$item->id}}">{{$item->name}}</option>
                                        @endforeach
                                    </select>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label>&nbsp;</label>
                                            <div class="i-checks"><label> <input type="checkbox" value="1" name="question_shuff" checked> Shuff</label></div>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Level</label>
                                            <select name="level" class="form-control form-group">
                                                <option value="1">Dễ</option>
                                                <option value="2">Trùng bình</option>
                                                <option value="3">Khó</option>
                                            </select>
                                        </div>
                                        <div class="col-md-5">
                                            <label>{{__('groups.state')}}</label>
                                            <select name="state" id="" class="form-control form-group">
                                                <option value="1">Kích hoạt</option>
                                                {{--<option value="2">Kích hoạt ẩn</option>--}}
                                                <option value="0">Không kích hoạt</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <label>Description</label>
                                    <textarea name="content" rows="20" class="form-group form-control"></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="col-md-12">
                                <fieldset>
                                    <legend><label style="color: #676a6c">Content type: </label></legend>
                                    <label for="media1"> <input id="media1" type="radio" value="1" name="media_type" checked> <i></i> Text </label>
                                    <label for="media2"> <input id="media2" type="radio" value="2" name="media_type"> <i></i> Image </label>
                                    <label for="media3"> <input id="media3" type="radio" value="3" name="media_type"> <i></i> Audio </label>
                                    <label for="media4"> <input id="media4" type="radio" value="4" name="media_type"> <i></i> Video </label>
                                </fieldset>
                                <fieldset id="media-fieldset">
                                    <legend id="media-legend" ><label style="color: #676a6c">Content</label></legend>
                                    <div id="media-box"></div>
                                </fieldset>
                                </div>
                            </div>
                        <input type="hidden" name="media_source" value="">
                        <input type="hidden" name="test_subject_id" value="{{$subject_id}}">
                        <input type="hidden" name="question_type_created" value="">
                        <input type="hidden" name="id" value="0">
                        {{--<div class="col-sm-12 form-group">--}}
                            {{--<button type="button" class="btn btn-primary float-right" name="add-function" style="float: right;margin-right: 13px;margin-top: 13px;"><i class="fa fa-save"></i> Save</button>--}}
                            {{--<button type="button" class="btn btn-primary float-right" name="add-question" style="float: right;margin-right: 13px;margin-top: 13px;"><i class="fa fa-plus"></i> Add Question</button>--}}
                        {{--</div>--}}
                    </form>
                </div>
                {{--<hr class="hr-line-dashed"/>--}}
                    <div class="ibox float-e-margins" style="margin-top: 13px">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover dataTables-questions-list" style="width: 100%">
                                    <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th>Câu hỏi</th>
                                        <th>DS Đáp án</th>
                                        <th>Đáp án đúng</th>
                                        <th>State</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary" name="add-function"><i class="fa fa-save"></i> Save</button>
                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                {{--<button type="button" class="btn btn-primary" name="add-function"><i class="fa fa-save"></i> Save</button>--}}
            </div>
        </div>
    </div>
</div>
