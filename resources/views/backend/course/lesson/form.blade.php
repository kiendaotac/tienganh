<div class="modal fade modal-full" id="modal-add-lesson" role="dialog" style="overflow-y: auto">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button class="close" data-dismiss="modal" type="button">
          ×
        </button>
        <h4 class="modal-title">
          <i class="fa fa-book">
          </i>
          {{ __('admin.lesson.modal_title') }}
        </h4>
      </div>
      <div class="modal-body">
        <form id="f-add" name="form-add-lesson">
          <input name="id" type="hidden" value="0">
          <div class="form-group">
            <label for="name-lesson">
              {{ __('admin.lesson.name') }} <span class="contraint">*</span>
            </label>
            <input class="form-control input-text-lesson" data-error="{{ __('admin.lesson.name_error') }}" id="name-lesson" name="name_lesson" type="text">
          </div>
          <div class="chose-curriculum">
            <div class="row form-group">
              <div class="col-lg-6 div-chose-curriculum">
                <label>
                  {{ __('admin.lesson.curriculum') }} <span class="contraint">*</span>
                </label>
                <div class="chose-curriculum-title">
                  <span id="id-curriculum" value="not">
                    {{ __('admin.lesson.id_curriculum') }}
                  </span>
                  <i class="fa fa-caret-down">
                  </i>
                </div>
                <div class="chose-curriculum-content list-curriculum-content">
                  <div>
                    <input class="form-control" id="search-curriculum" placeholder="{{ __('admin.lesson.search_curriculum_placeholder') }}" type="text">

                  </div>
                  <ul id="list-curriculum">
                    @foreach ($curriculums as $item)
                    <li data-id="{{ $item->id }}">
                      {{ $item->name }}
                    </li>
                    @endforeach
                  </ul>
                </div>
              </div>
              <div class="col-lg-6 div-chose-curriculum">
                <label>
                  {{ __('admin.lesson.chapter') }} <span class="contraint">*</span>
                </label>
                <div class="chose-curriculum-title">
                  <span id="id-lesson" value="0">
                    {{ __('admin.lesson.id_lesson') }}
                  </span>
                  <i class="fa fa-caret-down">
                  </i>
                </div>
                <div class="chose-curriculum-content box-list-curriculum">
                  <span class="list-lesson-notify">
                    {{ __('admin.lesson.list_lesson_notify') }}
                  </span>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-6 form-group">
              <label for="type">
                {{ __('admin.lesson.type') }} <span class="contraint">*</span>
              </label>
              <select class="form-control" id="type" name="type">
                <option value="title">
                  {{ __('admin.lesson.type_title') }}
                </option>
                <option value="lesson">
                  {{ __('admin.lesson.type_lesson') }}
                </option>
                <option value="test">
                  {{ __('admin.lesson.type_test') }}
                </option>
              </select>
            </div>
            <div class="col-lg-6 form-group">
              <div class="row">
                <div class="col-lg-6">
                  <label for="states-lesson">
                    {{ __('admin.states') }} <span class="contraint">*</span>
                  </label>
                  <select class="form-control" id="states-lesson" name="states_lesson">
                    <option value="1">{{ __('admin.states_active') }}</option>
                    <option value="0">{{ __('admin.states_not_active') }}</option>
                  </select>
                </div>
                <div class="col-lg-6">
                  <label for="ordering-lesson">
                    Ordering:
                  </label>
                  <input class="form-control" id="ordering-lesson" name="ordering_lesson" type="text" value="100">
                </div>
              </div>
            </div>
          </div>
          <div class="form-group">
            <label>{{ __('admin.lesson.document') }}</label>
            <ul class="document">
              <div class="add-document">
                <i class="fa fa-plus"></i>
              </div>
            </ul>
          </div>
          <div class="form-group">
            <label for="desc-lesson">
              {{ __('admin.desc') }} <span class="contraint">*</span>
            </label>
            <div class="list-type-content">
              <label class="radio-inline">
                <input checked="" name="type_content" type="radio" value="text">
                Text

              </label>
              <label class="radio-inline">
                <input name="type_content" type="radio" value="video">
                Video
              </label>
              <label class="radio-inline">
                <input name="type_content" type="radio" value="audio">
                Audio
              </label>
            </div>
            <textarea class="form-control input-text-lesson" data-error="{{ __('admin.desc_error') }}" id="desc-lesson" name="desc_lesson" rows="5" value="">
            </textarea>
          </div>
          <div class="media-video link-video-lesson">
            <label>
              {{ __('admin.lesson.link_video_lesson') }}
            </label>
            <div data-id="youtube" disabled="false">
              <i class="fa fa-youtube-square">
              </i>
              <input name="link_video_yt" placeholder="{{ __('admin.lesson.link_video_youtube_placeholder') }}" type="text">
            </div>
            <div class="none" data-id="vimeo">
              <i class="fa fa-vimeo-square">
              </i>
              <input disabled="disabled" name="link_video_vimeo" placeholder="{{ __('admin.lesson.link_video_vimeo_placeholder') }}" type="text">
            </div>
          </div>
          <div class="media-audio link-audio-lesson">
            <label>
              {{ __('admin.lesson.link_audio_lesson') }}
            </label>
            <div>
              <span class="chose-audio">
                <i class="fa fa-volume-up">
                </i>
                {{ __('admin.lesson.chose_audio') }}
              </span>
              <input name="linkaudiolesson" placeholder="{{ __('admin.lesson.link_audio_lesson_input') }}" type="text">

            </div>
          </div>
          <div class="checkbox">
            <label>
              <input name="review" type="checkbox">
              {{ __('admin.lesson.review') }}

            </label>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" id="save-lesson" type="button">
          {{ __('admin.save') }}
        </button>
        <button class="btn btn-default" data-dismiss="modal" type="button">
          {{ __('admin.close') }}
        </button>
      </div>
    </div>
  </div>
</div>
@section('script-lesson')
<script src="js/plugins/dataTables/datatables.min.js">
</script>
<script src="{{ asset('js/plugins/summernote/summernote.min.js') }}">
</script>
<script src="{{ asset('js/plugins/switchery/switchery.min.js') }}">
</script>
<script src="{{ asset('js/plugins/iCheck/icheck.min.js') }}">
</script>
<script src="{{ asset('js/perfect-scrollbar.min.js') }}">
</script>
<script>
  let idLesson = null,
  idQuicktest = null,
  clickEdit = false; // biết khi nào load câu hỏi ra bảng
  /* type lesson change */
  function typeLessonChange(){
    if($('select[name=type]').val() != 'title') {
     $('textarea[name=desc_lesson]').summernote({
      height: 200,
    });
     $('label[for=desc]').text('Nội dung:');
     $('.list-type-content').hide();
     $('.btn-question').remove();
     if($('select[name=type]').val() == 'lesson') {
      $('.list-type-content').show();
    }
    else{
      $('#modal-add-lesson').animate({scrollTop: 150}, 300);
      $('.link-video-lesson, .link-audio-lesson').hide();
      let content = '<div class="btn-question">'
      + '<button type="button" class="btn btn-success">Cập nhật câu hỏi</button>'
      + '</div>';
      $('form[name=form-add-lesson]').append(content);
    }
  }
  else {
    $('textarea[name=desc-lesson]').summernote('destroy');
    $('label[for=desc]').text('Mô tả:');
    $('.list-type-content').hide();
    $('.btn-question').remove();
    $('.link-video-lesson, .link-audio-lesson').hide();
  }
  $('.note-icon-video').closest('button').hide();
}
$(document).on('change', 'select[name=type]', function(){
  typeLessonChange();
});
/* chose type content lesson */
choseTypeContent();
function choseTypeContent(){
  $('input[name=type_content]').each(function(){
    if($(this).is(':checked')) {
     switch($(this).val()) {
       case 'text' : {
        $('.link-video-lesson').hide();
        $('.link-audio-lesson').hide();
        break;
      };
      case 'video' : {
       $('.link-video-lesson').show();
       $('.link-audio-lesson').hide();
       break;
     };
     case 'audio' : {
      $('textarea[name=desc-lesson]').hide();
      $('.link-video-lesson').hide();
      $('.link-audio-lesson').show();
      break;
    }
  }
}
});
}
$(document).on('click', 'input[name=type_content]', function(){
  choseTypeContent();
});
/* get media quicktest */
function getMedia(media_type,auidoname,videoname){
  src = '',
  provider = '',
  media = '';
  if(media_type == 'audio') {
    src = $('input[name='+auidoname+']').val();
  }
  else {
    src = $("."+videoname+" div:not('.none') input").val();
    provider = $("."+videoname+" div:not('.none')").data('id');
  }
  return media = {
    type: media_type,
    src: src,
    provider: provider
  };
}

/* get document */
function getDocument(){
  let documentLesson = [];
  $('.document li').each(function(){
    documentLesson.push($(this).find('a').attr('href'));
  })
  return documentLesson;
}
$(document).on('click', '#save-lesson', function(){
  let url           = 'lesson',
  type              = 'POST',
  id                = 0,
  media             = '',
  typeContentLesson = $('input[name=type_content]:checked').val(),
  typeLesson        = $('select[name=type]').val(),
  media_type        = null;
  if(idLesson != null){
    id   = idLesson;
    url = url + '/' + id;
    type = 'PUT';
  }
  if(checkNotValue('input-text-lesson')) {
    if($('span[id=id-curriculum]').attr('value') === 'not') return swal('Vui lòng chọn khóa học.');
    switch(typeLesson){
      case 'lesson':
      if(typeContentLesson === 'video'){
        if($(".link-video-lesson div:not('.none') input").val() === '') swal('Vui lòng nhập vào id video');
      }
      else if(typeContentLesson === 'audio') {
        if($('.link-audio-lesson input').val() === '') return swal('Vui lòng chọn hoặc nhập vào link audio');
      }
      break;
      case 'test':
      if($('input[name=percent_pass_lesson]').val() == 0) return swal({
        title: "Vui lòng nhập vào % đúng để hoàn thành bài kiểm tra",
        text: "(Bấm vào 'Cập nhật câu hỏi' để nhập)",
      });
        break;
      }
    }
    else return checkNotValue('input-text-lesson');
    switch(typeLesson){
      case 'test':
      media_type = $('input[name=type_quicktest]:checked').val();
      media = getMedia(media_type,'linkaudio','link-video-quicktest');
      break;
      case 'lesson':
      media_type = $('input[name=type_content]:checked').val();
      if(media_type != 'text') media = getMedia(media_type,'linkaudiolesson','link-video-lesson');
      break;
    }
    let documentLesson = getDocument();
    $(this).append('<i class="fa fa-refresh load" style="margin-left: 5px"></i>');

    $.ajax({
      url: url,
      type: type,
      dataType: 'JSON',
      data: {
        /* data lesson */
        name: $('input[name=name_lesson]').val(),
        curriculum_id: ($('#id-curriculum').attr('value') !== 'not') ? $('#id-curriculum').attr('value') : null,
        parent_id: $('#id-lesson').attr('value'),
        type: $('select[name=type]').val(),
        ordering: $('input[name=ordering_lesson]').val(),
        review: ($('input[name=review]').is(':checked')) ? 1 : 0,
        desc: $('textarea[name=desc_lesson]').val(),
        type_content: $('input[name=type_content]:checked').val(),
        document: JSON.stringify(documentLesson),
        states: $('select[name=states_lesson]').val(),
        /* data quicktest */
        media_type: media_type,
        media: JSON.stringify(media),
        percent_pass: $('input[name=percent_pass_lesson]').val(),
        show_hint: ($('input[name=show_hint]').is(':checked')) ? 1 : 0,
        review_question: ($('input[name=review_question]').is(':checked')) ? 1 : 0,
        showcorrect_answer: ($('input[name=showcorrect_answer]').is(':checked')) ? 1 : 0,
        showcheck_answer: ($('input[name=showcheck_answer]').is(':checked')) ? 1 : 0,
        typeLesson: typeLesson,
        id_quicktest: idQuicktest,
      },
      success: function(data){
        $('#save-lesson i').remove();
        let value = JSON.parse(JSON.stringify(data));
        notification(value.type, value.title, value.content);
        if(value.type === 'success') {
          if(mainUrl != 'curriculum') Table.ajax.reload();
          else loadLesson();
          $('#modal-add-lesson').modal('hide');
          $('#modal-add-test').modal('hide');
        }
      }
    });
  });
/* show modal add question */
$(document).on('click', '.btn-question button', function(){
  $('#modal-add-lesson').modal('hide');
  $('#modal-add-test').modal('show');
  $('.dataTables-question tbody').html('');
  $('.link-audio').show();
  $('body').css('overflow-y', 'hidden');
  if(clickEdit) {
    ajaxLoadQuestion();
  }
});
/* ajax load question */
function ajaxLoadQuestion(){
  $('.dataTables-question tbody').append('<tr class="tr-load"><td colspan="5"><i class="fa fa-refresh load"></i></td></tr>')
  $.ajax({
    url: 'lesson/getQuestion',
    type: 'GET',
    dataType: 'JSON',
    data: {
      quicktest_id: idQuicktest,
    },
    success: function(data){
      let value = JSON.parse(JSON.stringify(data));
      $('.dataTables-question tbody tr.tr-load').remove();
      clickEdit = false;
      $.each(value, function(index, item){
        createQuestionTable(item);
      });
    }
  });
} 
/* datatable list question */
let tableQuestion = $('.dataTables-question').DataTable({
  bSort: false,
  bInfo: false,
  responsive: true,
  language: {
    "lengthMenu": "Hiển thị _MENU_ bản ghi trên 1 trang",
    "zeroRecords": "Không có dữ liệu",
    "info": "Hiển thị trang _PAGE_ trong tổng số _PAGES_ trang",
    "infoEmpty": "Không có dữ liệu tồn tại",
    "infoFiltered": "(Lọc từ _MAX_ bản ghi)",
    "search": "Tìm kiếm:",
    "paginate": {
      "first":      "Trang đầu",
      "last":       "Trang cuối",
      "next":       "Trang tiếp theo",
      "previous":   "Trang trước"
    },
  },
});
/* short content */
$('.short-content').each(function(){
  let value = $(this).text();
  if(value.length > 100) {
    value = value.substr(0, 100) + '...';
    $(this).text(value);
  }
});
/* show modal detail test */
$(document).on('click', 'button[name=btn-view]', function(){
  let value = $(this).data('question'),
  options = JSON.parse(value.options);
  $('.detail-question-title').html(value.question);
  if(value.hint == null) $('#tab-1 .panel-body').text('Không có gợi ý');
  else $('#tab-1 .panel-body').text(value.hint);
  if(value.explain == null) $('#tab-2 .panel-body').text('Không có giải thích');
  else $('#tab-2 .panel-body').text(value.explain);
  $('.detail-question-option > .row').html('');
  $.each(options, function(index, item){
    let content = '<div class="col-lg-12">'
    + '<label><input type="radio" name="answer"><span class="option">'+item+'</span></label>'
    + '</div>';
    $('.detail-question-option > .row').append(content);
  });
  /* i-check */
  $('input[name=answer]').iCheck({
    radioClass: 'iradio_square-green',
  });
  $('.detail-question-option label').each(function(){
    if($(this).find('span.option').text() == value.correct) {
      $(this).find('.iradio_square-green').addClass('checked');
    }
  });
  $('#modal-detail-test').modal('show');
});
/* edit question */
$(document).on('click','button[name=btn-edit-question]',function(){
  let data = $(this).data('question'),
  options = JSON.parse(data.options);
  $('input[name=id_question]').val(data.id);
  $('#modal-add-test .note-editable').html(data.question);
  $.each(data, function(index,item){
    $('textarea[name='+index+']').val(item);
  });
  $('.list-options > .row').html('');
  let i = 0,
  countImg = 0,
  arrImg = ['png','gif','jpg','jpeg'];
  $.each(options, function(index,item){
    let op = item.substr(item.lastIndexOf('.')+1).toLowerCase();
    if(arrImg.indexOf(op) >= 0) countImg++;
    let content = '<div class="col-lg-6">' +
    createOption(index,item) +
    '</div>';
    $('.list-options > .row').append(content);
    if(item == data.correct) indexElement = i;
    i++;
  });
  $('input[name=correct]').iCheck({
    radioClass: 'iradio_square-green',
  });
  $('.list-options > .row > div:eq('+indexElement+')').find('.iradio_square-green').addClass('checked');
  $('.list-options > .row > div:eq('+indexElement+')').find('input').prop('checked','checked');
  let addOptionItem = '<div class="col-lg-6">'
  +'<div class="add-option-item">'
  + '<i class="fa fa-plus"></i>'
  + '</div>'
  + '</div>';
  $('.list-options > div.row').append(addOptionItem);
  if(countImg == i) $('.list-type-option input[value=image]').prop('checked','checked');
});
  // input switcher
  let nameSwitchLesson = [];
  let nameSwitchCurriculum = [];
  createSwitchery('js-switch',nameSwitchCurriculum);
  createSwitchery('js-switch-lesson',nameSwitchLesson);
  function createSwitchery(nameClass,nameSwitch){
    let i = 0;
    $('.'+nameClass).each(function(){
      nameSwitch[i] = Switchery($(this)[0], { color: '#1AB394', size: 'small' });
      i++;
    });
  }
  /* i-check */
  $('input[name=correct]').iCheck({
    radioClass: 'iradio_square-green',
  });
  /* hide div class ui-helper-hidden-accessible */
  $(document).on('hover', '.note-btn', function(){
    $('.ui-helper-hidden-accessible').remove();
  })
  $(document).on('click', '.note-btn', function(){
    $('.ui-helper-hidden-accessible').remove();
  });
  /* quicktest type content */
  $(document).on('click', 'input[name=type_quicktest]', function(){
    let value = $(this).val();
    switch(value){
      case 'audio' :{
        $('.link-audio-quicktest').show();
        $('.link-video-quicktest').hide();
        break;
      };
      case 'video' :{
        $('.link-audio-quicktest').hide();
        $('.link-video-quicktest').show();
        break;
      }
    }
  });
  let indexElement = null; // save index correct option click btn-edit-question
  /* add template options test */
  $(document).on('click', '.add-option-item i', function(){
    let size = $('.option-item').length;
    let letter = $('.option-item:eq('+(size-1)+') > div').text();
    let letterNext = '';
    if(size == 0) letterNext = 'A';
    else letterNext = String.fromCharCode(letter.charCodeAt(0) + 1);
    let content = createOption(letterNext,'');
    let addOptionItem = '<div class="col-lg-6">'
    +'<div class="add-option-item">'
    + '<i class="fa fa-plus"></i>'
    + '</div>'
    + '</div>';
    $(this).closest('.col-lg-6').html(content);
    $('.list-options > div.row').append(addOptionItem);
    $('input[name=correct]').iCheck({
      radioClass: 'iradio_square-green',
    });
    settingOption($('input[name=type_option]:checked').val());
    if(letterNext == 'Z') $('.add-option-item').closest('.col-lg-6').remove();
    if(indexElement != null) $('.list-options > .row > div:eq('+indexElement+')').find('.iradio_square-green').addClass('checked');
  });
  /* create div options quicktest */
  function createOption(letterNext,value){
    let content = '<div class="option-item">'
    + '<div class="option-name">'
    + letterNext
    + '</div>'
    + '<input type="text" name="option" class="input-text" data-error="Vui lòng nhập đầy đủ các phương án đã tạo." value="'+value+'">'
    + '<div class="correct"><input type="radio" name="correct"></div>'
    + '<i class="fa fa-times destroy-option"></i>'
    + '</div>';
    return content;
  }
  /* type option change */
  function settingOption(val){
    if(val == 'image') {
      $('input[name=option]').attr('readonly', 'readonly');
      $('input[name=option]').attr('placeholder', 'Nhấn vào đây để chọn ảnh');
    }
    else {
      $('input[name=option]').removeAttr('readonly');
      $('input[name=option]').removeAttr('placeholder');
    }
  }
  $(document).on('change', 'input[name=type_option]', function(){
    settingOption($(this).val());
  });
  /* shose image option */
  $(document).on('click', 'input[name=option]', function(){
    if($('input[name=type_option]:checked').val() === 'image') {
      nameClass = $(this);
      $('#module-file').modal('show');
    }
  });
  /* view image option */
  let time = 0;
  let optionInterval = null;
  $(document).on('mouseover', 'input[name=option]', function(e){
    if($('input[name=type_option]:checked').val() === 'image' && $(this).val() !== '') {
      let x = e.clientX;
      let y = e.clientY;
      let content = '<div class="box-image-option" style="left:'+x+'px; top:'+y+'px">'
      + '<img src="'+$(this).val()+'" alt="hình ảnh phương án"/>'
      + '</div>';
      optionInterval = setInterval(function(){
        time++;
        if(time == 2){
          $('body').append(content);
          time = 0;
          clearInterval(optionInterval);
        }
      }, 1000);
    }
  });
  $(document).on('mouseout', 'input[name=option]', function(){
    if(time < 2) {
      clearInterval(optionInterval);
      time = 0;
    }
    if($('input[name=type_option]:checked').val() === 'image' && $(this).val() !== '') {
      $('.box-image-option').remove();
    }
  });
  /* destroy option item */
  $(document).on('click', '.destroy-option', function(){
    $(this).closest('.col-lg-6').remove();
    let letter = 'A';
    $('.option-name').each(function(){
      $(this).text(letter);
      letter = String.fromCharCode(letter.charCodeAt(0) + 1);
    });
  });
  $(document).on('click', '#modal-add-test button[data-dismiss=modal]', function(){
    $('#modal-add-lesson').modal('show');
  });
  /* input link video */  
  $(document).on('click', '.media-video div.none', function(){
    let divVideo = $(this).closest('.media-video').find('div:not(.none)');
    if(divVideo.find('input').val() == '') {
      $(this).find('input').removeAttr('disabled');
      $(this).find('input').trigger('focus');
      $('.media-video div:not(.none) input').attr('disabled','disabled');
      $(this).removeAttr('class');
      divVideo.attr('class','none');
    }
    else{
      swal('Chỉ được nhập 1 loại id video.');
    }
  });
  /* add question */
  function createQuestionTable(value){
    let hint = value.hint,
    explain = value.explain;
    if(hint == null) hint = 'Không có gợi ý';
    if(explain == null) explain = 'Không có giải thích';
    let questionTable = '<tr>'
    + '<td class="short-content">'+value.question+'</td>'
    + '<td class="short-content">'+value.correct+'</td>'
    + '<td class="short-content">'+hint+'</td>'
    + '<td class="short-content">'+explain+'</td>'
    + '<td class="text-center"><button style="margin-right: 5px" type="button" name="btn-view" class="btn btn-success btn-flat btn-xs" title="Xem" data-question=\''+JSON.stringify(value)+'\'><i class="fa fa-eye"></i></button><button style="margin-right: 5px" type="button" name="btn-edit-question" class="btn btn-primary btn-flat btn-xs" title="Sửa" data-question=\''+JSON.stringify(value)+'\'><i class="fa fa-pencil"></i></button><button type="button" name="btn-delete-question" class="btn btn-danger btn-flat btn-xs" title="Xóa" data-id='+value.id+'><i class="fa fa-times"></i></button></td>'
    + '</tr>'
    $('.dataTables-question tbody').append(questionTable);
  }
  $('textarea[name=question]').summernote({
    height: 100,
  });
  /* reset form */
  let checkResetForm = false;
  $(document).on('click', '#refresh-form', function(){
    checkResetForm = true;
    addQuestion($(this));
  });
  /* delete question */
  $(document).on('click', 'button[name=btn-delete-question]', function(){
    let id = $(this).data('id'),
    button = $(this);
    deleteLesson('question', button, id, 'câu hỏi');
  });
  function deleteLesson(type,button,id,title){
    swal({
      title: "Bạn có muốn xóa "+title+" này không??",
      text: "Khi xóa Bài học sẽ không thể khôi phục lại",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "OK",
      cancelButtonText: "Không",
      closeOnConfirm: true,
      closeOnCancel: true
    }, function(config){
      if(config) {
        button.find('i').removeClass('fa-times');
        button.find('i').addClass('fa-refresh load');
        $.ajax({
          url: 'lesson/' + id,
          type: 'DELETE',
          dataType: 'JSON',
          data: {
            type: type
          },
          success: function(data){
            let value = JSON.parse(JSON.stringify(data));
            notification(value.type, value.title, value.content);
            if(value.type == 'success') {
              button.closest('tr').remove();
            }
          }
        });
      }
    });
  }
  /* perfect scroll */
  new PerfectScrollbar($('.modal-full .modal-body')[0]);
  /* add question */
  $(document).on('click', '#save-question', function(){
    addQuestion($(this));
  });
  function addQuestion(obj){
    if(checkNotValue('input-text')) {
      if(!$('input[name=correct]:checked')[0]) {
        checkResetForm = false;
        swal('Vui lòng chọn phương áp đúng.');
      }
      else {
        obj.append('<i class="fa fa-refresh load" style="margin-left: 5px"></i>');
        let option = '';
        $('.option-item').each(function(){
          option += '"'+$(this).find('.option-name').text()+'"'+':'+'"'+$(this).find('input[name=option]').val()+'"'+ ',';
        });
        option = option.substr(0, option.lastIndexOf(','));
        option = '{' + option + '}';
        if($('input[name=id_question]').val() == 0) {
          if(idLesson == null) {
            let media_type = $('input[name=type_quicktest]:checked').val(),
            media = getMedia(media_type,'linkaudio','link-video-quicktest');
            let documentLesson = getDocument();
            $.ajax({
              url: 'lesson',
              type: 'POST',
              dataType: 'JSON',
              data: {
                /* data lesson */
                name: $('input[name=name_lesson]').val(),
                curiculum_id: ($('#id-curriculum').attr('value') != 'not') ? $('#id-curriculum').attr('value') : null,
                parent_id: $('#id-lesson').attr('value'),
                type: $('select[name=type]').val(),
                ordering: $('input[name=ordering_lesson]').val(),
                review: ($('input[name=review]').is(':checked')) ? 1 : 0,
                states: 0,
                document: JSON.stringify(documentLesson),
                /* data quicktest */
                media_type: media_type,
                media: JSON.stringify(media),
                percent_pass: $('input[name=percent_pass_lesson]').val(),
                show_hint: ($('input[name=show_hint]').is(':checked')) ? 1 : 0,
                review_question: ($('input[name=review_question]').is(':checked')) ? 1 : 0,
                showcorrect_answer: ($('input[name=showcorrect_answer]').is(':checked')) ? 1 : 0,
                showcheck_answer: ($('input[name=showcheck_answer]').is(':checked')) ? 1 : 0,
                /* data question */
                question: $('textarea[name=question]').val(),
                options: option,
                correct: $('input[name=correct]:checked').closest('.option-item').find('input[name=option]').val(),
                explain: $('textarea[name=explain]').val(),
                hint: $('textarea[name=hint]').val(),
                check: 'lesson'
              },
              success: function(data){
                obj.find('i').remove();
                let value = JSON.parse(JSON.stringify(data));
                notification(value.type, value.title, value.content);
                if(value.type === 'success') {
                  createQuestionTable(value.lessonQuicktestQuestion);
                  $('.dataTables_empty').hide();
                  idLesson = value.lesson.id;
                  idQuicktest = value.lessonQuicktest.id;
                  Table.ajax.reload();
                  if(checkResetForm) {
                    resetFormQuestion();
                    checkResetForm = false;
                  }
                }
              }
            });
          }
          else {
            $.ajax({
              url: 'lesson',
              type: 'POST',
              dataType: 'JSON',
              data: {
                quicktest_id: idQuicktest,
                question: $('textarea[name=question]').val(),
                options: option,
                correct: $('input[name=correct]:checked').closest('.option-item').find('input[name=option]').val(),
                explain: $('textarea[name=explain]').val(),
                hint: $('textarea[name=hint]').val(),
                check: 'question'
              },
              success: function(data){
                obj.find('i').remove();
                let value = JSON.parse(JSON.stringify(data));
                notification(value.type, value.title, value.content);
                if(value.type === 'success') {
                  createQuestionTable(value.lessonQuicktestQuestion);
                  if(checkResetForm) {
                    resetFormQuestion();
                    checkResetForm = false;
                  }
                }
              }
            });
          }
        }
        else {
          $.ajax({
            url: 'quicktest-question/'+$('input[name=id_question]').val(),
            type: 'PUT',
            dataType: 'JSON',
            data: { 
              question: $('textarea[name=question]').val(),
              options: option,
              correct: $('input[name=correct]:checked').closest('.option-item').find('input[name=option]').val(),
              explain: $('textarea[name=explain]').val(),
              hint: $('textarea[name=hint]').val(),
            },
            success: function(data){
              obj.find('i').remove();
              let value = JSON.parse(JSON.stringify(data));
              notification(value.type, value.title, value.content);
              if(value.type == 'success') {
                if(checkResetForm) {
                  resetFormQuestion();
                  checkResetForm = false;
                }
                $('.dataTables-question tbody').html('');
                ajaxLoadQuestion();
              }
            }
          })
        }
      }
    }
    else {
      checkResetForm = false;
      return checkNotValue('input-text');
    }
  }
  /* reset default form add question */
  function resetFormQuestion(){
    $('form[name=form-add-test]').trigger('reset');
    $('textarea[name=question]').summernote('reset');
    $('input[name=id_question]').val(0);
    $('.option-item').closest('.col-lg-6').remove();
    $('.add-option-item i').trigger('click');
    for (let i = 0; i < 2; i++) {
      $('.add-option-item i').trigger('click');
    }
    $('.option-item iradio_square-green').removeClass('checked');
  }
</script>
@stop
