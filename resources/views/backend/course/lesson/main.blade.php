@extends('layouts.app')
@section('title')
<title>{{$currentFunction->name}} - {{\Illuminate\Support\Facades\Crypt::decryptString(config('custom.app_name'))}}</title>
@endsection
@section('style')
<link href="css/plugins/dataTables/datatables.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/plugins/summernote/summernote.css">
<link rel="stylesheet" type="text/css" href="{{ asset('css/perfect-scrollbar.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/plugins/switchery/switchery.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/plugins/iCheck/custom.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/custom.css') }}">
<style>
.document{
  border: #ddd solid 1px;
  padding-top: 10px;
  padding-bottom: 10px;
  max-height: 150px;
  overflow-y: auto;
}
.document li{
  margin-bottom: 10px;
  list-style: circle;
}
.document li span{
  position: relative;
  display: flex;
  align-items: center;
}
.document li:last-child{
  border-bottom: 0;
}
.destroy-document{
  position: static;
  margin-left: 10px;
}
.document li:hover .destroy-document{
  display: block;
}
.add-document i{
  width: 30px;
  height: 30px;
  line-height: 30px;
  font-size: 14px;
}
.modal-title i{
	margin-right: 10px;
}
li{
	list-style: none;
}
.curriculum-content > div{
	padding: 10px 20px 10px 20px;
	display: flex;
	justify-content: center;
}
.add-test-box-title{
	position: absolute;
	top: -12px;
	left: 5px;
	background: #fff;
	padding-left: 5px;
	padding-right: 5px;
	font-weight: bold;
}
.symbol{
	position: absolute;
	right: 7%;
	height: 20px;
	top: 0;
	bottom: 0;
	margin: auto;
}
.question-content{
	margin-top: 0 !important;
}
select[name=correct]{
	width: 20%;
}
input[name=answer]{
	margin-right: 5px;
}
.detail-question-option span{
	font-weight: 500;
}
.detail-question-option span{
	margin-left: 5px;
}
.dataTables-question tbody td img, .detail-question-title img{
	max-width: 250px !important;
	max-height: 150px !important;
}
.dataTables-question tbody tr.odd{
	display: none;
}
</style>
@endsection
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-10">
		<h2>{{ __('admin.title_page') }} {{mb_strtolower($currentFunction->name)}}</h2>
	</div>
	<div class="col-lg-2">

	</div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox float-e-margins">
				<div class="ibox-content">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover dataTables-curriculums">
							<thead>
								<tr>
									<th width="50">{{ __('admin.table_stt') }}</th>
									<th>{{ __('admin.lesson.table_name') }}</th>
									<th>{{ __('admin.lesson.table_curriculum') }}</th>
									<th width="150">{{ __('admin.lesson.table_type') }}</th>
									<th width="70">Ordering</th>
									<th>{{ __('admin.table_state') }}</th>
									<th>{{ __('admin.table_action') }}</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Modal add new function -->
@include('backend.course.lesson.form')
@include('backend.course.lesson.modal-test')
@include('backend.course.lesson.modal-detail-test')
@include('backend.system.file.module')
@endsection
@section('script')
<script src={{ asset('js/custom.js') }}></script>
<script>
	/* datatable curriculum */
	let mainUrl = '{{$currentFunction->route}}';
	let Table = $('.dataTables-curriculums').DataTable({
		bSort: false,
		bInfo: true,
		bLengthChange: false,
		processing: true,
		serverSide: true,
		searching: true,
		paging: true,
		pageLength: 15,
		responsive: true,
		ajax: mainUrl + '/getDatatable',
		dom: '<"row"<"col-sm-6"Bi><"col-sm-6"f>>rtp',
		buttons: {
			buttons: [
			{
				text: 'Thêm mới',
				action: function ( e, dt, node, config ) {
					resetForm();
					$('#modal-add-lesson').modal({backdrop: 'static', keyboard: false, show : true});
				}
			}
			],
			dom: {
				button: {
					tag: "button",
					className: "btn btn-primary"
				},
				buttonLiner: {
					tag: null
				}
			}
		},
		language: {
			"lengthMenu": "Hiển thị _MENU_ bản ghi trên 1 trang",
			"zeroRecords": "Không có dữ liệu",
			"info": "Hiển thị trang _PAGE_ trong tổng số _PAGES_ trang",
			"infoEmpty": "Không có dữ liệu tồn tại",
			"infoFiltered": "(Lọc từ _MAX_ bản ghi)",
			"search": "Tìm kiếm:",
			"paginate": {
				"first":      "Trang đầu",
				"last":       "Trang cuối",
				"next":       "Trang tiếp theo",
				"previous":   "Trang trước"
			},
		},
		columns     :   [
		{
			data    :   'id',
			className:  'text-center',
			render  :   function (data, type, row, meta) {
				return meta.row +1;
			}
		},
		{
			data    :   'name',
			render  : function (data, type, row, meta) {
				if (row.parent_id == 0){
					return '<b>'+data+'</b>';
				} else {
					return '&emsp;'+data;
				}
			}
		},
		{
			data    :   'curriculum',
		},
		{
			data    :   'type',
			className: "text-center",
			render  :   function (data, type, row, meta) {
				switch(data){
					case 'title':
					return 'Chương';
					break;
					case 'lesson':
					return 'Bài học';
					break;
					case 'test':
					return 'Bài kiểm tra';
					break;
				}
			}
		},
		// {   
		// 	data    :   'desc',
		// 	render  :   function (data, type, row, meta) {
		// 		var e = document.createElement('div');
		// 		$(e).html(data);
		// 		return ($(e)[0].childNodes[0]) ? $(e)[0].childNodes[0].nodeValue : '';
		// 	}
		// },
		{
			data    :   'ordering',
			className: 'text-center'
		},
		{
			data    :   'states',
			className: 'text-center',
			render  :   function (data, type, row, meta) {
				if(data == 0) return 'Không kích hoạt';
				if(data == 2) return 'Đang duyệt';
				return 'Kích hoạt';
			}
		},
		{
			data    :   'id',
			className: 'text-center',
			render  :   function (data, type, row, meta) {
				let button  =   '@permission("Edit-15")<button name="btn-edit" class="btn btn-primary btn-flat btn-xs" title="Sửa" data-data=\''+JSON.stringify(row)+'\'><i class="fa fa-edit"></i></button>@endpermission @permission("Delete-15")<button name="btn-delete" class="btn btn-remove btn-danger btn-flat btn-xs" title="Xóa" data-id='+data+'><i class="fa fa-remove"></i></button>@endpermission';
				return button;
			}
		}
		]
	});
	$('.link-video-quicktest').hide();
	/* perfect scroll */
	new PerfectScrollbar($('.table-responsive')[0]); 
	new PerfectScrollbar($('.modal-full .modal-body')[0]);
	new PerfectScrollbar($('#modal-add-lesson')[0]);
	new PerfectScrollbar($('#modal-add-test')[0]);
	new PerfectScrollbar($('#modal-detail-test')[0]);
	/* reset form */
	function resetForm() {
		$('select[name=type] option').each(function(){
			$(this).removeAttr('selected');
		})
		$('select[name=type]').removeAttr('disabled');
		$('select[name=type] option[value=title]').attr('selected','selected');
		$('input[name=id]').val(0);
		$('form[name=form-add-lesson], form[name=form-add-test]').trigger('reset');
		$('#id-curriculum').text('-- Chọn khóa học --');
		$('#id-curriculum').attr('value', 'not');
		$('#id-lesson').text('-- Không thuộc chương nào --');
		$('#id-lesson').attr('value', 0);
		$('textarea[name=desc_lesson]').summernote('destroy');
		$('textarea[name=desc_lesson]').val('');
		$('label[for=desc]').text('Mô tả:');
		$('.link-video-quicktest, .list-type-content, .link-audio-lesson').hide();
		$('.link-audio-quicktest').show();
		$('.btn-question').remove();
		resetSwitchery(nameSwitchLesson);
		typeLessonChange();
		idLesson = null;
		idParent = 0;
	}
	/* edit */
	$(document).on('click', 'button[name=btn-edit]', function(){
		$('textarea[name=desc_lesson]').summernote('destroy');
		let value = $(this).data('data');
		setValueForm(value);
	});
	/* hide input video... summernote question */
	let summerElement = $('textarea[name=question]').closest('.form-group').find('.note-editor');
	summerElement.find('.note-icon-link').closest('button').hide();
	summerElement.find('.note-icon-video').closest('button').hide();
	summerElement.find('.note-icon-question').closest('button').hide();
	/* delete lesson */
	$(document).on('click', 'button[name=btn-delete]', function(){
		let id = $(this).data('id'),
		button = $(this);
		deleteLesson('lesson', button, id, 'bài học');
	});
</script>
@endsection
