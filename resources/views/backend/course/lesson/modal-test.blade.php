
<div id="modal-add-test" class="modal fade modal-full" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><i class="fa fa-clock-o"></i>{{ __('admin.lesson.modal_test_title') }}</h4>
      </div>
      <div class="modal-body">
        <form name="form-add-test">
          <div class="row">
            <div class="col-lg-8">
              <div class="question box-content">
                <div class="form-group">
                  <input type="hidden" name="id_question" value="0">
                  <label for="question">{{ __('admin.lesson.question') }} <span class="contraint">*</span></label>
                  <textarea class="form-control input-text" id="question" name="question" data-error="{{ __('admin.lesson.question_error') }}"></textarea>
                </div>
                <div class="box-content test-options" style="border: none; border-top: #ddd solid 1px">
                  <div class="list-type-option">
                    <label class="radio-inline"><input type="radio" name="type_option" value="text" checked>{{ __('admin.lesson.type_option_text') }}</label>
                    <label class="radio-inline"><input type="radio" name="type_option" value="image">{{ __('admin.lesson.type_option_image') }}</label>
                  </div>
                  <div class="list-options">
                    <div class="row">
                      <div class="col-lg-6">
                        <div class="option-item">
                          <div class="option-name">A</div>
                          <input type="text" name="option" class="input-text" data-error="{{ __('admin.lesson.option_error') }}">
                          <div class="correct"><input type="radio" name="correct"></div>
                          <i class="fa fa-times destroy-option"></i>
                        </div>
                      </div>
                      <div class="col-lg-6">
                        <div class="option-item">
                          <div class="option-name">B</div>
                          <input type="text" name="option" class="input-text" data-error="{{ __('admin.lesson.option_error') }}">
                          <div class="correct"><input type="radio" name="correct"></div>
                          <i class="fa fa-times destroy-option"></i>
                        </div>
                      </div>
                      <div class="col-lg-6">
                        <div class="option-item">
                          <div class="option-name">C</div>
                          <input type="text" name="option" class="input-text" data-error="{{ __('admin.lesson.option_error') }}">
                          <div class="correct"><input type="radio" name="correct"></div>
                          <i class="fa fa-times destroy-option"></i>
                        </div>
                      </div>
                      <div class="col-lg-6">
                        <div class="add-option-item">
                          <i class="fa fa-plus" title="{{ __('admin.lesson.add_option_item_title') }}"></i>
                        </div>
                      </div>
                    </div>  
                  </div>
                  <span class="box-content-title" style="padding-left: 0; left: 0">{{ __('admin.lesson.modal_test_box_title_1') }}</span>
                </div>
                <div class="test-additional">
                  <div class="form-group">
                    <label for="hint">{{ __('admin.lesson.hint') }}</label>
                    <textarea class="form-control" name="hint" id="hint"></textarea>
                  </div>
                  <div class="form-group">
                    <label for="explain">{{ __('admin.lesson.explain') }}</label>
                    <textarea class="form-control" name="explain" id="explain"></textarea>
                  </div>
                  <span class="box-content-title" style="padding-left: 0; left: 0">{{ __('admin.lesson.modal_test_box_title_2') }}</span>
                </div>
                <div class="box-btn-question">
                  <button type="button" class="btn btn-primary" id="save-question">{{ __('admin.lesson.save_question') }}</button>
                  <button type="button" class="btn btn-default" id="refresh-form">{{ __('admin.lesson.save_question_reset') }}</button>
                </div>
                <span class="box-content-title">
                  {{ __('admin.lesson.modal_test_box_title_3') }}
                </span>
              </div>
            </div>
            <div class="col-lg-4">
              <div class="media box-content">
                <div class="quicktest-type-content">
                  <label class="radio-inline"><input type="radio" name="type_quicktest" value="audio" checked>Audio</label>
                  <label class="radio-inline"><input type="radio" name="type_quicktest" value="video">Video</label>
                </div>
                <div class="media-audio link-audio-quicktest">
                  <label>Audio: </label>
                  <div>
                    <span class="chose-audio">
                      <i class="fa fa-volume-up"></i>
                      {{ __('admin.lesson.chose_audio') }}
                    </span>
                    <input type="text" name="linkaudio" placeholder="{{ __('admin.lesson.link_audio_lesson_input') }}">
                  </div>
                </div>
                <div class="media-video link-video-quicktest">
                  <label>Video: </label>
                  <div disabled="false" data-id="youtube">
                    <i class="fa fa-youtube-square"></i>
                    <input type="text" name="linkvideoyt" placeholder="{{ __('admin.lesson.link_video_youtube_placeholder') }}">
                  </div>
                  <div class="none" data-id="vimeo">
                    <i class="fa fa-vimeo-square"></i>
                    <input type="text" name="linkvideovimeo" placeholder="{{ __('admin.lesson.link_video_vimeo_placeholder') }}" disabled="disabled">
                  </div>
                </div>
                <span class="box-content-title">
                  Media
                </span>
              </div>
              <div class="test-custom box-content">
                <div class="test-custom-item">
                  <span class="test-custom-item-title">{{ __('admin.lesson.show_hint') }}</span>
                  <div><input type="checkbox" name="show_hint" class="js-switch-lesson"></div>
                </div>
                <div class="test-custom-item">
                  <span class="test-custom-item-title">{{ __('admin.lesson.review_question') }}</span>
                  <div><input type="checkbox" name="review_question" class="js-switch-lesson"></div>
                </div>
                <div class="test-custom-item">
                  <span class="test-custom-item-title">{{ __('admin.lesson.showcorrect_answer') }}</span>
                  <div><input type="checkbox" name="showcorrect_answer" class="js-switch-lesson"></div>
                </div>
                <div class="test-custom-item">
                  <span class="test-custom-item-title">{{ __('admin.lesson.showcheck_answer') }}</span>
                  <div><input type="checkbox" name="showcheck_answer" class="js-switch-lesson"></div>
                </div>
                <div class="test-custom-item test-custom-item-input">
                  <span class="test-custom-item-title">{{ __('admin.lesson.percent_pass_lesson') }} <span class="contraint">*</span></span>
                  <div>
                    <input type="number" name="percent_pass_lesson" value="0">
                    <span class="symbol">%</span>
                  </div>
                </div>
                <span class="box-content-title">
                  {{ __('admin.lesson.modal_test_box_title_4') }}
                </span>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-12">
              <div class="question box-content">
                <div class="table-responsive">
                  <table class="table table-striped table-bordered table-hover dataTables-question">
                    <thead>
                      <tr>
                        <th>{{ __('admin.lesson.table_question') }}</th>
                        <th>{{ __('admin.lesson.table_option') }}</th>
                        <th>{{ __('admin.lesson.table_hint') }}</th>
                        <th>{{ __('admin.lesson.table_explain') }}</th>
                        <th width="100"></th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
                </div>
                <span class="box-content-title">
                  {{ __('admin.lesson.modal_test_box_title_5') }}
                </span>
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" id="save-lesson" type="button">{{ __('admin.lesson.save_quicktest') }}</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('admin.close') }}</button>
      </div>
    </div>
  </div>
</div>