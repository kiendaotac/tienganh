
<div id="modal-detail-test" class="modal fade" role="dialog" style="overflow-y: auto">
  <div class="modal-dialog modal-lg" style="width: 50%">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><i class="fa fa-clock-o"></i>{{ __('admin.lesson.modal_detail_title') }}</h4>
      </div>
      <div class="modal-body">
        <div class="detail-question-title"></div>
        <div class="detail-question-option">
          <div class="row">
          </div>
        </div>
        <div class="tabs-container">
          <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#tab-1">{{ __('admin.lesson.tab_hint') }}</a></li>
            <li class=""><a data-toggle="tab" href="#tab-2">{{ __('admin.lesson.tab_explant') }}</a></li>
          </ul>
          <div class="tab-content">
            <div id="tab-1" class="tab-pane active">
              <div class="panel-body"></div>
            </div>
            <div id="tab-2" class="tab-pane">
              <div class="panel-body"></div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('admin.close') }}</button>
      </div>
    </div>
  </div>
</div>