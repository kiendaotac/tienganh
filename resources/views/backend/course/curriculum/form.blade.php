
<div id="modal-add-curriculum" class="modal fade modal-full" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><i class="fa fa-book"></i>{{ __('admin.curriculum.modal_title') }}</h4>
      </div>
      <div class="modal-body">
        <form id="f-add" name="form-add-curriculum">
          <div class="row">
            <div class="col-lg-8 col-xs-12 col-sm-8">
              <div class="add-info-basic box-content">
                <input type="hidden" name="id" value="0">
                <input type="hidden" name="img" value="">
                <div class="form-group">
                  <label for="name">{{ __('admin.curriculum.name') }} <span class="contraint">*</span></label>
                  <input type="text" class="form-control input-text-curriculum" id="name" name="name" value="" data-error="{{ __('admin.curriculum.name_error') }}">
                </div>
                <div class="row">
                  <div class="col-lg-4">
                    <div class="form-group">
                      <label for="group">{{ __('admin.curriculum.group') }} <span class="contraint">*</span></label>
                      <select class="form-control" id="group" name="cate_id">
                        @foreach ($categories as $category)
                        @if ($category->parent_id == 0)
                        <option value="{{ $category->id }}" style="font-weight: bold">{{ $category->name }}</option>
                        @else <option value="{{ $category->id }}">&emsp;{{ $category->name }}</option>
                        @endif
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="col-lg-3">
                    <div class="form-group">
                      <label for="states">{{ __('admin.states') }} <span class="contraint">*</span></label>
                      <select class="form-control" id="states" name="states">
                        <option value="2" selected="">{{ __('admin.states_wait') }}</option>
                        <option value="1">{{ __('admin.states_active') }}</option>
                        <option value="0">{{ __('admin.states_not_active') }}</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-lg-2">
                    <div class="form-group">
                      <label for="ordering">Ordering:</label>
                      <input type="number" class="form-control" id="ordering" name="ordering" value="100">
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="desc">{{ __('admin.desc') }} <span class="contraint">*</span></label>
                  <textarea class="form-control input-text-curriculum" id="desc" name="desc" value="" data-error="{{ __('admin.desc_error') }}"></textarea>
                </div>
                <span class="box-content-title">
                  {{ __('admin.curriculum.box_title_1') }}
                </span>
              </div>
              <div class="add-curriculum box-content">
                <div class="content-curriculum">
                </div>
                <div class="box-add-section">
                  <i class="fa fa-plus"></i>
                  <input type="text" name="section" placeholder="{{ __('admin.curriculum.section_placeholder') }}" class="enter-input">
                </div>
                <span class="box-content-title">
                  {{ __('admin.curriculum.box_title_2') }}
                </span>
              </div>
            </div>
            <div class="col-lg-4 col-xs-12 col-sm-4">
              <div class="chose-image box-content">
                <div>{{ __('admin.chose_image') }}</div>
                <span class="box-content-title">
                  {{ __('admin.image_note') }} <span class="contraint">*</span>
                </span>
              </div>
              <div class="custom-curriculum box-content">
                <span class="box-content-title">
                  {{ __('admin.curriculum.box_title_3') }}
                </span>
                <div class="custom-item">
                  <div class="custom-item-title">
                    {{ __('admin.curriculum.featured') }}
                  </div>
                  <div class="custom-item-input">
                    <input type="checkbox" name="featured" class="js-switch" value="1">
                  </div>
                </div>
                <div class="custom-item">
                  <div class="custom-item-title">
                    {{ __('admin.curriculum.logintolearn') }}
                  </div>
                  <div class="custom-item-input">
                    <input type="checkbox" name="logintolearn" class="js-switch" value="1">
                  </div>
                </div>
                <div class="custom-item">
                  <div class="custom-item-title">
                    {{ __('admin.curriculum.maximum') }} <span class="contraint">*</span>
                  </div>
                  <div class="custom-item-input">
                    <input type="number" name="maximum" class="form-control input-text-curriculum maximum" data-error="Vui lòng nhập vào giới hạn học viên."> 
                  </div>
                </div>
                <div class="custom-item result_assesment">
                  <div class="custom-item-title">
                    {{ __('admin.curriculum.result_assesment') }}
                  </div>
                  <div class="custom-item-input">
                    <div class="radio custom-item-radio">
                      <label><input type="radio" name="result_assesment" value="1" checked>{{ __('admin.curriculum.result_assesment_radio_1') }}</label>
                    </div>
                    <div class="radio custom-item-radio">
                      <label><input type="radio" name="result_assesment" value="2">{{ __('admin.curriculum.result_assesment_radio_2') }}</label>
                    </div>
                  </div>
                </div>
                <div class="custom-item">
                  <div class="custom-item-title">
                    {{ __('admin.curriculum.percent_pass') }} <span class="contraint">*</span>
                  </div>
                  <div class="custom-item-input percent_pass">
                    <div>
                      <input type="text" name="percent_pass" class="form-control input-text-curriculum" placeholder="% bài học" data-error="{{ __('admin.curriculum.percent_pass_error') }}">
                      <span class="symbol">%</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="save">{{ __('admin.save') }}</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('admin.close') }}</button>
      </div>
    </div>

  </div>
</div>