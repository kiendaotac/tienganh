@extends('layouts.app')
@section('title')
<title>{{$currentFunction->name}} - {{\Illuminate\Support\Facades\Crypt::decryptString(config('custom.app_name'))}}</title>
@endsection
@section('style')
<link href="css/plugins/dataTables/datatables.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/plugins/dropzone/dropzone.css">
<link rel="stylesheet" type="text/css" href="css/plugins/summernote/summernote.css">
<link rel="stylesheet" type="text/css" href="{{ asset('css/plugins/switchery/switchery.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/plugins/iCheck/custom.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/custom.css') }}">
<style>
.document{
  border: #ddd solid 1px;
  padding-top: 10px;
  padding-bottom: 10px;
  max-height: 150px;
  overflow-y: auto;
}
.document li{
  margin-bottom: 10px;
  list-style: circle;
}
.document li span{
  position: relative;
  display: flex;
  align-items: center;
}
.document li:last-child{
  border-bottom: 0;
}
.destroy-document{
  position: static;
  margin-left: 10px;
}
.document li:hover .destroy-document{
  display: block;
}
.add-document i{
  width: 30px;
  height: 30px;
  line-height: 30px;
  font-size: 14px;
}
.box-btn-question{
  text-align: right;
}
.chose-image{
  height: 300px;
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-bottom: 15px;
}
.chose-image div{
  height: 100%;
  border: #ddd dashed 1px;
  width: 90%;
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 16px;
  cursor: pointer;
}
.chose-image img{
  width: 90%;
  height: 90%;
}
.box-add-section{
  width: 100%;
  border-top: #ddd solid 1px;
  height: 45px;
  font-size: 14px;
  background-color: #89898933;
  padding-left: 20px;
  display: flex;
  align-items: center;
  position: relative;
}
.box-add-section i{
  font-size: 16px;
  text-align: center;
  margin-right: 10px;
}
.box-add-section input{
  border: none;
  background-color: rgba(154, 236, 219,0);
  flex: 1;
  height: 100%;
}
.refresh{
  position: absolute;
  right: 10px;
  font-style: 16px;
}
input{
  outline: none;
}
input[name='maximum']{
  width: 150px;
}
.symbol{
  position: absolute;
  top: 5px;
  right: 10px;
  font-size: 16px;
  color: #898989;
}
.percent_pass div{
  position: relative;
  width: 150px;
}
.box-add-section input:focus{
  border: none;
}
.add-title{
  width: 100%;
}
.add-part{
  margin-bottom: 10px;
}
.part-title{
  height: 40px;
  border: #ddd solid 1px;
  padding-left: 20px;
  padding-right: 20px;
  font-size: 14px;
  display: flex;
  flex-direction: row;
  align-items: center;
  cursor: pointer;
  box-shadow: #ddd 0 0 5px;
}
.part-title span{
  flex: 1;
}
.part-title i{
  transition: all 0.5s;
}
.rt{
  transform: rotate(180deg);
}
.add-part > .row{
  margin-left: 0;
  margin-right: 0;
  border: #ddd solid 1px;
  border-top: none;
  display: none;
}
.add-part:first-child > .row{
  display: block;
}
.section-item{
  cursor: pointer;
  border-bottom: #ddd solid 1px;
}
.section-item:last-child{
  border: none;
}
.section-title{
  font-size: 14px;
  display: flex;
  align-items: center;
  height: 45px;
  background-color: #89898933;
  padding-left: 20px;
  padding-right: 20px;
}
.section-title-left,
.section-child-item-title
{
  flex: 1;
}
.section-title-left i{
  margin-right: 5px;
}
.section-title-right > i{
  transition: all 0.5s;
  font-size: 14px;
}
.section-content{
  padding-left: 25px;
  padding-right: 25px;
  padding-bottom: 25px;
}
.section-desc input{
  height: 45px;
  font-size: 14px;
  width: 100%;
  border: none;
}
.section-child-item-handling{
  font-size: 18px;
}
.section-title{
  font-size: 14px;
  display: flex;
  align-items: center;
  height: 45px;
  background-color: #89898933;
  padding-left: 20px;
  padding-right: 20px;
}
.section-title-left,
.section-child-item-title
{
  flex: 1;
}
.section-child{
  border: #ddd solid 1px;
}
.section-child-add, .section-child-item{
  height: 40px;
  display: flex;
  align-items: center;
  padding-left: 20px;
  position: relative;
}
.section-child-item{
  border-bottom: #ddd solid 1px;
}
.section-child-item span{
  font-size: 14px;
}

.section-child-add i{
  margin-right: 10px;
  font-size: 18px;
}
.lesson, .update-section-child{
  color: #3498db;
}
.clock{
  color: rgba(149, 165, 166,1);
}
.section-child-add input{
  height: 100%;
  flex: 1;
  border: none;
  font-size: 14px;
  color: unset;
}
.destroy-section{
  color: lightcoral;
  font-size: 20px !important;
  margin-right: 10px;
}
.destroy-section-child{
  color: lightcoral;
}
.section-child-item-handling > i,
.section-child-item-title i{
  margin-right: 10px;
  font-size: 18px;
}
.custom-item{
  display: flex;
  height: 40px;
  align-items: center;
  margin-bottom: 10px;
}
.custom-item-title{
  text-align: right;
  margin-right: 100px;
}
.custom-item div{
  flex: 1;
}
.result_assesment{
  height: unset;
}
.custom-item input[type='text']{
  padding-right: 30px;
  width: 100%;
}
</style>
@endsection
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-10">
    <h2>{{ __('admin.title_page') }} {{mb_strtolower($currentFunction->name)}}</h2>
  </div>
  <div class="col-lg-2">

  </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">
        <div class="ibox-content">
          <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover dataTables-curriculums">
              <thead>
                <tr>
                  <th width="50">{{ __('admin.table_stt') }}</th>
                  <th>{{ __('admin.curriculum.name') }}</th>
                  <th width="150">{{ __('admin.curriculum.table_category') }}</th>
                  <th width="100">{{ __('admin.curriculum.table_number_student') }}</th>
                  <th width="70">Ordering</th>
                  <th>{{ __('admin.table_state') }}</th>
                  <th>{{ __('admin.table_action') }}</th>
                </tr>
              </thead>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal add new function -->
@include('backend.course.curriculum.form')
@include('backend.course.lesson.form')
@include('backend.course.lesson.modal-test')
@include('backend.course.lesson.modal-detail-test')
@include('backend.system.file.module')
@endsection
@section('script')
<script src={{ asset('js/custom.js') }}></script>
<script>
  /* datatable curriculum */
  let mainUrl = '{{$currentFunction->route}}';
  let Table = $('.dataTables-curriculums').DataTable({
    bSort: false,
    bInfo: true,
    bLengthChange: false,
    processing: true,
    serverSide: true,
    searching: true,
    paging: true,
    pageLength: 15,
    responsive: true,
    ajax: mainUrl + '/getDatatable',
    dom: '<"row"<"col-sm-6"Bi><"col-sm-6"f>>rtp',
    buttons: {
      buttons: [
      {
        text: 'Thêm mới',
        action: function ( e, dt, node, config ) {
          resetForm();
          $('#modal-add-curriculum').modal({backdrop: 'static', keyboard: false, show : true});
        }
      }
      ],
      dom: {
        button: {
          tag: "button",
          className: "btn btn-primary"
        },
        buttonLiner: {
          tag: null
        }
      }
    },
    language: {
      "lengthMenu": "Hiển thị _MENU_ bản ghi trên 1 trang",
      "zeroRecords": "Không có dữ liệu",
      "info": "Hiển thị trang _PAGE_ trong tổng số _PAGES_ trang",
      "infoEmpty": "Không có dữ liệu tồn tại",
      "infoFiltered": "(Lọc từ _MAX_ bản ghi)",
      "search": "Tìm kiếm:",
      "paginate": {
        "first":      "Trang đầu",
        "last":       "Trang cuối",
        "next":       "Trang tiếp theo",
        "previous":   "Trang trước"
      },
    },
    columns     :   [
    {
      data    :   'id',
      className:  'text-center',
      render  :   function (data, type, row, meta) {
        return meta.row +1;
      }
    },
    {
      data    :   'name',
      render  : function (data, type, row, meta) {
        if(data == null) return '';
        return '<b>'+data+'</b>';
      }
    },
    {
      data    :   'category',
      className: "text-center",
      render  :   function (data, type, row, meta) {
        return data;
      }
    },
    {
      data    :   'students',
      className:  "text-center",
      render  :  function (data, type, row, meta) {
        return data.length;
      }
    },
    // {
    //   data    :   'desc',
    //   render  :   function (data, type, row, meta) {
    //     var e = document.createElement('div');
    //     $(e).html(data);
    //     return ($(e)[0].childNodes[0]) ? $(e)[0].childNodes[0].nodeValue : '';
    //   }
    // },
    {
      data    :   'ordering',
      className: 'text-center'
    },
    {
      data    :   'states',
      className: 'text-center',
      render  :   function (data, type, row, meta) {
        if(data == 0) return 'Không kích hoạt';
        if(data == 2) return 'Đang duyệt';
        return 'Kích hoạt';
      }
    },
    {
      data    :   'id',
      className: 'text-center',
      render  :   function (data, type, row, meta) {
        let button  =   '@permission("Edit-14")<button name="btn-edit" class="btn btn-primary btn-flat btn-xs" title="Sửa" data-data=\''+JSON.stringify(row)+'\'><i class="fa fa-edit"></i></button>@endpermission @permission("Delete-14")<button name="btn-delete" class="btn btn-remove btn-danger btn-flat btn-xs" title="Xóa" data-id='+data+'><i class="fa fa-remove"></i></button>@endpermission';
        return button;
      }
    }
    ]
  });
  /* perfect scroll */
  new PerfectScrollbar($('.table-responsive')[0]); 
  new PerfectScrollbar($('#modal-add-lesson')[0]);
  $('.modal-full .modal-body').each(function(){
    new PerfectScrollbar($(this)[0]);
  });
  new PerfectScrollbar($('#modal-detail-test')[0]);
  /* reset form */
  function resetForm() {
    idCurriculum = null;
    resetSwitchery(nameSwitchCurriculum);
    $('form[name=form-add-curriculum]')[0].reset();
    $('textarea[name=desc]').summernote('reset');
    $('.note-editable').html('');
    $('.section-item, .section-child-item').remove();
    $('input[name=id]').val(0);
    $('input[name=img]').val('');
    $('.chose-image > div').html('Nhấn vào để chọn ảnh');
    $('select[name=parent_id] option').show();
  }
  /* reset form lesson */
  function resetFormLesson(){
    $('.link-video-quicktest, .list-type-content, .link-audio-lesson, .link-video-lesson, .list-curriculum-content').hide();
  }
  /* summernote */
  $('textarea[name=desc]').summernote({
    height: 200,
  });
  /* hide div class ui-helper-hidden-accessible */
  $('.note-btn').hover(function(){
    $('.ui-helper-hidden-accessible').remove();
  });
  /* process modal add curriculum */
  $(document).on('click', '.part-title', function(){
    $(this).find('i').toggleClass('rt');
    $(this).closest('.add-part').find('.row').slideToggle();
  })
  /* setup key enter */
  $('body').on('keyup', '.enter-input', function(e){
    if(e.keyCode == 13)
    {
      $(this).trigger("enterKey");
    }
  });

  // add section
  let idCurriculum = null;
  $(document).on('enterKey', '.box-add-section input', function(){
    let name = $(this).val(),
    id = $('input[name=id]').val();
    if(name != ''){
      /* loading */
      $(this).closest('div').append('<i class="fa fa-refresh refresh load"></i>');
      if(id == 0  && idCurriculum == null) {
        addCurriculum(true, 0, name,false);
      }
      else {
        if(id != 0) idCurriculum = id;
        addLesson(name, idCurriculum, 0, 'title', null);
      }
    }
    else swal('Bạn chưa nhập tên bài học.');
  });
  /* ajax add curriculum */
  function addCurriculum(checkLesson, state, nameLesson,validator){
    let url = 'curriculum',
    type    = 'post',
    id      = ($('input[name=id]').val() != 0) ? $('input[name=id]').val() : idCurriculum;
    if(parseInt(id)) {
      url  = url + '/' + id;
      type = 'PUT';
    }
    $.ajax({
      url: url,
      type: type,
      dataType: 'json',
      data: {
        cate_id: $('select[name=cate_id]').val(),
        name: $('input[name=name]').val(),
        desc: $('textarea[name=desc]').val(),
        img: $('input[name=img]').val(),
        featured: ($('input[name=featured]').is(':checked')) ? '1' : '0',
        logintolearn: ($('input[name=logintolearn]').is(':checked')) ? '1' : '0',
        maximum: $('input[name=maximum]').val(),
        percent_pass: $('input[name=percent_pass]').val(),
        result_assesment: $('input[name=result_assesment]:checked').val(),
        ordering: $('input[name=ordering]').val(),
        states: state,
        validator: validator,
      },
      success: function(data){
        $('#save i').remove();
        let value = JSON.parse(JSON.stringify(data));
        if(value.id != null ) idCurriculum = value.id;
        if(checkLesson) addLesson(nameLesson, idCurriculum, 0, 'title', null);
        else {
          notification(value.type, value.title, value.content);
          if(value.type == 'success') {
            $('#modal-add-curriculum').modal('hide');
            Table.ajax.reload();
          }
        }
      }
    });
  }
  /* create content curriculum */
  function contentCurriculum(lesson, name, stt){
    let content = '<div class="section-item">'
    + '<div class="section-title">'
    + '<div class="section-title-left">'
    + '<i class="fa fa-bars"></i>'
    + '<span class="lesson-name" data-lesson=' + "'" + JSON.stringify(lesson) + "'" + '>' + name + '</span>'
    + '</div>'
    + '<div class="section-title-right">'
    + '<i class="fa fa-trash destroy-section" data-toggle="confirmation"></i>'
    + '<i class="fa fa-caret-up toggle-section-child"></i>'
    + '</div>'
    + '</div>'
    + '<div class="section-content">'
    + '<div class="section-desc">'
    + '<input type="text" name="section-desc" placeholder="Mô tả chương">'
    + '</div>'
    + '<div class="section-child">'
    + '<div class="section-child-list" stt='+ stt +'>'
    + '</div>'
    + '<div class="section-child-add">'
    + '<i class="fa fa-book lesson" data-type="lesson"></i>'
    + '<i class="fa fa-clock-o clock" data-type="test"></i>'
    + '<input type="text" name="lesson" placeholder="Tạo bài học mới" class="enter-input">'
    + '</div>'
    + '</div>'
    + '</div>'
    + '</div>';
    $('.content-curriculum').append(content);
  }
  /* ajax add lesson */
  function addLesson(name, id, parent_id, type, obj){
    $.ajax({
      url: 'lesson',
      type: 'post',
      dataType: 'json',
      data: {
        name: name,
        curriculum_id: id,
        type: type,
        parent_id: parent_id,
        review: 0,
        states: 1,
        typeLesson: 'title'
      },
      success: function(data){
        let value = JSON.parse(JSON.stringify(data));
        notification(value.type, value.title, value.content);
        if(value.type == 'success') {
          $('.refresh').remove();
          Table.ajax.reload();
          loadLesson();
        }
      }
    });
  }
  /* add lesson child parent_id != 0 */
  let typeLesson = 'lesson';
  $(document).on('enterKey', 'input[name=lesson]', function(){
    let name = $(this).val();
    if($('input[name=id]').val() != 0)  idCurriculum = $('input[name=id]').val();
    if(name != '') {
      /* loading */
      $(this).closest('div').append('<i class="fa fa-refresh refresh load"></i>');
      let lesson = JSON.parse(JSON.stringify($(this).closest('.section-item').find('.lesson-name').data('lesson')));
      addLesson(name, idCurriculum, lesson.id, typeLesson, $(this));
    }
    else swal('Bạn chưa nhập tên bài học.');
  });

  //add desc section
  let sectionDesc = null;
  $(document).on('focus', 'input[name=section-desc]', function(){
    sectionDesc = $(this).val();
  });
  $(document).on('blur', 'input[name=section-desc]', function(){
    let value = $(this).val();
    if(value !== "" && sectionDesc != value) {
      let lesson = JSON.parse(JSON.stringify($(this).closest('.section-item').find('.lesson-name').data('lesson')));
      console.log(idCurriculum);
      $.ajax({
        url: 'lesson/' + lesson.id,
        type: 'PUT',
        dataType: 'json',
        data: {
          curriculum_id: (idCurriculum != null) ? idCurriculum : $('input[name=id]').val(),
          states: 1,
          desc: value,
          parent_id: lesson.parent_id,
          typeLesson: 'updateDesc'
        },
        success: function(data){
          value = JSON.parse(JSON.stringify(data));
          notification(value.type, value.title, value.content);
        }
      });
    }
  });
  /* destroy lesson */
  $(document).on('click', '.destroy-section', function(){
    let sectionItem = $(this).closest('.section-item');
    let lesson = JSON.parse(JSON.stringify(sectionItem.find('.lesson-name').data('lesson')));
    destroyLesson(lesson.id, sectionItem, $(this));
  });
  $(document).on('click', '.destroy-section-child', function(){
    let sectionItem = $(this).closest('.section-child-item');
    let lesson = JSON.parse(JSON.stringify(sectionItem.find('.lesson-child-name').data('lesson')));
    destroyLesson(lesson.id, sectionItem, $(this));
  });
  /* ajax destroy lesson */
  function destroyLesson(id, sectionItem, element){
    swal({
      title: "Bạn có muốn xóa bài học này không??",
      text: "Khi xóa Bài học sẽ không thể khôi phục lại",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "OK",
      cancelButtonText: "Không",
      closeOnConfirm: true,
      closeOnCancel: true
    }, function(config){
      if(config){
        element.removeClass('fa fa-trash');
        element.addClass('fa fa-refresh load');
        $.ajax({
          url: 'lesson/' + id,
          type: 'DELETE',
          dataType: 'json',
          data: {
            type: 'lesson',
          },
          success: function(data){
            let value = JSON.parse(JSON.stringify(data));
            notification(value.type, value.title, value.content);
            if(value.type == 'success') {
              sectionItem.remove();
            }
          }
        });
      }
    });
  }
  // chose lesson or quizee
  $('body').on('click', '.section-child-add i', function(){
    $(this).closest('div').find('i').css('color','rgba(149, 165, 166)');
    $(this).css('color','#3498db');
    typeLesson = $(this).data('type');
  })
  //toggle show section content
  $('body').on('click', '.toggle-section-child', function(){
    $(this).toggleClass('rt');
    $(this).closest('.section-item').find('.section-content').slideToggle();
  });
  /* save curriculum */
  $(document).on('click', '#save', function(){
    if(checkNotValue('input-text-curriculum')) {
      if($('.chose-image img')[0]) {
        $(this).append('<i class="fa fa-refresh load" style="margin-left: 5px"></i>');
        let state = 2;
        if($('select[name=states]').val() != undefined) state = $('select[name=states]').val();
        addCurriculum(false,state,null,true);
      }
      else swal('Vui lòng chọn ảnh minh họa.');
    }
    else return checkNotValue('input-text-curriculum');
    
  });
  /* edit curriculum */
  let opActive = '',
  opNotActive = '',
  opWait = '';
  $(document).on('click', '[name=btn-edit]', function(){
    $('.content-curriculum').html('');
    resetSwitchery(nameSwitchCurriculum);
    let data = $(this).data('data');

    @if (Auth::user()->hasRole('Admin'))
    /* process state in form */
    if(data.states == 2) {
      if($('select[name=states] option[value=1]').length > 0) {
        opActive = $('select[name=states] option[value=1]')[0].outerHTML;
        $('select[name=states] option[value=1]').remove();
      }
      if($('select[name=states] option[value=0]').length > 0) {
        opNotActive = $('select[name=states] option[value=0]')[0].outerHTML;
        $('select[name=states] option[value=0]').remove();
      }
      if(opWait != '') {
        $('select[name=states]').append(opWait);
        opWait = '';
      }
    }
    else {
      if(opWait == '' && $('select[name=states] option[value=2]').length > 0) {
        opWait = $('select[name=states] option[value=2]')[0].outerHTML;
        $('select[name=states] option[value=2]').remove();
      }
      if(opActive != '') {
        $('select[name=states]').append(opActive);
        opActive = '';
      }
      if(opNotActive != '') {
        $('select[name=states]').append(opNotActive);
        opNotActive = '';
      }
    }
    @endif
    selectedCate(data.categories.id);
    $('#modal-add-curriculum .note-editable').html(data.desc);
    if(data.featured == 1) nameSwitchCurriculum[0].setPosition(true);
    if(data.logintolearn == 1) nameSwitchCurriculum[1].setPosition(true);
    $('input[name=result_assesment][value='+data.result_assesment+']').attr('checked','checked');
    let imgPath = data.img;
    let imgElement = null;
    if(imgPath != '' && imgPath != null) imgElement = '<img src="'+imgPath+'" alt="hình ảnh mô tả thể loại" />';
    else imgElement = 'Nhấn vào để chọn ảnh';
    $('.chose-image > div').html(imgElement);
    $.each(data, function (index, item) {
      if(index != 'result_assesment') $('[name='+index+']').val(item);
    });
    /* selected category click btn-edit */
    function selectedCate($id){
      $('select[name=cate_id] option').each(function(){
        if($(this).attr('value') == $id) $(this).attr('checked','checked');
      });
    }
    loadLesson();
    $('#modal-add-curriculum').modal({backdrop: 'static', keyboard: false, show : true});
  });
  function loadLesson(){
    $('.box-add-section').hide();
    $('.content-curriculum').html('<i class="fa fa-refresh load" style="font-size: 25px; width: 100%; text-align: center" title="Đang tải dữ liệu bài học"></i>');
    $.ajax({
      url: 'lesson/' + $('input[name=id]').val(),
      type: 'GET',
      dataType: 'JSON',
      success: function(data){
        let value = JSON.parse(JSON.stringify(data));
        $('.box-add-section').show();
        $('.content-curriculum > i').remove();
        $.each(value, function(index, item){
          if(item.parent_id == 0) {
            contentCurriculum(item, item.name, item.id);
          }
          else {
            let typeLessonElement = '<i class="fa fa-book lesson" data-type="lesson" style="color: rgb(52, 152, 219)"></i>'
            if(item.type == 'test') typeLessonElement = '<i class="fa fa-clock-o clock" data-type="test" style="color: rgb(52, 152, 219)"></i>';
            let content = '<div class="section-child-item">'
            + '<div class="section-child-item-title">'
            + typeLessonElement
            + '<span class="lesson-child-name" data-lesson=' + "'" + JSON.stringify(item) + "'" + '>' + item.name + '</span>'
            + '</div>'
            + '<div class="section-child-item-handling">'
            + '<i class="fa fa-pencil update-section-child" data-data=\''+JSON.stringify(item)+'\'></i>'
            + '<i class="fa fa-trash destroy-section-child" data-toggle="confirmation"></i>'
            + '</div>'
            + '</div>';
            $('.section-child-list[stt='+item.parent_id+']').append(content);
          }
        });
      }
    });
  }
  /* delete category */
  $(document).on('click', 'button[name=btn-delete]', function(){
    let id = $(this).data('id');
    let button = $(this);
    swal({
      title: "Bạn có muốn xóa khóa học này không?",
      text: "Khi xóa thì tất cả các bài học, bài kiểm tra của khóa học này đều sẽ bị xóa và không thể khôi phục lại",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "OK",
      cancelButtonText: "Không",
      closeOnConfirm: true,
      closeOnCancel: true
    }, function(config){
      if(config){
        button.find('i').removeClass('fa-times');
        button.find('i').addClass('fa-refresh load');
        $.ajax({
          url: mainUrl + '/' + id,
          type: 'DELETE',
          dataType: 'json',
          data: {
            type: 'lesson'
          },
          success: function(data){
            let value = JSON.parse(JSON.stringify(data));
            notification(value.type, value.title, value.content);
            if(value.type == 'success') {
              Table.ajax.reload();
            }
          }
        });
      }
    });
  });
  /* update lesson */
  $(document).on('click', '.update-section-child', function(){
    resetFormLesson();
    let value = $(this).data('data');
    let valueTitle = $(this).closest('.section-item').find('.lesson-name').data('lesson');
    $('#list-curriculum').html('<li data-id="'+valueTitle.curriculums.id+'">'+valueTitle.curriculums.name+'</li>');
    setValueForm(value);
    $('.list-curriculum-content').hide();
  });
</script>
@endsection
