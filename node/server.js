let app         =   require('express')(),
    http        =   require('http').Server(app),
    io          =   require('socket.io')(http),
    Redis       =   require('ioredis'),
    redis       =   new Redis(8081),
    mysql       =   require('mysql');
const util = require('util');
let mysqlInfor  =   {
    host        :   "localhost",
    user        :   "root",
    password    :   "",
    database    :   "mcq"
};
let connection  =   mysql.createConnection(mysqlInfor);
let port        =   8080;
const query = util.promisify(connection.query).bind(connection);

http.listen(port, function() {
    console.log('Listening on *:' + port);
});
redis.psubscribe('*', function (error, count) {
    //
});
redis.on('pmessage', function (partern, chanel, message) {
    if (chanel === 'shift.generate_off'){
        console.log('Start');
        let param       =   JSON.parse(message);
        let test_number =   param.shift.test_numbers;
        connection.beginTransaction(err => {
            if(err) throw err;
            for (let index = 1; index <= test_number; index++) {
                createExam(param);
                io.emit('shift.generate_off',{
                    shift_id    :   param.shift.id,
                    question    :   index,
                    total       :   test_number
                });
                console.log('Tạo đề thứ: ', index);
                // let data = new Array();
                // let sql_select_question = 'SELECT * FROM test_group_questions ORDER BY RAND() LIMIT 100';
                // connection.query(sql_select_question,(err , rows) =>{
                //     if (err) throw err;
                //     rows.forEach(element => {
                //         let tem = new Array();
                //         tem.push(param.shift_name, param.student_id, element.group_id, element.id,element.question, element.answer_options, element.correct_answer, element.answer_shuff, element.explain, element.hint, element.state);
                //         data.push(tem);
                //     });
                //
                //     let sql = "INSERT INTO dethi_test (name, student_id, group_id, question_id, question, answer_option, correct_answer, answer_shuff, ex, hint, state) VALUES ?";
                //     connection.query(sql,[data],(err) => {
                //         if(err) throw err;
                //     });
                //     connection.commit(err => {
                //         if(err) throw err;
                //         console.log('De thu: ' + index);
                //
                //         io.emit('shift.generate_off',{
                //             shift_id    :   param.shift_id,
                //             question    :   index,
                //             total       :   test
                //         });
                //     });
                //     if (index === test){
                //         let update_process  =   "UPDATE test_shifts SET process = 2 WHERE id=" + param.shift_id;
                //         connection.query(update_process,null, err => {
                //             if(err) throw err;
                //         });
                //         connection.commit(err => {
                //             if(err) throw err;
                //             console.log('Tạo xong đề id = ' + param.shift_id);
                //         });
                //     }
                // });
            }
        })
    }
});

function createExam(param) {
    let sql     =   "INSERT INTO test_generate_offlines (shift_id, student_id, time_start, time_end, state) VALUES ?";
    let data    =   [[param.shift.id, param.student_id, param.time_start, param.time_end, 0]];

    connection.query(sql,[data],(err,  results, fields) => {
        if(err) throw err;
        let exam_id    =   results.insertId;
        createGroup(exam_id, param);
    });
    connection.commit(err => {
        if(err) throw err;
    });
}

function createGroup(exam_id, param) {
    let sql_get_group   =   "SELECT * FROM `test_groups` WHERE `test_subject_id` = ? AND `question_type` = ? AND `level` = ? order by RAND() limit ?";

    /* Lấy tất cả form details của form hiện tại */
    let details         =   param.details;
    details.forEach(function (element) {
        let test_subject_id =   element.subject_id;
        let question_type   =   element.question_type;
        let level           =   element.level;
        let limit           =   element.number_group;
        let data            =   [test_subject_id, question_type, level, limit];
        connection.query(sql_get_group, data, (errors,  results, fields)=>{
            if (errors) throw  errors;
            /* Tạo group generate */
            results.forEach(function (row) {
                let generate_id         =   exam_id;
                let group_id            =   row.id;
                let name                =   row.name;
                let content             =   row.content;
                let media_type          =   row.media_type;
                let media_source        =   row.media_source;
                let question_type       =   row.question_type;
                let question_shuff      =   row.question_shuff;
                let show_explain        =   param.shift.testform.show_explain;
                let show_hint           =   param.shift.testform.show_hint;
                let total_point         =   element.total_point;
                let result              =   0;
                let CURRENT_TIMESTAMP   =   { toSqlString: function() { return 'CURRENT_TIMESTAMP()'; } };
                let group               =   [[generate_id, group_id, name, content, media_type, media_source, question_type, question_shuff, show_explain, show_hint, total_point, result, CURRENT_TIMESTAMP]];
                let sql_insert_group    =   "INSERT INTO test_generate_group_offlines (generate_id, group_id, name, content, media_type, media_source, question_type, question_shuff, show_explain, show_hint, total_point, result, created_at ) VALUES ?";
                connection.query(sql_insert_group,[group],(errors,  results, fields)=>{
                    if (errors) throw  errors;
                    let test_generate_group_id  =   results.insertId;
                    createQuestion(group_id, test_generate_group_id);
                });
            });
        })
    });
}

let createQuestion = async (group_id, test_generate_group_id)=> {
    let sql_get_question                =   "SELECT * FROM test_group_questions WHERE group_id = ? AND state = 1";
    let sql_insert_generate_questions   =   "INSERT INTO test_generate_question_offlines (test_generate_group_id, question_id, question, answer_options, answer, correct_answer, answer_shuff, `explain`, hint, result, created_at) VALUES ?";
    let CURRENT_TIMESTAMP               =   { toSqlString: function() { return 'CURRENT_TIMESTAMP()'; } };
    let data                            =   [group_id];
    let data_questions                  =   [];
    let results = await query(sql_get_question, [data]);
    results.forEach(function (element) {
        let question_id             =   element.id;
        let question                =   element.question;
        let answer_options          =   element.answer_options;
        let answer                  =   '';
        let correct_answer          =   element.correct_answer;
        let answer_shuff            =   element.answer_shuff;
        let explain                 =   element.explain;
        let hint                    =   element.hint;
        let result                  =   0;
        let data_question           =   [test_generate_group_id, question_id, question, answer_options, answer, correct_answer, answer_shuff, explain, hint, result, CURRENT_TIMESTAMP];
        data_questions.push(data_question);
    });
    if (data_questions.length){
        query(sql_insert_generate_questions,[data_questions], (errors,  results, fields) => {
            if (errors) throw  errors;
        });
    }
};
