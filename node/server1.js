const util      =   require('util');
let app         =   require('express')(),
    http        =   require('http').Server(app),
    io          =   require('socket.io')(http),
    Redis       =   require('ioredis'),
    redis       =   new Redis(8081),
    mysql       =   require('mysql');
let mysqlInfor  =   {
    host        :   "localhost",
    user        :   "root",
    password    :   "",
    database    :   "mcq"
};
let connection  =   mysql.createConnection(mysqlInfor);
let port        =   8080;
const query     =   util.promisify(connection.query).bind(connection);
const commit    =   util.promisify(connection.commit).bind(connection);

http.listen(port, function() {
    console.log('Listening on *:' + port);
});
redis.psubscribe('*', function (error, count) {
    //
});
redis.on('pmessage', function (partern, chanel, message) {
    if (chanel === 'shift.generate_off'){
        console.log('Start');
        let param       =   JSON.parse(message);
        let test_number =   param.shift.test_numbers;
        connection.beginTransaction(err => {
            if(err) throw err;
            createExams(test_number, param);
        })
    }
});

let createExams = async (test_number, param) => {
    for (let index = 1; index <= test_number; index++) {
        await createAnExam(param);
        io.emit('shift.generate_off',{
            shift_id    :   param.shift.id,
            question    :   index,
            total       :   test_number
        });
        console.log('Tạo đề thứ-:- ', index);
    }
};

let createAnExam = async (param) => {
    let sql                             =   "INSERT INTO test_generate_offlines (shift_id, student_id, time_start, time_end, state) VALUES ?";
    let sql_insert_generate_questions   =   "INSERT INTO test_generate_question_offlines (test_generate_group_id, question_id, question, answer_options, answer, correct_answer, answer_shuff, `explain`, hint, result, created_at) VALUES ?";
    let data                            =   [[param.shift.id, param.student_id, param.time_start, param.time_end, 0]];
    try {
        let respon      =   await query(sql,[data]);
        let commit      =   await connection.commit();
        let AnExam      =   await createGroup(respon.insertId, param);
        // console.log(mysql.format(sql_insert_generate_questions,[AnExam]));
        query(sql_insert_generate_questions,[AnExam]);// ko cho sử lý tuần tự chỗ này. Lấy dc câu hỏi là xong luôn. ko cần chờ insert xong.
    } catch (e) {
        console.log(e.toString());
    }
};

let createGroup = async (exam_id, param) => {
    let sql_get_group       =   "SELECT * FROM `test_groups` WHERE `test_subject_id` = ? AND `question_type` = ? AND `level` = ? order by RAND() limit ?";

    let groupQuestions      =   [];
    /* Lấy tất cả form details của form hiện tại */
    let details             =   param.details;
    for await (element of details) {
        let test_subject_id =   element.subject_id;
        let question_type   =   element.question_type;
        let level           =   element.level;
        let limit           =   element.number_group;
        let data            =   [test_subject_id, question_type, level, limit];
        try {
            let groups      =   await query(sql_get_group, data);
            for await (row of groups){
                let generate_id         =   exam_id;
                let group_id            =   row.id;
                let name                =   row.name;
                let content             =   row.content;
                let media_type          =   row.media_type;
                let media_source        =   row.media_source;
                let question_type       =   row.question_type;
                let question_shuff      =   row.question_shuff;
                let show_explain        =   param.shift.testform.show_explain;
                let show_hint           =   param.shift.testform.show_hint;
                let total_point         =   element.total_point;
                let result              =   0;
                let CURRENT_TIMESTAMP   =   { toSqlString: function() { return 'CURRENT_TIMESTAMP()'; } };
                let group               =   [[generate_id, group_id, name, content, media_type, media_source, question_type, question_shuff, show_explain, show_hint, total_point, result, CURRENT_TIMESTAMP]];
                let sql_insert_group    =   "INSERT INTO test_generate_group_offlines (generate_id, group_id, name, content, media_type, media_source, question_type, question_shuff, show_explain, show_hint, total_point, result, created_at ) VALUES ?";
                let insertGroup         =   await query(sql_insert_group,[group]);
                let questions           =   await createQuestions(group_id, insertGroup.insertId);
                groupQuestions          =   [...groupQuestions,...questions];
            };
        } catch (e) {
            console.log(e.toString());
        }
        // console.log(groupQuestions)
    };
    return new Promise((resolve, reject) => {
        return resolve(groupQuestions);
    })
};

let createQuestions = async (group_id, test_generate_group_id)=> {
    let sql_get_question                =   "SELECT * FROM test_group_questions WHERE group_id = ? AND state = 1";
    let CURRENT_TIMESTAMP               =   { toSqlString: function() { return 'CURRENT_TIMESTAMP()'; } };
    let data                            =   [group_id];
    let data_questions                  =   [];
    try {
        let results = await query(sql_get_question, [data]);
        results.forEach(function (element) {
            let question_id             =   element.id;
            let question                =   element.question;
            let answer_options          =   element.answer_options;
            let answer                  =   '';
            let correct_answer          =   element.correct_answer;
            let answer_shuff            =   element.answer_shuff;
            let explain                 =   element.explain;
            let hint                    =   element.hint;
            let result                  =   0;
            let data_question           =   [test_generate_group_id, question_id, question, answer_options, answer, correct_answer, answer_shuff, explain, hint, result, CURRENT_TIMESTAMP];
            data_questions.push(data_question);
        });
        return new Promise((resolve, reject) => {
            return resolve(data_questions);
        })
    } catch (e) {
        console.log(e.toString());
    }
};
