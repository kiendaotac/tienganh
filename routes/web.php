<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Administrator routes */
Route::domain(config('custom.admin_subdoman'))->group(function () {
    Auth::routes();
    Route::get('logout', 'Auth\\LoginController@logout');
    Route::middleware('auth')->group(function () {
        Route::get('/', function () { 
            return view('backend.system.authentication.home');
        });
        Route::middleware('PermissionToAccessFunction')->group(function () {
            $functions = \App\Functions::where('state', '>', 0)->where('access', 1)->get();
            foreach ($functions as $function) {
                if (class_exists('App\Http\Controllers\\' . $function->controller)) {
                    Route::resource($function->route, $function->controller);
                }
            }
        });
    });
});
/* Student login */
Route::get('login', 'Auth\\StudentLoginController@showStudentLoginForm')->name('get.student.login');
Route::get('register', 'Auth\\StudentLoginController@showStudentRegisterForm')->name('get.student.register');
Route::get('logout', 'Auth\\StudentLoginController@logoutStudent')->name('get.student.logout');
Route::post('login', 'Auth\\StudentLoginController@loginStudent')->name('post.student.login');
Route::post('register', 'Auth\\StudentLoginController@registerStudent')->name('post.student.register');

/* User routes */
Route::middleware('auth:student')->group(function () {
    $functions = \App\Functions::where('state', '>', 0)->where('access', 2)->get();
    foreach ($functions as $function) {
        if (class_exists('App\Http\Controllers\frontend\\' . $function->controller)) {
            Route::resource($function->route, 'frontend\\'.$function->controller);
        }
    }
});

/* Guest routes */
Route::group([], function () {
    $functions = \App\Functions::where('state', '>', 0)->where('access', 3)->get();
    foreach ($functions as $function) {
        if (class_exists('App\Http\Controllers\\' . $function->controller)) {
            Route::resource($function->route, $function->controller);
        }
    }
});

Route::get('/clear-cache', function() {
    Artisan::call('config:cache');
    Artisan::call('cache:clear');
    Artisan::call('view:clear');
    return 'xong';
});
