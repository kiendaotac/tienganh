/* perfect scroll */
new PerfectScrollbar($('.box-notify')[0]);
/* get notification */
$.ajax({
  url: 'notifications/getNotifi',
  type: 'GET',
  dataType: 'JSON',
  success: function(data){
    let value = JSON.parse(JSON.stringify(data));
    if(value.count != 0) $('.number-notify').text(value.count);
    $.each(value.notifications, function(index, val) {
      let content = '<li>' +
      '<a href="'+val.data.link+'">' +
      '<div>' +
      '<i class="'+val.data.icon+' fa-fw"></i>' + val.data.mess +
      '<span class="pull-right text-muted small">'+val.time+'</span>' +
      '</div></a></li>';
      $('.box-notify').append(content);
    });
  }
})
/* read notify */
$(document).on('click','.count-info', function(){
  $('.number-notify').text('');
  $.ajax({
    url: 'notifications/readNotifi',
    type: 'GET',
    dataType: 'JSON',
  })
});