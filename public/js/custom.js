
/* click chose uni */
$(document).on('click','#list-uni li, #list-department li, #list-major li',function(){
  let id = $(this).data('id');
  let spanElement = $(this).closest('.chose-uni').find('span:not(".contraint")');
  spanElement.text($(this).text());
  spanElement.attr('value',id);
  $(this).closest('.chose-uni').find('.chose-uni-title').trigger('click');
  if($(this).closest('ul')[0].id == 'list-uni'){
    loadDepartment(id);
  }
  if($(this).closest('ul')[0].id == 'list-department'){
    laodMajor(id);
  }
});
/* load major */
function laodMajor(id){
  console.log(id);
  let divContent = $('.list-major-notify').closest('.chose-uni-content');
  $('.list-major-notify').html('<i class="fa fa-refresh load"></i>');
  let url = 'majo/getMajor',
  key = 'major',
  holder = 'Tìm kiếm ngành';
  loadUni(url,id,key,holder,divContent);
}
/* ajax load when chose uni, department */
function loadUni(url,id,key,holder,divContent){
  $.ajax({
    url: url,
    type: 'GET',
    dataType: 'JSON',
    data: {
      id: id,
    },
    success: function(data){
      $('.list-'+key+'-notify').hide();
      divContent.html('<div><input type="text" id="search-'+key+'" class="form-control" placeholder="'+holder+'"></div><ul id="list-'+key+'"></ul>');
      let value = JSON.parse(JSON.stringify(data));
      $.each(value, function(index, item){
        let liElement = '<li data-id='+item.id+'>'+item.name+'</li>';
        divContent.find('ul').append(liElement);
      });
    }
  });
}
/* delete image avatar */
$(document).on('click', '.delete-avatar', function(){
  let pathImg = $(this).closest('.box-avatar').find('img').attr('src');
  if(pathImg == 'img/noavatar.png') swal('Bạn chưa chọn ảnh đại diện');
  else $(this).closest('.box-avatar').find('img').attr('src','img/noavatar.png');
});
let idParent = null;
/* check input options is have value */
function checkNotValue(className){
  let check = true;
  let input = [];
  $('.'+className).each(function(index, item){
    input.push($(this));
  });
  input = input.reverse();
  $.each(input, function(index, item){
    if(item.val() === '' || item.val() === '<p><br></p>') check = swal($(this).data('error'));
  })
  return check;
}
/* chose curiculum */
$(document).on('click', '.chose-curriculum-title', function(){
  showContent($(this),$(this).closest('.div-chose-curriculum').find('.chose-curriculum-content'));
  showContent($(this),$(this).closest('.div-chose-curriculum').find('.chose-curriculum-content-report'));
});
$(document).on('click', '.chose-uni-title', function(){
  showContent($(this),$(this).closest('.chose-uni').find('.chose-uni-content'));
});
$(document).on('click', '.chose-class-curriculum-title', function(){
  showContent($(this),$(this).closest('.chose-class-curriculum').find('.chose-class-curriculum-content'));
});
function showContent(obj,content){
  obj.find('i').toggleClass('rt');
  content.slideToggle();
}
/* chose curriculum or lesson */
$(document).on('click', '.chose-curriculum-content ul li, .chose-curriculum-content-report ul li', function(){
  let spanElement = $(this).closest('.div-chose-curriculum').find('.chose-curriculum-title span');
  spanElement.text($(this).text());
  spanElement.attr('value', $(this).data('id'));
  $(this).closest('.chose-curriculum-content, .chose-curriculum-content-report').slideToggle();
  if($(this).closest('ul')[0].id == 'list-curriculum'){
    $('.box-list-curriculum').html('<span class="list-lesson-notify">Hãy họn khóa học trước</span>');
    $('#id-lesson').text('-- Không thuộc chương nào --');
    $('#id-lesson').attr('value', 0);
    let divContent = $('.list-lesson-notify').closest('.chose-curriculum-content, .chose-curriculum-content-report');
    $('.list-lesson-notify').html('<i class="fa fa-refresh load"></i>');
    $.ajax({
      url: 'lesson/getLesson',
      type: 'GET',
      dataType: 'JSON',
      data: {
       curriculum_id: $(this).data('id')
     },  
     success: function(data){
      $('.list-lesson-notify').hide();
      let value = JSON.parse(JSON.stringify(data));
      divContent.html('<div><input type="text" id="search-curriculum" class="form-control" placeholder="Tìm kiếm chương"></div><ul id="list-lesson"></ul>');
      $.each(value, function(index, item){
        let liElement = '<li data-id='+item.id+'>'+item.name+'</li>';
        divContent.find('ul').append(liElement);
      });
      if(idParent != 0) $('#list-lesson li[data-id='+idParent+']').trigger('click');
      $('.chose-curriculum-content, .chose-curriculum-content-report').each(function(){
       if($(this).css('display') === 'block') {
        $(this).hide();
      }
    });
    }
  });
  }
  /* chọn khóa học trong student report */
  if($(this).closest('ul')[0].id == 'list-curriculum-class') {
    let divContent = $('.list-lesson-notify').closest('.class-content');
    $('.list-lesson-notify').html('<i class="fa fa-refresh load"></i>');
    $.ajax({
      url: 'student-report/getClass',
      type: 'GET',
      dataType: 'JSON',
      data: {
       curriculum_id: $(this).data('id')
     },  
     success: function(data){
      $('.list-lesson-notify').hide();
      let value = JSON.parse(JSON.stringify(data));
      divContent.html('<div><input type="text" id="search-class" class="form-control" placeholder="Tìm kiếm lớp"></div><ul id="list-curriculum-class"></ul>');
      $.each(value, function(index, item){
        let liElement = '<li data-id='+item.id+'>'+item.name+'</li>';
        divContent.find('ul').append(liElement);
      });
    }
  });
  }
});
/* search curiculum */
$(document).on('keyup', '#search-curriculum', function(){
  choseSearch($(this),$(this).closest('.chose-curriculum-content, .chose-curriculum-content-report'));
});
/* search university */
$(document).on('keyup', '#search-university,#search-department,#search-major', function(){
  choseSearch($(this),$(this).closest('.chose-uni-content'));
});
/* search class curriculum */
$(document).on('keyup', '#search-class-curriculum', function(){
  choseSearch($(this),$(this).closest('.chose-class-curriculum-content'));
});
function choseSearch(obj,content){
  let value = obj.val();
  let countHide = 0;
  if(value == '') {
    content.find('ul li').show();
    countHide = 0;
  }
  content.find('ul li').each(function(){
    if($(this).text().indexOf(value) >= 0) {
      $(this).show();
      content.find('ul li#not-search').remove();
    }
    else {
      countHide++;
      $(this).hide();
    }
  });
  if(countHide == content.find('ul li').length) {
    content.find('ul').append('<li id="not-search">Không tìm thấy</li>');
  }
}
/* check chose is image */
function checkChoseFile(extension,nameClass){
  console.log(nameClass);
  let arrExtension = [];
  if(nameClass == 'add-document') return true;
  if(nameClass == 'chose-audio') arrExtension = ['mp3','wma','wav','flac','aac'];
  if(nameClass == 'input-group-addon chose-excel') arrExtension = ['xls', 'xlsx'];
  else arrExtension = ['png', 'jpg', 'jpeg', 'gif'];
  if(arrExtension.indexOf(extension) == -1) return false;
  return true;
}
/* show modal upload file */
// let nameClass = null;
// let choseAudioElement = null;
$(document).on('click', '.chose-image, .chose-audio, .chose-avatar, .add-document, .chose-excel', function(){
  nameClass = $(this)[0].className;
  if(nameClass == 'chose-audio') choseAudioElement = $(this);
  $('#module-file').modal();
});
$(document).on('click', '#module-file .file-box', function(){
  let imgPath = $(this).find('.file').data('src');
  let fileExtension = imgPath.substr(imgPath.lastIndexOf('.')+1).toLowerCase();
  if(checkChoseFile(fileExtension,nameClass)) {
    switch(nameClass){
      case 'chose-image box-content':
      $('input[name=img]').val(imgPath);
      let imgElement = '<img src="'+imgPath+'" alt="hình ảnh mô tả thể loại" />'
      $('.chose-image > div').html(imgElement);
      $('#modal-add-category').modal('show');
      break;
      case 'note-icon-picture':
      $('.note-image-url').val(imgPath);
      $('.note-image-btn').trigger('click');
      break;
      case 'chose-audio':
      choseAudioElement.closest('div').find('input').val(imgPath);
      break;
      case 'chose-avatar':
      $('.avatar img').attr('src',imgPath);
      break;
      case 'add-document':
      let nameFile = imgPath.substr(imgPath.lastIndexOf('/')+1);
      let content = '<li>' +
      '<span>' +
      '<a href="'+imgPath+'">'+nameFile+'</a>' +
      '<i class="fa fa-times destroy-document"></i>' +
      '</span>' +
      '</li>';
      $('.add-document').remove();
      let addDocument = '<div class="add-document">'+
      '<i class="fa fa-plus"></i>'+
      '</div>';
      $('.document').append(content);
      $('.document').append(addDocument);
      break;
      case 'input-group-addon chose-excel':
      $('input[name="chose_excel"]').val(imgPath);
      break;
      default:
      nameClass.val(imgPath);
      break;
    }
    $('#module-file').modal('hide');
  }
  else {
    if(nameClass == 'chose-audio') swal('File này không phải là audio.');
    if(nameClass == 'input-group-addon chose-excel') swal('File này không phải là excel.');
    else swal('File này không phải là hình ảnh.');
  }
});
$(document).on('click', '#modal-upload', function(){
  let classList = $(this)[0].classList;
  if(classList[classList.length-1] != 'in') {
    $('#module-file').modal('show');
  }
});
$(document).on('click', '.note-btn', function(){
  if($(this).find('i')[0].className == 'note-icon-picture') {
    nameClass = $(this).find('i')[0].className;
    $('.note-editor').find('.modal:eq(1)').hide();
    $('.modal-backdrop:last-child').remove();
    $('#module-file').modal('show');
  }
});
function resetSwitchery(nameSwitch){
  $.each(nameSwitch, function(index,item){
    if(item.isChecked()) item.setPosition(true);
  });
}
/* set value form btn edit */
function setValueForm(value) {
  clickEdit = true;
  idLesson = value.id;
  idParent = value.parent_id;
  /* set document */
  $('.document').html('');
  $.each(value.documents, function(index, val) {
   let content = '<li>' +
   '<span>' +
   '<a href="'+val.link+'">'+val.title+'</a>' +
   '<i class="fa fa-times destroy-document"></i>' +
   '</span>' +
   '</li>';
   $('.document').append(content);
 });
  let addDocument = '<div class="add-document">'+
  '<i class="fa fa-plus"></i>'+
  '</div>';
  $('.document').append(addDocument);
  $('select[name=type]').attr('disabled','disabled');
  $('#modal-add-lesson .note-editable').html(value.desc);
  $('textarea[name=desc_lesson]').val(value.desc);
  if(mainUrl == 'lesson'){
    $('#id-curriculum').text('-- Chọn khóa học --');
    $('#id-curriculum').attr('value', 'not');
    $('#id-lesson').text('-- Không thuộc chương nào --');
    $('#id-lesson').attr('value', 0);
  }
  $('select[name=type] option[value='+value.type+']').prop('selected','selected');
  $('#list-curriculum li[data-id='+value.curriculum_id+']').trigger('click');
  if(mainUrl == 'curriculum') $('.list-curriculum-content').remove();
  $.each(value, function(index, item){
    let name = index+'_lesson';
    $('input[name='+name+']').val(item);
    $('select[name='+name+']').val(item);
  });
  if(value.type == 'test' && value.quicktest.length > 0) {
    idQuicktest = value.quicktest[0].id;
    $('input[name=percent_pass_lesson]').val(value.quicktest[0].percent_pass);
    if(value.quicktest[0].media != null){
      $('.quicktest-type-content input[value='+value.quicktest[0].media.type+']').trigger('click');
      switch(value.quicktest[0].media.provider){
        case 'youtube':
        $('.link-video-quicktest div[data-id=youtube] input').removeAttr('disabled');
        $('.link-video-quicktest div[data-id=youtube] input').val(value.quicktest[0].media.src);

        $('.link-video-quicktest div[data-id=youtube] input').trigger('focus');
        $('.link-video-quicktest div[data-id=youtube]').removeClass('none');
        $('.link-video-quicktest div[data-id=vimeo] input').attr('disabled', 'disabled');
        $('.link-video-quicktest div[data-id=vimeo]').addClass('none');
        break;
        case 'vimeo':
        $('.link-video-quicktest div[data-id=vimeo] input').removeAttr('disabled');
        $('.link-video-quicktest div[data-id=vimeo] input').val(value.quicktest[0].media.src);
        $('.link-video-quicktest div[data-id=vimeo]').removeClass('none');
        $('.link-video-quicktest div[data-id=youtube] input').attr('disabled', 'disabled');
        $('.link-video-quicktest div[data-id=youtube]').addClass('none');
        break;
        default:
        $('input[name=linkaudio]').val(value.quicktest[0].media.src);  
        break;
      }
    }
    resetSwitchery(nameSwitchLesson);
    let arrSupport = ['show_hint', 'review_question', 'showcorrect_answer', 'showcheck_answer'];
    $.each(value.quicktest[0], function(index, item){
      if(index != 'id'){
        $('input[name='+index+']').val(item);
        if(arrSupport.indexOf(index) >= 0) {
          for(let i = 0; i < nameSwitchLesson.length; i++){
            if(item == 1 && nameSwitchLesson[i].element.name == index) {
              nameSwitchLesson[i].setPosition(true);
            } 
          }
        }
      }
    });
  }
  if(value.type == 'lesson' && value.content.length > 0){
    $('.list-type-content input[value='+value.content[0].type+']').prop('checked','checked');
    choseTypeContent(); 
    if(value.content[0].type != 'text') {
      switch(value.content[0].desc.provider){
        case 'youtube':
        $('.link-video-lesson div[data-id=youtube] input').removeAttr('disabled');
        $('.link-video-lesson div[data-id=youtube] input').val(value.content[0].desc.src);

        $('.link-video-lesson div[data-id=youtube] input').trigger('focus');
        $('.link-video-lesson div[data-id=youtube]').removeClass('none');
        $('.link-video-lesson div[data-id=vimeo] input').attr('disabled', 'disabled');
        $('.link-video-lesson div[data-id=vimeo]').addClass('none');
        break;
        case 'vimeo':
        $('.link-video-lesson div[data-id=vimeo] input').removeAttr('disabled');
        $('.link-video-lesson div[data-id=vimeo] input').val(value.content[0].desc.src);
        $('.link-video-lesson div[data-id=vimeo]').removeClass('none');
        $('.link-video-lesson div[data-id=youtube] input').attr('disabled', 'disabled');
        $('.link-video-lesson div[data-id=youtube]').addClass('none');
        break;
        default:
        $('input[name=linkaudiolesson]').val(value.content[0].desc.src);
        break;
      }
    }
  }
  else{
    $('.list-type-content input[value=text]').trigger('click');
    choseTypeContent(); 
  }
  typeLessonChange();
  $('#modal-add-lesson').modal('show');
}

/* delete document */
$(document).on('click','.destroy-document', function(data){
  $(this).closest('li').remove();
});
